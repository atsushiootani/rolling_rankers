//
//  AppController+Http.m
//  
//
//  Created by OtaniAtsushi1 on 2015/04/30.
//
//

#import <Foundation/Foundation.h>
#import "AppController.h"
#import "AppController+Http.h"
#import "AppController+Data.h"
#import "RootViewController.h"
#import "iosCommon.h"
#import "GameHelper.h"

#define DEBUG_LAUNCH_ALERT_VIEW  (0 && DEBUG)



@interface AppController()<NSURLConnectionDelegate>

@end


@implementation AppController (Http)


id jsonObject;
NSDictionary* pronounces;


-(NSString*)getAppVersion{
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
}

-(BOOL) isAppVersionEqualOrNewerThan:(NSString*)version{
    return [version compare:[self getAppVersion] options:NSNumericSearch] != NSOrderedDescending;
}


#pragma mark - web

-(void) openWebPage:(NSString*) urlString{
    if (![urlString hasPrefix:@"http"]){
        urlString = [NSString stringWithFormat:@"http://%@", urlString];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: urlString]];
}


#pragma mark - pronounce

-(BOOL) initPronounce{
#if ENABLE_PRONOUNCE
    
    self.remoteConfig = [FIRRemoteConfig remoteConfig];
#if DEBUG
    self.remoteConfig.configSettings = [[FIRRemoteConfigSettings alloc]initWithDeveloperModeEnabled:YES];
#endif
    
    [self loadPronounces];
    return true;
    
#else
    return false;
#endif
}

//ロードする
-(void)loadPronounces{
#if ENABLE_PRONOUNCE
    //取得した値の有効期限
    // この時間(秒数)が経過するまでは、FirebaseConsoleに問い合わせに行かない
    // 常に最新を取得し続けたい場合は、0にしておかなければならない
    // アプリ内に保存しておく時間というわけではないので、一度フェッチした値はずっとそのまま生き続けるので問題ない
    float expirationDuration = 0;
    [self.remoteConfig fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error){
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            BOOL result = [self.remoteConfig activateFetched]; //新たにフェッチした値を反映した場合、真が返る。expirationDurationが経過していなかった場合は、偽が返る
            DNSLog(@"Firebase RemoteConfig fetch succeeded. Activation is (%@)", result == YES ? @"SUCCEEDED" : @"FAILED");
        }
        else{
            DNSLog(@"Firebase RemoteConfig fetch failed. (%d)", (int)status);
        }
    }];
#endif
}

//宣言取得完了時の処理
-(void)pronounceCompletedCallback{
}

//前回のデータでも構わないので、とにかく値があればtrueを返す
-(BOOL) hasPronounceLoaded{
    return YES;
}

-(int) getIntPronounce:(NSString*)key defaultValue:(int)defaultValue{
#if ENABLE_PRONOUNCE
    FIRRemoteConfigValue* val = [self.remoteConfig configValueForKey:key];
    if (val == nil || val.source == FIRRemoteConfigSourceStatic) { //データがなかった場合
        return defaultValue;
    }
    return val.numberValue.intValue;
#else
    return defaultValue;
#endif
}

-(float) getFloatPronounce: (NSString*)key defaultValue:(float)defaultValue{
#if ENABLE_PRONOUNCE
    FIRRemoteConfigValue* val = [self.remoteConfig configValueForKey:key];
    if (val == nil || val.source == FIRRemoteConfigSourceStatic) { //データがなかった場合
        return defaultValue;
    }
    return val.numberValue.floatValue;
#else
    return defaultValue;
#endif
}

-(double) getDoublePronounce:(NSString*)key defaultValue:(double)defaultValue{
#if ENABLE_PRONOUNCE
    FIRRemoteConfigValue* val = [self.remoteConfig configValueForKey:key];
    if (val == nil || val.source == FIRRemoteConfigSourceStatic) { //データがなかった場合
        return defaultValue;
    }
    return val.numberValue.doubleValue;
#else
    return defaultValue;
#endif
}

-(bool) getBoolPronounce:(NSString*)key defaultValue:(bool)defaultValue{
#if ENABLE_PRONOUNCE
    FIRRemoteConfigValue* val = [self.remoteConfig configValueForKey:key];
    if (val == nil || val.source == FIRRemoteConfigSourceStatic) { //データがなかった場合
        return defaultValue;
    }
    return val.boolValue;
#else
    return defaultValue;
#endif
}

-(NSString*) getStringPronounce:(NSString*)key defaultValue:(NSString*)defaultValue{
#if ENABLE_PRONOUNCE
    FIRRemoteConfigValue* val = [self.remoteConfig configValueForKey:key];
    if (val == nil || val.source == FIRRemoteConfigSourceStatic) { //データがなかった場合
        return defaultValue;
    }
    return val.stringValue;
#else
    return defaultValue;
#endif
}

//Pronounceで設定された重み付けの値を使って抽選を行う
//NSDictionaryの中身は以下
//  key: NSString => weight: NSNumber(intValue)
//defaultValueは、重み付けが無かった場合（取得できなかった場合）のデフォルト値
//
-(int) lot:(NSDictionary*) dic defaultValue:(int) defaultValue{
    int ret = defaultValue;
    
    int weights[dic.allKeys.count];
    
    int totalWeights = 0;
    for (int i = 0; i < dic.count; ++i){
        NSString* key = (NSString*)dic.allKeys[i];
        
        int thisWeight = [self getIntPronounce:key defaultValue:0];
        totalWeights += thisWeight;
        weights[i] = thisWeight;
    }
    
    if (totalWeights > 0) {
        int r = getIntRand(0, totalWeights - 1);
        
        int hitWeight = 0;
        for (int i = 0; i < dic.count; ++i) {
            NSString* key = (NSString*)dic.allKeys[i];
            
            hitWeight += weights[i];
            if (r < hitWeight) {
                ret = ((NSNumber*)dic[key]).intValue;
                break;
            }
        }
    }
    return ret;
}


#pragma mark - 起動時アラートビュー表示


//新規アプリ情報など、アラートビュー表示
-(void) launchAlertView:(BOOL) force{

#if ENABLE_PRONOUNCE_LAUNCH_ALERT_VIEW
    
#if DEBUG_LAUNCH_ALERT_VIEW
    force = YES;
#endif
    
    NSString* noticeId = [self getStringPronounce:@"_NOTICE_ID" defaultValue:@""];
    if (noticeId && noticeId.length > 0 &&
        (![self loadBoolDataWithKey:noticeId] || force)){
        NSString* title   = [self getStringPronounce:@"_NOTICE_TITLE"   defaultValue:@""];
        NSString* message = [self getStringPronounce:@"_NOTICE_MESSAGE" defaultValue:@""];
        NSString* yes     = [self getStringPronounce:@"_NOTICE_YES"     defaultValue:@""];
        NSString* no      = [self getStringPronounce:@"_NOTICE_NO"      defaultValue:@""];
        NSString* url     = [self getStringPronounce:@"_NOTICE_URL"     defaultValue:@""];
        
        if ([yes length] <= 0){
            yes = nil;
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        if (no != nil && no.length > 0) {
            [alertController addAction:[UIAlertAction actionWithTitle:no style:UIAlertActionStyleDefault handler:nil]];
        }
        
        if (yes != nil && yes.length > 0){
            [alertController addAction:[UIAlertAction actionWithTitle:yes style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                if (url && url.length > 0) {
                    [self openWebPage:url];
                }
            }]];
        }
        
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        [self saveBoolData:YES withKey:noticeId];
    }
    
#endif
}


@end


