//
//  AppController+Data.h
//  
//
//  Created by OtaniAtsushi1 on 2015/05/08.
//
//

#import "AppController.h"

//Storage
#define ENABLE_FIREBASE_STORAGE (1 && ENABLE_FIREBASE)
//pod 'Firebase/Storage'
#define FIREBASE_STORAGE_BUCKET_NAME  @"gs://admob-app-id-1943614299.appspot.com"


//Realtime Database
#define ENABLE_FIREBASE_DATABASE (1 && ENABLE_DATABASE)



@interface AppController (Data)


//Native_Pronounce.hと同じ並びであること
enum StorageType{
    StorageTypeNone = 0, //指定なし。ディレクトリ名ふくめ完全なパスを自前で指定する場合。非推奨
    StorageTypePublic,   //公開ディレクトリ。外部からもアクセス可能。SNSシェア用データなど
    StorageTypeCommon,   //共通ディレクトリ。認証済みユーザなら誰でもアクセス可能。ゲームパラメータなど
    StorageTypeSettings, //設定ディレクトリ。認証済みユーザなら誰でも読み込み可能。書き込みは不可。ゲームパラメータなど
    StorageTypeReadable, //ユーザ個別の公開ディレクトリ。ユーザ本人のみ書き込み可能。認証済みユーザなら誰でも読み込み可能。ランキングなど
    StorageTypeUser,     //ユーザ個別ディレクトリ。ユーザ本人のみアクセス可能。セーブデータなど
    StorageTypeSpecial,  //特殊ディレクトリ。特定のユーザだけアクセスできるなど
    StorageTypeAdmin,    //管理者用ディレクトリ。デバッグデータなど
};


#pragma mark - Firebase RealtimeDatabase

-(void) initDatabase;
-(void) selectRecordAtPath: (StorageType)type filePath:(NSString*)path                                         completion:(void(^)(bool, NSDictionary*))completion;
-(void) updateRecordAtPath: (StorageType)type filePath:(NSString*)path  updateData:(NSDictionary*)  updateData completion:(void(^)(bool, NSDictionary*))completion;
-(void) replaceRecordAtPath:(StorageType)type filePath:(NSString*)path replaceData:(NSDictionary*) replaceData completion:(void(^)(bool, NSDictionary*))completion;
-(void) deleteRecordAtPath: (StorageType)type filePath:(NSString*)path                                         completion:(void(^)(bool, NSDictionary*))completion;
-(void) setDatabaseRecord:  (StorageType)type filePath:(NSString*)path record:(NSDictionary*)record;
-(void) setDatabaseRecord:  (StorageType)type filePath:(NSString*)path str:(NSString*)str;


#pragma mark - Firebase Storage

-(void) initDataStorage;
-(void) downloadStorageDataAndSaveToFile:(StorageType)type filePath:(NSString*) filePath                    completion:(void (^)(bool, NSString*))completion;
-(void) downloadStorageDataAsString     :(StorageType)type filePath:(NSString*) filePath                    completion:(void (^)(bool, NSString*))completion;
-(void) downloadStorageData             :(StorageType)type filePath:(NSString*) filePath                    completion:(void (^)(bool, NSData*  ))completion;
-(void) uploadStorageDataWithString     :(StorageType)type filePath:(NSString*) filePath str:(NSString*)str completion:(void (^)(bool)) completion;
-(void) uploadStorageData               :(StorageType)type filePath:(NSString*) filePath data:(NSData*)data completion:(void (^)(bool)) completion;
-(void) deleteStorageData               :(StorageType)type filePath:(NSString*) filePath                    completion:(void (^)(bool)) completion;
-(NSString*) getStorageFullPath         :(StorageType)type filePath:(NSString*) filePath;


#pragma mark - save/load

-(void) saveString:(NSString*)data withKey:(NSString*)key;
-(NSString*) loadStringWithKey:(NSString*)key;
-(void) saveIntData:(NSInteger)data withKey:(NSString*)key;
-(NSInteger) loadIntDataWithKey:(NSString*)key;
-(void) saveFloatData:(float)data withKey:(NSString*)key;
-(float) loadFloatDataWithKey:(NSString*)key;
-(void) saveDoubleData:(double)data withKey:(NSString*)key;
-(double) loadDoubleDataWithKey:(NSString*)key;
-(void) saveBoolData:(BOOL)data withKey:(NSString*)key;
-(BOOL) loadBoolDataWithKey:(NSString*)key;
-(void) saveURLData:(NSURL*)data withKey:(NSString*)key;
-(NSURL*) loadURLDataWithKey:(NSString*)key;
-(void) saveArray:(NSArray*)array withKey:(NSString*)key;
-(NSArray*) loadArrayWithKey:(NSString*)key;
-(void) saveStringArray:(NSArray*)stringArray withKey:(NSString*)key;
-(NSArray*) loadStringArrayWithKey:(NSString*)key;
-(void) saveDictionary:(NSDictionary*)dic withKey:(NSString*)key;
-(NSDictionary*) loadDictionaryWithKey:(NSString*)key;
-(void) saveData:(NSData*)data withKey:(NSString*)key;
-(NSData*) loadDataWithKey:(NSString*)key;
-(void) saveObject:(NSObject*)object withKey:(NSString*)key;
-(NSObject*) loadObjectWithKey:(NSString*)key;

@end
