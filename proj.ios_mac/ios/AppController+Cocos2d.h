//
//  AppController+Cocos2d.h
//  
//
//  Created by OtaniAtsushi1 on 2016/01/18.
//
//

#import "AppController.h"

@interface AppController (Cocos2d)

-(void) pauseCocos2d;
-(void) resumeCocos2d;

@end
