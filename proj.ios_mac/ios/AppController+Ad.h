//
//  AppController+Ad.h
//
//
//  Created by OtaniAtsushi1 on 2014/11/20.
//
//

#import "AppController.h"

#import <functional>


#define ENABLE_AD        (1)
#define ENABLE_ADMOB     (ENABLE_AD && 1) //https://apps.admob.com/?pli=1#monetize/adunit:create
#define ENABLE_NEND      (ENABLE_AD && 0) //https://www.nend.net/m/site
#define ENABLE_IMOBILE   (ENABLE_AD && 0) //https://sppartner.i-mobile.co.jp/media.aspx
#define ENABLE_ADFURIKUN (ENABLE_AD && 1) //https://adfurikun.jp/adfurikun/service/edit
#define ENABLE_ADCOLONY  (ENABLE_AD && 1) //https://clients.adcolony.com/app/new
#define ENABLE_ADSTIR    (ENABLE_AD && 1) //https://ja.ad-stir.com/publisher/media/add/select_size?platform_name=webviewapp
#define ENABLE_APPLOVIN  (ENABLE_AD && 1) //https://www.applovin.com/




enum AdType{
    AD_UNKNOWN = -1,

    AD_NONE = 0,
    
#if ENABLE_ADMOB
    AD_ADMOB = 1,
#endif
    
#if ENABLE_NEND
    AD_NEND = 2,
#endif
    
#if ENABLE_IMOBILE
    AD_IMOBILE = 3,
#endif
    
#if ENABLE_ADFURIKUN
    AD_ADFURIKUN = 4,
#endif
    
#if ENABLE_ADCOLONY
    AD_ADCOLONY = 5,
#endif
    
#if ENABLE_ADSTIR
    AD_ADSTIR = 6,
#endif

#if ENABLE_APPLOVIN
    AD_APPLOVIN = 7,
#endif
};


@interface AppController (Ad)

//life cycle(RootViewControllerから呼んでもらう)
- (void)viewDidAppear:(BOOL)animated;
- (void)viewWillDisappear:(BOOL)animated;

//common
-(void) adRegister;
-(void) adEnterBackground;
-(void) adEnterForeground;
-(void) adBannerShow;
-(void) adBannerHide;
-(void) adBannerSetEnable;
-(void) adBannerSetDisable;
-(void) adIconShowAt:(int) pos;
-(void) adIconShow;
-(void) adIconHide;
-(void) adIconHideAt:(int) pos;
-(void) adHeaderShow;
-(void) adHeaderHide;
-(void) adInterstitialTransitionShow;
-(void) adInterstitialAlwaysShow;
-(BOOL) adMovieCanPlay;
-(void) adMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback;
-(BOOL) isAdMoviePlayingNow;

-(BOOL) checkApplicationInstalled:(NSString*) urlScheme;
-(void) openInstalledApplication:(NSString*) urlScheme;

@end

