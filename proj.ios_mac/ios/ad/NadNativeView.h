//
//  NadNativeView.h
//
//
//  Created by OtaniAtsushi1 on 2016/04/11.
//
//

#import <UIKit/UIKit.h>
#import "NADNativeViewRendering.h"

//広告画像サイズ
#define AD_SIZE_NEND_ICON       (CGSizeMake( 50,  50))
#define AD_SIZE_NEND_SMALL_RECT (CGSizeMake( 80,  60))
#define AD_SIZE_NEND_LARGE_RECT (CGSizeMake(300, 180))


@interface NadNativeView : UIView <NADNativeViewRendering> {
}

- (id) initWithFrame:(CGRect)frame;
- (NADNativeLabel *)prTextLabel;
- (NADNativeImageView *)adImageView;
- (NADNativeImageView *)logoImageView;
- (NADNativeLabel *)shortTextLabel;
- (NADNativeLabel *)longTextLabel;
- (NADNativeLabel *)promotionUrlLabel;
- (NADNativeLabel *)promotionNameLabel;
- (NADNativeLabel *)actionButtonTextLabel;


@end
