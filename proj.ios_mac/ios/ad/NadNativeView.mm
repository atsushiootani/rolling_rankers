//
//  NadNativeView.mm
//
//
//  Created by OtaniAtsushi1 on 2016/04/11.
//
//

#import "AppController+Ad.h"

#if ENABLE_NEND


#import <NadNativeView.h>



@interface NadNativeView()

//広告明示。必須。全文字表示すること
@property (nonatomic, strong) NADNativeLabel*     nPrTextLabel;

//広告画像。必須（テキストのみ広告の場合を除く）。拡大縮小30~150%の範囲で可能。上下左右6%まで切り抜き可能
@property (nonatomic, strong) NADNativeImageView* nAdImageView;

//ロゴ画像。任意。拡大縮小自由。切り抜き禁止
@property (nonatomic, strong) NADNativeImageView* nLogoImageView;

//3つの中から1つ必須
//広告見出し。15文字以上表示すること
@property (nonatomic, strong) NADNativeLabel*     nShortTextLabel;
//広告本文。26文字以上表示すること
@property (nonatomic, strong) NADNativeLabel*     nLongTextLabel;
//プロモーション名。1文字以上表示すること
@property (nonatomic, strong) NADNativeLabel*     nPromotionNameLabel;

//表示URL。任意。文字数制限なし
@property (nonatomic, strong) NADNativeLabel*     nPromotionUrlLabel;

//アクションボタン。任意。全文字表示すること
@property (nonatomic, strong) NADNativeLabel*     nActionButtonTextLabel;


//ref: https://github.com/fan-ADN/nendSDK-iOS/wiki/%E3%83%8D%E3%82%A4%E3%83%86%E3%82%A3%E3%83%96%E5%BA%83%E5%91%8A%E5%AE%9F%E8%A3%85%E6%89%8B%E9%A0%86

@end



@implementation NadNativeView

-(CGRect) getRectFromRate:(CGRect) rateRect{
    CGSize size = self.bounds.size;
    return CGRectMake(size.width  * rateRect.origin.x,
                      size.height * rateRect.origin.y,
                      size.width  * rateRect.size.width,
                      size.height * rateRect.size.height);
}

- (id) initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        CGFloat fontSize = 10.f;
        CGFloat fontViewHeight = 15.f;

        //広告画像
        //CGFloat imageSize = frame.size.height;
        //CGRect imageFrame = CGRectMake((frame.size.width - imageSize)/2, 0, imageSize, imageSize); //アイコン広告ようなので、縦横比は必ず1:1にすること（でないと表示されない。ログ出力も参照のこと）
        
        CGRect imageFrame;
        imageFrame.origin = CGPointMake(0,0);
        imageFrame.size = AD_SIZE_NEND_SMALL_RECT;
        _nAdImageView = [[NADNativeImageView alloc]initWithFrame:imageFrame];

#if DEBUG
        //_nAdImageView.backgroundColor = [UIColor redColor];
#endif
        [self addSubview:_nAdImageView];
        
        //広告明示
        //_nPrTextLabel = [[NADNativeLabel alloc]initWithFrame:CGRectMake(0, frame.size.height - fontViewHeight, 25, fontViewHeight)];
        _nPrTextLabel = [[NADNativeLabel alloc]initWithFrame:CGRectMake(2, 2, frame.size.width - 4, fontViewHeight)];
        _nPrTextLabel.font = [UIFont systemFontOfSize:fontSize];
        _nPrTextLabel.adjustsFontSizeToFitWidth = NO;
        _nPrTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
#if DEBUG
        //_nPrTextLabel.backgroundColor = [UIColor cyanColor];
#endif
        [self addSubview:_nPrTextLabel];
        
        //プロモーション名
        //_nPromotionNameLabel = [[NADNativeLabel alloc]initWithFrame:CGRectMake(25, frame.size.height - fontViewHeight, 30, fontViewHeight)];
        _nPromotionNameLabel = [[NADNativeLabel alloc]initWithFrame:CGRectMake(2, frame.size.height - 2 - fontViewHeight, frame.size.width - 4, fontViewHeight)];
        _nPromotionNameLabel.font = [UIFont systemFontOfSize:fontSize];
        _nPromotionNameLabel.adjustsFontSizeToFitWidth = NO;
        _nPromotionNameLabel.numberOfLines = 1;
        _nPromotionNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
#if DEBUG
        //_nPromotionNameLabel.backgroundColor = [UIColor greenColor];
#endif
        [self addSubview:_nPromotionNameLabel];
    }
    return self;
}

- (NADNativeLabel *)prTextLabel{
    return self.nPrTextLabel;
}
- (NADNativeImageView *)adImageView{
    return self.nAdImageView;
}
- (NADNativeImageView *)logoImageView{
    return self.nLogoImageView;
}
- (NADNativeLabel *)shortTextLabel{
    return self.nShortTextLabel;
}
- (NADNativeLabel *)longTextLabel{
    return self.nLongTextLabel;
}
- (NADNativeLabel *)promotionUrlLabel{
    return self.nPromotionUrlLabel;
}
- (NADNativeLabel *)promotionNameLabel{
    return self.nPromotionNameLabel;
}
- (NADNativeLabel *)actionButtonTextLabel{
    return self.nActionButtonTextLabel;
}

@end

#endif
