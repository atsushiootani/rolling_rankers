//
//  MovieReward6002.m(AdColony)
//
//  Copyright (c) A .D F. U. L. L. Y Co., Ltd. All rights reserved.
//
//

#import "MovieReward6002.h"

@interface MovieReward6002()<AdColonyDelegate, AdColonyAdDelegate>

@property (nonatomic, strong) NSString *adColonyAppId;
@property (nonatomic, strong) NSArray *adColonyZoneId;
@property (nonatomic, strong) NSString *adShowZoneId;
@property (nonatomic, assign) BOOL loadedFlg; /**< すでに読み込み済みかどうか */
@property (nonatomic, assign) BOOL displayedFlg; /**< 広告表示がすでに行われたか */

@end

@implementation MovieReward6002

-(id)init
{
    self = [super init];
    
    if ( self ) {
        _displayedFlg = NO;
        _loadedFlg = NO;
    }
    
    return self;
}


/**
 *  データの設定
 *
 *  @param data
 */
-(void)setData:(NSDictionary *)data
{
    NSString *colonyAppId = [NSString stringWithFormat:@"%@", [data objectForKey:@"app_id"]];
    NSString *colonyZoneId = [NSString stringWithFormat:@"%@", [data objectForKey:@"zone_id"]];
    self.adColonyAppId = colonyAppId;
    
    self.adColonyZoneId = @[colonyZoneId];
    self.adShowZoneId = colonyZoneId;
    
}

-(BOOL)isPrepared{
    return ([AdColony zoneStatusForZone:_adShowZoneId] == ADCOLONY_ZONE_STATUS_ACTIVE);
}

/**
 *  広告の読み込みを開始する
 */
-(void)startAd
{
    if ( _displayedFlg ) {
        // すでに再生中の場合は処理しない。
        return;
    }
    
    if ( _loadedFlg ) {
        // すでに読み込み済みの場合には処理しない。
        return;
    }
    //NSLog(@"startAd");
    
    // 動画の再読込は、再生完了後にAdColonyのSDKで自動的に行われる
    [AdColony configureWithAppID:_adColonyAppId
                         zoneIDs:_adColonyZoneId
                        delegate:self
                         logging:YES];
}

/**
 *  広告の表示を行う
 */
-(void)showAd
{
    // 表示を呼び出す
    [AdColony playVideoAdForZone:_adShowZoneId
                    withDelegate:self
                withV4VCPrePopup:NO
                andV4VCPostPopup:NO
     ];
    
}


/**
 * 対象のクラスがあるかどうか？
 *
 *  @return
 */
-(BOOL)isClassReference
{
    Class clazz = NSClassFromString(@"AdColony");
    if (clazz) {
    } else {
        NSLog(@"Not found Class: AdColony");
        return NO;
    }
    return YES;
}

-(void)dealloc{
    if(_adColonyAppId != nil){
        _adColonyAppId = nil;
    }
    if(_adColonyZoneId != nil){
        _adColonyZoneId = nil;
    }
    if(_adShowZoneId != nil){
        _adShowZoneId = nil;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/**
 *  広告の読み込みを中止
 *
 */
-(void)cancel
{
    [AdColony cancelAd];
    _displayedFlg = NO;
    _loadedFlg = NO;
}


// ------------------------------ -----------------
// ここからはAdColonyのDelegateを受け取る箇所
#pragma mark -  AdColonyDelegate


/**
 *  リワード広告が正常に完了したら呼ばれる
 *
 *  @param success
 *  @param currencyName
 *  @param amount
 *  @param zoneID
 */
- ( void ) onAdColonyV4VCReward:( BOOL )success currencyName:( NSString * )currencyName currencyAmount:( int )amount inZone:( NSString * )zoneID
{
    if (success) {
    }
    
}

/**
 *  広告が有効になったら呼ばれる
 *
 *  @param available
 *  @param zoneID
 */
- ( void ) onAdColonyAdAvailabilityChange:(BOOL)available inZone:(NSString*) zoneID
{
    
    if(available) {
        // 広告準備成功
        _loadedFlg = YES;
        
        if ( self.delegate ) {
            if ([self.delegate respondsToSelector:@selector(AdsFetchCompleted:)]) {
                [self.delegate AdsFetchCompleted:self];
            }
        }
        
    }
}


#pragma mark -  AdColonyAdDelegate

/**
 *  広告の表示を開始したら呼ばれる
 *
 *  @param zoneID
 */
- ( void ) onAdColonyAdStartedInZone:( NSString * )zoneID
{
    NSLog(@"onAdColonyAdStartedInZone");
    
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsDidShow:)]) {
            [self.delegate AdsDidShow:self];
        }
    }
    
    _displayedFlg = YES;
}


/**
 *  広告の表示を完了したら呼ばれる
 *
 *  @param info
 */
- ( void ) onAdColonyAdFinishedWithInfo:( AdColonyAdInfo * )info
{
    _displayedFlg = NO;
    _loadedFlg = NO;
    NSLog(@"onAdColonyAdFinishedWithInfo");
    
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsDidCompleteShow:)]) {
            [self.delegate AdsDidCompleteShow:self];
        }
    }
    if ( self.delegate ) {
        if ([self.delegate respondsToSelector:@selector(AdsDidHide:)]) {
            [self.delegate AdsDidHide:self];
        }
    }
}


/**
 *
 *  @param shown
 *  @param zoneID
 */
- ( void ) onAdColonyAdAttemptFinished:(BOOL)shown inZone:( NSString * )zoneID
{
    NSLog(@"onAdColonyAdAttemptFinished");
}

@end
