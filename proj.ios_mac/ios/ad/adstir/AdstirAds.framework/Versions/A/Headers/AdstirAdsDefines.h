//
//  AdstirAdsDefines.h
//  AdstirAds
//
//  Copyright © 2016 UNITED, Inc. All rights reserved.
//

#if defined(__cplusplus)
#define ADSTIR_EXTERN extern "C" __attribute__((visibility("default")))
#else
#define ADSTIR_EXTERN extern __attribute__((visibility("default")))
#endif  // defined(__cplusplus)

#if __has_feature(nullability)
#define asadnullable nullable
#define asadnonnull nonnull
#define __asadnullable __nullable
#define __asadnonnull __nonnull
#else
#define asadnullable
#define asadnonnull
#define __asadnullable
#define __asadnonnull
#endif
