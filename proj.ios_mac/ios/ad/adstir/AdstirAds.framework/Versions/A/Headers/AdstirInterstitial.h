//
//  AdstirInterstitial.h
//  AdstirAds
//
//  Copyright © 2016 UNITED, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdstirAdsDefines.h"

typedef NS_ENUM(NSUInteger, AdstirInterstitialError) {
    AdstirInterstitialErrorUnknown = 15000,
    AdstirInterstitialErrorInternal,
    AdstirInterstitialErrorNoFill,
    AdstirInterstitialErrorFailedToShow,
    AdstirInterstitialErrorInvalidSpot,
    AdstirInterstitialErrorDidNotInitialize,
    AdstirInterstitialErrorMediationAdapterNotFound,
};

@class AdstirInterstitial;

@protocol AdstirInterstitialDelegate <NSObject>

@optional
/**
 Called when get ready to show ad
 
 インタースティシャル広告の準備が完了した際に呼び出されます
 */
- (void)adstirInterstitialDidLoad:(AdstirInterstitial * __asadnonnull)interstitial;
/**
 Called when failed to load ad
 
 インタースティシャル広告の準備に失敗した際に呼び出されます
 */
- (void)adstirInterstitial:(AdstirInterstitial * __asadnonnull)interstitial didFailToLoadWithError:(NSError * __asadnonnull)error;
/**
 Called when interstitial ad is shown
 
 インタースティシャル広告が表示された際に呼び出されます
 */
- (void)adstirInterstitialDidShow:(AdstirInterstitial * __asadnonnull)interstitial;
/**
 Called when failed to show ad
 
 インタースティシャル広告の表示に失敗した際に呼び出されます
 */
- (void)adstirInterstitial:(AdstirInterstitial * __asadnonnull)interstitial didFailToShowWithError:(NSError * __asadnonnull)error;
/**
 Called when ad was closed
 
 インタースティシャル広告が閉じられたときに呼び出されます
 */
- (void)adstirInterstitialDidClose:(AdstirInterstitial * __asadnonnull)interstitial;

@end

@interface AdstirInterstitial : NSObject

@property (nonatomic, copy, asadnonnull) NSString *media;
@property (nonatomic, assign) NSUInteger spot;

@property (nonatomic, assign, readonly) BOOL isShowing;

@property (nonatomic, weak, asadnullable) id<AdstirInterstitialDelegate> delegate;

/**
 Set test mode status
 
 テストモードの有効/無効を切り替えます
 */
+ (void)setTestModeEnabled:(BOOL)enabled;
/**
 Get test mode status

 テストモードの状態を取得します
 */
+ (BOOL)testModeEnabled;

+ (void)prepareWithMedia:(NSString * __asadnonnull)media spots:(NSArray * __asadnonnull)spots;

- (asadnullable instancetype)initWithMedia:(NSString * __asadnonnull)media spot:(NSUInteger)spot;

- (void)load;

- (void)showFromViewController:(UIViewController * __asadnonnull)viewController;

- (BOOL)canShow;

@end