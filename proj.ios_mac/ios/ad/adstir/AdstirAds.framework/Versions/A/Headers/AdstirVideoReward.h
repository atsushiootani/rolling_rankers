//
//  AdstirRewardVideo.h
//  AdstirAds
//
//  Copyright © 2015 UNITED, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AdstirAdsDefines.h"

typedef NS_ENUM(NSUInteger, AdstirVideoRewardError) {
    AdstirVideoRewardErrorUnknown = 15100,
    AdstirVideoRewardErrorInternal,
    AdstirVideoRewardErrorNoFill,
    AdstirVideoRewardErrorFailedToPlayback,
    AdstirVideoRewardErrorInvalidSpot,
    AdstirVideoRewardErrorDidNotInitialize,
    AdstirVideoRewardErrorMediationAdapterNotFound,
};

@class AdstirVideoReward;

@protocol AdstirVideoRewardDelegate <NSObject>

@optional
/**
 動画の再生準備が完了した際に呼び出されます
 */
- (void)adstirVideoRewardDidLoad:(AdstirVideoReward * __asadnonnull)videoReward;
/**
 動画の再生準備に失敗した際に呼び出されます
 */
- (void)adstirVideoReward:(AdstirVideoReward * __asadnonnull)videoReward didFailToLoadWithError:(NSError * __asadnonnull)error;
/**
 動画の再生が開始された際に呼び出されます
 */
- (void)adstirVideoRewardDidStart:(AdstirVideoReward * __asadnonnull)videoReward;
/**
 動画の再生に失敗した際に呼び出されます
 */
- (void)adstirVideoReward:(AdstirVideoReward * __asadnonnull)videoReward didFailToPlaybackWithError:(NSError * __asadnonnull)error;
/**
 動画の再生が完了した際に呼び出されます
 */
- (void)adstirVideoRewardDidFinishPlayback:(AdstirVideoReward * __asadnonnull)videoReward;
/**
 リワードが付与された際に呼び出されます
 */
- (void)adstirVideoRewardDidComplete:(AdstirVideoReward * __asadnonnull)videoReward;
/**
 動画の再生が完了し、動画再生画面が閉じられたときに呼び出されます
 */
- (void)adstirVideoRewardDidClose:(AdstirVideoReward * __asadnonnull)videoReward;
/**
 動画の再生がキャンセルされた際に呼び出されます
 ただし、このイベントには全てのアドネットワークが対応しているわけではありません
 */
- (void)adstirVideoRewardDidCancel:(AdstirVideoReward * __asadnonnull)videoReward;

@end


@interface AdstirVideoReward : NSObject

@property (nonatomic, copy, asadnonnull) NSString *media;
@property (nonatomic, assign) NSUInteger spot;

@property (nonatomic, assign, readonly) BOOL isPlaying;

@property (nonatomic, weak) __asadnullable id<AdstirVideoRewardDelegate> delegate;

@property (nonatomic, copy, readonly, asadnullable) NSString *rewardCurrency;
@property (nonatomic, copy, readonly, asadnullable) NSString *rewardAmount;

+ (void)setMediaUserID:(NSString * __asadnonnull)mediaUserID;
+ (NSString * __asadnullable)mediaUserID;

/**
 Set test mode status
 
 テストモードの有効/無効を切り替えます
 */
+ (void)setTestModeEnabled:(BOOL)enabled;
/**
 Get test mode status
 
 テストモードの状態を取得します
 */
+ (BOOL)testModeEnabled;

+ (void)prepareWithMedia:(NSString * __asadnonnull)media spots:(NSArray * __asadnonnull)spots;

- (asadnullable instancetype)initWithMedia:(NSString * __asadnonnull)media spot:(NSUInteger)spot;

- (void)load;

- (void)show __attribute__ ((deprecated));
- (void)showFromViewController:(UIViewController * __asadnonnull)viewController;

- (BOOL)canShow;

@end
