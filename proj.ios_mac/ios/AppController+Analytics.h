//
//  AppController+Analytics.h
//  
//
//  Created by OtaniAtsushi1 on 2015/04/19.
//
//

#import "AppController.h"

#define ENABLE_ANALYTICS (1 && ENABLE_FIREBASE)
//GoogleService-Info.plist作成(https://console.firebase.google.com/)

@interface AppController (Analytics)

-(void) analytics_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
-(void) analytics_eventWithCategory:(NSString*)category action:(NSString*)action label:(NSString*)label value:(NSInteger)value;
-(void) analytics_eventName:(NSString*)eventName param:(NSDictionary*)param;
-(void) analytics_errorReport:(NSString*)message;

-(void) analytics_tutorialBegin;
-(void) analytics_tutorialEnded;
-(void) analytics_appOpen;
-(void) analytics_share:(NSString*)contentType itemId:(NSString*)itemId;
-(void) analytics_shareTwitter:(NSString*)itemId;
-(void) analytics_shareFacebook:(NSString*)itemId;
-(void) analytics_ecommercePurchaseBegin:(NSString*)itemName;
-(void) analytics_ecommercePurchase:(int)price itemName:(NSString*)itemName;
-(void) analytics_ecommercePurchase:(NSString*)itemName;
-(void) analytics_spendVirtualCurrency:(NSString*)itemName currencyName:(NSString*)currencyName value:(NSUInteger)value;
-(void) analytics_buyItem:(NSString*)itemName;
-(void) analytics_buyItem:(NSString*)itemName price:(NSUInteger)price;
-(void) analytics_unlockAchievement:(NSString*) achievementId;
-(void) analytics_selectContent:(NSString*) contentName;
-(void) analytics_selectContent:(NSString*) contentName itemId:(NSString*)itemId;
-(void) analytics_review;

@end
