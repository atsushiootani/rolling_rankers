//
//  AppController+Purchase.mm
//  
//
//  Created by OtaniAtsushi1 on 2015/11/17.
//
//

#import "AppController+Purchase.h"
#import "AppInfo.h"             //for ALL_PRODUCT_IDS
#import "RootViewController.h"  //for indicator
#import "iosCommon.h"           //for debug log


/**
 * クラス拡張
 * 実装を分けたいのでカテゴリで作っているが、変数宣言するにはクラス拡張でないといけない
 * お行儀的には、AppController.h で宣言すべきな気がするが、外部からは別に使わないので公開にする必要もない
 * よって、ここでクラス拡張しておく。ファイルも綺麗に分かれて、流用し易い
 */
@interface AppController()

//実行時エラーになったので、AppController.hに移動

@end


/**
 * カテゴリの実装
 */
@implementation AppController (Purchase)


#pragma mark - init

- (void)purchaseRegister:(LoadedCallback)loadedCallback{
#if ENABLE_PURCHASE
    [self purchaseRegister];
    self.loadedCallback  = loadedCallback;
#endif
}

- (void)purchaseRegister{
#if ENABLE_PURCHASE
    DNSLog(@"purchaseRegister start");
    
    self.requestHasFinished = NO;
    
    if (![SKPaymentQueue canMakePayments]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@""
                                                      message:@"お使いの端末ではApp内課金の機能が制限されています。[設定]>[一般]>[機能制限]>[App内課金]をONに設定したうえで再度お試しください"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //PaymentQueueにオブザーバー登録（これで課金処理の同期ができるようになる）
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    //[self showIndicator:@"App Storeに問い合わせ中…"]; //コメント外す場合は、対応する hideIndicatorも一緒に
    
    //productRequest
    NSSet *productSet = [NSSet setWithArray:ALL_PRODUCT_IDS];
    SKProductsRequest *productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productSet];
    productRequest.delegate = self;
    [productRequest start]; //-(void)productsRequest: didReceiveResponse: が呼び出される
    
    self.allProducts = nil;
    
    DNSLog(@"purchaseRegister end");
#endif
}


#pragma mark - fin

- (void)purchaseUnregister{
#if ENABLE_PURCHASE
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    self.allProducts = nil;
#endif
}


#pragma mark - life cycle event

- (void)applicationDidEnterBackground{
#if ENABLE_PURCHASE
#endif
}

- (void)applicationWillEnterForeground{
#if ENABLE_PURCHASE
#endif
}



#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
#if ENABLE_PURCHASE
    DNSLog(@"productsRequest: didReceiveResponse: start");
    
#if DEBUG
    DNSLog(@"Valid PRODUCTS:");
    for (SKProduct* prod in response.products) {
        DNSLog(@"  %@", prod.productIdentifier);
        // [self productDescription:prod];
    }
    DNSLog(@"Invalid PRODUCTS:");
    for (NSString* prodId in response.invalidProductIdentifiers) {
        DNSLog(@"  %@", prodId);
    }
#endif
    
    if (response.products.count > 0){
        self.allProducts = [[NSArray alloc] initWithArray:response.products];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"App Storeに登録されたアイテムがありません。" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    DNSLog(@"currentLocal = %@\n", [NSLocale currentLocale].localeIdentifier);
    if (response.invalidProductIdentifiers.count > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"情報取得に失敗したアイテムがあります。" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    self.initSucceeded = response.products.count > 0 && response.invalidProductIdentifiers.count == 0;

    if (self.loadedCallback) {
        self.loadedCallback(self.initSucceeded);
    }
    
    //[self hideIndicator];
    
    //リストア処理を自動的に行う
    //[self restoreItems];
    
    self.requestHasFinished = YES;
    
    DNSLog(@"productsRequest: didReceiveResponse: end");
#endif
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
#if ENABLE_PURCHASE
    DNSLog(@"request: didFailWithError: start");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"エラー" message:[NSString stringWithFormat:@"App Storeに接続できませんでした。\n%@", [error localizedDescription]] delegate:self cancelButtonTitle:@"はい" otherButtonTitles:nil, nil];
    [alert show];
    
    //[self hideIndicator];
    
    self.requestHasFinished = YES;
    
    DNSLog(@"request: didFailWithError: end");
#endif
}

- (void)requestDidFinish:(SKRequest *)request{
#if ENABLE_PURCHASE
    DNSLog(@"requestDidFinish: start");
    
    //[self hideIndicator];
    
    self.requestHasFinished = YES;
    
    DNSLog(@"requestDidFinish: end");
#endif
}

- (BOOL) hasRequestFinished{
#if ENABLE_PURCHASE
    return self.requestHasFinished;
#else
    return false;
#endif
}

-(BOOL) hasInitSucceeded{
#if ENABLE_PURCHASE
    return self.initSucceeded;
#else
    return false;
#endif
}


#pragma mark - 購入、リストアなどユーザアクション

- (void) purchaseItem:(NSString*)productId withSuccessCallback:(PurchaseCallback)successCallback failureCallback:(PurchaseCallback)failureCallback cancelCallback:(PurchaseCallback) cancelCallback closedCallback:(PurchaseClosedCallback) closedCallback{
#if ENABLE_PURCHASE
    DNSLog(@"buyItem: withCallback: start");
    
    self.purchaseCallback = successCallback;
    self.failureCallback = failureCallback;
    self.cancelCallback = cancelCallback;
    self.closedCallback = closedCallback;
    
    //待ち表示
    [self showIndicator:@"お待ちください……"];
    
    //productIdが実在していれば、PaymentQueueに追加
    /*
    bool productHasFound = NO;
    for (SKProduct *product in self.allProducts) {
        if ([product.productIdentifier isEqualToString:productId]) {
            
            SKPayment* payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addPayment:payment]; //ここでトランザクション開始。payment:updatedTransactions:が呼ばれるようになる
            
            productHasFound = YES;
            DNSLog(@"addPayment (PRODUCT_ID=%@)", productId);
            break;
        }
    }*/
    SKProduct* product = [self getProductWithId:productId];
    if (product){
        SKPayment* payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment]; //ここでトランザクション開始。payment:updatedTransactions:が呼ばれるようになる

        DNSLog(@"addPayment (PRODUCT_ID=%@)", productId);
    }
    
    //見つからなかった場合(起動時のSKProductリクエストで情報が返ってこなかった。通信が悪かったなど)
    //if (!productHasFound) {
    if (!product){
        [self hideIndicator];
        
        if (self.failureCallback) {
            self.failureCallback([productId cStringUsingEncoding:NSUTF8StringEncoding]);
        }
        if (self.closedCallback) {
            self.closedCallback();
        }
        
        //[self makeAlertViewWithTitle:@"" message:@"商品情報が取得できませんでした。電波のよい場所でアプリを再起動してください" okTitle:@"OK"];
        return;
    }
    
    DNSLog(@"buyItem: withCallback: end");
#endif
}

-(void) restoreItemsWithSuccessCallback:(PurchaseCallback)successCallback completedCallback:(RestoreCompletedCallback)completedCallback failureCallback:(RestoreCompletedCallback)failureCallback closedCallback:(RestoreCompletedCallback)closedCallback{
#if ENABLE_PURCHASE
    DNSLog(@"restoreItems start");
    
    [self showIndicator:@"購入情報を復元中です……"];
    
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    
    self.restoreCallback = successCallback;
    self.restoreCompletedCallback = completedCallback;
    self.restoreFailureCallback = failureCallback;
    self.restoreClosedCallback  = closedCallback;
    
    DNSLog(@"restoreItems end");
#endif
}


#pragma mark - SKPaymentQueue observerMethod

// Sent when the transaction array has changed (additions or state changes).  Client should check state of transactions and finish as appropriate.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    DNSLog(@"paymentQueue: updatedTransactions: start");
    
    for (SKPaymentTransaction *transaction in transactions) {
        
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                //nothing to do
                break;
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            case SKPaymentTransactionStateDeferred:
                [self deferredTransaction:transaction];
                break;
        }
    }
    
    DNSLog(@"paymentQueue: updatedTransactions: end");
}

// Sent when transactions are removed from the queue (via finishTransaction:).
- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions{
    DNSLog(@"paymentQueue: removedTransactions: start");
    
    [self hideIndicator];
    
    DNSLog(@"paymentQueue: removedTransactions: end");
}

// Sent when an error is encountered while adding transactions from the user's purchase history back to the queue.
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    DNSLog(@"paymentQueue: restoreCompletedTransactionsFailedWithError: start");
    
    [self hideIndicator];
    
    if (self.restoreFailureCallback) {
        DNSLog(@"failureCallback start");
        self.restoreFailureCallback();
        DNSLog(@"failureCallback end");
    }
    if (self.restoreClosedCallback) {
        DNSLog(@"restoreClosedCallback start");
        self.restoreClosedCallback();
        DNSLog(@"restoreClosedCallback end");
    }

    DNSLog(@"paymentQueue: restoreCompletedTransactionsFailedWithError: end");
}

// Sent when all transactions from the user's purchase history have successfully been added back to the queue.
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue{
    DNSLog(@"paymentQueueRestoreCompletedTransactionsFinished: start");
    
    [self hideIndicator];
    
    if (self.restoreCompletedCallback) {
        DNSLog(@"restoreCompletedCallback start");
        self.restoreCompletedCallback();
        DNSLog(@"restoreCompletedCallback end");
    }
    if (self.restoreClosedCallback) {
        DNSLog(@"restoreClosedCallback start");
        self.restoreClosedCallback();
        DNSLog(@"restoreClosedCallback end");
    }
    
    DNSLog(@"paymentQueueRestoreCompletedTransactionsFinished: end");
}

// Sent when the download state has changed.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads{
    DNSLog(@"paymentQueue: updatedDownloads: start");
    
    DNSLog(@"paymentQueue: updatedDownloads: end");
}


#pragma mark - inner method for SKPaymentQueue observerMethod

- (void) failedTransaction: (SKPaymentTransaction *)transaction{
    DNSLog(@"failedTransaction: start");
    
    [self hideIndicator];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    //エラーコード全種
    //SKErrorUnknown,
    //SKErrorClientInvalid,               // client is not allowed to issue the request, etc.
    //SKErrorPaymentCancelled,            // user cancelled the request, etc.
    //SKErrorPaymentInvalid,              // purchase identifier was invalid, etc.
    //SKErrorPaymentNotAllowed,           // this device is not allowed to make the payment
    //SKErrorStoreProductNotAvailable,    // Product is not available in the current storefront
    
    NSString *productId = transaction.payment.productIdentifier;
    const char *productId_c = [productId cStringUsingEncoding:NSUTF8StringEncoding];
    
    if (transaction.error.code == SKErrorPaymentCancelled) {
        //キャンセル時処理コールバック呼び出し
        if (self.cancelCallback) {
            DNSLog(@"cancelCallback start");
            self.cancelCallback(productId_c); //コールバック呼び出し。ここでキャンセル時処理を行うこと
            DNSLog(@"cancelCallback end");
        }
        if (self.closedCallback) {
            self.closedCallback();
        }
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"購入できませんでした。\n電波の良い場所で再びお試しください。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        //失敗時処理コールバック呼び出し
        if (self.failureCallback) {
            DNSLog(@"failureCallback start");
            self.failureCallback(productId_c); //コールバック呼び出し。ここで失敗時処理を行うこと
            DNSLog(@"failureCallback end");
        }
        if (self.closedCallback) {
            self.closedCallback();
        }
    }
    
    DNSLog(@"failedTransaction: end");
}

- (void) deferredTransaction: (SKPaymentTransaction *)transaction{
    DNSLog(@"deferredTransaction: start");
    
    [self hideIndicator];
    
    NSString *productId = transaction.payment.productIdentifier;
    const char *productId_c = [productId cStringUsingEncoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"購入の許可を待っています。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    //失敗時処理コールバック呼び出し//TODO: 失敗なのか?
    if (self.failureCallback) {
        DNSLog(@"failureCallback start");
        self.failureCallback(productId_c); //コールバック呼び出し。ここで失敗時処理を行うこと
        DNSLog(@"failureCallback end");
    }
    if (self.closedCallback) {
        self.closedCallback();
    }

    DNSLog(@"deferredTransaction: end");
}

//リストアしたときのトランザクション
- (void) restoreTransaction: (SKPaymentTransaction *)transaction{
    DNSLog(@"restoreTransaction: start");
    DNSLog(@"productId = %@ x %ld", transaction.payment.productIdentifier, (long)transaction.payment.quantity);
    
    //リストア時処理コールバック呼び出し
    NSString *productId = transaction.payment.productIdentifier;
    const char *productId_c = [productId cStringUsingEncoding:NSUTF8StringEncoding];
    if (self.restoreCallback) {
        DNSLog(@"restoreCallback start");
        self.restoreCallback(productId_c); //コールバック呼び出し。ここでリストア時処理を行うこと
        DNSLog(@"restoreCallback end");
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    [self hideIndicator];
    
    DNSLog(@"restoreTransaction: end");
}

//購入に成功したトランザクションを処理
//  非消費アイテムを再度購入しようとし、無料で復元された場合もここに来る
- (void) completeTransaction: (SKPaymentTransaction *)transaction{
    DNSLog(@"completeTransaction: start");
    DNSLog(@"productId = %@ x %ld", transaction.payment.productIdentifier, transaction.payment.quantity);
    
    //購入時処理コールバック呼び出し
    NSString *productId = transaction.payment.productIdentifier;
    const char *productId_c = [productId cStringUsingEncoding:NSUTF8StringEncoding];
    if (self.purchaseCallback) {
        DNSLog(@"purchaseCallback start");
        self.purchaseCallback(productId_c); //コールバック呼び出し。ここで購入時処理を行うこと
        DNSLog(@"purchaseCallback end");
    }
    if (self.closedCallback) {
        self.closedCallback();
    }

    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    [self hideIndicator];
    
    DNSLog(@"completeTransaction: end");
}


#pragma mark - user method

-(void)productDescription:(SKProduct*)production{
    DNSLog(@"PRODUCT info-------------------");
    DNSLog(@"productIdentifier = %@", production.productIdentifier);
    DNSLog(@"localizedTitle = %@", production.localizedTitle);
    DNSLog(@"localizedDescription = %@", production.localizedDescription);
    DNSLog(@"price = %@", production.price);
    DNSLog(@"priceLocale = %@", production.priceLocale);
    DNSLog(@"downloadable = %d", production.downloadable);
    DNSLog(@"downloadContentLengths = %lu", (unsigned long)production.downloadContentLengths.count);
    DNSLog(@"downloadContentVersion = %@", production.downloadContentVersion);
    DNSLog(@"PRODUCT info end---------------");
}

-(SKProduct*) getProductWithId:(NSString*) productId {
    for (SKProduct* product in self.allProducts) {
        if ([product.productIdentifier isEqualToString: productId]) {
            return product;
        }
    }
    return nil;
}

-(BOOL) productExists:(NSString*) productId {
    return !![self getProductWithId:productId];
}

-(NSString*) getProductName:(NSString*)productId{
    SKProduct* product = [self getProductWithId:productId];
    if (product && product.localizedTitle) {
        return product.localizedTitle;
    }
    
    return ([productId isEqualToString:@(PRODUCT_ID_1)] ? @(PRODUCT_NAME_1) :
            [productId isEqualToString:@(PRODUCT_ID_2)] ? @(PRODUCT_NAME_2) :
            [productId isEqualToString:@(PRODUCT_ID_3)] ? @(PRODUCT_NAME_3) :
            [productId isEqualToString:@(PRODUCT_ID_4)] ? @(PRODUCT_NAME_4) :
            @"");
}

-(NSString*) getProductDescription:(NSString*)productId{
    SKProduct* product = [self getProductWithId:productId];
    if (product && product.localizedDescription) {
        return product.localizedDescription;
    }

    return ([productId isEqualToString:@(PRODUCT_ID_1)] ? @(PRODUCT_DESCRIPTION_1) :
            [productId isEqualToString:@(PRODUCT_ID_2)] ? @(PRODUCT_DESCRIPTION_2) :
            [productId isEqualToString:@(PRODUCT_ID_3)] ? @(PRODUCT_DESCRIPTION_3) :
            [productId isEqualToString:@(PRODUCT_ID_4)] ? @(PRODUCT_DESCRIPTION_4) :
            @"");
}

-(NSString*) getProductPrice:(NSString*)productId{
    SKProduct* product = [self getProductWithId:productId];
    if (product && product.price && product.priceLocale) {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:product.priceLocale];
        return [numberFormatter stringFromNumber:product.price];
    }
    return @"";
}


#pragma mark - indicator

-(void) showIndicator:(NSString*)message{
    [self.viewController showIndicator:message];
}

-(void) hideIndicator{
    [self.viewController hideIndicator];
}

@end
