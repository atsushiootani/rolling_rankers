//
//  AppController+Purchase.h
//
//
//  Created by OtaniAtsushi1 on 2015/11/17.
//
//

#import "AppController.h"
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>


#define ENABLE_PURCHASE (1)
//iTunesConnect で課金アイテム作成し、AppInfo.h の PRODUCT_ID_*** を定義のこと

@interface AppController (Purchase)<SKProductsRequestDelegate, SKPaymentTransactionObserver>


//課金情報削除
-(void) purchaseUnregister;

//課金情報の取得、初期化
-(void) purchaseRegister;
-(void) purchaseRegister:(LoadedCallback)loadedCallback;

//初期化が終わったかどうか
-(BOOL) hasRequestFinished;

//初期化が成功したかどうか
-(BOOL) hasInitSucceeded;

//商品購入処理開始
-(void) purchaseItem:(NSString*)productId withSuccessCallback:(PurchaseCallback)successCallback failureCallback:(PurchaseCallback)failureCallback cancelCallback:(PurchaseCallback) cancelCallback closedCallback:(PurchaseClosedCallback) closedCallback;

//リストア処理
-(void) restoreItemsWithSuccessCallback:(PurchaseCallback)successCallback completedCallback:(RestoreCompletedCallback)comletedCallback failureCallback:(RestoreCompletedCallback)failureCallback closedCallback:(RestoreCompletedCallback)closedCallback;

//商品情報取得
-(NSString*) getProductName:(NSString*)productId;
-(NSString*) getProductDescription:(NSString*)productId;
-(NSString*) getProductPrice:(NSString*)productId;

//インジケータ
-(void) showIndicator:(NSString*)message;
-(void) hideIndicator;


@end
