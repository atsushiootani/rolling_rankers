//
//  AppController+ranking.mm
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/04.
//
//

#import "AppController+ranking.h"
#import "RootViewController.h"


@implementation AppController (ranking)

- (BOOL)registerRanking_application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
#if ENABLE_RANKING
    if ([[Rankers sharedInstance] openURL:url sourceApplication:sourceApplication annotation:annotation]){
        return YES;
    }
#endif
    return NO;
}


// iOS9 対応
- (BOOL)registerRanking_application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
#if ENABLE_RANKING
    if ([[Rankers sharedInstance] openURL:url options:options]){
        return YES;
    }
#endif
    return NO;
}



- (BOOL)registerRanking_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
#if ENABLE_RANKING
    [[Rankers sharedInstance] setRootViewController:self.viewController];
    return YES;
#else
    return NO;
#endif
}


@end
