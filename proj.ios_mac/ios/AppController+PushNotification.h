//
//  AppController+PushNotification.h
//
//
//  Created by OtaniAtsushi1 on 2014/09/15.
//
//

#import "AppController.h"

#define ENABLE_PUSH_NOTIFICATION (1)
//GoogleService-Info.plist

@interface AppController (PushNotification)

- (void)pushNotificationRegister;

@end
