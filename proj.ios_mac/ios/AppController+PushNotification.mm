//
//  AppController+PushNotification.mm
//
//
//  Created by OtaniAtsushi1 on 2014/09/15.
//
//

#import "AppController+PushNotification.h"
#import "AppController+Http.h"
#import "RootViewController.h"
#import "iosCommon.h"

@interface AppController()

@end


@implementation AppController (PushNotification)

- (void)pushNotificationRegister{
    
#if ENABLE_PUSH_NOTIFICATION
    if ([self isOSVersionEqualOrLater:8.0]) {
        UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
#endif
}

//push通知を受け取り、通知をタップして起動した時に呼び出される（通知に関係なく起動した場合は呼び出されない）
//起動中にpush通知が来た場合は、その瞬間に呼び出される
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    //NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Pring full message.
    //NSLog(@"%@", userInfo);
    

    //アラート通知の情報が設定されている場合、アラート通知を行う
    NSString* title   = userInfo[@"_NOTICE_TITLE"];
    if (title != nil && title.length > 0) {
        
        NSString* message = userInfo[@"_NOTICE_MESSAGE"];
        NSString* no      = userInfo[@"_NOTICE_NO"];
        NSString* yes     = userInfo[@"_NOTICE_YES"];
        NSString* url     = userInfo[@"_NOTICE_URL"];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        if (no != nil && no.length > 0) {
            [alertController addAction:[UIAlertAction actionWithTitle:no style:UIAlertActionStyleDefault handler:nil]];
        }
        
        if (yes != nil && yes.length > 0){
            [alertController addAction:[UIAlertAction actionWithTitle:yes style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                if (url && url.length > 0) {
                    [self openWebPage:url];
                }
            }]];
        }
        
        [self.viewController presentViewController:alertController animated:YES completion:nil];
    }
}

@end
