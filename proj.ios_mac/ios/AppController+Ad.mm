//
//  AppController+Ad.mm
//
//
//  Created by OtaniAtsushi1 on 2014/11/20.
//
//

#import "AppController+Ad.h"
#import "AppController+Cocos2d.h"
#import "AppController+Http.h"
#import "RootViewController.h"
#import "Screen.h"
#import "iosCommon.h"



#if ENABLE_ADMOB
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <GoogleMobileAds/GADInterstitial.h>
#import <GoogleMobileAds/GADAdSize.h>

#define ADMOB_BANNER_UNIT_ID                @"ca-app-pub-7661660979526123/1664412696"
#define ADMOB_INTERSTITIAL_TRANSITION_ID    @"ca-app-pub-7661660979526123/4617879097"
#define ADMOB_INTERSTITIAL_ALWAYS_ID        @"ca-app-pub-7661660979526123/6094612294"
#define ADMOB_REWARD_MOVIE_UNIT_ID          @"ca-app-pub-7661660979526123/7571345492"
#endif


#if ENABLE_NEND
#import "NADView.h"
#import "NADInterstitial.h"
#import "NADNative.h"
#import "NADNativeClient.h"
#import "NadNativeView.h"

#if DEBUG
//アイコン型ネイティブ広告のテスト
//#define NEND_NATIVE_API_KEY @"10d9088b5bd36cf43b295b0774e5dcf7d20a4071"
//#define NEND_NATIVE_SPOT_ID @"485500"
//横長（小）ネイティブ広告のテスト
#define NEND_NATIVE_API_KEY @"a3972604a76864dd110d0b02204f4b72adb092ae"
#define NEND_NATIVE_SPOT_ID @"485502"
//横長（大）ネイティブ広告のテスト
//#define NEND_NATIVE_API_KEY @"30fda4b3386e793a14b27bedb4dcd29f03d638e5"
//#define NEND_NATIVE_SPOT_ID @"485504"
#else
#define NEND_NATIVE_API_KEY  @"****************************************"
#define NEND_NATIVE_SPOT_ID  @"000000"
#endif
#endif


#if ENABLE_IMOBILE
#import "ImobileSdkAds/ImobileSdkAds.h"
#endif


#if ENABLE_ADFURIKUN
#import <ADFMovieReward/ADFmyMovieReward.h>
#import "MovieReward6000.h"//AppLovin
//#import "MovieReward6001.h"//UnityAds
#import "MovieReward6002.h"//AdColony
#import "MovieReward6004.h"//Maio
//#import "MovieReward6006.h"//vungle

#define ADFURIKUN_MOVIE_ID @"581c48b22d3495c76a0015d2" //注: 「アプリ/サイトID」ではなく、「広告枠ID」を記述のこと
#endif


#if ENABLE_ADCOLONY
#import <AdColony/AdColony.h>

#define ADCOLONY_APP_ID          @"appdbeb1fea5e944fcea1"
#define ADCOLONY_ZONE_ID         @"vzb85be7da0125432686"
#define ADCOLONY_V4VC_SECRET_KEY @"v4vcdb8a8f1b7e384e9393"
#endif


#if ENABLE_ADSTIR
#import <AdstirAds/AdstirAds.h>

#define ADSTIR_MEDIA_ID @"MEDIA-4a023ef3"
#define ADSTIR_SPOT_ID  1
#endif

#if ENABLE_APPLOVIN
#import "ALSdk.h"
#import "ALIncentivizedInterstitialAd.h"
#endif


#pragma mark - ad movie switch

const NSString* PRONOUNCE_AD_MOVIE_ADMOB     = @"_adm_am";
const NSString* PRONOUNCE_AD_MOVIE_ADFURIKUN = @"_adm_af";
const NSString* PRONOUNCE_AD_MOVIE_ADCOLONY  = @"_adm_ac";
const NSString* PRONOUNCE_AD_MOVIE_ADSTIR    = @"_adm_as";
const NSString* PRONOUNCE_AD_MOVIE_APPLOVIN  = @"_adm_al";

static NSDictionary* s_adMovieLot = @{
#if ENABLE_ADMOB
                                    PRONOUNCE_AD_MOVIE_ADMOB:     @(AD_ADMOB),
#endif
#if ENABLE_ADFURIKUN
                                    PRONOUNCE_AD_MOVIE_ADFURIKUN: @(AD_ADFURIKUN),
#endif
#if ENABLE_ADCOLONY
                                    PRONOUNCE_AD_MOVIE_ADCOLONY:  @(AD_ADCOLONY),
#endif
#if ENABLE_ADSTIR
                                    PRONOUNCE_AD_MOVIE_ADSTIR:    @(AD_ADSTIR),
#endif
#if ENABLE_APPLOVIN
                                    PRONOUNCE_AD_MOVIE_APPLOVIN:  @(AD_APPLOVIN),
#endif
};
AdType adMovieType = AD_ADFURIKUN; //default
//AdType adMovieType = AD_ADCOLONY;


@interface AppController ()
#if ENABLE_AD
<
#if ENABLE_ADMOB
GADInterstitialDelegate, GADRewardBasedVideoAdDelegate,
#endif
#if ENABLE_NEND
NADInterstitialDelegate, NADViewDelegate, NADNativeDelegate,
#endif
#if ENABLE_ADFURIKUN
ADFmyMovieRewardDelegate,
#endif
#if ENABLE_ADCOLONY
AdColonyDelegate, AdColonyAdDelegate,
#endif
#if ENABLE_ADSTIR
AdstirVideoRewardDelegate,
#endif
#if ENABLE_APPLOVIN
ALAdRewardDelegate, ALAdDisplayDelegate, ALAdLoadDelegate
#endif
>
#endif

@end



@implementation AppController (Ad)

//common
BOOL bannerDisabled;

#if ENABLE_ADMOB
GADBannerView*   admobBannerView;
GADInterstitial* admobScreenInterstitial;
GADInterstitial* admobAlwaysInterstitial;
#endif

#if ENABLE_NEND
NADNativeClient* nendClient;
NadNativeView* nadNativeView;
#endif

#if ENABLE_IMOBILE
#endif

#if ENABLE_ADFURIKUN
ADFmyMovieReward*        adfurikun;
#endif

#if ENABLE_ADCOLONY
#endif

#if ENABLE_ADSTIR
AdstirVideoReward* adstirMovieAd;
#endif

//movie common
std::function<void(int)>  movieInsentiveCallback;
std::function<void(bool)> movieClosedCallback;
std::function<void()>     movieFailureCallback;
BOOL                      isAdMoviePlaying;
BOOL                      needsAdMovieInsentive;


//interstitial
int screenNum = 0;
int screenInterval = 5;


#pragma mark - life cycle

- (void)viewDidAppear:(BOOL)animated{
#if ENABLE_ADMOB
    if (adMovieType == AD_ADMOB) {
        [self admobViewAppearAndRewardInsentive];
    }
#endif
#if ENABLE_ADFURIKUN
    if (adMovieType == AD_ADFURIKUN) {
        [self adfurikunViewAppearAndRewardInsentive];
    }
#endif
#if ENABLE_ADCOLONY
    if (adMovieType == AD_ADCOLONY) {
        [self adcolonyViewAppearAndRewardInsentive];
    }
#endif
#if ENABLE_ADSTIR
    if (adMovieType == AD_ADSTIR){
        [self adstirViewAppearAndRewardInsentive];
    }
#endif
#if ENABLE_APPLOVIN
    if (adMovieType == AD_APPLOVIN) {
        [self appLovinViewAppearAndRewardInsentive];
    }
#endif
}

- (void)viewWillDisappear:(BOOL)animated{
    
}


#pragma mark - switch

-(void) initSwitch{
    adMovieType = (AdType)[self lot: s_adMovieLot
                       defaultValue: adMovieType];
}


#pragma mark - public

//全ての広告を初期化
-(void) adRegister{
    //振り分け。必ず最初に
    [self initSwitch];
    screenInterval = [self getIntPronounce:@"_ad_int" defaultValue:5];
    
    [self admobRegister];
    [self nendAdRegister];
    [self imobileRegister];
    
#if ENABLE_ADMOB
    if (adMovieType == AD_ADMOB) {
        //[self admobRegister];
    }
#endif
#if ENABLE_ADFURIKUN
    if (adMovieType == AD_ADFURIKUN) {
        [self adfurikunRegister];
    }
#endif
#if ENABLE_ADCOLONY
    if (adMovieType == AD_ADCOLONY) {
        [self adcolonyRegister];
    }
#endif
#if ENABLE_ADSTIR
    if (adMovieType == AD_ADSTIR) {
        [self adstirRegister];
    }
#endif

#if ENABLE_APPLOVIN
    [self appLovinRegister];
#endif
}

//バックグラウンド・フォアグラウンド処理
-(void) adEnterBackground{
}
-(void) adEnterForeground{
}

//バナー広告ON/OFF
-(void) adBannerShow{
    if (!bannerDisabled) {
        [self admobBannerShow];//メディエーション表示なので、admobを呼ぶだけ
    }
}
-(void) adBannerHide{
    [self admobBannerHide];
}

//削除したバナーを復活させる
-(void) adBannerSetEnable{
    bannerDisabled = NO;
    [self adBannerShow];
}
//バナーを削除する(Showが呼び出されても)
-(void) adBannerSetDisable{
    bannerDisabled = YES;
    [self adBannerHide];
}

//アイコン広告を表示する
-(void) adIconShowAt:(int) pos{
    [self nendIconShowAt:pos];
}
-(void) adIconShow{
    [self nendIconShow];
}
-(void) adIconHide{
    [self nendIconHide];
}
-(void) adIconHideAt:(int) pos{
    [self nendIconHideAt:pos];
}

//インタースティシャル表示
-(void) adInterstitialTransitionShow{
    [self admobScreenInterstitialShow];//メディエーション表示なので、admobを呼ぶだけ
}
-(void) adInterstitialAlwaysShow{
    [self admobAlwaysInterstitialShow];
}

//ヘッダーの目立つ位置の広告を表示
-(void) adHeaderShow{
    [self nendHeaderShow];
}
-(void) adHeaderHide{
    [self nendHeaderHide];
}

//動画
-(BOOL) adMovieCanPlay{
#if ENABLE_ADMOB
    if (adMovieType == AD_ADMOB) {
        return [self admobCanPlay];
    }
#endif
#if ENABLE_ADFURIKUN
    if (adMovieType == AD_ADFURIKUN) {
        return [self adfurikunCanPlay];
    }
#endif
#if ENABLE_ADCOLONY
    if (adMovieType == AD_ADCOLONY) {
        return [self adcolonyCanPlay];
    }
#endif
#if ENABLE_ADSTIR
    if (adMovieType == AD_ADSTIR){
        return [self adstirCanPlay];
    }
#endif
#if ENABLE_APPLOVIN
    if (adMovieType == AD_APPLOVIN) {
        return [self appLovinCanPlay];
    }
#endif
    return false;
}

-(void) adMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback{
#if ENABLE_ADMOB
    if (adMovieType == AD_ADMOB) {
        [self admobMoviePlay:insentiveCallback failureCallback:failureCallback closedCallback:closedCallback];
    }
#endif
#if ENABLE_ADFURIKUN
    if (adMovieType == AD_ADFURIKUN) {
        [self adfurikunMoviePlay:insentiveCallback failureCallback:failureCallback closedCallback:closedCallback];
    }
#endif
#if ENABLE_ADCOLONY
    if (adMovieType == AD_ADCOLONY) {
        [self adcolonyMoviePlay:insentiveCallback failureCallback:failureCallback closedCallback:closedCallback];
    }
#endif
#if ENABLE_ADSTIR
    if (adMovieType == AD_ADSTIR){
        [self adstirMoviePlay:insentiveCallback failureCallback:failureCallback closedCallback:closedCallback];
    }
#endif
#if ENABLE_APPLOVIN
    if (adMovieType == AD_APPLOVIN) {
        [self appLovinMoviePlay:insentiveCallback failureCallback:failureCallback closedCallback:closedCallback];
    }
#endif
}

//moviePlaying
-(BOOL) isAdMoviePlayingNow{
    return isAdMoviePlaying;
}

//urlスキームで他のアプリが開けるかチェック
-(BOOL) checkApplicationInstalled:(NSString*) urlScheme{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlScheme]];
}
-(void) openInstalledApplication:(NSString*) urlScheme{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlScheme]];
}


#pragma mark - ad rect

//縦が短い端末は、フッターが8割の大きさになる. HomeSceneのフッター生成部分と数値を合わせること
-(BOOL) is4_3Screen{
    return [UIScreen mainScreen].bounds.size.height < 568;
}

//ゲーム座標からスクリーン座標へ変換
-(CGRect) getScreenRectFromGameRect:(CGRect) gameRect{
    float xScale = [UIScreen mainScreen].bounds.size.width  / VIRTUAL_SCREEN_WIDTH;
    float yScale = [UIScreen mainScreen].bounds.size.height / VIRTUAL_SCREEN_HEIGHT;
    return CGRectMake(gameRect.origin.x * xScale,
                      (VIRTUAL_SCREEN_HEIGHT - gameRect.origin.y - gameRect.size.height) * yScale,
                      gameRect.size.width  * xScale,
                      gameRect.size.height * yScale);
}

//フッターサイズ（画面サイズによって変わる）
-(float) getFooterHeight{
    float height = FOOTER_HEIGHT * [UIScreen mainScreen].bounds.size.height / 568;
    return height - 1; //誤差で1ピクセルずれていたため
}

//バナー広告の位置
-(CGRect) getBannerRect{
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGSize adSize = CGSizeMake(320, 50);
    CGRect rect = CGRectMake((screenRect.size.width - adSize.width) / 2, screenRect.size.height - adSize.height - [self getFooterHeight] / 2, adSize.width, adSize.height);
    return rect;
}

//アイコン広告の位置
-(CGRect) getIconRect:(int) pos {
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGFloat xmargin = 64, ymargin = 1;
    CGSize iconMargin = CGSizeMake(4,4);
    CGSize adSize = CGSizeMake(64 - iconMargin.width * 2, 64 - iconMargin.height * 2);
    CGRect rect = CGRectMake(xmargin * pos + iconMargin.width,
                             screenRect.size.height - adSize.height - iconMargin.height + ymargin,
                             adSize.width,
                             adSize.height);
    return rect;
}

//左上のレクタングル
-(CGRect) getLTRectangleRect{
#if ENABLE_NEND
    CGPoint pos = CGPointMake(10, 45);
    CGSize adSize = AD_SIZE_NEND_SMALL_RECT;
    CGRect rect = CGRectMake(pos.x, pos.y,
                             adSize.width, adSize.height);
    return rect;
#else
    return CGRectMake(10, 50, 80, 60);
#endif
}


#pragma mark - google admob

-(void) admobRegister{
#if ENABLE_ADMOB
    /**
     * バナー
     */
    GADAdSize size = kGADAdSizeBanner;
    admobBannerView = [[GADBannerView alloc]initWithAdSize:size origin:[self getBannerRect].origin];
    admobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID;
    admobBannerView.rootViewController = self.viewController;
    
    GADRequest *request = [GADRequest request];
#if DEBUG
    request.testDevices = @[@"d29a4f23606706a9eeeed746d5daf3fa6551daf2"];
#endif
    [admobBannerView loadRequest:request];
    
    [self.viewController.view addSubview:admobBannerView];
    admobBannerView.hidden = YES;
    
    /**
     * 遷移時インタースティシャル
     */
    DNSLog(@"admob interstitial request");
    admobScreenInterstitial = [[GADInterstitial alloc]initWithAdUnitID:ADMOB_INTERSTITIAL_TRANSITION_ID];
    [admobScreenInterstitial loadRequest:request];
    admobScreenInterstitial.delegate = self;

    /**
     * 常駐インタースティシャル
     */
    DNSLog(@"admob interstitial request");
    admobAlwaysInterstitial = [[GADInterstitial alloc]initWithAdUnitID:ADMOB_INTERSTITIAL_ALWAYS_ID];
    [admobAlwaysInterstitial loadRequest:request];
    admobAlwaysInterstitial.delegate = self;
    
    /**
     * 動画リワード
     */
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:[GADRequest request]
                                           withAdUnitID:ADMOB_REWARD_MOVIE_UNIT_ID];
    [GADRewardBasedVideoAd sharedInstance].delegate = self;

#endif
}

//バナーON/OFF
-(void) admobBannerShow{
#if ENABLE_ADMOB
    admobBannerView.hidden = NO;
#endif
}
-(void) admobBannerHide{
#if ENABLE_ADMOB
    admobBannerView.hidden = YES;
#endif
}

//遷移時インタースティシャル表示
-(void) admobScreenInterstitialShow{
#if ENABLE_ADMOB
    if ([admobScreenInterstitial isReady]) {
        [admobScreenInterstitial presentFromRootViewController:self.viewController];
    }
#endif
}

//常駐インタースティシャル表示
-(void) admobAlwaysInterstitialShow{
#if ENABLE_ADMOB
    if ([admobAlwaysInterstitial isReady]) {
        [admobAlwaysInterstitial presentFromRootViewController:self.viewController];
    }
#endif
}

#if ENABLE_ADMOB //GADInterstitialDelegate

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    DNSLog(@"interstitialDidReceiveAd");
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    DNSLog(@"didFailToReceiveAdWithError, %@", [error description]);
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad{
    DNSLog(@"interstitialWillPresentScreen");
}

/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad{
    DNSLog(@"interstitialWillDismissScreen");
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad{
    DNSLog(@"interstitialWillLeaveApplication");
}

//表示終了時
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    if ([interstitial.adUnitID isEqualToString:ADMOB_INTERSTITIAL_TRANSITION_ID]) {
        DNSLog(@"admob interstitial request");
        admobScreenInterstitial = [[GADInterstitial alloc]initWithAdUnitID:ADMOB_INTERSTITIAL_TRANSITION_ID];
        [admobScreenInterstitial loadRequest:[GADRequest request]];
        admobScreenInterstitial.delegate = self;
    }
    else if ([interstitial.adUnitID isEqualToString:ADMOB_INTERSTITIAL_ALWAYS_ID]) {
        DNSLog(@"admob interstitial request");
        admobAlwaysInterstitial = [[GADInterstitial alloc]initWithAdUnitID:ADMOB_INTERSTITIAL_ALWAYS_ID];
        [admobAlwaysInterstitial loadRequest:[GADRequest request]];
        admobAlwaysInterstitial.delegate = self;
    }
}

#endif

//動画再生時
-(void)admobMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback{
#if ENABLE_ADMOB
    movieInsentiveCallback = insentiveCallback;
    movieClosedCallback  = closedCallback;
    movieFailureCallback = failureCallback;
    
    //念のため
    if (![self admobCanPlay]){
        DNSLog(@"admobMoviePlay is not ready");
        
        return;
    }
    
    [self pauseCocos2d];
    [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self.viewController];
    
    //もうここでインセンティブ付与しちゃう(AdsDidCompleteShowが正しく呼ばれない広告があるので)
    //...とやりたいところだが、インセンティブが「60秒間ブースト」なので、広告再生中に裏でブーストかかるのは困る。ので、viewDidAppear時にインセンティブ付与
    needsAdMovieInsentive = YES;
    
    isAdMoviePlaying = YES;
#endif
}

//動画広告終了時
-(void)admobViewAppearAndRewardInsentive{
#if ENABLE_ADMOB
    if (isAdMoviePlaying) {
        [self resumeCocos2d];
        
        if (needsAdMovieInsentive && movieInsentiveCallback) {
            movieInsentiveCallback(1);
            movieInsentiveCallback = nil;
        }
        
        if (movieClosedCallback) {
            movieClosedCallback(needsAdMovieInsentive);
            movieClosedCallback = nil;
        }
        
        needsAdMovieInsentive = NO;
        isAdMoviePlaying = NO;
    }
#endif
}

-(BOOL)admobCanPlay{
#if ENABLE_ADMOB
    return [GADRewardBasedVideoAd sharedInstance].isReady;
#else
    return false;
#endif
}

#if ENABLE_ADMOB //GADRewardBasedVideoAdDelegate

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    [self admobViewAppearAndRewardInsentive];
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load. %@", [error description]);
}

/// Tells the delegate that the reward based video ad has rewarded the user.
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd didRewardUserWithReward:(GADAdReward *)reward{
}

#endif


#pragma mark - nend

-(void)nendAdRegister{
#if ENABLE_NEND
    /**
     * バナーはadmobメディエーションで表示するので不要
     */
    
    /**
     * インタースティシャルもadmobメディエーションで表示するので不要
     */
    
    /**
     * ネイティブ広告
     */
    /*
    //インスタンス生成
    nendClient = [[NADNativeClient alloc] initWithSpotId:NEND_NATIVE_SPOT_ID
                                                  apiKey:NEND_NATIVE_API_KEY
                                   advertisingExplicitly:NADNativeAdvertisingExplicitlyPR];
    //デレゲート
    nendClient.delegate = self;
    
    //ログ出力
#if DEBUG
    //[NADNativeLogger setLogLevel:NADNativeLogLevelDebug];
#else
    [NADNativeLogger setLogLevel:NADNativeLogLevelNone];
#endif
    
    //表示用View作成
    nadNativeView = [[NadNativeView alloc]initWithFrame:[self getLTRectangleRect]];
    nadNativeView.hidden = YES;
#if DEBUG
    //[nadNativeView setBackgroundColor:[UIColor whiteColor]];
#endif
    [self.viewController.view addSubview:nadNativeView];
    //ロード
    [nendClient loadWithCompletionBlock:^(NADNative *ad, NSError *error) {
        if (ad) {
            // 成功
            [ad intoView:nadNativeView];
        } else {
            // 失敗
            NSLog(@"error: %@", error);
        }
    }];
     */
#endif
}

-(void)nendIconShowAt:(int) pos{
#if ENABLE_NEND
#endif
}

-(void)nendIconShow{
#if ENABLE_NEND
#endif
}

-(void)nendIconHide{
#if ENABLE_NEND
#endif
}

-(void)nendIconHideAt:(int) pos{
#if ENABLE_NEND
#endif
}

-(void)nendHeaderShow{
#if ENABLE_NEND
    nadNativeView.hidden = NO;
#endif
}

-(void)nendHeaderHide{
#if ENABLE_NEND
    nadNativeView.hidden = YES;
#endif
}


#pragma mark - imobile

-(void)imobileRegister{
#if ENABLE_IMOBILE
    /**
     * バナーはadmobメディエーションで表示するので不要
     */
    
    /**
     * インタースティシャルもメディエーションで表示するので不要
     */
    
    //テキストポップアップ
    //[ImobileSdkAds registerWithPublisherID:IMOBILE_PID MediaID:IMOBILE_MID SpotID:IMOBILE_SID_TEXTPOPUP];
    //[ImobileSdkAds startBySpotID:IMOBILE_SID_TEXTPOPUP];
#endif
}

-(void)imobileTextPopupShow{
#if ENABLE_IMOBILE
    //[ImobileSdkAds showBySpotID:IMOBILE_SID_TEXTPOPUP];
#endif
}


#pragma mark - adfurikun

-(void)adfurikunRegister{
#if ENABLE_ADFURIKUN
    
    if (![ADFmyMovieReward isSupportedOSVersion]) {
        DNSLog(@"ADFURIKUN  not supported OS version.");
        return;
    }
    
    NSString* adid = [self getStringPronounce:@"__adfid" defaultValue: ADFURIKUN_MOVIE_ID];
    
    [ADFmyMovieReward initWithAppID:adid viewController:self.viewController];
    adfurikun = [ADFmyMovieReward getInstance:adid delegate:self];
    
    movieInsentiveCallback = nil;
    movieClosedCallback = nil;
    movieFailureCallback = nil;
    isAdMoviePlaying = NO;
#endif
}

-(BOOL)adfurikunCanPlay{
#if ENABLE_ADFURIKUN
    return adfurikun && [adfurikun isPrepared];
#else
    return false;
#endif
}

-(void)adfurikunMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback{
#if ENABLE_ADFURIKUN
    movieInsentiveCallback = insentiveCallback;
    movieClosedCallback  = closedCallback;
    movieFailureCallback = failureCallback;
    
    //念のため
    if (![self adfurikunCanPlay]){
        DNSLog(@"adfurikunMoviePlay is not ready");

        [self AdsPlayFailed];
        return;
    }
    
    [self pauseCocos2d];
    [adfurikun play];
    
    //もうここでインセンティブ付与しちゃう(AdsDidCompleteShowが正しく呼ばれない広告があるので)
    //...とやりたいところだが、インセンティブが「60秒間ブースト」なので、広告再生中に裏でブーストかかるのは困る。ので、viewDidAppear時にインセンティブ付与
    needsAdMovieInsentive = YES;
    
    isAdMoviePlaying = YES;
#endif
}

//動画広告終了時(多重に呼び出しても大丈夫)
-(void)adfurikunViewAppearAndRewardInsentive{
#if ENABLE_ADFURIKUN
    if (isAdMoviePlaying) {
        [self resumeCocos2d];
        
        if (needsAdMovieInsentive && movieInsentiveCallback) {
            movieInsentiveCallback(1);
            movieInsentiveCallback = nil;
        }

        if (movieClosedCallback) {
            movieClosedCallback(needsAdMovieInsentive);
            movieClosedCallback = nil;
        }
        
        needsAdMovieInsentive = NO;
        isAdMoviePlaying = NO;
    }
#endif
}


#if ENABLE_ADFURIKUN

- (void)AdsFetchCompleted:(BOOL)isTestMode_inApp{
    DNSLog(@"ADFURIKUN fetch completed. %d", isTestMode_inApp);
}

- (void)AdsDidCompleteShow{
    DNSLog(@"ADFURIKUN did complete show");
    //ここは、動画が流れ終わったタイミングであり、まだ静止画広告が表示されたまま
    //よってゲームに復帰する処理は行えない
    //  厳密には、インセンティブ付与処理のみ行うのが正しいが、分けるほどでもない
    //インセンティブ付与及びゲーム復帰処理は、adfurikunViewDidAppear メソッドの中で行っているので、そちらを参照
}

- (void)AdsDidHide{
    DNSLog(@"ADFURIKUN did hide");
    [self adfurikunViewAppearAndRewardInsentive];
}

//AdsDidCompleteShowとAdsDidHide
//adColony…閉じた時に同時に呼ばれる。広告先に飛んだ場合、インセンティブが付与されないままになる可能性あり
//vungle…同様
//maio…正しいタイミングで呼び出される
//appLovin…正しいタイミングで呼び出される

-(void)AdsFetchError{
    DNSLog(@"ADFURIKUN fetch error");
    
    //再作成
    [adfurikun dispose];
    [self adfurikunRegister];
}

-(void)AdsPlayFailed{
    DNSLog(@"ADFURIKUN play error");

    if (movieFailureCallback){
        movieFailureCallback();
        movieFailureCallback = nil;
    }
}

#endif



#pragma mark - adcolony

-(void)adcolonyRegister{
#if ENABLE_ADCOLONY
    bool logging = NO;
#if DEBUG
    logging = YES;
#endif
    [AdColony configureWithAppID:ADCOLONY_APP_ID
                         zoneIDs:@[ADCOLONY_ZONE_ID]
                        delegate:self
                         logging:logging];
#endif
}

-(bool)adcolonyCanPlay{
#if ENABLE_ADCOLONY
    return [AdColony isVirtualCurrencyRewardAvailableForZone:ADCOLONY_ZONE_ID];
#else
    return false;
#endif
}

//動画再生する。再生成功時のコールバックも指定（このコールバック内でインセンティブ処理を行う）
-(void)adcolonyMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback{
#if ENABLE_ADCOLONY
    
    movieInsentiveCallback = insentiveCallback;
    movieClosedCallback    = closedCallback;
    movieFailureCallback   = failureCallback;

    if (![self adcolonyCanPlay]){
        if (movieFailureCallback) {
            movieFailureCallback();
            movieFailureCallback = nil;
        }
        if (movieClosedCallback) {
            movieClosedCallback(NO);
            movieClosedCallback = nil;
        }
        movieInsentiveCallback = nil;
        return;
    }

    [self pauseCocos2d];
    [AdColony playVideoAdForZone:ADCOLONY_ZONE_ID withDelegate:self];

    needsAdMovieInsentive = YES;
    isAdMoviePlaying = YES;

#else
    if (failureCallback) {
        failureCallback();
    }
    if (closedCallback) {
        closedCallback(NO);
    }
#endif
}

//動画広告終了時
-(void)adcolonyViewAppearAndRewardInsentive{
#if ENABLE_ADCOLONY
    if (isAdMoviePlaying) {
        [self resumeCocos2d];
        
        if (needsAdMovieInsentive && movieInsentiveCallback) {
            movieInsentiveCallback(1);
            movieInsentiveCallback = nil;
        }
        
        if (movieClosedCallback) {
            movieClosedCallback(needsAdMovieInsentive);
            movieClosedCallback = nil;
        }
        
        needsAdMovieInsentive = NO;
        isAdMoviePlaying = NO;
    }
#endif
}


#if ENABLE_ADCOLONY

//再生したあとに呼び出されるコールバック
-(void)onAdColonyV4VCReward:(BOOL)success currencyName:(NSString*)currencyName currencyAmount:(int)amount inZone:(NSString*)zoneID{
    if (success && [zoneID isEqualToString: ADCOLONY_ZONE_ID]) {
        if (movieInsentiveCallback) {
            movieInsentiveCallback(amount);
            movieInsentiveCallback = nil;
        }
    }
    else{
        if (movieFailureCallback){
            movieFailureCallback();
            movieFailureCallback = nil;
        }
    }
}

#endif


//インセンティブの名前取得（adcolony管理画面で設定したもの）
-(NSString*)adcolonyGetBonusInsentiveName{
#if ENABLE_ADCOLONY
    return [AdColony getVirtualCurrencyNameForZone:ADCOLONY_ZONE_ID];
#else
    return @"";
#endif
}

//インセンティブの数量取得（adcolony管理画面で設定したもの）
-(int)adcolonyGetBonusInsentiveAmount{
#if ENABLE_ADCOLONY
    return [AdColony getVirtualCurrencyRewardAmountForZone:ADCOLONY_ZONE_ID];
#else
    return 0;
#endif
}

//今日まだインセンティブが取得できるかどうか（上限が管理画面で設定されている）
-(bool)adcolonyCanGetInsentive{
#if ENABLE_ADCOLONY
    return [AdColony isVirtualCurrencyRewardAvailableForZone:ADCOLONY_ZONE_ID];
#else
    return false;
#endif
}

//今日あと何回インセンティブが取得できるか（上限が管理画面で設定されている）
-(int)adcolonyGetInsentiveAvailableNumToday{
#if ENABLE_ADCOLONY
    return [AdColony getVirtualCurrencyRewardsAvailableTodayForZone:ADCOLONY_ZONE_ID];
#else
    return 0;
#endif
}

#if ENABLE_ADCOLONY
-(void) onAdColonyAdStartedInZone:(NSString*)zoneID{
    [self pauseCocos2d];
}

-(void) onAdColonyAdAttemptFinished:(BOOL)shown inZone:(NSString*)zoneID{
    [self resumeCocos2d];
}

#endif



#pragma mark - adstir

-(void) adstirRegister{
#if ENABLE_ADSTIR
    //全体
    [AdstirVideoReward setMediaUserID:[[UIDevice currentDevice].identifierForVendor UUIDString]];
    [AdstirVideoReward prepareWithMedia:ADSTIR_MEDIA_ID spots:@[@ADSTIR_SPOT_ID]];

    //動画広告
    adstirMovieAd = [[AdstirVideoReward alloc] initWithMedia:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID];
    adstirMovieAd.delegate = self;
    [adstirMovieAd load];
#endif
}

-(BOOL) adstirCanPlay{
#if ENABLE_ADSTIR
    return [adstirMovieAd canShow];
#else
    return false;
#endif
}

-(void)adstirMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback{
#if ENABLE_ADSTIR
    
    movieInsentiveCallback = insentiveCallback;
    movieClosedCallback    = closedCallback;
    movieFailureCallback   = failureCallback;
    
    if (![adstirMovieAd canShow]) {
        if (movieFailureCallback) {
            movieFailureCallback();
            movieFailureCallback = nil;
        }
        if (movieClosedCallback) {
            movieClosedCallback(NO);
            movieClosedCallback = nil;
        }
        movieInsentiveCallback = nil;
        return;
    }
    
    [self pauseCocos2d];
    [adstirMovieAd showFromViewController:self.viewController];
    
    needsAdMovieInsentive = YES;
    isAdMoviePlaying = YES;
    
#else
    
    if (failureCallback) {
        failureCallback();
    }
    if (closedCallback) {
        closedCallback(NO);
    }
#endif
}

- (void)adstirViewAppearAndRewardInsentive{
#if ENABLE_ADSTIR
    if (isAdMoviePlaying) {
        [self resumeCocos2d];
        
        if (needsAdMovieInsentive && movieInsentiveCallback) {
            movieInsentiveCallback(1);
            movieInsentiveCallback = nil;
        }
        
        if (movieClosedCallback) {
            movieClosedCallback(needsAdMovieInsentive);
            movieClosedCallback = nil;
        }
        
        needsAdMovieInsentive = NO;
        isAdMoviePlaying = NO;
    }
#endif
    
}

#if ENABLE_ADSTIR
- (void)adstirVideoRewardDidLoad:(AdstirVideoReward *)videoReward{
    DNSLog(@"adstir loaded");
}

- (void)adstirVideoReward:(AdstirVideoReward *)videoReward didFailToLoadWithError:(NSError *)error{
    DNSLog(@"adstir load error. %@", [error description]);
    [adstirMovieAd load];
}

- (void)adstirVideoRewardDidStart:(AdstirVideoReward *)videoReward{
    DNSLog(@"adstir play start");
}

- (void)adstirVideoRewardDidComplete:(AdstirVideoReward *)videoReward{
    DNSLog(@"adstir play completed");
    [adstirMovieAd load];
}

- (void)adstirVideoRewardDidCancel:(AdstirVideoReward *)videoReward{
    DNSLog(@"adstir play cancelled");
    [adstirMovieAd load];
}

- (void)adstirVideoRewardDidClose:(AdstirVideoReward *)videoReward{
    DNSLog(@"adstir closed");
    [self adstirViewAppearAndRewardInsentive];
}
#endif


#pragma mark - app lovin

- (void)appLovinRegister{
#if ENABLE_APPLOVIN
    [ALSdk initializeSdk];
    [ALIncentivizedInterstitialAd preloadAndNotify: self]; //ALAdLoadDelegate
    [ALIncentivizedInterstitialAd shared].adDisplayDelegate = self; //ALAdDisplayDelegate
#endif
}

-(void)appLovinInterstitialShow{
#if ENABLE_APPLOVIN
    if (++screenNum >= screenInterval) {
        screenNum -= screenInterval;
        if ([ALInterstitialAd isReadyForDisplay]){
            [ALIncentivizedInterstitialAd showAndNotify: self]; //ALAdRewardDelegate
        }
        else{
            // No interstitial ad is currently available.  Perform failover logic...
        }
    }
#endif
}

-(BOOL) appLovinCanPlay{
#if ENABLE_APPLOVIN
    return [ALIncentivizedInterstitialAd isReadyForDisplay];
#else
    return false;
#endif
}

-(void)appLovinMoviePlay:(std::function<void(int)>) insentiveCallback failureCallback:(std::function<void()>) failureCallback closedCallback:(std::function<void(bool)>)closedCallback{
#if ENABLE_APPLOVIN
    
    movieInsentiveCallback = insentiveCallback;
    movieClosedCallback    = closedCallback;
    movieFailureCallback   = failureCallback;
    
    if (![ALIncentivizedInterstitialAd isReadyForDisplay]) {
        if (movieFailureCallback) {
            movieFailureCallback();
            movieFailureCallback = nil;
        }
        if (movieClosedCallback) {
            movieClosedCallback(NO);
            movieClosedCallback = nil;
        }
        movieInsentiveCallback = nil;
        return;
    }
    
    [self pauseCocos2d];
    [ALIncentivizedInterstitialAd showAndNotify: self];
    
    needsAdMovieInsentive = YES;
    isAdMoviePlaying = YES;
    
#else
    
    if (failureCallback) {
        failureCallback();
    }
    if (closedCallback) {
        closedCallback(NO);
    }
#endif
}

- (void)appLovinViewAppearAndRewardInsentive{
#if ENABLE_APPLOVIN
    if (isAdMoviePlaying) {
        [self resumeCocos2d];
        
        if (needsAdMovieInsentive && movieInsentiveCallback) {
            movieInsentiveCallback(1);
            movieInsentiveCallback = nil;
        }
        
        if (movieClosedCallback) {
            movieClosedCallback(needsAdMovieInsentive);
            movieClosedCallback = nil;
        }
        
        needsAdMovieInsentive = NO;
        isAdMoviePlaying = NO;
    }
#endif
}

#if ENABLE_APPLOVIN

//ALAdLoadDelegate
- (void) adService: (alnonnull ALAdService *) adService didLoadAd: (alnonnull ALAd *) ad{
    DNSLog(@"video=%d, %@, %@", [ad isVideoAd], [adService description], [ad description]);
}

- (void) adService: (alnonnull ALAdService *) adService didFailToLoadAdWithError: (int) code{
    DNSLog(@"applovin load ad failed(%d), %@", code, (code == 204 ? @"no fill": @"")); //204
}

//ALAdRewardDelegate
//動画を見、報酬を受け取ることができる（appLovinのサーバ側に報酬を付与した段階で呼ばれる）
- (void) rewardValidationRequestForAd: (alnonnull ALAd *) ad didSucceedWithResponse: (alnonnull NSDictionary *) response{
    DNSLog(@"applovin reward succeeded. %@", [response descriptionInStringsFileFormat]);
    [self appLovinViewAppearAndRewardInsentive];
}
//動画を見たが、フリークエンシーキャップに到達していて付与できない時
- (void) rewardValidationRequestForAd: (alnonnull ALAd *) ad didExceedQuotaWithResponse: (alnonnull NSDictionary *) response{
    DNSLog(@"applovin reward has watched but frequency capped. %@", [response descriptionInStringsFileFormat]);
    [self appLovinViewAppearAndRewardInsentive];
}
//appLovinサーバが報酬付与を拒否した時。ユーザの不正行為のあった場合など
- (void) rewardValidationRequestForAd: (alnonnull ALAd *) ad wasRejectedWithResponse: (alnonnull NSDictionary *) response{
    DNSLog(@"applovin reward rejected. %@", [response descriptionInStringsFileFormat]);
    [self appLovinViewAppearAndRewardInsentive];
}
//失敗時。サーバの応答がないなど
- (void) rewardValidationRequestForAd: (alnonnull ALAd *) ad didFailWithError: (NSInteger) responseCode{
    DNSLog(@"applovin reward failed. %d", (int)responseCode);
    [self appLovinViewAppearAndRewardInsentive];
}
//ユーザが「動画を見ない」を選択した時に呼ばれる（appLovin管理画面で、動画前システムウィンドウを表示するオプションを選択していた時のみ）
- (void) userDeclinedToViewAd: (alnonnull ALAd *) ad{
    
}

#endif

@end
