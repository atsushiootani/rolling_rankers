//
//  AppController+Cocos2d.mm
//  
//
//  Created by OtaniAtsushi1 on 2016/01/18.
//
//

#import "AppController+Cocos2d.h"
#import "cocos2d.h"
#import "SimpleAudioEngine.h"


@implementation AppController (Cocos2d)


-(void) pauseCocos2d{
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
    cocos2d::Director::getInstance()->stopAnimation();
    cocos2d::Director::getInstance()->pause();
}

-(void) resumeCocos2d{
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    cocos2d::Director::getInstance()->startAnimation();
    cocos2d::Director::getInstance()->resume();
}

@end
