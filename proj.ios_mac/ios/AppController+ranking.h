//
//  AppController+ranking.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/04.
//
//

#import "AppController.h"

#define ENABLE_RANKING (1 && !TARGET_OS_SIMULATOR) //RankingManager.h/cpp とも協調すること。なお TARGET_OS_SIMULATOR はObjC側でしか定義されていない


#import <RankersSDK/RankersSDK.h>

@interface AppController (ranking)

- (BOOL)registerRanking_application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
- (BOOL)registerRanking_application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options;
- (BOOL)registerRanking_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
