//
//  AppController+Analytics.mm
//
//
//  Created by OtaniAtsushi1 on 2015/04/19.
//
//

#import "AppController+Analytics.h"
#import "AppController+Data.h"
#import "iosCommon.h"

#if ENABLE_ANALYTICS
#import <FirebaseAnalytics/FIRAnalytics.h>
#endif


@implementation AppController (Analytics)

- (void)analytics_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
#if ENABLE_ANALYTICS
    NSString* KEY_FOR_USER_ID = @"_key_for_analytics_user_id";
    NSString* userId = [self loadStringWithKey:KEY_FOR_USER_ID];
    if (userId == nil) {
        userId = [NSString stringWithFormat:@"%lf", [[NSDate date] timeIntervalSince1970]];
        [self saveString:userId withKey:KEY_FOR_USER_ID];
    }
    [FIRAnalytics setUserID:userId];
#endif
}

-(void) analytics_eventWithCategory:(NSString*)category action:(NSString*)action label:(NSString*)label value:(NSInteger)value{
#if ENABLE_ANALYTICS
    //こんなとこでプログラムが停止するのは嫌なので、例外処理で囲んでおく
    @try{
        [FIRAnalytics logEventWithName:kFIREventSelectContent parameters:@{
                                                                           @"category":category,
                                                                           @"action":action,
                                                                           @"label":label,
                                                                           @"value":@(value)
                                                                           }];
    }
    @catch (NSException *e) {
        DNSLog(@"GoogleAnalytics exception: (category=%@, action=%@, label=%@, value=%lld. %@", category, action, label, (long long)value, e.description);
    }
#endif
}

-(void) analytics_eventName:(NSString*)eventName param:(NSDictionary*)param{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:eventName parameters:param];
#endif
}


-(void) analytics_errorReport:(NSString*)message{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:@"error_report" parameters:
     @{@"message": message}];
#endif
}

-(void) analytics_tutorialBegin{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventTutorialBegin parameters:nil];
#endif
}

-(void) analytics_tutorialEnded{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventTutorialComplete parameters:nil];
#endif
}

-(void) analytics_appOpen{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventAppOpen parameters:nil];
#endif
}

-(void) analytics_share:(NSString*)contentType itemId:(NSString*)itemId{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventShare parameters:
     @{kFIRParameterContentType: contentType,
       kFIRParameterItemID:itemId}];
#endif
}

-(void) analytics_shareTwitter:(NSString*)itemId{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventShare parameters:
     @{kFIRParameterContentType: @"Twitter",
       kFIRParameterItemID:itemId}];
#endif
}

-(void) analytics_shareFacebook:(NSString*)itemId{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventShare parameters:
     @{kFIRParameterContentType: @"Facebook",
       kFIRParameterItemID:itemId}];
#endif
}

-(void) analytics_ecommercePurchaseBegin:(NSString*)itemName{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventBeginCheckout parameters:
     @{kFIRParameterCoupon:itemName}];
#endif
}

-(void) analytics_ecommercePurchase:(int)price itemName:(NSString*)itemName{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventEcommercePurchase parameters:
     @{kFIRParameterCurrency: @"JPY",
       kFIRParameterValue:@(price),
       kFIRParameterCoupon:itemName}];
#endif
}

-(void) analytics_ecommercePurchase:(NSString*)itemName{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventEcommercePurchase parameters:
     @{kFIRParameterCoupon:itemName}];
#endif
}

-(void) analytics_spendVirtualCurrency:(NSString*)itemName currencyName:(NSString*)currencyName value:(NSUInteger)value{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventSpendVirtualCurrency parameters:
     @{kFIRParameterItemName: itemName,
       kFIRParameterVirtualCurrencyName: currencyName,
       kFIRParameterValue: @(value)}];
#endif
}

-(void) analytics_buyItem:(NSString*)itemName{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventSpendVirtualCurrency parameters:
     @{kFIRParameterItemName: itemName}];
#endif
}

-(void) analytics_buyItem:(NSString*)itemName price:(NSUInteger)price{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventSpendVirtualCurrency parameters:
     @{kFIRParameterItemName: itemName,
       kFIRParameterValue: @(price)}];
#endif
}

-(void) analytics_unlockAchievement:(NSString*) achievementId{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventUnlockAchievement parameters:
     @{kFIRParameterAchievementID: achievementId}];
#endif
}

-(void) analytics_selectContent:(NSString*) contentName{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventSelectContent parameters:
     @{kFIRParameterContentType: contentName}];
#endif
}

-(void) analytics_selectContent:(NSString*) contentName itemId:(NSString*)itemId{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventSelectContent parameters:
     @{kFIRParameterContentType: contentName,
       kFIRParameterItemID: itemId}];
#endif
}

-(void) analytics_review{
#if ENABLE_ANALYTICS
    [FIRAnalytics logEventWithName:kFIREventSelectContent parameters:
     @{kFIRParameterContentType: @"review"}];
#endif
}

@end
