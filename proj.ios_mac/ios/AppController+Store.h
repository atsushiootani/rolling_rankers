//
//  AppController+Store.h
//  
//
//  Created by OtaniAtsushi1 on 2016/02/11.
//
//

#import "AppController.h"

@interface AppController (Store)

#pragma mark - store

-(void) openStore;
-(void) openStore:(NSString*)packageName;
-(BOOL) checkStorePageExists;
-(BOOL) checkStorePageExists:(NSString*)packageName;


#pragma mark - review

-(void) reviewPromptShowWithTitle:(NSString*)title message:(NSString*)message now:(NSString*) now later:(NSString*) later never:(NSString*)never;
-(BOOL) hasReviewDone;
-(BOOL) hasReviewRejected;
-(BOOL) hasNotReviewedYet;


@end
