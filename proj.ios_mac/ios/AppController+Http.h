//
//  AppController+Http.h
//
//
//  Created by OtaniAtsushi1 on 2015/04/30.
//
//

#import "AppController.h"

#define ENABLE_PRONOUNCE (1 && ENABLE_FIREBASE)

#define ENABLE_PRONOUNCE_LAUNCH_ALERT_VIEW (1 && ENABLE_PRONOUNCE)


@interface AppController (Http)

-(void) openWebPage:(NSString*) urlString;

-(BOOL)      initPronounce;
-(BOOL)      hasPronounceLoaded;
-(int)       getIntPronounce:   (NSString*)key defaultValue:(int)      defaultValue;
-(float)     getFloatPronounce: (NSString*)key defaultValue:(float)    defaultValue;
-(double)    getDoublePronounce:(NSString*)key defaultValue:(double)   defaultValue;
-(bool)      getBoolPronounce:  (NSString*)key defaultValue:(bool)     defaultValue;
-(NSString*) getStringPronounce:(NSString*)key defaultValue:(NSString*)defaultValue;

-(int) lot:(NSDictionary*) dic defaultValue:(int) defaultValue;

-(void) launchAlertView:(BOOL) force;

@end

