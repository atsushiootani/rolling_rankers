//
//  AppController+Data.mm
//
//
//  Created by OtaniAtsushi1 on 2015/05/08.
//
//

#import "AppController+Data.h"
#import "AppController+User.h"
#import "iosCommon.h"


@implementation AppController (Data)


#pragma mark - Firebase RealtimeDatabase
//内部的には全てJSONで表現されている

FIRDatabaseReference* database;

//書き込み方式4種
//setValue
//childByAutoId(未実装。レコードごとにIDが発生する)
//updateChildValues(未実装)
//runTransactionBlock


-(void) initDatabase{
    database = [[FIRDatabase database] reference];
}

//パスで指定したレコード取得
-(void) selectRecordAtPath:(StorageType)type filePath:(NSString*)path completion:(void (^)(bool, NSDictionary*))completion{

    NSString* fullPath = [self getDatabseFullPath:type filePath:path];
    FIRDatabaseQuery* query = [[database child:fullPath] queryOrderedByKey]; //主キーの昇順で並べる
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Get user value
        DNSLog(@"%@", [snapshot.value description]);
        
        id result = snapshot.value;
        completion(snapshot != nil, result);
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        if (completion) {
            completion(false, nil);
        }
    }];
}

//パスで指定したレコードを更新or追加
-(void) updateRecordAtPath:(StorageType)type filePath:(NSString*)path updateData:(NSDictionary*) updateData completion:(void(^)(bool, NSDictionary*)) completion{

    NSString* fullPath = [self getDatabseFullPath:type filePath:path];
    DNSLog(@"%@", [updateData description]);
    [[database child:fullPath] runTransactionBlock:^FIRTransactionResult* _Nonnull(FIRMutableData* _Nonnull currentData) {
        NSMutableDictionary *post = currentData.value;
        
        //引数の要素を追加
        if ([post isEqual:[NSNull null]]) {
            post = [updateData mutableCopy];
        }
        else{
            [post addEntriesFromDictionary:updateData];
        }
        
        // Set value and report transaction success
        [currentData setValue:post];
        return [FIRTransactionResult successWithValue:currentData];

    } andCompletionBlock:^(NSError* _Nullable error, BOOL committed, FIRDataSnapshot* _Nullable snapshot) {
        // Transaction completed
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        
        if (completion) {
            completion(error == nil && committed, snapshot.value);
        }
    }];
}

//パスで指定したレコードを丸々入れ替える
-(void) replaceRecordAtPath:(StorageType)type filePath:(NSString*)path replaceData:(NSDictionary*) replaceData completion:(void(^)(bool, NSDictionary*)) completion{
    
    NSString* fullPath = [self getDatabseFullPath:type filePath:path];
    [[database child:fullPath] runTransactionBlock:^FIRTransactionResult* _Nonnull(FIRMutableData* _Nonnull currentData) {
        
        // Set value and report transaction success
        [currentData setValue:replaceData];
        return [FIRTransactionResult successWithValue:currentData];
        
    } andCompletionBlock:^(NSError* _Nullable error, BOOL committed, FIRDataSnapshot* _Nullable snapshot) {
        // Transaction completed
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        
        if (completion) {
            completion(error == nil && committed, snapshot.value);
        }
    }];
}

//パスで指定したレコードを削除
-(void) deleteRecordAtPath:(StorageType)type filePath:(NSString*)path completion:(void(^)(bool, NSDictionary*))completion{
    NSString* fullPath = [self getDatabseFullPath:type filePath:path];
    FIRDatabaseReference* record = [database child:fullPath];
    if (record) {
        [[database child:path] setValue:nil];
        if (completion) {
            completion(true, nil);
        }
    }
    else{
        if (completion) {
            completion(false, nil);
        }
    }
}

-(void) setDatabaseRecord:(StorageType)type filePath:(NSString*)path record:(NSDictionary*)record{
    NSString* fullPath = [self getDatabseFullPath:type filePath:path];
    [[database child:fullPath] setValue:record];
}
-(void) setDatabaseRecord:(StorageType)type filePath:(NSString*)path str:(NSString*)str{
    [[database child:path] setValue:str];
}

-(NSString*) getDatabseFullPath:(StorageType)type filePath:(NSString*)filePath{
    //Storageと同じルール
    return [self getStorageFullPath:type filePath:filePath];
}


#pragma mark - Firebase Storage

FIRStorage *storage;
FIRStorageReference *storageRootRef;

-(void) initDataStorage{
#if ENABLE_FIREBASE_STORAGE
    //初期化
    storage = [FIRStorage storage];
    storageRootRef = [storage referenceForURL:FIREBASE_STORAGE_BUCKET_NAME];
#endif
}

//ストレージからダウンロードしてローカルにセーブし、セーブファイルパスを返す
-(void) downloadStorageDataAndSaveToFile:(StorageType)type filePath:(NSString*) filePath completion:(void (^)(bool, NSString*))completion{
#if ENABLE_FIREBASE_STORAGE
    
    NSString* fullPath = [self getStorageFullPath:type filePath:filePath];
    FIRStorageReference *fileRef = [storageRootRef child:fullPath];
    
    int64_t maxSize = 1 * 1024 * 1024;
    [fileRef dataWithMaxSize:maxSize completion:^(NSData* data, NSError* error){
        if (error != nil) {
            DNSLog(@"Error! storage filename = %@; %@", fullPath, [error description]);
            if (completion) {
                completion(false, nil);
            }
        }
        else{
            NSString* saveFilePath = [self getWriteToFilePath:fullPath];
            BOOL ret = [self writeDataToFile:data path:saveFilePath];
            if (completion) {
                completion(ret, saveFilePath);
            }
        }
    }];
#else
    if (completion) {
        completion(false, "");
    }
#endif
}

//ストレージからダウンロードし、文字列にして返す
-(void) downloadStorageDataAsString:(StorageType)type filePath:(NSString*) filePath completion:(void (^)(bool, NSString*))completion{
    [self downloadStorageData:type filePath:filePath completion:^(bool succeeded, NSData* data){
        NSString* str = @"";
        if (succeeded) {
            str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        if (completion) {
            completion(succeeded, str);
        }
    }];
}

//ストレージからダウンロードし、生データのまま返す
-(void) downloadStorageData:(StorageType)type filePath:(NSString*) filePath completion:(void (^)(bool, NSData*))completion{
#if ENABLE_FIREBASE_STORAGE
    
    NSString* fullPath = [self getStorageFullPath:type filePath:filePath];
    FIRStorageReference *fileRef = [storageRootRef child:fullPath];
    
    int64_t maxSize = 1 * 1024 * 1024;
    [fileRef dataWithMaxSize:maxSize completion:^(NSData* data, NSError* error){
        if (error != nil) {
            DNSLog(@"Error! storage filename = %@; %@", fullPath, [error description]);
            if (completion) {
                completion(false, nil);
            }
        }
        else{
            size_t len = [data length];
            if (len > 0) {
                if (completion) {
                    completion(true, data);
                }
            }
            else{
                DNSLog(@"Error! storage filename = %@; %@", fullPath, @"fetched but no data!");
                if (completion) {
                    completion(false, nil);
                }
            }
        }
    }];
#else
    if (completion) {
        completion(false, nil);
    }
#endif
}

//ストレージに文字列をアップロードする
-(void) uploadStorageDataWithString:(StorageType)type filePath:(NSString*) filePath str:(NSString*)str completion:(void (^)(bool)) completion{
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [self uploadStorageData:type filePath:filePath data:data completion:completion];
}

//ストレージに生データをアップロードする
-(void) uploadStorageData:(StorageType)type filePath:(NSString*) filePath data:(NSData*)data completion:(void (^)(bool)) completion{
    NSString* fullPath = [self getStorageFullPath:type filePath:filePath];
    FIRStorageReference *fileRef = [storageRootRef child:fullPath];
    
    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
    metadata.contentType = @"application/json";
    
    /*FIRStorageUploadTask *uploadTask =*/ [fileRef putData:data metadata:metadata completion:^(FIRStorageMetadata* metadata, NSError* error) {
        if (error != nil) {
            // Uh-oh, an error occurred!
            DNSLog(@"uploadStorageData error. %@", [error description]);
            if (completion) {
                completion(false);
            }
        } else {
            // Metadata contains file metadata such as size, content-type, and download URL.
            //NSURL* downloadURL = metadata.downloadURL;
            if (completion) {
                completion(true);
            }
        }
    }];
}

-(void) deleteStorageData:(StorageType)type filePath:(NSString*) filePath completion:(void (^)(bool)) completion{
    NSString* fullPath = [self getStorageFullPath:type filePath:filePath];
    FIRStorageReference *fileRef = [storageRootRef child:fullPath];

    [fileRef deleteWithCompletion:^(NSError *error){
        if (error != nil) {
            // Uh-oh, an error occurred!
            DNSLog(@"deleteStorageData error. %@", [error description]);
            if (completion) {
                completion(false);
            }
        } else {
            // File deleted successfully
            if (completion) {
                completion(true);
            }
        }
    }];
}

//ストレージ用の完全なパスを生成して返す
-(NSString*) getStorageFullPath:(StorageType)type filePath:(NSString*)filePath{
    NSString* fullPath = @"";
    if (type == StorageTypePublic) {
        fullPath = [fullPath stringByAppendingPathComponent:@"public/"];
    }
    else if (type == StorageTypeCommon) {
        fullPath = [fullPath stringByAppendingPathComponent:@"common/"];
    }
    else if (type == StorageTypeSettings) {
        fullPath = [fullPath stringByAppendingPathComponent:@"settings/"];
    }
    else if (type == StorageTypeReadable) {
        fullPath = [fullPath stringByAppendingPathComponent:@"readable/"];
        fullPath = [fullPath stringByAppendingPathComponent:[self getUserAuthId]];
    }
    else if (type == StorageTypeUser) {
        fullPath = [fullPath stringByAppendingPathComponent:@"user/"];
        fullPath = [fullPath stringByAppendingPathComponent:[self getUserAuthId]];
    }
    else if (type == StorageTypeSpecial) {
        fullPath = [fullPath stringByAppendingPathComponent:@"special/"];
    }
    else if (type == StorageTypeAdmin) {
        fullPath = [fullPath stringByAppendingPathComponent:@"admin/"];
    }
    return [fullPath stringByAppendingPathComponent:filePath];
}

-(NSString*) getWriteToFilePath:(NSString*)path{
    return [NSString stringWithFormat:@"%@/Documents/%@", NSHomeDirectory(), path];
}

//ローカルに保存する
-(BOOL) writeDataToFile:(NSData*)data path:(NSString*)path {
    NSError* error;

    //中間ディレクトリを作成する
    NSString* dir = [path stringByDeletingLastPathComponent];
    BOOL ret = [[NSFileManager defaultManager] createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:&error];
    if (!ret) {
        return NO;
    }

    return [data writeToFile:path atomically:YES];
}

#pragma mark - save/load

-(void) saveString:(NSString*)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:key];
    [userDefaults synchronize];
}

-(NSString*) loadStringWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

-(void) saveIntData:(NSInteger)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:data forKey:key];
    [userDefaults synchronize];
}

-(NSInteger) loadIntDataWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] integerForKey:key];
}

-(void) saveFloatData:(float)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setFloat:data forKey:key];
    [userDefaults synchronize];
}

-(float) loadFloatDataWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] floatForKey:key];
}

-(void) saveDoubleData:(double)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setDouble:data forKey:key];
    [userDefaults synchronize];
}

-(double) loadDoubleDataWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:key];
}

-(void) saveBoolData:(BOOL)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:data forKey:key];
    [userDefaults synchronize];
}

-(BOOL) loadBoolDataWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

-(void) saveURLData:(NSURL*)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setURL:data forKey:key];
    [userDefaults synchronize];
}

-(NSURL*) loadURLDataWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] URLForKey:key];
}

-(void) saveArray:(NSArray*)array withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:array forKey:key];
    [userDefaults synchronize];
}

-(NSArray*) loadArrayWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] arrayForKey:key];
}

-(void) saveStringArray:(NSArray*)stringArray withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:stringArray forKey:key];
    [userDefaults synchronize];
}

-(NSArray*) loadStringArrayWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] stringArrayForKey:key];
}

-(void) saveDictionary:(NSDictionary*)dic withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:dic forKey:key];
    [userDefaults synchronize];
}

-(NSDictionary*) loadDictionaryWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] dictionaryForKey:key];
}

-(void) saveData:(NSData*)data withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:key];
    [userDefaults synchronize];
}

-(NSData*) loadDataWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] dataForKey:key];
}

-(void) saveObject:(NSObject*)object withKey:(NSString*)key{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:object forKey:key];
    [userDefaults synchronize];
}

-(NSObject*) loadObjectWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

@end
