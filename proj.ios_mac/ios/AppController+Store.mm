//
//  AppController+Store.mm
//  
//
//  Created by OtaniAtsushi1 on 2016/02/11.
//
//

#import "AppController+Store.h"
#import "AppController+Data.h"
#import "AppController+Http.h"
#import "RootViewController.h"
#import "AppInfo.h"

enum{
    kStatusReviewNotYet = 0, //まだレビューしていない
    kStatusReviewDone   = 1, //レビュー済み
    kStatusReviewNever  = 2, //レビュー拒否
};

static NSString* SAVE_KEY_FOR_REVIEW_STATUS = @"native_save_key_for_review_status";

@implementation AppController (Store)


#pragma mark - store

-(void) openStore{
    [self openStore:@(APP_ID)];
}

-(void) openStore:(NSString*)packageName{
    NSString *url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", packageName];
    [self openWebPage:url];
}

//ストアページが存在しているか調べる
-(BOOL) checkStorePageExists{
    return [self checkStorePageExists:@(APP_ID)];
}

-(BOOL) checkStorePageExists:(NSString*)packageName{
    //app Store にlookupし、存在してれば配信済み！
    //iVersion.m の checkForNewVersionInBackground を参考
    NSTimeInterval REQUEST_TIMEOUT = 10.0;
    NSString *iTunesServiceURL = [NSString stringWithFormat:@"http://itunes.apple.com/%@/lookup?id=%@", @"JP", packageName];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:iTunesServiceURL]
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:REQUEST_TIMEOUT];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
    if (data && statusCode == 200) {
        //in case error is garbage...
        error = nil;
        
        id json = nil;
        if ([NSJSONSerialization class]) {
            json = [[NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingOptions)0 error:&error][@"results"] lastObject];
        }
        else {
            //convert to string
            json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        
        if (!error) {
            NSString *bundleID = json[@"bundleId"];
            
            // レスポンスjsonにバンドルIDが一致していれば、ストアに置いてあると判定する
            if ([bundleID isEqualToString:[[NSBundle mainBundle] bundleIdentifier]]) {
                //if ([bundleID isEqualToString: @"info.dtgames.Cayenne"]) {
                return true;
            }
        }
    }
    return false;
}


#pragma mark - review

-(void) reviewPromptShowWithTitle:(NSString*)title message:(NSString*)message now:(NSString*) now later:(NSString*) later never:(NSString*)never {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    //never
    [alertController addAction:[UIAlertAction actionWithTitle:never style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self saveIntData:kStatusReviewNever withKey:SAVE_KEY_FOR_REVIEW_STATUS];
    }]];
    
    //now
    [alertController addAction:[UIAlertAction actionWithTitle:now style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self saveIntData:kStatusReviewDone withKey:SAVE_KEY_FOR_REVIEW_STATUS];
        NSString *url = @(APP_STORE_URL);
        [self openWebPage:url];
    }]];
    
    //later
    [alertController addAction:[UIAlertAction actionWithTitle:later style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    
    [self.viewController presentViewController:alertController animated:YES completion:nil];
}

-(BOOL) hasReviewDone{
    NSInteger status = [self loadIntDataWithKey:SAVE_KEY_FOR_REVIEW_STATUS];
    return status == kStatusReviewDone;
}

-(BOOL) hasReviewRejected{
    NSInteger status = [self loadIntDataWithKey:SAVE_KEY_FOR_REVIEW_STATUS];
    return status == kStatusReviewNever;
}

-(BOOL) hasNotReviewedYet{
    NSInteger status = [self loadIntDataWithKey:SAVE_KEY_FOR_REVIEW_STATUS];
    return status == kStatusReviewNotYet;
}

@end
