/****************************************************************************
 Copyright (c) 2013      cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "RootViewController.h"
#import "cocos2d.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "MBProgressHUD.h"
#import "AppController+Ad.h"
#import "AppController+Cocos2d.h"
#import "Screen.h"

@interface RootViewController()<UIWebViewDelegate>

/**
 * alertView関係
 */
@property (nonatomic, strong) UIAlertView* alertViewFromNative;
@property (nonatomic, assign) BOOL shouldAlertViewCallback;
@property (nonatomic, assign) NSInteger alertViewSelectedIndex;

/**
 * indicator関係
 */
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

/**
 * keyboard関係
 */
@property (nonatomic, strong) UIAlertView* alertViewForKeyboard;
@property (nonatomic, assign) BOOL shouldAlertViewForKeyboardCallback;
@property (nonatomic, assign) NSInteger alertViewForKeyboardSelectedIndex;
@property (nonatomic, strong) NSString* keyboardText;

/**
 * WebView関係
 */
@property (nonatomic, strong) UIWebView *webView;

@end


@implementation RootViewController


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}

*/
// Override to allow orientations other than the default portrait orientation.
// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}

// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations{
#ifdef __IPHONE_6_0
    return UIInterfaceOrientationMaskAllButUpsideDown;
#endif
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    auto glview = cocos2d::Director::getInstance()->getOpenGLView();

    if (glview)
    {
        CCEAGLView *eaglview = (__bridge CCEAGLView*) glview->getEAGLView();

        if (eaglview)
        {
            CGSize s = CGSizeMake([eaglview getWidth], [eaglview getHeight]);
            cocos2d::Application::getInstance()->applicationScreenSizeChanged((int) s.width, (int) s.height);
        }
    }
}

//fix not hide status on ios7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
}


#pragma mark - life cycle

- (void)viewDidAppear:(BOOL)animated{
    [(AppController *)[UIApplication sharedApplication].delegate viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [(AppController *)[UIApplication sharedApplication].delegate viewWillDisappear:animated];
}


#pragma mark - alert view

//アラートビュー作成（選択肢1つ）
-(void) makeAlertViewWithTitle:(NSString*)title message:(NSString*)message okTitle:(NSString*)okTitle{
    self.shouldAlertViewCallback = NO;
    self.alertViewFromNative = [[UIAlertView alloc]initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:okTitle
                                               otherButtonTitles:nil];
    [self.alertViewFromNative show];
}

//アラートビュー作成（選択肢2つ）
-(void) makeAlertViewWithTitle:(NSString*)title message:(NSString*)message noTitle:(NSString*)noTitle yesTitle:(NSString*)yesTitle{
    self.shouldAlertViewCallback = NO;
    self.alertViewFromNative = [[UIAlertView alloc]initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:noTitle
                                               otherButtonTitles:yesTitle, nil];
    [self.alertViewFromNative show];
}

//アラートビュー作成（選択肢3つ）
-(void) makeAlertViewWithTitle:(NSString*)title message:(NSString*)message cancelTitle:(NSString*)cancelTitle option1Title:(NSString*)option1Title option2Title:(NSString*)option2Title{
    self.shouldAlertViewCallback = NO;
    self.alertViewFromNative = [[UIAlertView alloc]initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:cancelTitle
                                               otherButtonTitles:option1Title, option2Title, nil];
    [self.alertViewFromNative show];
}

//アラートビュー作成（選択肢4つ）
-(void) makeAlertViewWithTitle:(NSString*)title message:(NSString*)message cancelTitle:(NSString*)cancelTitle option1Title:(NSString*)option1Title option2Title:(NSString*)option2Title option3Title:(NSString*)option3Title{
    self.shouldAlertViewCallback = NO;
    self.alertViewFromNative = [[UIAlertView alloc]initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:cancelTitle
                                               otherButtonTitles:option1Title, option2Title, option3Title, nil];
    [self.alertViewFromNative show];
}

//アラートビューのボタンが押された時のdelegateメソッド
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == self.alertViewFromNative) {
        self.alertViewSelectedIndex = buttonIndex;
        self.shouldAlertViewCallback = YES; //コールバックを呼び出すべき、というフラグだけ建てる（呼び出しはここでは行わない）
    }
    //キーボードの場合
    else if (alertView == self.alertViewForKeyboard) {
        [self alertViewForKeyboardClickedButttonAtIndex:buttonIndex];
    }
}

//アラートビューのボタンが押されたかどうかチェック
//  押されたボタンのインデックスを返す。押されていなければ-1
-(int) checkAndResetAlertViewCallback{
    if (self.shouldAlertViewCallback) {
        self.shouldAlertViewCallback = NO;
        self.alertViewFromNative = nil;
        return (int)self.alertViewSelectedIndex;
    }
    else{
        return -1;
    }
}


#pragma mark - indicator

//インジケータの表示
-(void) showIndicator:(NSString*)message{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    [hud setLabelText:message];
}

//インジケータの非表示
-(void) hideIndicator{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


#pragma mark - keyboard

//キーボードを表示
-(void) showKeyboardWith:(NSString*)message text:(NSString*) text{
    self.alertViewForKeyboard = [[UIAlertView alloc]initWithTitle:@""
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"キャンセル"
                                               otherButtonTitles:@"OK", nil];
    self.alertViewForKeyboard.alertViewStyle = UIAlertViewStylePlainTextInput;
    [self.alertViewForKeyboard textFieldAtIndex:0].text = text;
    [self.alertViewForKeyboard textFieldAtIndex:0].clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.alertViewForKeyboard show];
}

//キーボードを表示
-(void) showKeyboardNumPadWith:(NSString*)message text:(NSString*) text{
    self.alertViewForKeyboard = [[UIAlertView alloc]initWithTitle:@""
                                                          message:message
                                                         delegate:self
                                                cancelButtonTitle:@"キャンセル"
                                                otherButtonTitles:@"OK", nil];
    self.alertViewForKeyboard.alertViewStyle = UIAlertViewStylePlainTextInput;
    [self.alertViewForKeyboard textFieldAtIndex:0].text = text;
    [self.alertViewForKeyboard textFieldAtIndex:0].clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.alertViewForKeyboard textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
    
    [self.alertViewForKeyboard show];
}

-(void) alertViewForKeyboardClickedButttonAtIndex:(NSInteger)buttonIndex {
    self.alertViewForKeyboardSelectedIndex = buttonIndex;
    self.shouldAlertViewForKeyboardCallback = YES; //コールバックを呼び出すべき、というフラグだけ建てる（呼び出しはここでは行わない）
    self.keyboardText = [self.alertViewForKeyboard textFieldAtIndex:0].text;
}

-(int) checkAndResetKeyboardCallback{
    if (self.shouldAlertViewForKeyboardCallback) {
        self.shouldAlertViewForKeyboardCallback = NO;
        self.alertViewForKeyboard = nil;
        return (int)self.alertViewForKeyboardSelectedIndex;
    }
    else{
        return -1;
    }
}

-(const char*) getKeyboardInputText{
    return [self.keyboardText UTF8String];
}


#pragma mark - sleep mode

-(void) sleepableMode{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

-(void) keepScreenMode{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}


#pragma mark - web

-(CGRect)getWebViewRect{
    CGRect screenRect = [UIScreen mainScreen].bounds;
    
    //上部ヘッダー、下部フッターと広告を避けて表示
    //CGRect rect = CGRectMake((screenRect.size.width - adSize.width) / 2, screenRect.size.height - adSize.height - [self getFooterHeight] / 2, adSize.width, adSize.height);
    
    CGRect rect = CGRectMake(0,
                             HEADER_HEIGHT / 2,
                             screenRect.size.width,
                             screenRect.size.height - HEADER_HEIGHT / 2 - FOOTER_HEIGHT / 2 - AD_BANNER_HEIGHT / 2);
    return rect;
}

-(BOOL)hasWebView{
    return self.webView != nil;
}

-(void)preloadWebView:(NSURL *)url{
    self.webView = [[UIWebView alloc] init];
    //wv.delegate = self;
    self.webView.frame = [self getWebViewRect];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:req];
    
    //preloadなので隠す
    self.webView.hidden = YES;
}

-(void)showWebView{
    if (self.webView != nil) {
        self.webView.hidden = NO;
    }
}

-(void)hideWebView{
    if (self.webView != nil) {
        self.webView.hidden = YES;
    }
}

-(void)openWebView:(NSURL *)url{
    self.webView = [[UIWebView alloc] init];
    //wv.delegate = self;
    self.webView.frame = [self getWebViewRect];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:req];
}

-(void)closeWebView{
    if (self.webView != nil) {
        [self.webView removeFromSuperview];
        self.webView = nil;
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // クリックしたリンクのURLを取得
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        //NSString* urlString = [[[request URL] standardizedURL] absoluteString];
        BOOL f = YES;
        if (f/*[urlString hasPrefix:@""]*/) {
            return YES;
        }
        else{
            [self closeWebView];
            return NO;
        }
    }
    return YES;
}

@end
