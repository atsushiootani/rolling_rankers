#import <UIKit/UIKit.h>


#define ENABLE_FIREBASE (1)
//$ pod install


#if ENABLE_FIREBASE
#import <Firebase.h>
#endif


@class RootViewController;


//コールバック関数型
typedef void (^LoadedCallback)(bool succeeded);
typedef void (^PurchaseCallback)(const char*);
typedef void (^PurchaseClosedCallback)();
typedef void (^RestoreCompletedCallback)();


/**
 * クラス
 */
@interface AppController : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}



#pragma mark - common

-(float) getOSVersion;
-(BOOL) isOSVersionEqualOrLater:(float)version;


#pragma mark - main

@property(nonatomic, readonly) RootViewController* viewController;



#pragma mark - Purchase

//商品情報一覧
@property (nonatomic, strong) NSArray *allProducts;
@property (nonatomic, assign) BOOL requestHasFinished;
@property (nonatomic, assign) BOOL initSucceeded;

//情報取得完了コールバック
@property (nonatomic, strong) LoadedCallback loadedCallback;

//購入コールバック
@property (nonatomic, strong) PurchaseCallback purchaseCallback;
@property (nonatomic, strong) PurchaseCallback failureCallback;
@property (nonatomic, strong) PurchaseCallback cancelCallback;
@property (nonatomic, strong) PurchaseClosedCallback closedCallback;

//リストアコールバック
@property (nonatomic, strong) PurchaseCallback restoreCallback; //1商品のリストアごとに呼び出される(引数にproductionIdを取る)
@property (nonatomic, strong) RestoreCompletedCallback restoreCompletedCallback; //全商品のリストアが完了したときに呼び出される(productionIdは引数に取らない)
@property (nonatomic, strong) RestoreCompletedCallback restoreFailureCallback;  //リストアに失敗したときに呼び出される(productionIdは引数に取らない)
@property (nonatomic, strong) RestoreCompletedCallback restoreClosedCallback;  //リストアが終了した時に呼び出される(Complete,Failureの後にも呼び出される。また何らかの理由で中断された時にも呼び出される想定)
//リストアにはキャンセルがない


#pragma mark - Http

#if ENABLE_FIREBASE
//Firebase RemoteConfig
@property (nonatomic, strong) FIRRemoteConfig* remoteConfig;
#endif

@end


