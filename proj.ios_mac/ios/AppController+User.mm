//
//  AppController+User.mm
//  
//
//  Created by OtaniAtsushi1 on 2016/10/17.
//
//

#import "AppController+User.h"
#import "iosCommon.h"


@implementation AppController (User)

FIRUser*  user;
std::function<void(std::string uid)> signInCallback;
std::function<void()> signOutCallback;

-(void) initUserAuth{
    user = [FIRAuth auth].currentUser; //ログイン済みの場合、アプリを再起動してもここでログイン状態になる。その際も、signInCallbackは呼び出される
    if (user == nil) {
        [self userAuthentication];
    }
    
    [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth *_Nonnull auth,
                                                    FIRUser *_Nullable u) {
        if (u != nil) {
            // User is signed in.
            if (signInCallback) {
                signInCallback([u.uid UTF8String]);
            }
        } else {
            // No user is signed in.
            if (signOutCallback){
                signOutCallback();
            }
        }
    }];
}

-(void) userAuthentication{
#if ENABLE_USER_AUTHENTICATION
    [[FIRAuth auth] signInAnonymouslyWithCompletion:^(FIRUser *_Nullable u, NSError *_Nullable error) {
        if (error != nil) {
            DNSLog(@"user authentication error: %@", [error description]);
        }
        else{
            user = u;
            DNSLog(@"usr authentication succeed: %@", u.uid);
        }
    }];
#else
#endif
}

-(BOOL) isUserAuthenticated{
    return user != nil;
}

-(NSString*) getUserAuthId{
    return (user != nil) ? user.uid : @""; //nilより空文字列の方が、C++側で取り回しやすい(そのままstd::stringインスタンスに変換できるなど)
}

-(NSString*) getUserAuthName{
    return (user != nil) ? user.displayName : @"";
}

-(NSString*) getUserAuthEmail{
    return (user != nil && user.emailVerified) ? user.email : @"";
}

-(void) setUserAuthListenerSignIn:(std::function<void(std::string uid)>) inCallback signOut:(std::function<void()>) outCallback{
    signInCallback  = inCallback;
    signOutCallback = outCallback;
}

@end
