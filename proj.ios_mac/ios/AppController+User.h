//
//  AppController+User.h
//
//
//  Created by OtaniAtsushi1 on 2016/10/17.
//
//

#import "AppController.h"


#define ENABLE_USER_AUTHENTICATION (1 && ENABLE_FIREBASE)
//pod 'Firebase/Auth'


@interface AppController (User)

-(void) initUserAuth;
-(void) userAuthentication;
-(BOOL) isUserAuthenticated;
-(NSString*) getUserAuthId;
-(NSString*) getUserAuthName;
-(NSString*) getUserAuthEmail;
-(void) setUserAuthListenerSignIn:(std::function<void(std::string uid)>) inCallback signOut:(std::function<void()>) outCallback;

@end
