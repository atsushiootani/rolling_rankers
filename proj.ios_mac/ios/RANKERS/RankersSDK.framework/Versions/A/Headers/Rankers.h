//
//  Rankers.h
//  RankersSDK
//
//  Created by KamedaKyosuke on 2015/12/14.
//  Copyright © 2015年 com.kayac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(int8_t, RankersAPIResponseType){
    RankersAPIResponseTypeClientError = 0,        // ネットワークに繋がっていない場合やサーバからのレスポンスがタイムアウトした場合
                                                  // NSError のインスタンスにエラーの情報が格納されている
                                                  // 環境を変更して、再度実行すると解決する場合がある

    RankersAPIResponseTypeInternalServerError,    // RANKERS のサーバで何か障害が起きている場合
                                                  // 再度実行すると解決する場合がある

    RankersAPIResponseTypeMaintenance,            // RANKERS がメンテナンス中の場合
                                                  // NSDictionary のインスタンスにメンテナンス情報が格納されている
                                                  // { "error" : { "code"         : ...,     NSNumber : エラーコード
                                                  //               "message"      : ...,     NSString : ユーザ向けメッセージ
                                                  //               "debug_message : ...}}    NSString : 開発者向けメッセージ。含まれないこともある

    RankersAPIResponseTypeErrorResponse,          // パラメータ誤り等でエラーが発生した場合
                                                  // 同じパラメータで再度実行した場合は失敗する
                                                  // NSDictionary のインスタンスにエラー情報が格納されている
                                                  // { "error" : { "code"         : ...,     NSNumber : エラーコード
                                                  //               "message"      : ...,     NSString : ユーザ向けメッセージ
                                                  //               "debug_message : ...}}    NSString : 開発者向けメッセージ。含まれないこともある

    RankersAPIResponseTypeSucessResponse          // API 呼び出しに成功した場合
};

/**
 * RankersAPIHandler がエラー時に格納される json の `{ @"error" : { @"code" : ..., }}` の特別な値
 */
typedef NS_ENUM(int32_t, RankersAPIResponseErrorCode){
    /**
     * 録画が必要な大会で `postScore:...` を呼び出した際にすでに録画が停止されていた際に使用されるエラーコードです。
     * RankersAPIHandler の RankersAPIResponseType は RankersAPIResponseTypeErrorResponse となります。
     */
    RankersAPIResponseErrorCodeCaptureAlreadyStopped = 10001,
};

/**
 * 非同期で結果を返す際に使われるコールバックの型です
 */
typedef void(^RankersAPIHandler)(RankersAPIResponseType responseType, NSDictionary * __nullable json, NSError * __nullable error);

@protocol RankersDelegate < NSObject >
@required
/**
 * ユーザが大会情報からチャレンジを行った際に呼び出されます
 * @param delegate Rankers クラスの delegate に設定したインスタンス
 * @param competition 大会情報
 */
- (void)rankersDelegate:(nonnull id<RankersDelegate>)delegate startGameWithCompetition:(nonnull NSDictionary*)competition;

@optional
/**
 * 大会情報の画面を開いた際に呼び出されます
 * @param delegate Rankers クラスの delegate に設定したインスタンス
 */
- (void)didCompetitionAppearWithRankersDelegate:(nonnull id<RankersDelegate>)delegate;

/**
 * 大会情報の画面を閉じた際に呼び出されます
 * @param delegate Rankers クラスの delegate に設定したインスタンス
 */
- (void)didCompetitionDisappearWithRankersDelegate:(nonnull id<RankersDelegate>)delegate;

/**
 * 動画の再生開始の際に呼び出されます
 * 動画の音声と重複しないように、アプリの音声をフェードアウトしてください
 * @param delegate Rankers クラスの delegate に設定したインスタンス
 */
- (void)didStartMovieWithRankersDelegate:(nonnull id<RankersDelegate>)delegate;

/**
 * 動画の再生終了の際に呼び出されます
 * `didStartMovieWithRankersDelegate:...` でフェードアウトしたアプリの音声を戻してください
 * @param delegate Rankers クラスの delegate に設定したインスタンス
 */
- (void)didStopMovieWithRankersDelegate:(nonnull id<RankersDelegate>)delegate;

@end

@interface Rankers : NSObject

/**
 * RankersSDK からイベントを受け取る delegate を設定します
 */
@property(nullable, nonatomic, weak, getter=delegate, setter=setDelegate:) id<RankersDelegate> delegate;

/**
 * Rankers の画面を表示する ViewController を設定します。
 */
@property(nullable, nonatomic, weak, getter=rootViewController, setter=setRootViewController:) UIViewController *rootViewController;

@property(nullable, nonatomic, assign, getter=rootViewControllerFunc, setter=setRootViewControllerFunc:) UIViewController * _Nullable(*rootViewControllerFunc)();

+ (nonnull instancetype)sharedInstance;

/**
 * RankersSDK の初期化を行います
 * @param SDKID ドキュメントに記載されている値を入力してください
 * @param sdkUrlScheme ドキュメントに記載されている値を入力してください
 */
- (void)setupWithSDKID:(nonnull NSString*)SDKID sdkUrlScheme:(nonnull NSString*)sdkUrlScheme;

/**
 * RankersSDK の初期化を行います
 * @param SDKID ドキュメントに記載されている値を入力してください
 * @param sdkUrlScheme ドキュメントに記載されている値を入力してください
 * @param appVersion `大会情報を編集`ページで設定することによりバージョンを満たすかどうかをチェックすることができます。
 *                   バージョンチェックの結果は `updateCompetitionWithHandler:` のコールバックに格納されています
 */
- (void)setupWithSDKID:(nonnull NSString*)SDKID sdkUrlScheme:(nonnull NSString*)sdkUrlScheme appVersion:(uint32_t)appVersion;

/**
 * 大会情報を取得します。
 * @param handler 結果を取得するための非同期のコールバック
 */
- (void)updateCompetitionWithHandler:(nonnull RankersAPIHandler)handler;

/**
 * プレイヤーの初期の名前を設定します。
 * @param playerName 初期の名前。すでにプレイヤーが存在する場合は使用されません。
 */
- (void)setPlayerName:(nonnull NSString*)playerName;

/**
 * 大会情報を表示します。
 */
- (void)showCompetition;

/**
 * 大会結果タブを選択した状態で大会情報を表示します
 */
- (void)showCompetitionResult;

/**
 * スコアの投稿を行います。
 * @param score 得点
 * @param scoreString ユーザに表示される値
 * @param handler 結果を取得するための非同期のコールバック
 */
- (void)postScore:(int)score scoreString:(nonnull NSString *)scoreString handler:(nonnull RankersAPIHandler)handler;

/**
 * スコアの投稿を行います。
 * @param score 得点
 * @param scoreString ユーザに表示される値
 * @param payload 付加情報。 `updateCompetitionWithHandler:` や `RankersDelegate rankersDelegate:startGameWithCompetition:` で取得することができます
 * @param handler 結果を取得するための非同期のコールバック
 */
- (void)postScore:(int)score scoreString:(nonnull NSString *)scoreString payload:(nonnull NSString*)payload handler:(nonnull RankersAPIHandler)handler;

/**
 * 今回のプレイでスコアの投稿を行わないこと状態になった際に呼び出してください。
 * @warning 録画が必要な大会の場合、この処理を呼び出すことで録画の停止が行われます。
 */
- (void)disposeScore;

/**
 * 動画連携した際に現在録画中かどうかを調べる
 */
- (BOOL)isCapturing;

/**
 * 動画連携した際に録画可能かどうかを調べる
 */
- (BOOL)isRecSupported;

#if 7 <= __clang_major__
- (BOOL)openURL:(nonnull NSURL *)url sourceApplication:(nonnull NSString *)sourceApplication annotation:(nonnull id)annotation;
- (BOOL)openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options;
#else
- (BOOL)openURL:(nonnull NSURL *)url sourceApplication:(nonnull NSString *)sourceApplication annotation:(nonnull id)annotation;
- (BOOL)openURL:(nonnull NSURL *)url options:(nonnull NSDictionary*)options;
#endif
@end
