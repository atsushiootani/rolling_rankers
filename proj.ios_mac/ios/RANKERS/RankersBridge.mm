#include "RankersBridge.h"

#import <RankersSDK/RankersSDK.h>

@interface RankersDelegateWrapper : NSObject < RankersDelegate >
@property(nonatomic, assign) RankersBridge::Delegate *delegate;
+ (instancetype)sharedInstance;
@end

@implementation RankersDelegateWrapper

+ (instancetype)sharedInstance
{
    static RankersDelegateWrapper *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}

- (void)rankersDelegate:(id<RankersDelegate>)delegate startGameWithCompetition:(NSDictionary *)competition
{
    if(self.delegate != NULL){
        const char *res = NULL;
        if(competition != nil){
            NSData *d = [NSJSONSerialization dataWithJSONObject:competition
                                                        options:0
                                                          error:nil];
            NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
            res = [s cStringUsingEncoding:NSUTF8StringEncoding];
        }
        self.delegate->startGame(res);
    }
}

- (void)didCompetitionAppearWithRankersDelegate:(id<RankersDelegate>)delegate
{
    if(self.delegate != NULL){
        self.delegate->didCompetitionAppear();
    }
}

- (void)didCompetitionDisappearWithRankersDelegate:(id<RankersDelegate>)delegate
{
    if(self.delegate != NULL){
        self.delegate->didCompetitionDisappear();
    }
}

- (void)didStartMovieWithRankersDelegate:(nonnull id<RankersDelegate>)delegate
{
    if(self.delegate != NULL){
        self.delegate->didStartMovie();
    }
}

- (void)didStopMovieWithRankersDelegate:(nonnull id<RankersDelegate>)delegate
{
    if(self.delegate != NULL){
        self.delegate->didStopMovie();
    }
}

@end

@interface RankersAPIHandlerWrapper : NSObject
@property(nonatomic, assign) RankersBridge::APIHandler *handler;
@end

@implementation RankersAPIHandlerWrapper
@end

void RankersBridge::setup(const char* sdkid, const char* sdkurl)
{
    NSString *s   = [NSString stringWithCString:sdkid encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithCString:sdkurl encoding:NSUTF8StringEncoding];
    [[Rankers sharedInstance] setupWithSDKID:s sdkUrlScheme:url];
}

void RankersBridge::setup(const char* sdkid, const char* sdkurl, uint32_t appVersion)
{
    NSString *s   = [NSString stringWithCString:sdkid encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithCString:sdkurl encoding:NSUTF8StringEncoding];
    [[Rankers sharedInstance] setupWithSDKID:s sdkUrlScheme:url appVersion:appVersion];
}

void RankersBridge::set_delegate(RankersBridge::Delegate *delegate)
{
    [RankersDelegateWrapper sharedInstance].delegate = delegate;
    [Rankers sharedInstance].delegate = [RankersDelegateWrapper sharedInstance];
}

void RankersBridge::update_competition_with_handler(APIHandler *handler)
{
    RankersAPIHandlerWrapper *apiHandler = [[RankersAPIHandlerWrapper alloc] init];
    apiHandler.handler = handler;
    [[Rankers sharedInstance] updateCompetitionWithHandler:^(RankersAPIResponseType responseType, NSDictionary * _Nullable json, NSError * _Nullable error) {
        const char *res = NULL;
        if(json != nil){
            NSData *d = [NSJSONSerialization dataWithJSONObject:json
                                                        options:0
                                                          error:nil];
            NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
            res = [s cStringUsingEncoding:NSUTF8StringEncoding];
        }
        
        const char *error_message = NULL;
        if(error != nil){
            error_message = [error.description cStringUsingEncoding:NSUTF8StringEncoding];
        }
        apiHandler.handler->handler((RankersBridge::APIResponseType)responseType, res, error_message);
        [apiHandler release];
    }];
}

void RankersBridge::set_player_name(const char* player_name)
{
    NSString *s   = [NSString stringWithCString:player_name encoding:NSUTF8StringEncoding];
    [[Rankers sharedInstance] setPlayerName:s];
}

void RankersBridge::show_competition()
{
    [[Rankers sharedInstance] showCompetition];
}

void RankersBridge::show_competition_result()
{
    [[Rankers sharedInstance] showCompetitionResult];
}

void RankersBridge::post_score(int score, const char* score_src, APIHandler *handler)
{
    RankersAPIHandlerWrapper *apiHandler = [[RankersAPIHandlerWrapper alloc] init];
    apiHandler.handler = handler;
    NSString *s = [NSString stringWithCString:score_src encoding:NSUTF8StringEncoding];
    [[Rankers sharedInstance] postScore:score
                            scoreString:s
                                handler:^(RankersAPIResponseType responseType, NSDictionary * _Nullable json, NSError * _Nullable error)
    {
        const char *res = NULL;
        if(json != nil){
            NSData *d = [NSJSONSerialization dataWithJSONObject:json
                                                        options:0
                                                          error:nil];
            NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
            res = [s cStringUsingEncoding:NSUTF8StringEncoding];
        }
        
        const char *error_message = NULL;
        if(error != nil){
            error_message = [error.description cStringUsingEncoding:NSUTF8StringEncoding];
        }
        apiHandler.handler->handler((RankersBridge::APIResponseType)responseType, res, error_message);
        [apiHandler release];
    }];
}

void RankersBridge::post_score(int score, const char* score_src, const char* payload, APIHandler *handler)
{
    RankersAPIHandlerWrapper *apiHandler = [[RankersAPIHandlerWrapper alloc] init];
    apiHandler.handler = handler;
    NSString *s = [NSString stringWithCString:score_src encoding:NSUTF8StringEncoding];
    NSString *p = [NSString stringWithCString:payload encoding:NSUTF8StringEncoding];
    [[Rankers sharedInstance] postScore:score
                            scoreString:s
                                payload:p
                                handler:^(RankersAPIResponseType responseType, NSDictionary * _Nullable json, NSError * _Nullable error)
     {
         const char *res = NULL;
         if(json != nil){
             NSData *d = [NSJSONSerialization dataWithJSONObject:json
                                                         options:0
                                                           error:nil];
             NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
             res = [s cStringUsingEncoding:NSUTF8StringEncoding];
         }
         
         const char *error_message = NULL;
         if(error != nil){
             error_message = [error.description cStringUsingEncoding:NSUTF8StringEncoding];
         }
         apiHandler.handler->handler((RankersBridge::APIResponseType)responseType, res, error_message);
         [apiHandler release];
     }];
}

void RankersBridge::dispose_score()
{
    [[Rankers sharedInstance] disposeScore];
}

bool RankersBridge::is_capturing()
{
    return [[Rankers sharedInstance] isCapturing];
}

bool RankersBridge::is_rec_supported()
{
    return [[Rankers sharedInstance] isRecSupported];
}
