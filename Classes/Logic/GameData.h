//
//  GameData.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/05.
//
//

#ifndef GameData_h
#define GameData_h

#include "Consts.h"


#pragma mark - Achievement

struct AchievementData{
public:
    enum Type{
        TYPE_FIRST,     //初めての◯◯系
        TYPE_LENGTH,    //距離系
        TYPE_GIFT,      //ギフト獲得
        TYPE_GIFT_COMP, //ギフトコンプ率
        TYPE_COIN,      //コイン
        TYPE_AD,        //広告見た回数
        TYPE_LOGIN,     //ログイン日数
    };
    
public:
    int index;
    Type type;
    int  num;
    int  rewardLevel;
    
public:
    AchievementData() : index(-1), type(TYPE_FIRST), num(0), rewardLevel(0) {}
    AchievementData(int i, Type t, int n, int r) : index(i), type(t), num(n), rewardLevel(r) {}
    
    int getRewardCoinNum() const;
};


extern const AchievementData s_achievementData[ACHIEVEMENT_COUNT];



#pragma mark - LoginBonus

struct LoginBonusData{
public:
    enum Type{
        
    };
    
public:
    int index;
    int type;
public:
    LoginBonusData() : index(-1), type(0) {}
    LoginBonusData(int i, int t) : index(i), type(t) {}
    
    int getCoinNum() const;
};

extern const LoginBonusData s_loginBonusData[LOGIN_BONUS_COUNT];


#endif /* GameData_h */
