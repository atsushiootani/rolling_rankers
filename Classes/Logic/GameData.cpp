//
//  GameData.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/05.
//
//

#include "GameData.h"


const AchievementData s_achievementData[ACHIEVEMENT_COUNT] = {
    AchievementData(0, AchievementData::TYPE_FIRST, 1, 3),

    AchievementData(1, AchievementData::TYPE_LENGTH,  150, 1),
    AchievementData(2, AchievementData::TYPE_LENGTH, 1000, 2),
    AchievementData(3, AchievementData::TYPE_LENGTH, 10000, 2),
    AchievementData(4, AchievementData::TYPE_LENGTH, 34500, 3),
    AchievementData(5, AchievementData::TYPE_LENGTH, 42195, 4),
    AchievementData(6, AchievementData::TYPE_LENGTH, 200000, 5),
    AchievementData(7, AchievementData::TYPE_LENGTH, 552600, 6),
    AchievementData(8, AchievementData::TYPE_LENGTH, 1500000, 8),
    AchievementData(9, AchievementData::TYPE_LENGTH, 3300000, 10),
    AchievementData(10, AchievementData::TYPE_LENGTH, 7000000, 13),
    AchievementData(11, AchievementData::TYPE_LENGTH, 1248000, 20),

    AchievementData(12, AchievementData::TYPE_GIFT_COMP, 2, 1),
    AchievementData(13, AchievementData::TYPE_GIFT_COMP, 10, 3),
    AchievementData(14, AchievementData::TYPE_GIFT_COMP, 25, 5),
    AchievementData(15, AchievementData::TYPE_GIFT_COMP, 50, 10),
    AchievementData(16, AchievementData::TYPE_GIFT_COMP, 75, 15),
    AchievementData(17, AchievementData::TYPE_GIFT_COMP, 100, 20),

    AchievementData(18, AchievementData::TYPE_COIN, 30000, 2),
    AchievementData(19, AchievementData::TYPE_COIN, 150000, 3),
    AchievementData(20, AchievementData::TYPE_COIN, 500000, 4),
    AchievementData(21, AchievementData::TYPE_COIN, 1500000, 5),
    AchievementData(22, AchievementData::TYPE_COIN, 5000000, 6),

    AchievementData(23, AchievementData::TYPE_GIFT, 44, 1),

    AchievementData(24, AchievementData::TYPE_AD,   1,  1),
    AchievementData(25, AchievementData::TYPE_AD,   3,  3),
    AchievementData(26, AchievementData::TYPE_AD,   5,  5),
    AchievementData(27, AchievementData::TYPE_AD,  10, 10),
    AchievementData(28, AchievementData::TYPE_AD,  30, 15),
    AchievementData(29, AchievementData::TYPE_AD, 100, 20),

    AchievementData(30, AchievementData::TYPE_LOGIN,  7,  3),
    AchievementData(31, AchievementData::TYPE_LOGIN, 30, 10),
    AchievementData(32, AchievementData::TYPE_LOGIN, 90, 30),
};

int AchievementData::getRewardCoinNum() const{
    return rewardLevel * DEFAULT_NEED_COIN_TO_GACHA;
}

const LoginBonusData s_loginBonusData[LOGIN_BONUS_COUNT] = {
    LoginBonusData( 0, 1),
    LoginBonusData( 1, 2),
    LoginBonusData( 2, 1),
    LoginBonusData( 3, 2),
    LoginBonusData( 4, 1),
    LoginBonusData( 5, 2),
    LoginBonusData( 6, 3),
    LoginBonusData( 7, 1),
    LoginBonusData( 8, 2),
    LoginBonusData( 9, 1),
    LoginBonusData(10, 2),
    LoginBonusData(11, 1),
    LoginBonusData(12, 2),
    LoginBonusData(13, 3),
    LoginBonusData(14, 1),
    LoginBonusData(15, 2),
    LoginBonusData(16, 1),
    LoginBonusData(17, 2),
    LoginBonusData(18, 1),
    LoginBonusData(19, 2),
    LoginBonusData(20, 3),
    LoginBonusData(21, 1),
    LoginBonusData(22, 2),
    LoginBonusData(23, 1),
    LoginBonusData(24, 2),
    LoginBonusData(25, 1),
    LoginBonusData(26, 2),
    LoginBonusData(27, 3),
    LoginBonusData(28, 1),
    LoginBonusData(29, 4),
};

int LoginBonusData::getCoinNum() const{
    if      (type == 2) { return  6000; }
    else if (type == 3) { return 12000; }
    else if (type == 4) { return 36000; }
    else                { return  3000; }
}
