//
//  StageData.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/11.
//
//

#ifndef StageData_h
#define StageData_h

#include "cocos2d.h"
USING_NS_CC;

enum StageDifficulty{
    STAGE_DIFFICULTY_EASY,
    STAGE_DIFFICULTY_NORMAL,
    STAGE_DIFFICULTY_HARD,
};

//特別なデータ番号
#define STAGE_DATA_INDEX_DEMO          (0)
#define STAGE_DATA_INDEX_COIN_RUSH_A (101)

//特別な地形数値
#define STAGE_DATA_NARAKU_HEIGHT (-99)


extern int getStageDataCount();

//指定したインデックスのステージデータを取得する
extern bool getStageDataWithIndex(int index, StageDifficulty difficulty, float** groundHeights, int *groundNum, Vec2** coinPos, int *coinNum, Vec2** rockPos, int *rockNum);

extern bool getStageDataPlane(float** groundHeights, int *groundNum, Vec2** coinPos, int *coinNum, Vec2** rockPos, int *rockNum);


#endif /* StageData_h */
