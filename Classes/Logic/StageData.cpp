//
//  StageData.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/11.
//
//

#include "StageData.h"
#include "GameHelper.h"


int getStageDataCount(){
    return 100;
}


bool getStageDataWithIndex(int index, StageDifficulty difficulty, float** groundHeights, int* groundNum, Vec2** coinPos, int* coinNum, Vec2** rockPos, int* rockNum){
#define GROUND_NUM_MAX (50)
#define COIN_NUM_MAX   (40)
#define ROCK_NUM_MAX   (20)
    static float GROUND_HEIGHTS[GROUND_NUM_MAX] = {};
    static Vec2  COIN_POS[COIN_NUM_MAX] = {};
    static Vec2  ROCK_POS[ROCK_NUM_MAX] = {};
    
    for (int i = 0; i < sizeof(GROUND_HEIGHTS)/sizeof(GROUND_HEIGHTS[0]); ++i) {
        GROUND_HEIGHTS[i] = 0;
    }
    for (int i = 0; i < sizeof(COIN_POS)/sizeof(COIN_POS[0]); ++i) {
        COIN_POS[i] = Vec2::ZERO;
    }
    for (int i = 0; i < sizeof(ROCK_POS)/sizeof(ROCK_POS[0]); ++i) {
        ROCK_POS[i] = Vec2::ZERO;
    }
    
    int nGround = 0;
    int nCoin = 0;
    int nRock = 0;
    
#define IS_EASY         (difficulty == STAGE_DIFFICULTY_EASY)
#define IS_NORMAL       (difficulty == STAGE_DIFFICULTY_NORMAL)
#define IS_HARD         (difficulty == STAGE_DIFFICULTY_HARD)
#define IS_UPPER_EASY   (difficulty >= STAGE_DIFFICULTY_EASY)
#define IS_UPPER_NORMAL (difficulty >= STAGE_DIFFICULTY_NORMAL)
#define IS_UPPER_HARD   (difficulty >= STAGE_DIFFICULTY_HARD)
    
#define ON_EASY(a)         do { if (IS_EASY)         { a; } } while(0)
#define ON_NORMAL(a)       do { if (IS_NORMAL)       { a; } } while(0)
#define ON_HARD(a)         do { if (IS_HARD)         { a; } } while(0)
#define ON_UPPER_EASY(a)   do { if (IS_UPPER_EASY)   { a; } } while(0)
#define ON_UPPER_NORMAL(a) do { if (IS_UPPER_NORMAL) { a; } } while(0)
#define ON_UPPER_HARD(a)   do { if (IS_UPPER_HARD)   { a; } } while(0)
    
#define ADD_GROUND_DATA(height) do { GROUND_HEIGHTS[nGround++] = (height);      assert(nGround < GROUND_NUM_MAX); } while(0)
#define ADD_COIN_DATA(x, y)     do { COIN_POS[nCoin++] = Vec2((x) + 0.5f, (y)); assert(nCoin   < COIN_NUM_MAX);   } while(0)
#define ADD_ROCK_DATA(x, y)     do { ROCK_POS[nRock++] = Vec2((x) + 0.5f, (y)); assert(nRock   < ROCK_NUM_MAX);   } while(0)
    
#define ADD_GROUND_NARAKU()     do { ADD_GROUND_DATA(STAGE_DATA_NARAKU_HEIGHT); } while(0)
#define ADD_COIN_GROUND()       do { ADD_COIN_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 1] + 0.7f); } while(0)
#define ADD_COIN_AIR()          do { ADD_COIN_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 2] + 2.5f); } while(0) //1つ前からジャンプするため, nGround - 2
#define ADD_COIN_ABOVE_NARAKU   do { ADD_COIN_DATA(nGround-0.5f,GROUND_HEIGHTS[nGround - 2] + 2.5f); } while(0)
#define ADD_COIN_RUSH_1()       do { ADD_COIN_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 2] + 0.6f); } while(0)
#define ADD_COIN_RUSH_2()       do { ADD_COIN_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 2] + 1.9f); } while(0)
#define ADD_COIN_RUSH_3()       do { ADD_COIN_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 2] + 3.2f); } while(0)
#define ADD_ROCK_FALL()         do { ADD_ROCK_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 1] + 9.0f); } while(0)
#define ADD_ROCK_GROUND()       do { ADD_ROCK_DATA(nGround - 1, GROUND_HEIGHTS[nGround - 1]       ); } while(0)
    
#define RAND(p, data)  do { if (getRand() < (p)) { data; } } while(0)
    
#define P (index-- == 0)
    
datahead:
    
    //デモ画面用
    if (index == STAGE_DATA_INDEX_DEMO){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //コインラッシュ
    else if (index == STAGE_DATA_INDEX_COIN_RUSH_A){
        for (int i = 0; i < 10; ++i) {
            ADD_GROUND_DATA(0); ADD_COIN_RUSH_1(); ADD_COIN_RUSH_2(); ADD_COIN_RUSH_3();
        }
    }

    //平らで地面にコイン
    else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); ADD_COIN_GROUND();
        ADD_GROUND_DATA(0); RAND(0.7, ADD_COIN_GROUND());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //平らで空中にコイン
    else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); ADD_COIN_AIR(); ON_UPPER_HARD(RAND(0.2, ADD_ROCK_FALL()));
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); ADD_COIN_AIR(); ON_UPPER_NORMAL(ADD_ROCK_GROUND());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //平らで岩二つ
    /*else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); ON_UPPER_NORMAL(ADD_ROCK_FALL());
        ADD_GROUND_DATA(0); ADD_COIN_AIR();
        ADD_GROUND_DATA(0); ON_UPPER_NORMAL(ADD_ROCK_FALL());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }*/
    //奈落
    else if (P || P || P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_NARAKU(); RAND(0.5, ADD_COIN_DATA(nGround - 0.5f, GROUND_HEIGHTS[nGround - 2] + 2.5f)); //奈落のちょうど中央に配置
        ADD_GROUND_NARAKU();
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //上段
    else if (P || P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); RAND(0.2, ADD_COIN_GROUND());
        ADD_GROUND_DATA(0); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(0); RAND(0.5, ADD_COIN_AIR());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //上段長め
    else if (P || P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); RAND(0.2, ADD_COIN_GROUND());
        ADD_GROUND_DATA(0); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1); ADD_COIN_AIR(); ON_UPPER_HARD(ADD_ROCK_FALL());
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1); ON_UPPER_HARD(RAND(0.2, ADD_ROCK_FALL()));
        ADD_GROUND_DATA(0); RAND(0.5, ADD_COIN_AIR());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //2段上
    else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); RAND(0.5, ADD_COIN_GROUND());
        ADD_GROUND_DATA(0); ON_UPPER_HARD(RAND(0.5, ADD_ROCK_FALL()));
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1); RAND(0.5, ADD_COIN_GROUND());
        ADD_GROUND_DATA(1); ON_UPPER_HARD(RAND(0.3, ADD_ROCK_FALL()));
        ADD_GROUND_DATA(2);
        ADD_GROUND_DATA(2);
        ADD_GROUND_DATA(2); RAND(0.7, ADD_COIN_GROUND());
        ADD_GROUND_DATA(2);
        ADD_GROUND_DATA(2);
        ADD_GROUND_DATA(1); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(1);
        ADD_GROUND_DATA(0); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    //1段下
    else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(-1); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(-1);
        ADD_GROUND_DATA(-1); RAND(0.5, ADD_COIN_GROUND());
        ADD_GROUND_DATA(-1); ON_UPPER_HARD(RAND(0.5, ADD_ROCK_FALL()));
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
    }
    /*//2段下
    else if (P){
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(-1); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(-1);
        ADD_GROUND_DATA(-1);
        ADD_GROUND_DATA(-1);
        ADD_GROUND_DATA(-2); RAND(0.2, ADD_ROCK_GROUND());
        ADD_GROUND_DATA(-2);
        ADD_GROUND_DATA(-2); RAND(0.7, ADD_COIN_GROUND());
        ADD_GROUND_DATA(-2);
        ADD_GROUND_DATA(-2); ON_UPPER_HARD(ADD_ROCK_FALL());
        ADD_GROUND_DATA(-1);
        ADD_GROUND_DATA(-1); RAND(0.5, ADD_COIN_GROUND());
        ADD_GROUND_DATA(-1);
        ADD_GROUND_DATA(-1); ON_UPPER_HARD(ADD_ROCK_FALL());
        ADD_GROUND_DATA(0);
        ADD_GROUND_DATA(0); RAND(0.2, ADD_COIN_GROUND());
    }*/
    else{
        goto datahead;
    }
    /**/

    *groundHeights = GROUND_HEIGHTS;
    *groundNum = nGround;
    
    *coinPos = COIN_POS;
    *coinNum = nCoin;
    
    *rockPos = ROCK_POS;
    *rockNum = nRock;
    
    return true;
}

bool getStageDataPlane(float** groundHeights, int *groundNum, Vec2** coinPos, int *coinNum, Vec2** rockPos, int *rockNum){
    return getStageDataWithIndex(0, STAGE_DIFFICULTY_EASY, groundHeights, groundNum, coinPos, coinNum, rockPos, rockNum);
}
