//
//  Stage.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/30.
//
//



#include "Stage.h"
#include "StageData.h"
#include "GameHelper.h"
#include "Consts.h"


//復活時の無敵時間
#define MAX_RESPAWN_DURATION_SEC (2.f  * DEFAULT_GAME_SPEED)
#define MIN_RESPAWN_DURATION_SEC (0.5f * DEFAULT_GAME_SPEED)

//ブースト時間
#define BOOST_DURATION_SEC   (5.f * DEFAULT_GAME_SPEED)

//何個先まで地形を作っておくか
#define GROUND_DATA_FUTURE_COUNT (10)

//過ぎた分を消す
#define GROUND_DATA_PASSED_COUNT  (3)



Stage::~Stage(){
    
}

Stage::Stage(){
    setup();
}


void Stage::setup(){
    state = STATE_PREPARE;
    
    clearGround();
    
    //時間
    time = 0.f;
    respawnedDate = -10.f;
    playSpeed = 1.f;
    
    //ボール
    ballPos    = Vec2(getBallRadius(), getBallRadius() * 1.05f);
    ballSpeed  = Vec2(getBallInitRunSpeed(), 0);
    prevBallSpeed = ballSpeed;
    jumpSteps = 1; //空中から始まるから
    ballDropping = false;
    
    //ブースト
    boostEndGroundX = -1;
}

void Stage::start(){
    state = STATE_IN_GAME;
    time = 0.f;

    ballPos.x -= getCurrentGroundBaseX() * getGroundBaseWidth(); //先頭に戻す
    
    //地形作成
    clearGround();
    addGroundData(0);
    while (procNextGround()) {
    }
}

void Stage::setDelegate(StageDelegate* delegate){
    this->delegate = delegate;
}

void Stage::unsetDelegate(){
    this->delegate = nullptr;
}

void Stage::update(float dt) {

    //プレイ速度を乗じる
    dt *= playSpeed;
    
    //ボール処理
    if (state == STATE_PREPARE || state == STATE_IN_GAME || state == STATE_RESPAWN || state == STATE_BOOST || state == STATE_BOOST_END) {
        updateBall(dt);
        updateRocks(dt);
    }
    else if (state == STATE_GAMEOVER) {
        //ゲームオーバー時の挙動
        updateBall(dt); //ボールの吹き飛びを処理するため
    }
    
    //衝突チェック
    do {
        //地形との衝突チェック
        if (state == STATE_IN_GAME) {
            if (collisionCheck()){
                procGameOver();
                break;
            }
        }
        
        //落下物との衝突チェック
        if (state == STATE_IN_GAME) {
            if (collisionRockCheck() != rockData.end()) {
                procGameOver();
                break;
            }
        }

        //コイン獲得チェック
        if (state == STATE_IN_GAME || state == STATE_RESPAWN || state == STATE_BOOST || state == STATE_BOOST_END) {
            auto it = collisionCoinCheck();
            if (it != coinData.end()) {
                Vec2 coinPos = *it;
                coinData.erase(it);
                    
                if (delegate) {
                    delegate->onGotCoin(this, coinPos);
                }
            }
        }
    } while(0);

    //ステージ追加
    procNextGround();
    
    //状態遷移
    if (state == STATE_RESPAWN || state == STATE_BOOST_END) {
        //一定時間経過したら、IN_GAMEになる
        if (time >= respawnedDate + MAX_RESPAWN_DURATION_SEC * playSpeed) {
            state = STATE_IN_GAME;
        }
    }
    else if (state == STATE_BOOST){
        //ラッシュ地形の最後に到達したら、通常状態に戻る
        if (getCurrentGroundBaseX() >= boostEndGroundX) {

            state = STATE_IN_GAME;

            if (delegate) {
                delegate->onBoostEnded(this);
            }
        }
    }
}


#pragma mark - 物理挙動

//ボールが進行
void Stage::updateBall(float dt) {

    //dtが極小の場合は何もしない(0割など)
    if (dt < 0.001) {
        return;
    }
    
    time += dt;
    
    //処理前の座標を記録
    prevBallPos   = ballPos;
    prevBallSpeed = ballSpeed;

    //定速移動量
    Vec2 constantMove = Vec2(getBallCurrentRunSpeed(), 0) * dt;
    
    //加速度による移動量
    Vec2 thisAcc = getBallForce();
    Vec2 accMove = ballSpeed * dt + 0.5 * thisAcc * dt * dt;
    
    //ballPos += accMove;
    //横方向は定速のみ、縦方向は加速度のみ
    ballPos += Vec2(constantMove.x, accMove.y);
    
    //高さ固定処理(重力無視、ジャンプ不可の想定)
    bool beFixedHeight = false;//(state == STATE_BOOST);
    if (beFixedHeight) {
        ballPos.y = prevBallPos.y;
    }

    //めり込み防止処理を行うかどうか
    bool doKickback = true;//(state != STATE_BOOST);
    
    if (doKickback) {
        //めり込み防止処理
        bool kickbacked = kickbackBallY();

        //速度を記録しておく
        ballSpeed = (ballPos - prevBallPos) / dt;
        
        //めり込み防止をした場合、速度はゼロとする（めり込み処理を速度計算に入れると、とんでもない高速で移動したことになってしまうため）
        if (kickbacked) {
            ballSpeed.y = 0;
        }
        
        //ジャンプ状態を更新
        if (kickbacked) {
            jumpSteps = 0;
        }
        else{
            jumpSteps = std::max(jumpSteps, 1);
        }

        //垂直落下状態を更新
        ballDropping = false;
    }
    else{
        //速度を記録しておく
        ballSpeed = (ballPos - prevBallPos) / dt;
    }
}

//ボールがジャンプ
bool Stage::procBallJump(){
    if ((state == STATE_IN_GAME || state == STATE_RESPAWN || state == STATE_BOOST || state == STATE_BOOST_END) && (jumpSteps <= 1)) {

        ++jumpSteps;
        
        //滞空時間から、ジャンプの初速を決める
        ballSpeed.y = -1 * getGravity() * getJumpDurationSec();
        
        if (delegate) {
            delegate->onJump(this, getBallGroundPos());
        }
        
        return true;
    }
    else{
        return false;
    }
}

//垂直落下の処理
bool Stage::procDrop(){
    static float DROP_SPEED = -5.f;
    
    if ((state == STATE_IN_GAME || state == STATE_RESPAWN || state == STATE_BOOST || state == STATE_BOOST_END) && (jumpSteps >= 1) && !ballDropping) {
        
        ballSpeed.y += DROP_SPEED;
        ballDropping = true;
        
        if (delegate) {
            delegate->onDrop(this, getBallGroundPos());
        }
    }
    
    return true;
}

//ブースト処理(一定時間無敵で高速水平移動)
bool Stage::procBoost(){
    
    if (state == STATE_BOOST) {
        return false;
    }
    
    state = STATE_BOOST;

    //地形を全部リセット
    //clearGround();
    //maxGroundData = getCurrentGroundBaseX() - GROUND_DATA_PASSED_COUNT;
    
    //ブースト時間分の地形を全部作る
    for (int i = 0; i < 6; ++i) {
        addGroundData(STAGE_DATA_INDEX_COIN_RUSH_A); //1回のラッシュ地形でコイン10個

        //ラッシュの最後までいったら、ブースト終了
        boostEndGroundX = maxGroundData;
    }
    
    //終わった直後は平坦なステージにしておく
    addGroundData(STAGE_DATA_INDEX_DEMO);
    
    if (delegate) {
        delegate->onBoost(this);
    }
    
    return true;
}


//落下物の処理
void Stage::updateRocks(float dt) {
    static float ROCK_MOVE_START_DISTANCE = 24.f;
    static float ROCK_FALL_SPEED = 5.f;
    
    for (auto it = rockData.begin(); it != rockData.end(); ++it) {
        Vec2 rockPos = *it;
        
        //範囲内の落下物を一定速で落としていく
        if ((rockPos.x <= getBallPos().x + ROCK_MOVE_START_DISTANCE) && (rockPos.x > getBallPos().x)) {
            float groundHeight = getGroundHeightAt(rockPos.x);
            
            if (rockPos.y > groundHeight) {
                rockPos.y = std::max(rockPos.y - ROCK_FALL_SPEED * dt, groundHeight);
                *it = rockPos;

                if (rockPos.y <= groundHeight) {
                    if (delegate) {
                        delegate->onRockCrashed(this, rockPos);
                    }
                }
            }
        }
    }
}


//指定の座標が空中かどうか
bool Stage::isAboveTheGround(Vec2 pos) {
    float groundHeight = getGroundHeightAt(pos.x);
    return pos.y > groundHeight;
}

//指定の座標にあるボールが空中かどうか
bool Stage::isBallAboveTheGround(Vec2 ballPos) {
    return isAboveTheGround(Vec2(ballPos.x, ballPos.y - getBallRadius()));
}

//ボールのめり込み防止
bool Stage::kickbackBallY(){
    
    //めり込んでいたら
    if (!isBallAboveTheGround(ballPos)) {
        //持ち上げる
        float height = getGroundHeightAtBall();
        ballPos.y = height + getBallRadius();
        return true;
    }
    return false;
}

//衝突検知
bool Stage::collisionCheck(){
    
    //ボールの右端点で衝突判定する
    Vec2 hitCheckPos = getBallPos() + Vec2(getBallRadius(), 0);
    return !isAboveTheGround(hitCheckPos);
}

//コイン獲得したかチェック
std::vector<Vec2>::iterator Stage::collisionCoinCheck(){
    for (auto it = coinData.begin(); it != coinData.end(); ++it) {
        Vec2 coinPos = *it;
        
        float length = (coinPos - getBallPos()).length();
        if (length <= getBallRadius() + getCoinRadius()) {
            return it;
        }
    }
    
    return coinData.end();
}

//落下物に衝突したかチェック
std::vector<Vec2>::iterator Stage::collisionRockCheck(){
    for (auto it = rockData.begin(); it != rockData.end(); ++it) {
        Vec2 rockPos = *it;
        
        float length = (rockPos - getBallPos()).length();
        if (length <= getBallRadius() + getRockRadius()) {
            return it;
        }
    }
    
    return rockData.end();
}


#pragma mark - 定数系取得

float Stage::getGravity(){
    static float GRAVITY = 9.8f;
    return -1 * GRAVITY;
}
float Stage::getJumpDurationSec(){
    static float JUMP_DURATION_SEC = 0.6f;//なぜか四分の1にしないとただしく動かない
    return JUMP_DURATION_SEC;
}
float Stage::getBallRadius(){
    static float BALL_RADIUS = .7f;
    return BALL_RADIUS;
}
float Stage::getBallInitRunSpeed() {
    static float BALL_SPEED_INIT = 5.f;
    return BALL_SPEED_INIT;
}
float Stage::getGroundBaseWidth(){
    static float GROUND_BASE_WIDTH  = 3.f;
    return GROUND_BASE_WIDTH;
}
float Stage::getGroundBaseHeight(){
    static float GROUND_BASE_HEIGHT  = 1.4f;
    return GROUND_BASE_HEIGHT;
}
float Stage::getCoinRadius() {
    static float COIN_RADIUS = .7f; //ボールと同じ大きさ(でないと、地上のコインが地面にめり込む)
    return COIN_RADIUS;
}
float Stage::getRockRadius(){
    static float ROCK_RADIUS = .7f; //ボールと同じ大きさ
    return ROCK_RADIUS;
}


#pragma mark - 状態変数系取得

Stage::State Stage::getState(){
    return state;
}
bool  Stage::hasStarted(){
    return state >= STATE_IN_GAME;
}
bool  Stage::hasGameOver(){
    return state == STATE_GAMEOVER;
}
bool  Stage::isBoosting(){
    return state == STATE_BOOST;
}
bool  Stage::canCountCoinForBoost(){
    return state == STATE_IN_GAME || state == STATE_RESPAWN;
}
float Stage::getTime(){
    return time;
}
float Stage::getBallCurrentRunSpeed() {
    static float BOOST_SPEED_COEF = 1.5f;
    static float GAMEOVER_SPEED_COEF = -0.2f;
    return getBallInitRunSpeed() *
    (isBoosting()  ? BOOST_SPEED_COEF :
     hasGameOver() ? GAMEOVER_SPEED_COEF :
     1.f);
}
Vec2 Stage::getBallForce(){
    return Vec2(0, getGravity());
}
Vec2 Stage::getBallPos(){
    return ballPos;
}
Vec2 Stage::getBallGroundPos(){
    return ballPos + Vec2(0, -getBallRadius());
}
bool Stage::getBallJumping(){
    return jumpSteps >= 1;
}
int Stage::getBallJumpingSteps(){
    return jumpSteps;
}
bool Stage::getBallDropping(){
    return ballDropping;
}
Vec2 Stage::getCurrentVelocity(){
    return ballSpeed;
}
float Stage::getGroundHeightAt(float x){
    if (x < 0) {
        return 0;
    }
    int index = getGroundBaseX(x);
    return groundData[index] * getGroundBaseHeight();
}
float Stage::getGroundHeightAtBall(){
    return getGroundHeightAt(ballPos.x);
}
bool Stage::isGroundNarakuAt(float x){
    return getGroundHeightAt(x) <= STAGE_DATA_NARAKU_HEIGHT + 1.f;//epsilon
}
bool Stage::isGroundNarakuAtBall(){
    return isGroundNarakuAt(ballPos.x);
}
int Stage::getGroundBaseX(float x){
    return x / getGroundBaseWidth();
}
int Stage::getCurrentGroundBaseX(){
    return getGroundBaseX(getBallPos().x);
}
Vec2 Stage::getGroundPosFromIndexPos(Vec2 indexPos){
    return Vec2(indexPos.x * getGroundBaseWidth(), indexPos.y * getGroundBaseHeight());
}
double Stage::getBallDistanceToShow(){
    return getBallPos().x / getGroundBaseWidth();
}
const std::vector<Vec2> Stage::getCoinData(){
    return coinData;
}
const std::vector<Vec2> Stage::getRockData(){
    return rockData;
}
int Stage::getLastGroundDataIndex() {
    return lastGroundDataIndex;
}
StageDifficulty Stage::getCurrentStageDifficulty(){
    static float EASY_TIME   = 16.f * DEFAULT_GAME_SPEED;
    static float NORMAL_TIME = 32.f * DEFAULT_GAME_SPEED;
    return (  getTime() < EASY_TIME   ? STAGE_DIFFICULTY_EASY
            : getTime() < NORMAL_TIME ? STAGE_DIFFICULTY_NORMAL
            :                           STAGE_DIFFICULTY_HARD);
}


#pragma mark - proc

bool Stage::procNextGround(){
    bool ret = false;
    
    float ballX = getBallPos().x;
    int x = getCurrentGroundBaseX();
    
    //古いデータを消す
    for (auto it = groundData.begin(); it != groundData.end(); ) {
        if (it->first < x - GROUND_DATA_PASSED_COUNT) {
            it = groundData.erase(it);
        }
        else{
            ++it;
        }
    }
    for (auto it = coinData.begin(); it != coinData.end(); ) {
        Vec2 pos = *it;
        if (pos.x < ballX - GROUND_DATA_PASSED_COUNT * getGroundBaseWidth()) {
            it = coinData.erase(it);
        }
        else{
            ++it;
        }
    }
    for (auto it = rockData.begin(); it != rockData.end(); ) {
        Vec2 pos = *it;
        if (pos.x < ballX - GROUND_DATA_PASSED_COUNT * getGroundBaseWidth()) {
            it = rockData.erase(it);
        }
        else{
            ++it;
        }
    }
    
    //先読み分が足りなくなったら、データ追加
    if (x > maxGroundData - GROUND_DATA_FUTURE_COUNT) {
        int index = 0;
        if (state == STATE_PREPARE || state == STATE_GAMEOVER) {
            index = STAGE_DATA_INDEX_DEMO;
        }
        else{
            index = getIntRand(1, getStageDataCount() - 1);
        }
        
        if (addGroundData(index)){
            ret = true;
        }
    }
    
    return ret;
}

bool Stage::addGroundData(int index){
    float* groundHeights;
    int groundNum;
    Vec2* coinPos;
    int coinNum;
    Vec2* rockPos;
    int rockNum;
    if (getStageDataWithIndex(index, getCurrentStageDifficulty(), &groundHeights, &groundNum, &coinPos, &coinNum, &rockPos, &rockNum)) {
        
        for (int i = 0; i < groundNum; ++i) {
            groundData[maxGroundData + i] = groundHeights[i];
        }
        
        for (int i = 0; i < coinNum; ++i) {
            Vec2 pos = getGroundPosFromIndexPos(Vec2(coinPos[i].x + maxGroundData, coinPos[i].y));
            coinData.push_back(pos);
        }

        for (int i = 0; i < rockNum; ++i) {
            Vec2 pos = getGroundPosFromIndexPos(Vec2(rockPos[i].x + maxGroundData, rockPos[i].y));
            rockData.push_back(pos);
        }
        
        maxGroundData += groundNum;
        lastGroundDataIndex = index;
        return true;
    }
    return false;
}

void Stage::clearGround(){
    groundData.clear();
    coinData.clear();
    rockData.clear();
    maxGroundData = 0;
}

//ゲームオーバー時の処理
void Stage::procGameOver(){
    state = STATE_GAMEOVER;
    
    //ボールの下が奈落の場合、奈落に真っ逆さま
    if (getGroundHeightAt(getBallPos().x) < -10) {
        static float r = 1;
        ballSpeed.y = getGravity() * r;
    }
    //下が地面の場合は、斜め後ろに吹っ飛ぶ
    else {
        static float t = -0.5;
        ballSpeed.y = getGravity() * t;
    }
    
    
    if (delegate) {
        delegate->onGameOver(this);
    }
}

//復活（コンティニュー）
bool Stage::procRespawn(){
    
    //地形を全部平らに(STATE_RESPAWNにする前に行う。ゲームオーバー状態は、平の地形固定なので)
    clearGround();
    maxGroundData = getCurrentGroundBaseX() - GROUND_DATA_PASSED_COUNT;
    while (procNextGround()){
    }
    
    //リスポーン状態に
    state = STATE_RESPAWN;
    respawnedDate = time;
    
    //ボールは基準の高さに移動
    jumpSteps = 0;
    ballPos.y = getBallRadius();
    
    return true;
}

