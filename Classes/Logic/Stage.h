//
//  Stage.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/30.
//
//

#ifndef Stage_h
#define Stage_h

#include "StageData.h"
#include "cocos2d.h" //for Vec2
USING_NS_CC;



class StageDelegate;


//ステージ。
// 水平な世界に、地面と落下物があり、その中を球が自動で走り抜ける
// 何かにぶつかったら終了

class Stage{
public:
    ~Stage();
    Stage();
    
public:
    enum State{
        STATE_PREPARE,
        STATE_IN_GAME,
        STATE_BOOST,
        STATE_BOOST_END,
        STATE_GAMEOVER,
        STATE_RESPAWN,
    };
    
public:
    void setup();
    void start();
    void setDelegate(StageDelegate* delegate);
    void unsetDelegate();
    void update(float dt);
    void setPlaySpeed(float speed) { playSpeed = speed; }

    //物理挙動
    void updateBall(float dt);
    bool procBallJump();
    bool procDrop();
    bool procBoost();
    void updateRocks(float dt);
    bool isAboveTheGround(Vec2 pos);
    bool isBallAboveTheGround(Vec2 pos);
    bool kickbackBallY();
    bool collisionCheck();
    std::vector<Vec2>::iterator collisionCoinCheck();
    std::vector<Vec2>::iterator collisionRockCheck();
    
    //定数系取得
    float getGravity();
    float getJumpDurationSec();
    float getBallRadius();
    float getBallInitRunSpeed();
    float getGroundBaseWidth();
    float getGroundBaseHeight();
    float getCoinRadius();
    float getRockRadius();

    //状態変数系取得
    State getState();
    bool  hasStarted();
    bool  hasGameOver();
    bool  isBoosting();
    bool  canCountCoinForBoost();
    float getTime();
    float getBallCurrentRunSpeed();
    Vec2  getBallForce();
    Vec2  getBallPos();
    Vec2  getBallGroundPos();
    bool  getBallJumping();
    int   getBallJumpingSteps();
    bool  getBallDropping();
    Vec2  getCurrentVelocity();
    float getGroundHeightAt(float x);
    float getGroundHeightAtBall();
    bool  isGroundNarakuAt(float x);
    bool  isGroundNarakuAtBall();
    int   getGroundBaseX(float x);
    int   getCurrentGroundBaseX();
    Vec2 getGroundPosFromIndexPos(Vec2 indexPos);
    double getBallDistanceToShow();
    const std::vector<Vec2> getCoinData();
    const std::vector<Vec2> getRockData();
    int getLastGroundDataIndex();
    StageDifficulty getCurrentStageDifficulty();
    
    //地形
    bool procNextGround();
    bool addGroundData(int index);
    void clearGround();
    
    //状態処理
    void procGameOver();
    bool procRespawn();
    
private:
    
    //state
    State state;
    
    //物理時間
    float time;
    float respawnedDate;
    float playSpeed;
    
    //地面
    std::map<int, float> groundData;
    int maxGroundData;
    int lastGroundDataIndex;
    
    //コイン
    std::vector<Vec2> coinData;
    
    //落下物
    std::vector<Vec2> rockData;
    
    //ボール
    Vec2 ballPos;
    Vec2 prevBallPos;
    Vec2 ballSpeed;
    Vec2 prevBallSpeed;
    int jumpSteps; //0: 着地、1:ジャンプ、2:2段ジャンプ中
    bool ballDropping;
    
    //ブースト
    int boostEndGroundX;

    //delegate
    StageDelegate* delegate;
};


class StageDelegate{
public:
    virtual void onJump(Stage*, Vec2 pos){}
    virtual void onDrop(Stage*, Vec2 pos){}
    virtual void onGotCoin(Stage*, Vec2 pos){}
    virtual void onBoost(Stage*) {}
    virtual void onBoostEnded(Stage*) {}
    virtual void onRockCrashed(Stage*, Vec2 pos){}
    virtual void onGameOver(Stage*){}
};


#endif /* Stage_h */
