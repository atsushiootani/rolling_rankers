//
//  Sound.h
//
//
//  Created by OtaniAtsushi1 on 2015/04/22.
//
//

#ifndef Sound_h
#define Sound_h

#include "SimpleAudioEngine.h"
#include "Cocos2d.h"
#include <string>

USING_NS_CC;
using namespace CocosDenshion;

extern SimpleAudioEngine* registerSoundWithMixMode();
extern SimpleAudioEngine* registerSoundWithExclusiveMode();
extern SimpleAudioEngine* registerSoundWithModerateMode();
extern SimpleAudioEngine* registerSoundWithEnableBackgroundMode();
extern SimpleAudioEngine* registerSoundWithEnableInputMode();

extern SimpleAudioEngine* getSimpleAudioEngine();

extern bool playBgm(std::string bgmName, bool loop = true);
extern void stopBgm();
extern void pauseBgm();
extern void resumeBgm();
extern bool isBgmPlaying();
extern bool setBgmVolume(float vol);
extern bool fadeInBgm(Node* node, float duration);
extern bool fadeOutBgm(Node* node, float duration);
extern void preloadBgm(std::string bgmName);
extern void unloadBgm(std::string bgmName);

extern unsigned int playEffect(std::string seName);
extern void stopEffect(unsigned int soundId);
extern void stopAllEffects();
extern void preloadEffect(std::string seName);
extern void unloadEffect(std::string seName);

#endif
