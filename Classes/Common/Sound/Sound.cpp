//
//  Sound.cpp
//  
//
//  Created by OtaniAtsushi1 on 2015/04/22.
//
//

#include "Sound.h"
#include "Debug.h"
#include "CocosHelper.h"
#include "Native.h"
#include "AndroidHelper.h"

USING_NS_CC;

static std::string s_playingBgm = "";

//初期化処理
SimpleAudioEngine* registerSoundWithMixMode(){
    //他のアプリと混声する
#if CC_TARGET_OS_IPHONE //TODO:
    SimpleAudioEngine::configureModeMix();
#endif
    return SimpleAudioEngine::getInstance();
}

SimpleAudioEngine* registerSoundWithExclusiveMode(){
    //他のアプリの音を止める
#if CC_TARGET_OS_IPHONE //TODO:
    SimpleAudioEngine::configureModeExclusive();
#endif
    return SimpleAudioEngine::getInstance();
}

SimpleAudioEngine* registerSoundWithModerateMode(){
    //他のアプリの音が無い時だけ発音する
#if CC_TARGET_OS_IPHONE //TODO:
    SimpleAudioEngine::configureModeModerate();
#endif
    return SimpleAudioEngine::getInstance();
}

SimpleAudioEngine* registerSoundWithEnableBackgroundMode(){
    //バックグラウンドでも発音を許可する
#if CC_TARGET_OS_IPHONE //TODO:
    SimpleAudioEngine::configureModeEnableBackground();
#endif
    return SimpleAudioEngine::getInstance();
}

SimpleAudioEngine* registerSoundWithEnableInputMode(){
    //音声の入力も許可する
#if CC_TARGET_OS_IPHONE //TODO:
    SimpleAudioEngine::configureModeEnableInput();
#endif
    return SimpleAudioEngine::getInstance();
}

SimpleAudioEngine* getSimpleAudioEngine() {
    return SimpleAudioEngine::getInstance();
}


bool playBgm(std::string bgmName, bool loop/* = true*/) {
    if (s_playingBgm != bgmName) {
        if (getSimpleAudioEngine()->isBackgroundMusicPlaying()) {
            getSimpleAudioEngine()->stopBackgroundMusic();
        }
        DPRINTF("playBgm : %s\n", bgmName.c_str());
        getSimpleAudioEngine()->playBackgroundMusic(bgmName.c_str(), loop);
    }
    
    s_playingBgm = bgmName;
    
    return true;
}

void stopBgm(){
    getSimpleAudioEngine()->stopBackgroundMusic();
}

void pauseBgm(){
    getSimpleAudioEngine()->pauseBackgroundMusic();
}

void resumeBgm(){
    getSimpleAudioEngine()->resumeBackgroundMusic();
}

bool isBgmPlaying() {
    return getSimpleAudioEngine()->isBackgroundMusicPlaying();
}

bool setBgmVolume(float vol){
    // 0.0 <= vol <= 1.0f
    getSimpleAudioEngine()->setBackgroundMusicVolume(vol);
    return true;
}

bool fadeInBgm(Node* node, float duration) {
    setBgmVolume(0.0);
    node->runAction(BgmFade::create(duration, 1.0));
    return true;
}

bool fadeOutBgm(Node* node, float duration){
    //CDSoundSourceFader
    setBgmVolume(1.0);
    node->runAction(Sequence::create(BgmFade::create(duration, 0.0),
                                     CallFunc::create([](){ setBgmVolume(1.0); }),
                                     nullptr));
    return true;
}

void preloadBgm(std::string bgmName){
    getSimpleAudioEngine()->preloadBackgroundMusic(bgmName.c_str());
}
void unloadBgm(std::string bgmName){
    //do nothing. SimpleAudioEngineに対応するメソッドなし
}

unsigned int playEffect(std::string seName){
    //Native_Print(seName.c_str());
    unsigned int ret = getSimpleAudioEngine()->playEffect(seName.c_str());
    //Native_Print(std::string("ret=") + std::to_string(ret));
    return ret;
}

void stopEffect(unsigned int soundId){
    getSimpleAudioEngine()->stopEffect(soundId);
}

void stopAllEffects(){
    getSimpleAudioEngine()->stopAllEffects();
}

void preloadEffect(std::string seName){
    getSimpleAudioEngine()->preloadEffect(seName.c_str());
}

void unloadEffect(std::string seName){
    getSimpleAudioEngine()->unloadEffect(seName.c_str());
}

