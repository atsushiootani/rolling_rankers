//
//  GameManager.h
//
//
//
//
//

#ifndef GameManager_h
#define GameManager_h

#include "GameItemGroup.h"
#include "SaveManager.h"
#include "Consts.h"
#include "AndroidHelper.h"
#include <ctime>
#include <vector>


//セーブする必要のあるデータの目印
#define NEED_SAVE


class GameManagerDelegate;

#pragma mark - GameManager

/**
 * GameManager
 */
class GameManager : public SaveDataProtocol, public GameItemGroupDelegate{
	//singleton
public:
	static GameManager* getInstance();

	//ctor
private:
	GameManager(){init();}
	GameManager(const GameManager &other){}
	GameManager &operator = (const GameManager &other) { return *this; }

	void init();

	//method
public:
#pragma mark - consts
    int getRewardCoin();
    int getContinueCoin();
    int getGachaCoin();
    int getPlayIntervalCoin();
    
public:
#pragma mark - coin
    int addCoin(int add);
    int subCoin(int sub);
    int getCoin();
    int getTotalCoin() { return totalCoin; }
    
#pragma mark - chara
    void setCharaIndex(int index) { selectedCharaIndex = index; }
    int  getCharaIndex()          { return selectedCharaIndex; }
    bool hasCharaUnlocked(int index);
    int  getLockedCharaCount();
    int  getUnlockedCharaCount();
    bool canPlayGacha();
    int playGacha();
    
#pragma mark - score
    bool   recordScore(double s);
    double getHighScore();
    
#pragma mark - achievement
public:
    bool isAchievementUnlocked(int index);
    int  getUnlockedAchievementCount();
    int  getAllAchievementCount();
    void unlockAchievement(int index);
private:
    bool checkAchievementCanUnlock(int index);
    void updateAllAchievements();
    
#pragma mark - login bonus
public:
    int    setLoginBonusGotNow();
    int    getLastGotLoginBonusIndex();
    time_t getLastGotLoginBonusDate();
    bool   canGetLoginBonus(time_t now);
    
#pragma mark - ad movie
public:
    void addAdMovieWatchedCount();
    
#pragma mark - 記録
public:
    void addPlayNum() { ++playNum; }
    int  getPlayNum() { return playNum; }
    
#pragma mark - save
public:
    bool isSaveOk();
    
#pragma mark - background
public:
    void onEnterBackground(); //バックグラウンドに移行するときに呼び出す
    void onEnterForeground(); //バックグラウンドから復帰したときに呼び出す
    void onApplicationLaunched();
    
#pragma mark - update
    void update(double dt);
    
#pragma mark - delegate
public:
    void setDelegate(GameManagerDelegate* delegate);
    GameManagerDelegate* getDelegate() const;
    
#pragma mark - 課金処理
    bool procPurchase(   std::string productId);
    bool procRestore(    std::string productId);
    bool procCanPurchase(std::string productId);

#pragma mark - GameItemGroupDelegate
public:
    virtual int  getItemMoney(GameItemGroup*) override;
    virtual int  subItemMoney(GameItemGroup*, int itemIndex, int sub) override;
    virtual int  addItemMoney(GameItemGroup*, int itemIndex, int add) override;
    virtual bool isSaveItemOk(GameItemGroup*) override;
    virtual bool saveItemMoney(GameItemGroup*) override;

#pragma mark - SaveDataProtocol
public:
    virtual bool saveAll() override;
    virtual bool loadAll() override;
    virtual const std::string getSaveDataPrefix() override;

protected:
    virtual bool saveImpl(SaveManager*) override;
    virtual bool loadImpl(SaveManager*) override;


    
#pragma mark - 変数

private:
    //現在時刻
    time_t lastUpdateTime;
    
    //コイン
    NEED_SAVE int coin;
    NEED_SAVE int totalCoin;
    
    //選択中キャラ
    NEED_SAVE int selectedCharaIndex;
    
    //キャラを入手したかどうか
    NEED_SAVE bool isCharaUnlocked[CHARA_COUNT];
    
    //ハイスコア
    NEED_SAVE double highScore;
    
    //総走行距離
    NEED_SAVE double totalLength;
    
    //実績
    NEED_SAVE bool achievements[ACHIEVEMENT_COUNT];
    
    //ログインボーナス
    NEED_SAVE int loginBonusDays;
    NEED_SAVE time_t lastLoginBonusGotDate;
    
    //動画視聴回数
    NEED_SAVE int adMovieWatchedCount;
    
    //プレイ回数
    NEED_SAVE int playNum;
    
    
    //delegate
    GameManagerDelegate* delegate;
};



#pragma mark - getter method

extern GameManager* getGameManager();



#pragma mark - delegate

/**
 * GameManagerからの通知を受け取るクラス
 */
class GameManagerDelegate{
public:
    //バックグラウンドに入った時のコールバック
    virtual void onEnterBackground(GameManager* gameManager) = 0;
    
    //フォアグラウンドに入った時のコールバック
    virtual void onEnterForeground(GameManager* gameManager) = 0;
    
    //実績解除時のコールバック
    virtual void onUnlockAchievement(GameManager* gameManager, int index) = 0;
    
    //実績解除可能通知
    virtual void onCanUnlockAchievement(GameManager* gameManager, int index) = 0;
};

#endif
