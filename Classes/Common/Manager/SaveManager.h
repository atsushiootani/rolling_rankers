//
//  SaveManager.h
//
//
//
//
//

#ifndef SaveManager_h
#define SaveManager_h

#include "cocos2d.h"
#include "SaveUtils.h"

USING_NS_CC;


class SaveDataProtocol;


class SaveManager{
    //singleton
public:
    static SaveManager* getInstance();
    
    //ctor
private:
    SaveManager()
    {
        init();
    }
    SaveManager(const SaveManager &other)
    {
        *this = other;
    }
    SaveManager &operator = (const SaveManager &other) { return *this; }
    
    void init();
    
public:
    bool save(SaveDataProtocol*);
    bool load(SaveDataProtocol*);
    
private:
    const char *getFilePath();
    
};


extern SaveManager* getSaveManager();


#pragma mark - SaveDataProtocol

//セーブしたいデータを保持するクラスが実装すべきプロトコル

class SaveDataProtocol{
public:
    virtual bool saveAll();
    virtual bool loadAll();

protected:
    virtual const std::string getSaveDataPrefix() = 0;
    virtual bool saveImpl(SaveManager*) = 0;
    virtual bool loadImpl(SaveManager*) = 0;
    
    std::string getSaveDataKeyName(const char* name);

    friend class SaveManager;
};


#endif
