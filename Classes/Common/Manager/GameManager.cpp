//
//  GameManager.cpp
//
//
//
//
//

#include "GameManager.h"
#include "SaveManager.h"
#include "PurchaseManager.h"
#include "GameData.h"
#include "GameHelper.h"
#include "Native.h"
#include "AppInfo.h" //for purchase item id
#include "Debug.h"
#include <random>



#pragma mark - singleton

GameManager* GameManager::getInstance() {
	static GameManager instance;
	return &instance;
}

//初期化
//セーブデータから取得するものはあとから上書きされる
void GameManager::init() {

    coin = 0;
    
    //delegate
    delegate = nullptr;
    
    //課金周り
    getPurchaseManager()->init();
    getPurchaseManager()->setPurchaseProc(   std::bind(&GameManager::procPurchase,    this, std::placeholders::_1));
    getPurchaseManager()->setRestoreProc(    std::bind(&GameManager::procRestore,     this, std::placeholders::_1));
    getPurchaseManager()->setCanPurchaseProc(std::bind(&GameManager::procCanPurchase, this, std::placeholders::_1));
}


#pragma mark - consts

int GameManager::getRewardCoin(){
    return Native_GetIntPronounce(PRONOUNCE_REWARD_COIN, DEFAULT_COIN_REWARD);
}
int GameManager::getContinueCoin(){
    return Native_GetIntPronounce(PRONOUNCE_CONTINUE_COIN, DEFAULT_COIN_TO_CONTINUE);
}
int GameManager::getGachaCoin(){
    return Native_GetIntPronounce(PRONOUNCE_GACHA_COIN, DEFAULT_NEED_COIN_TO_GACHA);
}
int GameManager::getPlayIntervalCoin(){
    return Native_GetIntPronounce(PRONOUNCE_REWARD_PLAY_INTERVAL, 4);
}

#pragma mark - coin

int GameManager::addCoin(int add){
    coin += add;
    totalCoin += add;
    updateAllAchievements();
    return coin;
}
int GameManager::subCoin(int sub){
    return coin -= sub;
}
int GameManager::getCoin(){
    return coin;
}


#pragma mark - chara

bool GameManager::hasCharaUnlocked(int index) {
    return isCharaUnlocked[index];
}
int GameManager::getLockedCharaCount(){
    int num = 0;
    for (int i = 0; i < CHARA_COUNT; ++i) {
        if (!hasCharaUnlocked(i)) {
            ++num;
        }
    }
    return num;
}
int GameManager::getUnlockedCharaCount(){
    return CHARA_COUNT - getLockedCharaCount();
}
bool GameManager::canPlayGacha(){
    return ((getCoin() >= getGameManager()->getGachaCoin()) &&
            (getUnlockedCharaCount() > 0));
}

//ガチャを引く
int GameManager::playGacha(){
    if (!canPlayGacha()) {
        return -1;
    }
    
    int newIndex = getIntRand(0, CHARA_COUNT - 1);

    isCharaUnlocked[newIndex] = true;
    subCoin(getGameManager()->getGachaCoin());
    
    Native_AnalyticsBuyItem(std::string("gacha_") + std::to_string(newIndex));
    updateAllAchievements();
    
    return newIndex;
}

#pragma mark - score

bool  GameManager::recordScore(double s) {
    totalLength += s;

    bool newRecord = false;
    if (s > highScore) {
        highScore = s;
        newRecord = true;
    }
    
    updateAllAchievements();
    
    return newRecord;
}

double GameManager::getHighScore() {
    return highScore;
}


#pragma mark - achievement

bool GameManager::isAchievementUnlocked(int index){
    return achievements[index];
}
int  GameManager::getUnlockedAchievementCount(){
    int c = 0;
    for (int i = 0; i < sizeof(achievements)/sizeof(achievements[0]); ++i) {
        if (isAchievementUnlocked(i)) {
            ++c;
        }
    }
    return c;
}
int  GameManager::getAllAchievementCount(){
    return ACHIEVEMENT_COUNT;
}

void GameManager::unlockAchievement(int index){
    if (achievements[index]) {
        Native_ErrorReport(std::string("duplicate achievement unlock : ") + std::to_string(index));
        return;
    }
    
    achievements[index] = true;

    //報酬コイン付与
    addCoin(s_achievementData[index].getRewardCoinNum());
    
    Native_AnalyticsUnlockAchievement(std::to_string(index));
}

bool GameManager::checkAchievementCanUnlock(int index){
    const AchievementData& data = s_achievementData[index];
        
    if (data.type == AchievementData::TYPE_FIRST) {
        return totalLength > 0;
    }
    else if (data.type == AchievementData::TYPE_LENGTH) {
        return totalLength >= data.num;
    }
    else if (data.type == AchievementData::TYPE_GIFT) {
        return hasCharaUnlocked(data.num);
    }
    else if (data.type == AchievementData::TYPE_GIFT_COMP) {
        return (100.0 * getUnlockedCharaCount() / CHARA_COUNT) >= data.num;
    }
    else if (data.type == AchievementData::TYPE_COIN) {
        return totalCoin >= data.num;
    }
    else if (data.type == AchievementData::TYPE_AD) {
        return adMovieWatchedCount >= data.num;
    }
    else if (data.type == AchievementData::TYPE_LOGIN) {
        return loginBonusDays >= data.num;
    }
    else{
        DERROR("not defined achievement type(%d)\n", data.type);
        return false;
    }
}

void GameManager::updateAllAchievements(){
    for (int i = 0; i < ACHIEVEMENT_COUNT; ++i) {
        if (!isAchievementUnlocked(i)) {
            if (checkAchievementCanUnlock(i)){
                
                //解除
                //unlockAchievement(i);
                
                //通知
                if (delegate) {
                    delegate->onCanUnlockAchievement(this, i);
                    //delegate->onUnlockAchievement(this, i);
                }

                //報酬コイン付与
                //addCoin(s_achievementData[i].getRewardCoinNum());
                //これによってまた実績が解除される場合も有る(コイン獲得額など)
            }
        }
    }
}


#pragma mark - login bonus

//1 ~ LOGIN_BONUS_COUNT が返る(0はならない)
int   GameManager::setLoginBonusGotNow(){
    if (!canGetLoginBonus(getNowDate())) {
        return -1;
    }
    
    loginBonusDays++;
    lastLoginBonusGotDate = getNowDate();

    int today = getLastGotLoginBonusIndex();
    addCoin(s_loginBonusData[today - 1].getCoinNum());
    
    updateAllAchievements();

    Native_AnalyticsSelectContent("login_bonus", loginBonusDays);
    return today;
}

//1 ~ LOGIN_BONUS_COUNT が返る(0はならない)
int    GameManager::getLastGotLoginBonusIndex(){
    if (loginBonusDays == 0) {
        return 0;
    }
    return (loginBonusDays - 1) % LOGIN_BONUS_COUNT + 1;
}
time_t GameManager::getLastGotLoginBonusDate(){
    return lastLoginBonusGotDate;
}
bool GameManager::canGetLoginBonus(time_t now){
    long today   = (now                   + (9 * 3600)) / 86400;
    long lastDay = (lastLoginBonusGotDate + (9 * 3600)) / 86400;
    return today > lastDay;
}


#pragma mark - ad 

void GameManager::addAdMovieWatchedCount() {
    ++adMovieWatchedCount;
    updateAllAchievements();
}


#pragma mark - save

bool GameManager::isSaveOk(){
    return true; //TODO: hasTutorialEnded()
}


#pragma mark - バックグラウンド関連

/**
 * バックグラウンドに入るときに行う処理
 */
void GameManager::onEnterBackground(){
    if (delegate){
        delegate->onEnterBackground(this);
    }
}

/**
 * フォアグラウンドになったときに行う処理
 */
void GameManager::onEnterForeground(){
    if (delegate) {
        delegate->onEnterForeground(this);
    }
}

/**
 * アプリが起動したときの処理
 */
void GameManager::onApplicationLaunched(){
    //対応するdelegateは存在しない
    // 通常、この段階でdelegateすべきcocos2d::Scene などのインスタンスはまだ存在していないから
}


#pragma mark - update

void GameManager::update(double dt){
    time_t now = getNowDate();
    
    lastUpdateTime = now;
}


#pragma mark - delegate

void GameManager::setDelegate(GameManagerDelegate* delegate){
    this->delegate = delegate;
}

GameManagerDelegate* GameManager::getDelegate() const{
    return delegate;
}


#pragma mark - 課金処理

//購入時に行う処理
bool GameManager::procPurchase(   std::string productId){
    if      (productId == PRODUCT_ID_1) { addCoin(ADD_COIN_OF_PURCHASE_1); }
    else if (productId == PRODUCT_ID_2) { addCoin(ADD_COIN_OF_PURCHASE_2); }
    else if (productId == PRODUCT_ID_3) { addCoin(ADD_COIN_OF_PURCHASE_3); }
    else if (productId == PRODUCT_ID_4) { addCoin(ADD_COIN_OF_PURCHASE_4); }
    else {
        std::string error = std::string("Purchased no defined productId(%s)", productId.c_str());
        DERROR("%s", error.c_str());
        Native_ErrorReport(error);
    }
    
    saveAll();
    return true;
}

//リストア時に行う処理
bool GameManager::procRestore(    std::string productId){
    return procPurchase(productId); //やる処理は同じ
}

//購入可能かの判定(ゲーム的なこと)
bool GameManager::procCanPurchase(std::string productId){
    return true; //常に全てのアイテムが購入可能
}


#pragma mark - GameItemGroupDelegate

//所持金を返す
int  GameManager::getItemMoney(GameItemGroup*) {
    return 0; //TODO
}

//所持金を減らす
int  GameManager::subItemMoney(GameItemGroup*, int itemIndex, int sub) {
    return 0; //TODO
}

//所持金を増やす
int  GameManager::addItemMoney(GameItemGroup*, int itemIndex, int add) {
    return 0; //TODO
}

//セーブ可能か
bool GameManager::isSaveItemOk(GameItemGroup*) {
    return isSaveOk();
}

//セーブさせる
bool GameManager::saveItemMoney(GameItemGroup*) {
    if (!isSaveOk()) {
        return false;
    }

    SAVE_BEGIN;

    //TODO

    SAVE_END;
    
    return true;
}


#pragma mark - SaveDataProtocol

bool GameManager::saveAll() {
    if (!isSaveOk()) {
        return false;
    }
    
    return getSaveManager()->save(this);
}

bool GameManager::loadAll() {
    if (!isSaveOk()) {
        return false;
    }
    
    return getSaveManager()->load(this);
}

const std::string GameManager::getSaveDataPrefix() {
    return "GameManager_";
}

bool GameManager::saveImpl(SaveManager*) {
    SAVE_BEGIN;
    
    SAVE_INT(coin);
    SAVE_INT(totalCoin);
    SAVE_INT(selectedCharaIndex);
    SAVE_TIME(lastUpdateTime);
    SAVE_BOOL_ARRAY(isCharaUnlocked, CHARA_COUNT);
    SAVE_DOUBLE(highScore);
    SAVE_DOUBLE(totalLength);
    SAVE_BOOL_ARRAY(achievements, ACHIEVEMENT_COUNT);
    SAVE_INT(loginBonusDays);
    SAVE_TIME(lastLoginBonusGotDate);
    SAVE_INT(adMovieWatchedCount);
    SAVE_INT(playNum);
    
    SAVE_END;
    
    return true;
}

bool GameManager::loadImpl(SaveManager*) {
    LOAD_BEGIN;

    LOAD_INT_WITH_DEF(coin, 0);
    LOAD_INT_WITH_DEF(totalCoin, 0);
    LOAD_INT_WITH_DEF(selectedCharaIndex, 0);
    LOAD_TIME_WITH_DEF(lastUpdateTime, getNowDate());
    LOAD_BOOL_ARRAY_WITH_DEF(isCharaUnlocked, CHARA_COUNT, false);
    isCharaUnlocked[0] = true; //最初のキャラだけは初めからアンロック
    LOAD_DOUBLE(highScore);
    LOAD_DOUBLE(totalLength);
    LOAD_BOOL_ARRAY_WITH_DEF(achievements, ACHIEVEMENT_COUNT, false);
    LOAD_INT(loginBonusDays);
    LOAD_TIME(lastLoginBonusGotDate);
    LOAD_INT(adMovieWatchedCount);
    LOAD_INT(playNum);
    
    LOAD_END;
    
    return true;
}


#pragma mark - func

GameManager* getGameManager() {
	return GameManager::getInstance();
}

