//
//  SaveManager.cpp
//  
//
//  Created by OtaniAtsushi1 on 2014/12/09.
//
//

#include "SaveManager.h"
#include "SaveUtils.h"
#include "Consts.h"


#pragma mark - SaveManager

SaveManager* SaveManager::getInstance() {
    static SaveManager instance;
    return &instance;
}

void SaveManager::init(){
}

bool SaveManager::save(SaveDataProtocol* protocol){

    return protocol->saveImpl(this);
}


bool SaveManager::load(SaveDataProtocol* protocol){
    
    return protocol->loadImpl(this);
}

const char *SaveManager::getFilePath(){
    return (FileUtils::getInstance()->getWritablePath() + "save.data").c_str();
}


#pragma mark - func

SaveManager* getSaveManager(){
    return SaveManager::getInstance();
}


#pragma mark - protocol

bool SaveDataProtocol::saveAll(){
    return getSaveManager()->save(this);
}

bool SaveDataProtocol::loadAll(){
    return getSaveManager()->load(this);
}

std::string SaveDataProtocol::getSaveDataKeyName(const char* name){
    return (std::string(getSaveDataPrefix()) + name);
}

