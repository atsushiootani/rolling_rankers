//
//  PurchaseManager.cpp
//  balls
//
//  Created by OtaniAtsushi1 on 2016/09/05.
//
//

#include "AndroidHelper.h"
#include "PurchaseManager.h"
#include "Native_Purchase.h"
#include "Native_Analytics.h"
#include "Debug.h"


//デバッグ用。課金全て成功
#define DEBUG_PURCHASE_ALL_SUCCESS (0 && DEBUG)

//デバッグ用。リストアするもの無し
#define DEBUG_RESTORE_ALL_SKIP     (0 && DEBUG)


#pragma mark - singleton

PurchaseManager* PurchaseManager::getInstance() {
    static PurchaseManager instance;
    return &instance;
}


#pragma mark - init

//課金の初期化処理
void PurchaseManager::init(){
    Native_PurchaseRegister(std::bind(&PurchaseManager::hasInitializedCallback,   this, std::placeholders::_1));
}


#pragma mark - public func

//課金の初期化が完了したかどうか
PurchaseManager::PurchaseStatus PurchaseManager::getPurchaseStatus(){
    bool finished = Native_PurchaseRequestHasFinished();
    bool success  = Native_PurchaseInitHasSucceeded();
    if (!finished) {
        return PURCHASE_STATUS_INITIALIZING;
    }
    else if (success) {
        return PURCHASE_STATUS_INIT_SUCCESS;
    }
    else{
        return PURCHASE_STATUS_INIT_FAILURE;
    }
}

//商品が購入できるかどうか
bool PurchaseManager::canPurchaseItem(std::string productId){
    if (!canPurchaseProc) {
        return true;
    }
    
    return canPurchaseProc(productId);
}

//購入処理
void PurchaseManager::purchaseItem(std::string productId){
    
    //購入時の処理が登録されていなければ、処理しない
    if (!purchaseProc) {
        std::string error = "no purchase Item set.";
        DERROR("%s\n", error.c_str());
        Native_ErrorReport(error);
        
        purchaseFailureCallback(productId);
        purchaseClosedCallback();
        return;
    }
    
    //analytics
    try{
        Native_AnalyticsEcommercePurchaseBegin(productId);
    }catch(...){
    }

    
#if DEBUG_PURCHASE_ALL_SUCCESS
    purchaseSuccessCallback(productId);
    purchaseClosedCallback();
    
#else
    //処理
    Native_PurchaseItem(productId,
                        std::bind(&PurchaseManager::purchaseSuccessCallback, this, std::placeholders::_1),
                        std::bind(&PurchaseManager::purchaseFailureCallback, this, std::placeholders::_1),
                        std::bind(&PurchaseManager::purchaseCancelCallback,  this, std::placeholders::_1),
                        std::bind(&PurchaseManager::purchaseClosedCallback,  this));
#endif
}

//リストアを行う
void PurchaseManager::restorePurchases(){
    //リストア時の処理が登録されていなければ、処理しない
    if (!restoreProc) {
        std::string error = "no restore Item set.";
        DERROR("%s\n", error.c_str());
        Native_ErrorReport(error);
        
        restoreFailedCallback();
        restoreClosedCallback();
        return;
    }
    
#if DEBUG_RESTORE_ALL_SKIP
    restoreCompletedCallback();
    restoreClosedCallback();
    
#else
    Native_Restore(std::bind(&PurchaseManager::restoreSucceededCallback, this, std::placeholders::_1),
                   std::bind(&PurchaseManager::restoreFailedCallback,    this),
                   std::bind(&PurchaseManager::restoreCompletedCallback, this),
                   std::bind(&PurchaseManager::restoreClosedCallback,    this));
#endif
}


#pragma mark - inner func

//課金処理の初期化が完了したコールバック
void PurchaseManager::hasInitializedCallback(bool success){
    if (delegate) {
        delegate->purchaseHasInitializedCallback(this, success);
    }
    
    //Androidのみ、リストアが常に必要
#ifdef CC_TARGET_OS_ANDROID
    restorePurchases();
#endif
}

//購入成功時のコールバック
void PurchaseManager::purchaseSuccessCallback(std::string productId){
    bool error = purchaseProc(productId);
    
    if (delegate) {
        delegate->purchaseSuccessCallback(this, productId);
    }
    
    if (!error) {
        Native_AnalyticsEcommercePurchase(productId);
    }
    else{
        Native_ErrorReport("PURCHASE", productId);
    }
}

//購入失敗時のコールバック
void PurchaseManager::purchaseFailureCallback(std::string productId){
    DPRINTF("payment failure :%s\n", productId.c_str());
    
    if (delegate) {
        delegate->purchaseFailureCallback(this, productId);
    }
}

//購入キャンセル時のコールバック
void PurchaseManager::purchaseCancelCallback(std::string productId){
    DPRINTF("payment cancel :%s\n", productId.c_str());
    
    if (delegate) {
        delegate->purchaseCancelCallback(this, productId);
    }
}

//購入終了時のコールバック
void PurchaseManager::purchaseClosedCallback(){
    DPRINTF("payment closed\n");
    
    if (delegate) {
        delegate->purchaseClosedCallback(this);
    }
}

//リストア成功時のコールバック
void PurchaseManager::restoreSucceededCallback(std::string productId) {
    //このメソッドが呼ばれたということは、復元処理が必要なので、購入時と同じ処理を行う
    bool error = restoreProc(productId);
    
    if (delegate) {
        delegate->restoreSuccessCallback(this, productId);
    }
    
    if (!error) {
        Native_AnalyticsCustom("RESTORE", "productId", productId);
    }
    else{
        Native_ErrorReport("RESTORE", productId);
    }
}

//リストア失敗時のコールバック
void PurchaseManager::restoreFailedCallback(){
    if (delegate) {
        delegate->restoreFailureCallback(this);
    }
}

//リストア完了時のコールバック
void PurchaseManager::restoreCompletedCallback() {
    if (delegate) {
        delegate->restoreCompletedCallback(this);
    }
}

//リストア終了時のコールバック
void PurchaseManager::restoreClosedCallback() {
    if (delegate) {
        delegate->restoreClosedCallback(this);
    }
}


#pragma mark - func

PurchaseManager* getPurchaseManager(){
    return PurchaseManager::getInstance();
}
