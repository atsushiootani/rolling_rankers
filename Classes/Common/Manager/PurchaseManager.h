//
//  PurchaseManager.h
//  balls
//
//  Created by OtaniAtsushi1 on 2016/09/05.
//
//

#ifndef PurchaseManager_h
#define PurchaseManager_h


#include <functional>

class PurchaseManagerDelegate;



class PurchaseManager{
    //singleton
public:
    static PurchaseManager* getInstance();
    
    //ctor
private:
    PurchaseManager()                                         {}
    PurchaseManager(            const PurchaseManager &other) {}
    PurchaseManager &operator= (const PurchaseManager &other) { return *this; }

public:
    enum PurchaseStatus {
        PURCHASE_STATUS_UNKNOWN,
        PURCHASE_STATUS_INITIALIZING,
        PURCHASE_STATUS_INIT_SUCCESS,
        PURCHASE_STATUS_INIT_FAILURE,
    };
    
    
public:
    //初期化処理
    void init();
    
    //setter
    void setDelegate(PurchaseManagerDelegate* delegate)                      { this->delegate        = delegate; }
    void setPurchaseProc(   std::function<bool(std::string)> purchaseProc)   { this->purchaseProc    = purchaseProc; }
    void setRestoreProc(    std::function<bool(std::string)> restoreProc)    { this->restoreProc     = restoreProc; }
    void setCanPurchaseProc(std::function<bool(std::string)> canPurcaseProc) { this->canPurchaseProc = canPurchaseProc; }
    
    //状態取得
    PurchaseStatus getPurchaseStatus();
    
    //購入可能か
    bool canPurchaseItem(std::string productId);
    
    //購入
    void purchaseItem(std::string productId);
    
    //リストア
    void restorePurchases();
    
    
private:
    void hasInitializedCallback(bool success);
    void purchaseSuccessCallback(std::string productId);
    void purchaseFailureCallback(std::string productId);
    void purchaseCancelCallback(std::string productId);
    void purchaseClosedCallback();
    void restoreSucceededCallback(std::string productId);
    void restoreFailedCallback();
    void restoreCompletedCallback();
    void restoreClosedCallback();
    
private:
    PurchaseManagerDelegate* delegate;
    std::function<bool(std::string)> purchaseProc;
    std::function<bool(std::string)> restoreProc;
    std::function<bool(std::string)> canPurchaseProc;
};


class PurchaseManagerDelegate{
public:
    //初期化完了時のコールバック
    virtual void purchaseHasInitializedCallback(PurchaseManager* purchaseManager, bool success) = 0;
    
    //購入成功時のコールバック
    virtual void purchaseSuccessCallback(       PurchaseManager* purchaseManager, std::string productId) = 0;
    
    //購入失敗時のコールバック
    virtual void purchaseFailureCallback(       PurchaseManager* purchaseManager, std::string productId) = 0;
    
    //購入キャンセル時のコールバック
    virtual void purchaseCancelCallback(        PurchaseManager* purchaseManager, std::string productId) = 0;
    
    //購入終了時のコールバック
    virtual void purchaseClosedCallback(        PurchaseManager* purchaseManager) = 0;
    
    //リストア成功時のコールバック
    virtual void restoreSuccessCallback(        PurchaseManager* purchaseManager, std::string productId) = 0;
    
    //リストア失敗時のコールバック
    virtual void restoreFailureCallback(        PurchaseManager* purchaseManager) = 0;
    
    //リストア完了時のコールバック
    virtual void restoreCompletedCallback(      PurchaseManager* purchaseManager) = 0;
    
    //リストア終了時のコールバック
    virtual void restoreClosedCallback(         PurchaseManager* purchaseManager) = 0;

};


#pragma mark - func

extern PurchaseManager* getPurchaseManager();


#endif /* PurchaseManager_h */
