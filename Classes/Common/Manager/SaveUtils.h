//
//  SaveUtils.h
//  balls
//
//  Created by OtaniAtsushi1 on 2016/08/27.
//
//

#ifndef SaveUtils_h
#define SaveUtils_h

#include "AndroidHelper.h"


/**
 * セーブ・ロード周り
 */

//セーブ対象を取得する(SaveManagerの実装によって変えてよい)
#define SAVE_TARGET   this

//型のチェック
#define CHECK_TYPE(a, b)   assert(typeid(a) == typeid(b))

//長さチェック
#define CHECK_GREATER_THAN(a,b)  assert((a) >= (b))


/**
 * セーブ・ロード開始時のチェック
 *   型、長さ等。実行時にアサートがかかる（本当はコンパイル時エラーを出したい）
 */

#define CHECK_ALL do{ \
} while(0)


#define GET_KEY(name)  (getSaveDataKeyName(#name).c_str())


/**
 * ロード
 */

//開始処理
#define LOAD_BEGIN    do{                                 \
    CHECK_ALL;                                            \
    UserDefault* userDefault = UserDefault::getInstance()

/**
 * 単体
 */
#define LOAD_INT_WITH_DEF(   name, def)     SAVE_TARGET->name = userDefault->getIntegerForKey(GET_KEY(name), def)
#define LOAD_FLOAT_WITH_DEF( name, def)     SAVE_TARGET->name = userDefault->getFloatForKey  (GET_KEY(name), def)
#define LOAD_DOUBLE_WITH_DEF(name, def)     SAVE_TARGET->name = userDefault->getDoubleForKey (GET_KEY(name), def)
#define LOAD_BOOL_WITH_DEF(  name, def)     SAVE_TARGET->name = userDefault->getBoolForKey   (GET_KEY(name), def)
#define LOAD_STRING_WITH_DEF(name, def)     SAVE_TARGET->name = userDefault->getStringForKey (GET_KEY(name), def)
#define LOAD_CCDATA_WITH_DEF(name, def)     SAVE_TARGET->name = userDefault->getDataForKey   (GET_KEY(name), def)
#define LOAD_LONG_WITH_DEF(  name, def)     SAVE_TARGET->name = std::stol(  userDefault->getStringForKey(GET_KEY(name), std::to_string(def)))
#define LOAD_ULONG_WITH_DEF( name, def)     SAVE_TARGET->name = std::stoul( userDefault->getStringForKey(GET_KEY(name), std::to_string(def)))
#define LOAD_LL_WITH_DEF(    name, def)     SAVE_TARGET->name = std::stoll( userDefault->getStringForKey(GET_KEY(name), std::to_string(def)))
#define LOAD_ULL_WITH_DEF(   name, def)     SAVE_TARGET->name = std::stoull(userDefault->getStringForKey(GET_KEY(name), std::to_string(def)))

#define LOAD_INT(name)                      LOAD_INT_WITH_DEF(   name, (name))
#define LOAD_FLOAT(name)                    LOAD_FLOAT_WITH_DEF( name, (name))
#define LOAD_DOUBLE(name)                   LOAD_DOUBLE_WITH_DEF(name, (name))
#define LOAD_BOOL(name)                     LOAD_BOOL_WITH_DEF(  name, (name))
#define LOAD_STRING(name)                   LOAD_STRING_WITH_DEF(name, (name))
#define LOAD_CCDATA(name)                   LOAD_CCDATA_WITH_DEF(name, (name))
#define LOAD_LONG(name)                     LOAD_LONG_WITH_DEF(  name, (name))
#define LOAD_ULONG(name)                    LOAD_ULONG_WITH_DEF( name, (name))
#define LOAD_LL(name)                       LOAD_LL_WITH_DEF(    name, (name))
#define LOAD_ULL(name)                      LOAD_ULL_WITH_DEF(   name, (name))
#define LOAD_DNUM(arg)   LOAD_ULL(arg)

//enum用
#define LOAD_ENUM_WITH_DEF(name, type, def) SAVE_TARGET->name = static_cast<type>(userDefault->getIntegerForKey(GET_KEY(name), def))
#define LOAD_ENUM(name, type)               LOAD_ENUM_WITH_DEF(name, type, 0)

//型専用
#define LOAD_TIME_WITH_DEF(name, def)       LOAD_LONG_WITH_DEF(name, def)
#define LOAD_TIME(name)                     LOAD_LONG(name)
#define LOAD_SCORE_WITH_DEF(name, def)      LOAD_DOUBLE_WITH_DEF(name, def)
#define LOAD_SCORE(name)                    LOAD_DOUBLE(name)

/**
 * 配列
 */
//汎用型配列ロード(デフォルト値あり)
#define LOAD_ARRAY_TYPE_WITH_DEF(type, name, length, def) do {                   \
    for (int i = 0; i < length; ++i) {                                           \
        std::string key = std::string(GET_KEY(name)) + std::to_string(i);        \
        SAVE_TARGET->name[i] = userDefault->get##type##ForKey(key.c_str(), def); \
    }                                                                            \
}while(0)

//汎用型配列ロード(デフォルト値なし。データがない場合は書き換えない)
#define LOAD_ARRAY_TYPE(type, name, length) do {                                                  \
    for (int i = 0; i < length; ++i) {                                                            \
        std::string key = std::string(GET_KEY(name)) + std::to_string(i);                         \
        SAVE_TARGET->name[i] = userDefault->get##type##ForKey(key.c_str(), SAVE_TARGET->name[i]); \
    }                                                                                             \
}while(0)

#define LOAD_INT_ARRAY_WITH_DEF(   name, length, def)  LOAD_ARRAY_TYPE_WITH_DEF(Integer, name, length, def)
#define LOAD_INT_ARRAY(            name, length)       LOAD_ARRAY_TYPE(         Integer, name, length)
#define LOAD_BOOL_ARRAY_WITH_DEF(  name, length, def)  LOAD_ARRAY_TYPE_WITH_DEF(Bool,    name, length, def)
#define LOAD_BOOL_ARRAY(           name, length)       LOAD_ARRAY_TYPE(         Bool,    name, length)
#define LOAD_FLOAT_ARRAY_WITH_DEF( name, length, def)  LOAD_ARRAY_TYPE_WITH_DEF(Float,   name, length, def)
#define LOAD_FLOAT_ARRAY(          name, length)       LOAD_ARRAY_TYPE(         Float,   name, length)
#define LOAD_DOUBLE_ARRAY_WITH_DEF(name, length, def)  LOAD_ARRAY_TYPE_WITH_DEF(Double,  name, length, def)
#define LOAD_DOUBLE_ARRAY(         name, length)       LOAD_ARRAY_TYPE(         Double,  name, length)
#define LOAD_STRING_ARRAY_WITH_DEF(name, length, def)  LOAD_ARRAY_TYPE_WITH_DEF(String,  name, length, def)
#define LOAD_STRING_ARRAY(         name, length)       LOAD_ARRAY_TYPE(         String,  name, length)

/*
#define LOAD_BOOL_ARRAY(name, length) do {                                        \
    std::string data = userDefault->getStringForKey(GET_KEY(name), "");           \
    int dataLen = (int)data.size();                                               \
    for (int i = 0; i < length; ++i) {                                            \
        SAVE_TARGET->name[i] = (i < dataLen && data.at(i) == '1') ? true : false; \
    }                                                                             \
} while(0)
*/

/**
 * std::vector
 */
//汎用型Vectorロード(デフォルト値必須。ロードしたものを順にpushしていくため)
#define LOAD_VECTOR_TYPE_WITH_DEF(type, trueType, name, def) do {                                  \
    SAVE_TARGET->name->clear();                                                                    \
    int count = userDefault->getIntegerForKey((std::string(GET_KEY(name)) + "_count").c_str(), 0); \
    for (int i = 0; i < count; ++i) {                                                              \
        std::string key = std::string(GET_KEY(name)) + std::to_string(i);                          \
        auto val = userDefault->get##type##ForKey(key.c_str(), def);                               \
        SAVE_TARGET->name->push_back(static_cast<trueType>(val));                                  \
    }                                                                                              \
}while(0)

#define LOAD_INT_VECTOR_WITH_DEF(   name, def)       LOAD_VECTOR_TYPE_WITH_DEF(Integer, int,         name, def)
#define LOAD_INT_VECTOR(            name)            LOAD_VECTOR_TYPE_WITH_DEF(Integer, int,         name, 0)
#define LOAD_ENUM_VECTOR_WITH_DEF(  name, type, def) LOAD_VECTOR_TYPE_WITH_DEF(Integer, type,        name, def)
#define LOAD_ENUM_VECTOR(           name, type)      LOAD_VECTOR_TYPE_WITH_DEF(Integer, type,        name, 0)
#define LOAD_BOOL_VECTOR_WITH_DEF(  name, def)       LOAD_VECTOR_TYPE_WITH_DEF(Bool,    bool,        name, def)
#define LOAD_BOOL_VECTOR(           name)            LOAD_VECTOR_TYPE_WITH_DEF(Bool,    bool,        name, false)
#define LOAD_FLOAT_VECTOR_WITH_DEF( name, def)       LOAD_VECTOR_TYPE_WITH_DEF(Float,   float,       name, def)
#define LOAD_FLOAT_VECTOR(          name)            LOAD_VECTOR_TYPE_WITH_DEF(Float,   float,       name, 0.f)
#define LOAD_DOUBLE_VECTOR_WITH_DEF(name, def)       LOAD_VECTOR_TYPE_WITH_DEF(Double,  double,      name, def)
#define LOAD_DOUBLE_VECTOR(         name)            LOAD_VECTOR_TYPE_WITH_DEF(Double,  double,      name, 0.0)
#define LOAD_STRING_VECTOR_WITH_DEF(name, def)       LOAD_VECTOR_TYPE_WITH_DEF(String,  std::string, name, def)
#define LOAD_STRING_VECTOR(         name)            LOAD_VECTOR_TYPE_WITH_DEF(String,  std::string, name, "")

//終了処理
#define LOAD_END       } while(0)




/**
 * セーブ
 */

//開始処理
#define SAVE_BEGIN do {                                   \
    CHECK_ALL;                                            \
    UserDefault* userDefault = UserDefault::getInstance()

/**
 * 単体
 */
#define SAVE_INT(name)    userDefault->setIntegerForKey(GET_KEY(name), SAVE_TARGET->name)
#define SAVE_FLOAT(name)  userDefault->setFloatForKey  (GET_KEY(name), SAVE_TARGET->name)
#define SAVE_DOUBLE(name) userDefault->setDoubleForKey (GET_KEY(name), SAVE_TARGET->name)
#define SAVE_BOOL(name)   userDefault->setBoolForKey   (GET_KEY(name), SAVE_TARGET->name)
#define SAVE_STRING(name) userDefault->setStringForKey (GET_KEY(name), SAVE_TARGET->name)
#define SAVE_CCDATA(name) userDefault->setDataForKey   (GET_KEY(name), SAVE_TARGET->name)
#define SAVE_LONG(name)   userDefault->setStringForKey (GET_KEY(name), std::to_string(SAVE_TARGET->name))
#define SAVE_ULONG(name)  userDefault->setStringForKey (GET_KEY(name), std::to_string(SAVE_TARGET->name))
#define SAVE_LL(name)     userDefault->setStringForKey (GET_KEY(name), std::to_string(SAVE_TARGET->name))
#define SAVE_ULL(name)    userDefault->setStringForKey (GET_KEY(name), std::to_string(SAVE_TARGET->name))
#define SAVE_DNUM(arg)    SAVE_ULL(arg)

#define SAVE_ENUM(name, type)  SAVE_INT(name)

#define SAVE_TIME(name)   SAVE_LONG(name)
#define SAVE_SCORE(name)  SAVE_DOUBLE(name)

/**
 * 配列
 */
#define SAVE_ARRAY_TYPE(type, name, length) do{                            \
    for (int i = 0; i < length; ++i) {                                     \
        std::string key = std::string(GET_KEY(name)) + std::to_string(i);  \
        userDefault->set##type##ForKey(key.c_str(), SAVE_TARGET->name[i]); \
    }                                                                      \
} while(0)

#define SAVE_INT_ARRAY(   name, length)   SAVE_ARRAY_TYPE(Integer, name, length)
#define SAVE_BOOL_ARRAY(  name, length)   SAVE_ARRAY_TYPE(Bool,    name, length)
#define SAVE_FLOAT_ARRAY( name, length)   SAVE_ARRAY_TYPE(Float,   name, length)
#define SAVE_DOUBLE_ARRAY(name, length)   SAVE_ARRAY_TYPE(Double,  name, length)
#define SAVE_STRING_ARRAY(name, length)   SAVE_ARRAY_TYPE(String,  name, length)

/*
#define SAVE_BOOL_ARRAY(name, length) do {                \
    std::string data = "";                                \
    for (int i = 0; i < length; ++i) {                    \
        data += SAVE_TARGET->name[i] ? "1" : "0";         \
    }                                                     \
    userDefault->setStringForKey(GET_KEY(name), data);    \
} while(0)
 */

/**
 * std::vector
 */
#define SAVE_VECTOR_TYPE(type, name) do {                                                  \
    int count = (int)SAVE_TARGET->name->size();                                            \
    userDefault->setIntegerForKey((std::string(GET_KEY(name)) + "_count").c_str(), count); \
    for (int i = 0; i < count; ++i) {                                                      \
        std::string key = std::string(GET_KEY(name)) + std::to_string(i);                  \
        userDefault->set##type##ForKey(key.c_str(), SAVE_TARGET->name->at(i));             \
    }                                                                                      \
}while(0)

#define SAVE_INT_VECTOR(name)         SAVE_VECTOR_TYPE(Integer, name)
#define SAVE_BOOL_VECTOR(name)        SAVE_VECTOR_TYPE(Bool,    name)
#define SAVE_ENUM_VECTOR(name, type)  SAVE_VECTOR_TYPE(Integer, name)



//終了処理
#define SAVE_END         userDefault->flush(); } while(0)



/**
 * 削除
 */

//開始処理
#define DELETE_BEGIN do {                                 \
    CHECK_ALL;                                            \
    UserDefault* userDefault = UserDefault::getInstance()

//削除。すべてのデータ型に対応
#define DELETE_VALUE(name)           userDefault->deleteValueForKey(GET_KEY(name))

//終了処理
#define DELETE_END   } while(0)



#endif /* SaveUtils_h */
