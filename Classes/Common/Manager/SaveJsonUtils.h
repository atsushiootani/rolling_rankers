//
//  SaveJsonUtils.h
//  
//
//  Created by OtaniAtsushi1 on 2016/10/22.
//
//

#ifndef SaveJsonUtils_h
#define SaveJsonUtils_h


#include "picojson.h"


/**
 * JSONとの変換
 */


//解釈中のオブジェクト名
#define JSON_OBJECT jsonObject
#define JSON_VALUE  jsonValue

//Jsonオブジェクトの型
#define JSON_t   picojson::value


//JSONをstringで取得
inline std::string getStringFromJson(const JSON_t& json) {
    return picojson::value(json).serialize();
}

//stringをJSONに変換
inline JSON_t getJsonFromString(const std::string& str) {
    JSON_t json;
    const char* cJsonString = str.c_str();
    picojson::parse(json, cJsonString, cJsonString + strlen(cJsonString));
    return json;
}

//正当なJSONか
inline bool isValidJson(const JSON_t& json){
    return !json.is<picojson::null>();
}

/**
 * セーブ
 */

//セーブ開始
#define BEGIN_ADD_TO_JSON_WITH(_json)        do{ \
  picojson::object JSON_OBJECT; \
  JSON_t&          JSON_VALUE  = _json; \

//変数名と値を直接指定
#define ADD_VARIABLE_TO_JSON(name, variable) do { JSON_OBJECT[#name] = picojson::value(variable); } while (0)

//プリミティブ型をJSONに追加
#define ADD_VALUE_TO_JSON(type, name)  do {           \
  JSON_OBJECT[#name] = picojson::value((type)(name)); \
} while(0)
#define ADD_NUMBER_VALUE_TO_JSON(name)  ADD_VALUE_TO_JSON(double,      name)
#define ADD_STRING_VALUE_TO_JSON(name)  ADD_VALUE_TO_JSON(std::string, name)

//配列型をJSONに追加
#define ADD_ARRAY_TO_JSON(type, name)  do {                \
  picojson::array a;                                       \
  for (int i = 0; i < sizeof(name)/sizeof(name[0]); ++i){  \
    a.push_back(picojson::value((type)(name[i])));         \
  }                                                        \
  JSON_OBJECT[#name] = picojson::value(a);                 \
} while(0)
#define ADD_NUMBER_ARRAY_TO_JSON(name)  ADD_ARRAY_TO_JSON(double,      name)
#define ADD_STRING_ARRAY_TO_JSON(name)  ADD_ARRAY_TO_JSON(std::string, name)

//std::vector型をJSONに追加
#define ADD_VECTOR_TO_JSON(type, name, var) do {           \
  picojson::array a;                                       \
  for (auto it = (var).begin(); it != (var).end(); ++it) { \
    a.push_back(picojson::value((type)(*it)));             \
  }                                                        \
  JSON_OBJECT[#name] = picojson::value(a);                 \
} while(0)
#define ADD_NUMBER_VECTOR_TO_JSON(name)   ADD_VECTOR_TO_JSON(double,      name,  name)
#define ADD_STRING_VECTOR_TO_JSON(name)   ADD_VECTOR_TO_JSON(std::string, name,  name)
#define ADD_NUMBER_PVECTOR_TO_JSON(name)  ADD_VECTOR_TO_JSON(double,      name, *name)
#define ADD_STRING_PVECTOR_TO_JSON(name)  ADD_VECTOR_TO_JSON(std::string, name, *name)

//std::map型をJSONに追加
#define ADD_MAP_TO_JSON(type, name, var) do {                   \
  picojson::object o;                                           \
  for (auto it = (var).begin(); it != (var).end(); ++it){       \
    o[it->first] = picojson::value((type)(it->second));         \
  }                                                             \
  JSON_OBJECT[#name] = picojson::value(o);                      \
} while(0)
#define ADD_NUMBER_MAP_TO_JSON(name)   ADD_MAP_TO_JSON(double,         name,  name)
#define ADD_STRING_MAP_TO_JSON(name)   ADD_MAP_TO_JSON(std::to_string, name,  name)
#define ADD_NUMBER_PMAP_TO_JSON(name)  ADD_MAP_TO_JSON(double,         name, *name)
#define ADD_STRING_PMAP_TO_JSON(name)  ADD_MAP_TO_JSON(std::to_string, name, *name)

//サブオブジェクトをJSON化して追加
#define ADD_JSON_TO_JSON(name, variable) do { \
  JSON_t j;                                   \
  (variable)->makeForSaveJsonData(j);         \
  JSON_OBJECT[#name] = j;                     \
} while (0)
#define ADD_SUB_JSON_TO_JSON(name)   ADD_JSON_TO_JSON(name, name)

//セーブ終了
#define END_ADD_TO_JSON                      \
  JSON_VALUE = picojson::value(JSON_OBJECT); \
} while (0)



/**
 * ロード
 */

//ロード開始
#define BEGIN_FETCH_FROM_JSON_WITH(json) do { \
  const JSON_t& JSON_OBJECT = json;

//変数名と値を直接指定
#define GET_VALUE_FROM_JSON(type, name, def) ((JSON_OBJECT.is<picojson::object>() && JSON_OBJECT.get(#name).is<type>()) ? (JSON_OBJECT.get(#name).get<type>()) : def)
#define GET_NUMBER_VALUE_FROM_JSON(name)     GET_VALUE_FROM_JSON(double,      name,  0)
#define GET_STRING_VALUE_FROM_JSON(name)     GET_VALUE_FROM_JSON(std::string, name, "")
#define GET_JSON_FROM_JSON(name)             ((JSON_OBJECT.is<picojson::object>()) ? (JSON_OBJECT.get(#name)) : JSON_t())

//プリミティブ型のロード
#define FETCH_VALUE_FROM_JSON(type, name) do {   \
  if (JSON_OBJECT.is<picojson::object>()) {      \
    picojson::value v = JSON_OBJECT.get(#name);  \
    if (v.is<type>()) {                          \
      name = v.get<type>();                      \
    }                                            \
  }                                              \
} while(0)
#define FETCH_NUMBER_VALUE_FROM_JSON(name)  FETCH_VALUE_FROM_JSON(double,      name)
#define FETCH_STRING_VALUE_FROM_JSON(name)  FETCH_VALUE_FROM_JSON(std::string, name)

//配列型のロード
#define FETCH_ARRAY_FROM_JSON(type, name) do {         \
  if (JSON_OBJECT.is<picojson::object>()) {            \
    picojson::value v = JSON_OBJECT.get(#name);        \
    if (v.is<picojson::array>()) {                     \
      picojson::array a = v.get<picojson::array>();    \
      int i = 0;                                       \
      for (auto it = a.begin(); it != a.end(); ++it) { \
        name[i++] = it->get<type>();                   \
      }                                                \
    }                                                  \
  }                                                    \
} while(0)
#define FETCH_NUMBER_ARRAY_FROM_JSON(name) FETCH_ARRAY_FROM_JSON(double,      name)
#define FETCH_STRING_ARRAY_FROM_JSON(name) FETCH_ARRAY_FROM_JSON(std::string, name)

//vectorのロード
#define FETCH_VECTOR_FROM_JSON(type, name, var) do {   \
  (var).clear();                                       \
  if (JSON_OBJECT.is<picojson::object>()) {            \
    picojson::value v = JSON_OBJECT.get(#name);        \
    if (v.is<picojson::array>()) {                     \
      picojson::array a = v.get<picojson::array>();    \
      for (auto it = a.begin(); it != a.end(); ++it) { \
        (var).push_back(it->get<type>());              \
      }                                                \
    }                                                  \
  }                                                    \
} while(0)
#define FETCH_NUMBER_VECTOR_FROM_JSON(name)   FETCH_VECTOR_FROM_JSON(double,      name,  name)
#define FETCH_STRING_VECTOR_FROM_JSON(name)   FETCH_VECTOR_FROM_JSON(std::string, name,  name)
#define FETCH_NUMBER_PVECTOR_FROM_JSON(name)  FETCH_VECTOR_FROM_JSON(double,      name, *name)
#define FETCH_STRING_PVECTOR_FROM_JSON(name)  FETCH_VECTOR_FROM_JSON(std::string, name, *name)

//map型のロード(mapのキー型はstd::stringであること。JSONの仕様のため)
#define FETCH_MAP_FROM_JSON(valueType, name, var) do { \
  (var).clear();                                       \
  if (JSON_OBJECT.is<picojson::object>()) {            \
    picojson::value v = JSON_OBJECT.get(#name);        \
    if (v.is<picojson::object>()) {                    \
      picojson::object o = v.get<picojson::object>();  \
      for (auto it = o.begin(); it != o.end(); ++it) { \
        std::string     key   = it->first;             \
        picojson::value value = it->second;            \
        (var)[key] = value.get<valueType>();           \
      }                                                \
    }                                                  \
  }                                                    \
} while(0)
#define FETCH_NUMBER_MAP_FROM_JSON(name)  FETCH_MAP_FROM_JSON(double,      name,  name)
#define FETCH_STRING_MAP_FROM_JSON(name)  FETCH_MAP_FROM_JSON(std::string, name,  name)
#define FETCH_NUMBER_PMAP_FROM_JSON(name) FETCH_MAP_FROM_JSON(double,      name, *name)
#define FETCH_STRING_PMAP_FROM_JSON(name) FETCH_MAP_FROM_JSON(std::string, name, *name)

//サブオブジェクトのJSONを反映
#define FETCH_JSON_FROM_JSON(name, variable) do {              \
  if (JSON_OBJECT.is<picojson::object>()) {                    \
    (variable)->reflectLoadedJsonData(JSON_OBJECT.get(#name)); \
  }                                                            \
} while (0)
#define FETCH_SUB_JSON_FROM_JSON(name)  FETCH_JSON_FROM_JSON(name, name)


//ロード終了
#define END_FETCH_FROM_JSON  } while(0)





#endif /* SaveJsonUtils_h */
