//
//  SceneManager.cpp
//
//
//  Created by OtaniAtsushi1 on 2015/03/21.
//
//
#include "SceneManager.h"
#include "GameManager.h"
#include "Native.h"
#include "Sound.h"
#include "GameData.h"
#include "SoundData.h"
#include "TextData.h"
#include "ResourceData.h"
#include "GameHelper.h"
#include "CocosHelper.h"
#include "Screen.h"
#include "Debug.h"


#pragma mark - instance

SceneManager *SceneManager::_instance = nullptr;


#pragma mark - singleton

SceneManager *SceneManager::getInstance() {
    if (!_instance) {
        _instance = new SceneManager();
    }
    return _instance;
}


#pragma mark - ctor/dtor

//ctor
SceneManager::SceneManager()
: context(nullptr)
, sceneValues()
, fadeLayer(nullptr)
, needsToShowAchievement()
, touchListenerMap()
{
}


#pragma mark - context

void SceneManager::setContext(Node* context){
    this->context = context;
}

Node* SceneManager::getContext(){
    return context;
}


#pragma mark - scene value

void SceneManager::setSceneValue(SceneFlag sceneFlag, int val){
    sceneValues[sceneFlag] = val;
}
void SceneManager::unsetSceneValue(SceneFlag sceneFlag){
    sceneValues[sceneFlag] = 0;
}
int SceneManager::getSceneValue(SceneFlag sceneFlag){
    return sceneValues[sceneFlag];
}
int SceneManager::popSceneValue(SceneFlag sceneFlag){
    int val = getSceneValue(sceneFlag);
    unsetSceneValue(sceneFlag);
    return val;
}


#pragma mark - fade

void SceneManager::unregisterFade() {
    SAFE_REMOVE_FROM_PARENT(fadeLayer);
}

void SceneManager::registerFade(Node* node){
    fadeLayer = createLayerColor(node, Color4B::BLACK, 0, Size(SCREEN_WIDTH * 2, SCREEN_HEIGHT * 2), Vec2::ANCHOR_MIDDLE); //なんのnodeについていても必ず画面全体を覆う感じ
    //fadeLayer->setGlobalZOrder(zGlobalOrder);
    fadeLayer->setOpacity(0);
}

void SceneManager::fadeIn(float duration, Color3B color /*= Color3B::BLACK*/){
    fadeLayer->setColor(color);
    fadeLayer->setOpacity(255);
    fadeLayer->runAction(FadeOut::create(duration));
}

void SceneManager::fadeOut(float duration, Color3B color /*= Color3B::BLACK*/){
    fadeLayer->setColor(color);
    fadeLayer->setOpacity(0);
    fadeLayer->runAction(FadeIn::create(duration));
}

void SceneManager::fadeOn(Color3B color /*= Color3B::BLACK*/, int opacity/* = 255*/){
    fadeLayer->setColor(color);
    fadeLayer->setOpacity(opacity);
}

void SceneManager::fadeOff(){
    fadeLayer->setOpacity(0);
}


#pragma mark - ポップアップ

void SceneManager::registerPopupTopNode(Node* popupNode){
    popupTopNode = popupNode;
}

//表示すべき実績があれば、ポップアップ表示
int SceneManager::checkAndShowPopupAchievement(std::function<void()> drawRefreshProc){
    
    if (needsToShowAchievement.size() <= 0) {
        fadeOff();
        return -1;
    }
    
    int index = needsToShowAchievement.front();
    while (getGameManager()->isAchievementUnlocked(index)) {
        needsToShowAchievement.erase(needsToShowAchievement.begin());
        if (needsToShowAchievement.size() <= 0) {
            fadeOff();
            return -1;
        }
        index = needsToShowAchievement.front();
    }

    //表示
    showPopupAchievement(index, drawRefreshProc);
    
    
    return index;
}

//指定の実績をポップアップ表示
void SceneManager::showPopupAchievement(int index, std::function<void()> drawRefreshProc){
    
    //裏を暗くし、操作を止める
    fadeOn(Color3B::BLACK, 192);
    auto touchListener = stopTouchEventListenerUnder(fadeLayer);
    
    delayedExecution(context, 0.5f, [=](){
    achievementButton = createOneMenu(popupTopNode, "achievement_plate.png",
                                      [=](Ref* pSender){
                                          
                                          playEffect(SE_COIN);
                                          
                                          auto data = s_achievementData[index];
                                          std::string text = string("報酬として") + getStringFromNumber(data.getRewardCoinNum(), true) + "コインを手に入れました！";
                                          Native_AlertView("", text, "OK", [=](int){
                                              playEffect(SE_OK);
                                              
                                              delayedExecution(popupTopNode, 0.1f, [=](){
                                                  auto item = achievementButton->getItem(0);
                                                  item->runAction(Sequence::create(getDialogCloseScaleAction(item),
                                                                                   CallFunc::create([=](){
                                                      
                                                      SAFE_REMOVE_FROM_PARENT(achievementButton);
                                                      SAFE_REMOVE_FROM_PARENT(achievementLabel);
                                                      restartTouchEventListenerUnder(fadeLayer, touchListener);
                                                      
                                                      //ここで実際に付与
                                                      getGameManager()->unlockAchievement(index);
                                                      getGameManager()->saveAll();
                                                      drawRefreshProc();
                                                      
                                                      //まだあるなら次へ
                                                      if (needsToShowAchievement.size() > 0) {
                                                          checkAndShowPopupAchievement(drawRefreshProc);
                                                      }
                                                      else{
                                                          fadeOff();
                                                      }
                                                  }),
                                                                                   nullptr));
                                              });
                                          });
                                      },
                                      1000, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(popupTopNode));
    auto item = achievementButton->getItemImage(0);
    item->setAutoSelected(false); //タップした時にスケーリングしない
    
    auto parent = item->getNormalImage();
    Size itemSize = parent->getContentSize();
    
    std::string iconName = getResrouceAchievementIconName(true, s_achievementData[index].type);
    auto icon = createSprite(parent, iconName, 1, Vec2::ANCHOR_MIDDLE, Vec2(itemSize.width - itemSize.height / 2, itemSize.height / 2));
    
    float width = itemSize.width - icon->getContentSize().width - 20;
    auto title = createTTFLabel(parent, FONT_TEXT_NAME, "", 48, TextHAlignment::CENTER, 2, Vec2::ANCHOR_MIDDLE, Vec2(365, 120));
    setLabelWidth(title, getTextAchievementTitle(index), width);
    auto desc  = createTTFLabel(parent, FONT_TEXT_NAME, "", 32, TextHAlignment::CENTER, 2, Vec2::ANCHOR_MIDDLE, Vec2(365,60));
    setLabelWidth(desc, getTextAchievementDesc(index), width);
    
    achievementLabel = createTTFLabel(popupTopNode, FONT_BOLD_TEXT_NAME, "実績達成！", 36, TextHAlignment::CENTER, 100, Vec2::ANCHOR_MIDDLE_BOTTOM, getLocalCenterPos(popupTopNode) + Vec2(0, itemSize.height * 0.7));
    achievementLabel->setColor(Color3B::YELLOW);
    
    item->runAction(getDialogOpenScaleAction(item));
    
    playEffect(SE_GACHA_GET);
    });
}

//表示すべき実績情報を追加
bool SceneManager::addToNeedAchivementIndex(int index){
    needsToShowAchievement.push_back(index);
    return true;
}


#pragma mark - タッチ

void SceneManager::stopTouchEvent(Node* node){
    auto listener = createSingleTouchListener(node,    //nodeに付けたタッチリスナーが、
                                              [](Touch* touch, Event* event){return true;}, //すべてのタッチを受け入れ、
                                              nullptr, nullptr, nullptr,
                                              true,
                                              kEventListenerPriority_SceneManager);
    touchListenerMap[node] = listener;
}

void SceneManager::restartTouchEvent(Node* node){
    auto listener = touchListenerMap[node];
    if (listener) {
        destroySingleTouchListener(listener, node);
        touchListenerMap.erase(node);
    }
}


#pragma mark - dialog action

//ダイアログが開くときのスケールアニメーション
FiniteTimeAction* SceneManager::getDialogOpenScaleAction(Node* node){
    return Sequence::create(CallFunc::create([=](){ node->setScale(0.01f); }),
                            ScaleTo::create(0.08f, 1.12f),
                            ScaleTo::create(0.08f, 0.9f),
                            ScaleTo::create(0.05f, 1.05f),
                            ScaleTo::create(0.04f, 0.98f),
                            ScaleTo::create(0.03f, 1.f),
                            nullptr);
}

//ダイアログが閉じる時のスケールアニメーション
//  完全に消えはしないので、アニメーション終了後にすぐ消すか非表示にすること
FiniteTimeAction* SceneManager::getDialogCloseScaleAction(Node* node){
    return Sequence::create(ScaleTo::create(0.13f, 1.08f),
                            ScaleTo::create(0.13f, 0.05f),
                            CallFunc::create([=](){ node->setVisible(false); }),
                            nullptr);
}

//シェイクアクション
FiniteTimeAction* SceneManager::getShakeAction(Node* node){
    float RATE = 0.8f;

    float duration = 0.05f * RATE;
    Vec2 startPos = node->getPosition();
    return Sequence::create(MoveTo::create(duration, startPos + Vec2(12, 12) * RATE),
                            MoveTo::create(duration, startPos + Vec2( 5, -5) * RATE),
                            MoveTo::create(duration, startPos + Vec2(-4, 10) * RATE),
                            MoveTo::create(duration, startPos + Vec2( 6,  9) * RATE),
                            MoveTo::create(duration, startPos + Vec2(-2,-11) * RATE),
                            MoveTo::create(duration, startPos),
                            nullptr);
}


#pragma mark - ss

void SceneManager::takeScreenShot(string spriteName, std::function<void(const std::string& fullpath)> callback){
    auto renderTarget = createSprite(nullptr, spriteName, 1, Vec2::ZERO, Vec2::ZERO);
    std::string fname = std::string("screenshot_") + std::to_string(clock()) + ".png";
    renderNodeAndSaveToFileAsPng(renderTarget, fname, [=](RenderTexture*, const std::string& fullpath){
        if (callback) {
            callback(fullpath);
        }
    });
}


#pragma mark - 実績解除


#pragma mark - func

SceneManager* getSceneManager(){
    return SceneManager::getInstance();
}
