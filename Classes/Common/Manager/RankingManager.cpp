//
//  RankingManager.cpp
//
//
//  Created by OtaniAtsushi1 on 2016/11/04.
//
//

#include "RankingManager.h"
#include "SimpleAudioEngine.h"
#include "AndroidHelper.h"
#include "Debug.h"
#include "picojson.h"
#include "GameHelper.h" //fot getNowDate()
#include "Native.h"


#pragma mark - RankingHandler

#if ENABLE_RANKING

class RankingHandler_UpdateRanking: public RankersBridge::APIHandler{
public:
    
    RankingHandler_UpdateRanking(std::function<void(const char* json)> onSucceeded, std::function<void(RankersBridge::APIResponseType responseType, const char* json, const char* error_message)> onFailed)
    : onSucceeded(onSucceeded)
    , onFailed(onFailed)
    {
    }
    
    
    virtual void handler(RankersBridge::APIResponseType responseType, const char *json, const char *error_message) override{
        if (json) {
            DPRINTF("json = %s\n", json);
        }
        if (error_message) {
            DPRINTF("error = %s\n", error_message);
        }

        if(RankersBridge::APIResponseType::SucessResponse == responseType){
            // アプリで用意した大会動線を表示してください。
            onSucceeded(json);
        }
        else {
            // エラーが発生した場合
            // 詳しい情報は RankersBridge/RankersBridge.h の `RankersBridge::APIResponseType` の定義に書かれています。
            // 必要に応じて再度大会情報を取得してください。
            
            onFailed(responseType, json, error_message);
        }
    }
    
    std::function<void(const char* json)> onSucceeded;
    std::function<void(RankersBridge::APIResponseType responseType, const char* json, const char* error_message)> onFailed;
    
};
#endif


#pragma mark - RankingManager

RankingManager* RankingManager::getInstance(){
    static RankingManager s_instance;
    return &s_instance;
}

void RankingManager::_init(){
    canFetchRankingInfo = false;
    isRankingPlaying = false;
    onStartGameCallback = nullptr;
    onQuitGameCallback = nullptr;
    delegate = nullptr;
}

//大会初期化
bool RankingManager::registerRanking(int version /*= 1*/){
#if ENABLE_RANKING
    RankersBridge::setup(RANKERS_SDK_ID, RANKERS_SDK_URL, version);
    RankersBridge::set_delegate(this);
    
    return true;
#else
    return false;
#endif
}

// 大会情報を取得する
bool RankingManager::fetchRankingInfo(){
#if ENABLE_RANKING

    static RankingHandler_UpdateRanking handler(std::bind(&RankingManager::onFetchSucceeded, this, std::placeholders::_1),
                                                std::bind(&RankingManager::onFetchFailed, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

    RankersBridge::update_competition_with_handler(&handler);
    
    return true;
#else 
    return false;
#endif
}

void RankingManager::onFetchSucceeded(const char* json){
    //成功時の処理
    //DPRINTF(json);
    
    canFetchRankingInfo = true;
    
    try{
        std::string jsonString = json;
        const char* json2 = jsonString.c_str();
        
        picojson::value jsonData;
        picojson::parse(jsonData, json2, json2 + strlen(json2));

        bool success = (jsonData.is<picojson::object>() &&
                        jsonData.get("success").is<bool>() &&
                        jsonData.get("success").get<bool>());
        
        if (success) {
            picojson::value c = jsonData.get("competition");
            if (c.is<picojson::object>()){
                startRankingUnixtime = (time_t)(c.get("start_at_unixtime").is<double>() ?
                                                c.get("start_at_unixtime").get<double>() : 0);
                endRankingUnixtime   = (time_t)(c.get(  "end_at_unixtime").is<double>() ?
                                                c.get(  "end_at_unixtime").get<double>() : 0);
            }
        }
        
    } catch (...){
        Native_ErrorReport("RANKERS json parse exception");
        canFetchRankingInfo = false;
        //isRankingOpen = false;
    }
}

void RankingManager::onFetchFailed(RankersBridge::APIResponseType responseType, const char* json, const char* error_message){
    //失敗時の処理
    canFetchRankingInfo = false;
    //isRankingOpen = false;
}


// アプリで用意した大会動線を押下した際に RANKERS の大会画面を表示します
bool RankingManager::showRankingView(std::function<void()> onStartGameCallback, std::function<void()> onQuitGameCallback/*= nullptr*/){
#if ENABLE_RANKING
    RankersBridge::show_competition();
    
    this->onStartGameCallback = onStartGameCallback;
    this->onQuitGameCallback = onQuitGameCallback;
    
    return true;
#else
    return false;
#endif
}

bool RankingManager::canRankingEntry(time_t time){
#if ENABLE_RANKING
    return canFetchRankingInfo && (startRankingUnixtime <= time && time <= endRankingUnixtime);
    //return isRankingOpen;
#else
    return false;
#endif
}

bool RankingManager::canRankingEntryNow(){
    return canRankingEntry(getNowDate());
}

bool RankingManager::isRankingPlayingNow(){
#if ENABLE_RANKING
    return isRankingPlaying;
#else
    return false;
#endif
}


bool RankingManager::recordScore(int score, std::string scoreString, std::function<void()> onRetryGameCallback, std::function<void()> onQuitGameCallback/*= nullptr*/){
#if ENABLE_RANKING
    static RankingHandler_UpdateRanking handler([=](const char*json){
        //成功時の処理
        isRankingPlaying = false;
    }, [=](RankersBridge::APIResponseType responseType, const char* json, const char* error_message){
        //失敗時の処理
        isRankingPlaying = false;
    });

    RankersBridge::post_score(score, scoreString.c_str(), &handler);
    
    this->onStartGameCallback = onRetryGameCallback;
    this->onQuitGameCallback = onQuitGameCallback;
    
    return true;
#else
    return false;
#endif
}

void RankingManager::setDelegate(RankingManagerDelegate* d){
    delegate = d;
}
void RankingManager::unsetDelegate(){
    delegate = nullptr;
}
RankingManagerDelegate* RankingManager::getDelegate(){
#if ENABLE_RANKING
    return delegate;
#else
    return nullptr;
#endif
}


#pragma mark - RankersBridge::Delegate

#if ENABLE_RANKING

// 大会開始時に呼び出されます
// 大会用の設定でゲームを開始してください。
void RankingManager::startGame(const char *json){
#if ENABLE_RANKING
    isRankingPlaying = true;
    
    if (onStartGameCallback) {
        onStartGameCallback();
        onStartGameCallback = nullptr;
    }
    onQuitGameCallback = nullptr;
#endif
}

// 大会情報画面を開いた際に呼び出されます。
void RankingManager::didCompetitionAppear(){
#if ENABLE_RANKING
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
    cocos2d::Director::getInstance()->stopAnimation();
    cocos2d::Director::getInstance()->pause();
#endif
}

// 大会情報画面を閉じてゲームに戻る際に呼び出されます。
void RankingManager::didCompetitionDisappear(){
#if ENABLE_RANKING
    //startGameの後に呼ばれる
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    cocos2d::Director::getInstance()->startAnimation();
    cocos2d::Director::getInstance()->resume();
    
    onStartGameCallback = nullptr;
    if (onQuitGameCallback) {
        onQuitGameCallback();
        onQuitGameCallback = nullptr;
    }
#endif
}

// 動画（ユーザが投稿した動画など）が再生される際に呼び出されます。
void RankingManager::didStartMovie(){
#if ENABLE_RANKING
#endif
}

// 動画（ユーザが投稿した動画など）が終了される際に呼び出されます。
void RankingManager::didStopMovie(){
#if ENABLE_RANKING
#endif
}

#endif


#pragma mark - getter method

RankingManager* getRankingManager(){
    return RankingManager::getInstance();
}
