//
//  SceneManager.h
//
//
//  Created by OtaniAtsushi1 on 2015/03/21.
//
//

#ifndef SceneManager_h
#define SceneManager_h

#include "cocos2d.h"
#include "Consts.h"
#include <map>


USING_NS_CC;

class SceneManager {
private:
    static SceneManager *_instance;
    SceneManager();
    
public:
    static SceneManager *getInstance();
    

public:
    //シーンをまたいでやり取りする情報
    enum SceneFlag{
        SCENE_FLAG_NONE = 0,
        
        
        //フラグ総数
        SCENE_FLAG_COUNT,
    };
    
public:
    
    //context
    void setContext(Node* context);
    Node* getContext();
    
    //シーンをまたぐフラグ
    void setSceneValue(SceneFlag sceneFlag, int val);
    void unsetSceneValue(SceneFlag sceneFlag);
    int getSceneValue(SceneFlag sceneFlag);
    int popSceneValue(SceneFlag sceneFlag);
    
    //フェード
    void unregisterFade();
    void registerFade(Node* node);
    void fadeIn( float duration, Color3B color = Color3B::BLACK);
    void fadeOut(float duration, Color3B color = Color3B::BLACK);
    void fadeOn( Color3B color = Color3B::BLACK, int opacity = 255);
    void fadeOff();
    
    //ポップアップ
    void registerPopupTopNode(Node* popupNode);
    int  checkAndShowPopupAchievement(std::function<void()> drawRefreshProc);
    void showPopupAchievement(int index, std::function<void()> drawRefreshProc);
    bool addToNeedAchivementIndex(int index);

    //タッチ
    void stopTouchEvent(Node* node);
    void restartTouchEvent(Node* node);
    
    //汎用アクション
    FiniteTimeAction* getDialogOpenScaleAction(Node* node);
    FiniteTimeAction* getDialogCloseScaleAction(Node* node);
    FiniteTimeAction* getShakeAction(Node* node);
    
    //スクショ
    void takeScreenShot(std::string spriteName, std::function<void(const std::string& fullpath)> callback);

    
private:
    Node* context;
    LayerColor* fadeLayer;
    Node* popupTopNode;
    
    int sceneValues[SCENE_FLAG_COUNT];
    
    
    //実績
    Menu* achievementButton;
    Label* achievementLabel;
    std::vector<int> needsToShowAchievement;
    
    std::map<Node*, EventListenerTouchOneByOne*> touchListenerMap;
};


extern SceneManager* getSceneManager();

#endif
