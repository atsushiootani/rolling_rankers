//
//  RankingManager.h
//
//
//  Created by OtaniAtsushi1 on 2016/11/04.
//
//

#ifndef RankingManager_h
#define RankingManager_h

#define ENABLE_RANKING (1)  //AppController+Ranking.h と協調すること。なお TARGET_OS_SIMULATOR はcpp側のコンパイルでは使用できない

#if ENABLE_RANKING
#include "RankersBridge.h"
#endif

#include <functional>

#define RANKERS_SDK_ID  "rks00000071_5eef61c3a3280441bf07"
#define RANKERS_SDK_URL "rk147825163767"

class RankingManagerDelegate;

class RankingManager
#if ENABLE_RANKING
: public RankersBridge::Delegate
#endif
{
    //singleton
public:
    static RankingManager* getInstance();
    
    //ctor
private:
    RankingManager(){ _init(); }
    RankingManager(const RankingManager &other){}
    RankingManager &operator = (const RankingManager &other) { return *this; }
    void _init();
    
public:
    bool registerRanking(int version = 1);
    bool fetchRankingInfo();
    void onFetchSucceeded(const char* json);
    void onFetchFailed(RankersBridge::APIResponseType responseType, const char* json, const char* error_message);
    bool showRankingView(std::function<void()> onStartGameCallback, std::function<void()> onQuitGameCallback = nullptr);
    
    bool canRankingEntry(time_t time);
    bool canRankingEntryNow();
    bool isRankingPlayingNow();
    bool recordScore(int score, std::string scoreString, std::function<void()> onRetryGameCallback, std::function<void()> onQuitGameCallback = nullptr);
    
    
    void setDelegate(RankingManagerDelegate* d);
    void unsetDelegate();
    RankingManagerDelegate* getDelegate();
    
    
private:
    
#if ENABLE_RANKING
    //RankersBridge::Delegate
    virtual void startGame(const char *json) override;
    virtual void didCompetitionAppear() override;
    virtual void didCompetitionDisappear() override;
    virtual void didStartMovie() override;
    virtual void didStopMovie() override;
#endif
    
private:
    //bool isRankingOpen;
    bool canFetchRankingInfo;
    time_t startRankingUnixtime;
    time_t endRankingUnixtime;
    bool isRankingPlaying;
    std::function<void()> onStartGameCallback;
    std::function<void()> onQuitGameCallback;
    RankingManagerDelegate* delegate;
};



#pragma mark - delegate

class RankingManagerDelegate{
public:
};


#pragma mark - getter method

extern RankingManager* getRankingManager();



#endif /* RankingManager_h */
