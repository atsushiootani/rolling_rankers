//
//  Native_User_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2016/10/17.
//
//

#import <Foundation/Foundation.h>
#import "Native_User.h"
#import "Native_Common_ios.h"
#import <string>

void Native_InitUserAuth(){
    [getAppController() initUserAuth];
}
void Native_UserAuthentication(){
    [getAppController() userAuthentication];
}
bool Native_IsUserAuthenticated(){
    return [getAppController() isUserAuthenticated];
}
std::string Native_GetUserAuthId(){
    return [[getAppController() getUserAuthId] UTF8String];
}
std::string Native_GetUserAuthName(){
    return [[getAppController() getUserAuthName] UTF8String];
}
std::string Native_GetUserAuthEmail(){
    return [[getAppController() getUserAuthEmail] UTF8String];
}
void Native_SetUserAuthListenerSignIn(std::function<void(std::string uid)> inCallback, std::function<void()> outCallback){
    [getAppController() setUserAuthListenerSignIn:inCallback signOut:outCallback];
}
