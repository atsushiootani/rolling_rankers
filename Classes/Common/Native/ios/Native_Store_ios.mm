//
//  Native_Store_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#import <Foundation/Foundation.h>
#import "Native_Store.h"
#import "Native_Common_ios.h"
#import "AppInfo.h"
#import <string>


//ストアページを開く
void Native_OpenStore(){
    [getAppController() openStore];
}

void Native_OpenStore(std::string packageName){
    [getAppController() openStore: @(packageName.c_str())];
}

bool Native_CheckStorePageExists(){
    return [getAppController() checkStorePageExists];
}

bool Native_CheckStorePageExists(std::string packageName){
    return [getAppController() checkStorePageExists:@(packageName.c_str())];
}


#pragma mark - review

void Native_ReviewPromptShow(std::string title, std::string text, std::string now, std::string later, std::string never) {
    //無条件で呼び出す。レビュー済みか、「あとで」からどれだけ時間が経ったかは、呼び出し元で判定のこと
    //  プロンプトを出すタイミングは割と限定されるので、「あとで」の時間設定は特にする必要を感じなかった
    [getAppController() reviewPromptShowWithTitle:@(title.c_str()) message:@(text.c_str()) now:@(now.c_str()) later:@(later.c_str()) never:@(never.c_str())];
}

bool Native_ReviewHasDone(){
    return [getAppController() hasReviewDone];
}

bool Native_ReviewHasRejected(){
    return [getAppController() hasReviewRejected];
}

bool Native_ReviewHasNotYet(){
    return [getAppController() hasNotReviewedYet];
}
