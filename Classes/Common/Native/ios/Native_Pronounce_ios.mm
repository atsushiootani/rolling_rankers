//
//  Native_Pronounce_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2016/02/08.
//
//

#import "Native_Pronounce.h"
#import "Native_Common_ios.h"
#import "Native_Consts.h"

#pragma mark - pronounce

bool Native_InitPronounce(){
    return [getAppController() initPronounce];
}

bool Native_HasPronounceLoaded(){
    return [getAppController() hasPronounceLoaded];
}

int Native_GetIntPronounce(std::string key, int defaultValue) {
    return [getAppController() getIntPronounce:[NSString stringWithUTF8String:key.c_str()] defaultValue:defaultValue];
}

float Native_GetFloatPronounce(std::string key, float defaultValue) {
    return [getAppController() getFloatPronounce:[NSString stringWithUTF8String:key.c_str()] defaultValue:defaultValue];
}

double Native_GetDoublePronounce(std::string key, double defaultValue) {
    return [getAppController() getDoublePronounce:[NSString stringWithUTF8String:key.c_str()] defaultValue:defaultValue];
}

bool Native_GetBoolPronounce(std::string key, bool defaultValue) {
    return [getAppController() getBoolPronounce:[NSString stringWithUTF8String:key.c_str()] defaultValue:defaultValue];
}

std::string Native_GetStringPronounce(std::string key, std::string defaultValue) {
    return [[getAppController() getStringPronounce:[NSString stringWithUTF8String:key.c_str()] defaultValue:[NSString stringWithUTF8String:defaultValue.c_str()]] cStringUsingEncoding:NSUTF8StringEncoding];
}

void Native_ShowPronounceLaunchAlertView(bool force/* = false*/){
    [getAppController() launchAlertView:force];
}


bool Native_IsInSubmit(){
    return Native_GetBoolPronounce(PRONOUNCE_SUBMIT, false);
}


#pragma mark - datbase

std::string getStringFromNSDictionary(NSDictionary* dic) {
    if ((dic != nil) &&
        (![dic isEqual:[NSNull null]]) &&
        ([NSJSONSerialization isValidJSONObject:dic])) {
        
        NSData* data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
        NSString* string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //NSLog(@"%@", string);
        return [string cStringUsingEncoding:NSUTF8StringEncoding];
    }
    return "";
}

void Native_CheckDBCallback(){
    //do nothing
}

void Native_InitDatabase(){
    [getAppController() initDatabase];
}

void Native_SelectDatabaseAtPath (NativeStorageType type, std::string path,                         std::function<void(bool, std::string jsonString)> completion){
    [getAppController() selectRecordAtPath:(StorageType)type filePath:@(path.c_str()) completion:^(bool succeeded, NSDictionary* dic){
        if (completion) {
            completion(succeeded, getStringFromNSDictionary(dic));
        }
    }];
}

void Native_UpdateDatabaseAtPath (NativeStorageType type, std::string path, std::string jsonRecord, std::function<void(bool, std::string jsonString)> completion){
    NSData *data = [@(jsonRecord.c_str()) dataUsingEncoding:NSUTF8StringEncoding];
    id updateData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [getAppController() updateRecordAtPath:(StorageType)type filePath:@(path.c_str()) updateData:updateData completion:^(bool succeeded, NSDictionary* dic){
        if (completion) {
            completion(succeeded, getStringFromNSDictionary(dic));
        }
    }];
}

void Native_ReplaceDatabaseAtPath(NativeStorageType type, std::string path, std::string jsonRecord, std::function<void(bool, std::string jsonString)> completion){
    NSData *data = [@(jsonRecord.c_str()) dataUsingEncoding:NSUTF8StringEncoding];
    id replaceData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [getAppController() replaceRecordAtPath:(StorageType)type filePath:@(path.c_str()) replaceData:replaceData completion:^(bool succeeded, NSDictionary* dic){
        if (completion) {
            completion(succeeded, getStringFromNSDictionary(dic));
        }
    }];
}
void Native_DeleteDatabaseAtPath (NativeStorageType type, std::string path,                         std::function<void(bool, std::string jsonString)> completion){
    [getAppController() deleteRecordAtPath:(StorageType)type filePath:@(path.c_str()) completion:^(bool succeeded, NSDictionary* dic){
        if (completion) {
            completion(succeeded, getStringFromNSDictionary(dic));
        }
    }];
}


#pragma mark - storage

void Native_CheckStorageCallback(){
    //do nothing
}

bool Native_InitStorage(){
    [getAppController() initDataStorage];
    return true;
}

void Native_GetStorageDataAndSaveToFile(NativeStorageType type, std::string path, std::function<void(bool, std::string)> completion){
    [getAppController() downloadStorageDataAndSaveToFile:(StorageType)type filePath:@(path.c_str()) completion:^(bool succeeded, NSString* saveFilePath){
        if (completion) {
            completion(succeeded, (saveFilePath != nil) ? [saveFilePath UTF8String] : "");
        }
    }];
}

void Native_GetStorageDataAsString(NativeStorageType type, std::string path, std::function<void(bool, std::string)> completion){
    [getAppController() downloadStorageDataAsString:(StorageType)type filePath:@(path.c_str()) completion:^(bool succeeded, NSString* str){
        if (completion) {
            completion(succeeded, (str != nil) ? [str UTF8String] : "");
        }
    }];
}

void Native_GetStorageDataAndAlloc    (NativeStorageType type, std::string path, std::function<void(bool, unsigned char*, size_t)> completion){
    [getAppController() downloadStorageData:(StorageType)type filePath:@(path.c_str()) completion:^(bool succeeded, NSData* data){
        unsigned char* byteData = nullptr;
        size_t len = 0;
        if (succeeded) {
            len = data.length;
            byteData = new unsigned char[len];
            [data getBytes:byteData length:len];
        }
        if (completion) {
            completion(succeeded, byteData, len);
        }
    }];
}

void Native_DeallocStorageData(unsigned char* data){
    delete [] data;
}


void Native_SetStorageDataWithString(NativeStorageType type, std::string path, std::string str, std::function<void(bool)> completion){
    [getAppController() uploadStorageDataWithString:(StorageType)type filePath:@(path.c_str()) str:@(str.c_str()) completion:^(bool succeeded){
        if (completion) {
            completion(succeeded);
        }
    }];
}
void Native_SetStorageData          (NativeStorageType type, std::string path, unsigned char* byteData, size_t len, std::function<void(bool)> completion){
    NSData* data = [NSData dataWithBytes:byteData length:len];
    [getAppController() uploadStorageData:(StorageType)type filePath:@(path.c_str()) data:data completion:^(bool succeeded){
        if (completion) {
            completion(succeeded);
        }
    }];
}

void Native_DeleteStorageData       (NativeStorageType type, std::string path,                                      std::function<void(bool)> completion){
    [getAppController() deleteStorageData:(StorageType)type filePath:@(path.c_str()) completion:^(bool succeeded){
        if (completion) {
            completion(succeeded);
        }
    }];
}

