//
//  Native_Analytics_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2015/04/19.
//
//

#import <Foundation/Foundation.h>
#import "Native_Analytics.h"
#import "Native_Common_ios.h"


void Native_Analytics(std::string category, std::string action, std::string label, int value) {
    [getAppController() analytics_eventWithCategory:@(category.c_str())
                                             action:@(action.c_str())
                                              label:@(label.c_str())
                                              value:value];
}

void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1){
    [getAppController() analytics_eventName:@(eventName.c_str())
                                      param:@{@(key1.c_str()):@(value1.c_str())}];
}
void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1, std::string key2, std::string value2){
    [getAppController() analytics_eventName:@(eventName.c_str())
                                      param:@{@(key1.c_str()):@(value1.c_str()),
                                              @(key2.c_str()):@(value2.c_str()),
                                              }];
}
void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1, std::string key2, std::string value2, std::string key3, std::string value3){
    [getAppController() analytics_eventName:@(eventName.c_str())
                                      param:@{@(key1.c_str()):@(value1.c_str()),
                                              @(key2.c_str()):@(value2.c_str()),
                                              @(key3.c_str()):@(value3.c_str()),
                                              }];
}

void Native_ErrorReport(std::string error, std::string subData/*= ""*/){
    //Native_Analytics(ANALYTICS_ERROR_CATEGORY, error, subData, 1);
    [getAppController() analytics_errorReport:@((error + " " + subData).c_str())];
}


void Native_AnalyticsTutorialBegin(){
    [getAppController() analytics_tutorialBegin];
}
void Native_AnalyticsTutorialEnded(){
    [getAppController() analytics_tutorialEnded];
}
void Native_AnalyticsAppOpen(){
    [getAppController() analytics_appOpen];
}
void Native_AnalyticsShare(std::string contentType, std::string itemId){
    [getAppController() analytics_share:@(contentType.c_str()) itemId:@(itemId.c_str())];
}
void Native_AnalyticsShareTwitter(std::string itemId){
    [getAppController() analytics_shareTwitter:@(itemId.c_str())];
}
void Native_AnalyticsShareFacebook(std::string itemId){
    [getAppController() analytics_shareFacebook:@(itemId.c_str())];
}
void Native_AnalyticsEcommercePurchaseBegin(std::string itemName){
    [getAppController() analytics_ecommercePurchaseBegin:@(itemName.c_str())];
}
void Native_AnalyticsEcommercePurchase(int price, std::string itemName){
    [getAppController() analytics_ecommercePurchase:price itemName:@(itemName.c_str())];
}
void Native_AnalyticsEcommercePurchase(std::string itemName){
    [getAppController() analytics_ecommercePurchase:@(itemName.c_str())];
}
void Native_AnalyticsSpendVirtualCurrency(std::string itemName, std::string currencyName, int value){
    [getAppController() analytics_spendVirtualCurrency:@(itemName.c_str()) currencyName:@(currencyName.c_str()) value:value];
}
void Native_AnalyticsBuyItem(std::string itemName){
    [getAppController() analytics_buyItem:@(itemName.c_str())];
}
void Native_AnalyticsBuyItem(std::string itemName, int price){
    [getAppController() analytics_buyItem:@(itemName.c_str()) price:price];
}
void Native_AnalyticsUnlockAchievement(std::string achievementId){
    [getAppController() analytics_unlockAchievement:@(achievementId.c_str())];
}
void Native_AnalyticsSelectContent(std::string contentName){
    [getAppController() analytics_selectContent:@(contentName.c_str())];
}
void Native_AnalyticsSelectContent(std::string contentName, std::string itemId){
    [getAppController() analytics_selectContent:@(contentName.c_str()) itemId:@(itemId.c_str())];
}
void Native_AnalyticsSelectContent(std::string contentName, int itemId){
    [getAppController() analytics_selectContent:@(contentName.c_str()) itemId:@(std::to_string(itemId).c_str())];
}

void Native_AnalyticsReview(){
    [getAppController() analytics_review];
}

