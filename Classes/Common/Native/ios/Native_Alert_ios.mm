//
//  Native_Alert_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>
#import "Native_Alert.h"
#import "Native_Common_ios.h"


static AlertCallback_t  s_alertCallback;
static std::function<void(const char*)> s_keyboardInputCallback;
static std::function<void()>            s_keyboardCancelCallback;


//アラートビュー
bool Native_IsAlertOn(){
    return !!s_alertCallback;
}

void Native_CheckAlertViewCallback(){
    AlertButtonIndex selectedIndex = (AlertButtonIndex)[getAppController().viewController checkAndResetAlertViewCallback];
    if (selectedIndex >= 0) {
        if (s_alertCallback) {
            s_alertCallback(selectedIndex);
            s_alertCallback = nullptr;
        }
    }
}

void Native_AlertView(std::string title, std::string message, std::string buttonTitle, AlertCallback_t alertCallback /*= nullptr*/) {
    if (Native_IsAlertOn()) {
        assert(false && "cannot open AlertView simultaneously");
        return;
    }
    
    [getAppController().viewController makeAlertViewWithTitle:@(title.c_str())
                                                      message:@(message.c_str())
                                                      okTitle:@(buttonTitle.c_str())];
    
    s_alertCallback = alertCallback;
}

void Native_AlertView(std::string title, std::string message, std::string noTitle, std::string yesTitle, AlertCallback_t alertCallback /*= nullptr*/){
    if (Native_IsAlertOn()) {
        assert(false && "cannot open AlertView simultaneously");
        return;
    }

    [getAppController().viewController makeAlertViewWithTitle:@(title.c_str())
                                                      message:@(message.c_str())
                                                      noTitle:@(noTitle.c_str())
                                                     yesTitle:@(yesTitle.c_str())];
    s_alertCallback = alertCallback;
}

void Native_AlertView(std::string title, std::string message, std::string cancelTitle, std::string opt1Title, std::string opt2Title, AlertCallback_t alertCallback /*= nullptr*/){
    if (Native_IsAlertOn()) {
        assert(false && "cannot open AlertView simultaneously");
        return;
    }
    
    [getAppController().viewController makeAlertViewWithTitle:@(title.c_str())
                                                      message:@(message.c_str())
                                                  cancelTitle:@(cancelTitle.c_str())
                                                 option1Title:@(opt1Title.c_str())
                                                 option2Title:@(opt2Title.c_str())];
    s_alertCallback = alertCallback;
}

void Native_AlertView(std::string title, std::string message, std::string cancelTitle, std::string opt1Title, std::string opt2Title, std::string opt3Title, AlertCallback_t alertCallback /*= nullptr*/){
    if (Native_IsAlertOn()) {
        assert(false && "cannot open AlertView simultaneously");
        return;
    }
    
    [getAppController().viewController makeAlertViewWithTitle:@(title.c_str())
                                                      message:@(message.c_str())
                                                  cancelTitle:@(cancelTitle.c_str())
                                                 option1Title:@(opt1Title.c_str())
                                                 option2Title:@(opt2Title.c_str())
                                                 option3Title:@(opt3Title.c_str())];
    s_alertCallback = alertCallback;
}

//インジケータ
void Native_ShowIndicator(std::string message){
    [getAppController().viewController showIndicator:[NSString stringWithUTF8String:message.c_str()]];
}

void Native_HideIndicator(){
    [getAppController().viewController hideIndicator];
}

//キーボード
void Native_CheckKeyboardCallback(){
    int result = [getAppController().viewController checkAndResetKeyboardCallback];
    if (result == 0) { //キャンセル
        if (s_keyboardCancelCallback) {
            s_keyboardCancelCallback();
            s_keyboardCancelCallback = nullptr;
        }
    }
    else if (result == 1) { //入力あり
        if (s_keyboardInputCallback) {
            s_keyboardInputCallback([getAppController().viewController getKeyboardInputText]);
            s_keyboardInputCallback = nullptr;
        }
    }
}

void Native_ShowKeyboard(std::string message, std::string text, std::function<void(const char*)> inputCallback, std::function<void()> cancelCallback/* = nullptr*/){
    s_keyboardInputCallback = inputCallback;
    s_keyboardCancelCallback = cancelCallback;
    [getAppController().viewController showKeyboardWith:@(message.c_str()) text:@(text.c_str())];
}
void Native_ShowKeyboardNumPad(std::string message, std::string text, std::function<void(const char*)> inputCallback, std::function<void()> cancelCallback/* = nullpt
*/){
    s_keyboardInputCallback = inputCallback;
    s_keyboardCancelCallback = cancelCallback;
    [getAppController().viewController showKeyboardNumPadWith:@(message.c_str()) text:@(text.c_str())];
}

//スリープモード
void Native_SleepableMode(){
    [getAppController().viewController sleepableMode];
}
void Native_KeepScreenMode(){
    [getAppController().viewController keepScreenMode];
}

