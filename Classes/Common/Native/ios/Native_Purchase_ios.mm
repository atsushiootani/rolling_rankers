//  Native_Purchase_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2015/03/28.
//
//

#import <Foundation/Foundation.h>
#import "Native_Purchase.h"
#import "Native_Common_ios.h"
#import "AppInfo.h"
#import <string>


void Native_CheckPurchaseCallback(){
    //do nothing in iOS
}


bool Native_PurchaseUnRegister(){
    [getAppController() purchaseUnregister];
    return YES;
}

bool Native_PurchaseRegister(LoadedCallback_t loadedCallback){
    [getAppController() purchaseRegister:^(bool succeeded){loadedCallback(succeeded);}];
    return YES;
}

bool Native_PurchaseRequestHasFinished(){
    return [getAppController() hasRequestFinished];
}

bool Native_PurchaseInitHasSucceeded(){
    return [getAppController() hasInitSucceeded];
}

//ios,AndroidともにclosedCallbackはSDK準拠ではないが、ゲーム側の実用のために作ったメソッド. Native自前層が正しくcocos側コードを呼び出す責任を負う(ゲーム側は、正しいタイミングで呼ばれると思っていて良い. closedはcompletedの直後、failureの直後、処理中断時に呼ばれる)
bool Native_Restore(PurchaseCallback_t successCallback, RestoreCompletedCallback_t failureCallback, RestoreCompletedCallback_t completedCallback, PurchaseClosedCallback_t closedCallback) {
    [getAppController() restoreItemsWithSuccessCallback:^(const char* prodId){if(successCallback)  {successCallback(prodId);}}
                                      completedCallback:^()                  {if(completedCallback){completedCallback();}}
                                        failureCallback:^()                  {if(failureCallback)  {failureCallback();}}
                                         closedCallback:^()                  {if(closedCallback)   {closedCallback();}}
     ];
    return true;
}

//iosはClosedを処理しなくても、終了処理もれすることがないので、closedCallbackは無視する
bool Native_PurchaseItem(std::string productId, PurchaseCallback_t successCallback, PurchaseCallback_t failureCallback, PurchaseCallback_t cancelCallback, PurchaseClosedCallback_t closedCallback) {
    [getAppController() purchaseItem:[NSString stringWithUTF8String:productId.c_str()]
                 withSuccessCallback:^(const char* prodId){if(successCallback){successCallback(prodId);}}
                     failureCallback:^(const char* prodId){if(failureCallback){failureCallback(prodId);}}
                      cancelCallback:^(const char* prodId){if(cancelCallback) {cancelCallback(prodId);}}
                      closedCallback:^()                  {if(closedCallback) {closedCallback();}}
     ];
    return YES;
}

std::string Native_GetProductName(std::string productId){
    NSString* name = [getAppController() getProductName:[NSString stringWithUTF8String:productId.c_str()]];
    return [name cStringUsingEncoding:NSUTF8StringEncoding];
}

std::string Native_GetProductDescription(std::string productId){
    NSString* name = [getAppController() getProductDescription:[NSString stringWithUTF8String:productId.c_str()]];
    return [name cStringUsingEncoding:NSUTF8StringEncoding];
}

std::string Native_GetProductPrice(std::string productId){
    NSString* name = [getAppController() getProductPrice:[NSString stringWithUTF8String:productId.c_str()]];
    return [name cStringUsingEncoding:NSUTF8StringEncoding];
}
