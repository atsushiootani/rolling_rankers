//
//  Native_Common_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2015/11/03.
//
//

#import <Foundation/Foundation.h>
#import "Native_Common_ios.h"


AppController *getAppController() {
    return (AppController *)[UIApplication sharedApplication].delegate;
}

RootViewController *getRootViewController() {
    return getAppController().viewController;
}


