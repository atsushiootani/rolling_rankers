//
//  Native_SNS_ios.mm
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <functional>
#import "Native_SNS.h"
#import "Native_Common_ios.h"

void Native_CheckSNSCallback(){
    //do nothing in iOS
}

void _commonPost(NSString* serviceType, std::string text, std::string imageName, std::string url, ShareCallback_t successCallback, ShareCallback_t failureCallback, ShareCallback_t cancelCallback, ShareCallback_t alwaysCallback) {
    if ([SLComposeViewController isAvailableForServiceType:serviceType]) {
        NSString* textString  = @(text.c_str());
        NSString* imageString = @(imageName.c_str());
        NSString* urlString   = @(url.c_str());
        
        SLComposeViewController *service = [SLComposeViewController composeViewControllerForServiceType:serviceType];
        if (textString) {
            [service setInitialText:textString];
        }
        if (imageString) {
            if (!(serviceType == SLServiceTypeFacebook && urlString)) {
                [service addImage:[UIImage imageNamed:imageString]];
            }
        }
        if (urlString) {
            [service addURL:[NSURL URLWithString:urlString]];
        }
        [service setCompletionHandler:^(SLComposeViewControllerResult result){
            switch (result) {
                case SLComposeViewControllerResultDone:
                    if (successCallback) {
                        successCallback();
                    }
                    if (alwaysCallback) {
                        alwaysCallback();
                    }
                    break;
                case SLComposeViewControllerResultCancelled:
                    if (cancelCallback) {
                        cancelCallback();
                    }
                    if (alwaysCallback) {
                        alwaysCallback();
                    }
                    break;
                default:
                    break;
            }
            [getRootViewController() dismissViewControllerAnimated:YES completion:nil];
        }];
        [getRootViewController() presentViewController:service animated:YES completion:NULL];
    }
    else{
        NSString* message
        = serviceType == SLServiceTypeFacebook ? @"Facebookアプリがインストールされていません"
        : serviceType == SLServiceTypeTwitter  ? @"Twitterアプリがインストールされていません"
        :                                        @"アプリがインストールされていません";
        [[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        if (failureCallback) {
            failureCallback();
        }
        if (alwaysCallback) {
            alwaysCallback();
        }
    }
}


void Native_TwitterPost(std::string tweet, std::string imageName, std::string url, ShareCallback_t successCallback, ShareCallback_t failureCallback, ShareCallback_t cancelCallback, ShareCallback_t alwaysCallback) {
    _commonPost(SLServiceTypeTwitter, tweet, imageName, url, successCallback, failureCallback, cancelCallback, alwaysCallback);
}

void Native_FacebookPost(std::string text, std::string imageName, std::string url, ShareCallback_t successCallback, ShareCallback_t failureCallback, ShareCallback_t cancelCallback, ShareCallback_t alwaysCallback) {
    _commonPost(SLServiceTypeFacebook, text, imageName, url, successCallback, failureCallback, cancelCallback, alwaysCallback);
}
