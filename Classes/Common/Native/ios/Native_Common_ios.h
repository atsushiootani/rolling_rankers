//
//  Native_Common_ios.h
//
//
//  Created by OtaniAtsushi1 on 2015/11/03.
//
//

#ifndef Native_Common_ios_h
#define Native_Common_ios_h

/**
 * Native_***.mm から呼び出す共通処理ファイル
 * ObjCファイルであり、cppファイルからインクルードしないこと
 */


#import "AppController.h"
#import "AppController+Ad.h"
#import "AppController+Analytics.h"
#import "AppController+Data.h"
#import "AppController+Http.h"
#import "AppController+Purchase.h"
#import "AppController+Store.h"
#import "AppController+User.h"
#import "RootViewController.h"

//インスタンス取得
extern AppController *getAppController();
extern RootViewController *getRootViewController();

#endif
