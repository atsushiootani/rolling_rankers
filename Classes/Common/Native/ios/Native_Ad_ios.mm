//
//  Native_Ad_ios.mm
//  
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#import <Foundation/Foundation.h>
#import "Native_Ad.h"
#import "Native_Alert.h"
#import "Native_Common_ios.h"
#import "AppController+Ad.h"
#import "AppController+Http.h"
#import "GameHelper.h"
#import "Debug.h"

//コールバック処理
void Native_CheckAdCallback(){
    //do nothing in iOS
}


//初期化処理
void Native_AdRegister(){
    [getAppController() adRegister];
}

//バックグラウンド・フォアグラウンド処理
void Native_AdEnterBackground(){
    [getAppController() adEnterBackground];
}
void Native_AdEnterForeground(){
    [getAppController() adEnterForeground];
}


//広告全部非表示
void Native_AdAllHide() {
    Native_AdBannerHide();
}

//バナー
void Native_AdBannerShow(){
    [getAppController() adBannerShow];
}

void Native_AdBannerHide(){
    [getAppController() adBannerHide];
}

void Native_AdBannerSetEnable(){
    [getAppController() adBannerSetEnable];
}

void Native_AdBannerSetDisable(){
    [getAppController() adBannerSetDisable];
}


//アイコン
void Native_AdIconShowAt(int pos){
    [getAppController() adIconShowAt:pos];
}
void Native_AdIconShow(){
    [getAppController() adIconShow];
}
void Native_AdIconHide(){
    [getAppController() adIconHide];
}
void Native_AdIconHideAt(int pos){
    [getAppController() adIconHideAt:pos];
}

//インタースティシャル
void Native_AdInterstitialShow(){
    [getAppController() adInterstitialTransitionShow];
}

void Native_AdInterstitialAlwaysShow(){
    [getAppController() adInterstitialAlwaysShow];
}

//ウォール
void Native_AdWallShow(){
    [getAppController() adInterstitialAlwaysShow]; //iOSはウォールが出せないので、インタースティシャルで代用
}

//ヘッダー位置
void Native_AdHeaderShow(){
    [getAppController() adHeaderShow];
}
void Native_AdHeaderHide(){
    [getAppController() adHeaderHide];
}


//インセンティブ付与動画広告
void Native_AdInsentiveShow(std::function<void(int)> insentiveCallback, std::function<void()> failureCallback, std::function<void(bool)> closedCallback){
    //アラートビューが開いている時は、エラーになる
    if (Native_IsAlertOn()){
        failureCallback();
        closedCallback(false);
        DPRINTF("アラートビューが開いている時は、動画広告再生できない\n");
    }
    [getAppController() adMoviePlay:insentiveCallback failureCallback: failureCallback closedCallback:closedCallback];
}

bool Native_AdCanGetInsentive(){
    return [getAppController() adMovieCanPlay];
}

bool Native_AdMovieIsPlaying() {
    return [getAppController() isAdMoviePlayingNow];
}

//他のアプリ
bool Native_CheckApplicationInstalled(std::string urlScheme){
    return [getAppController() checkApplicationInstalled:@(urlScheme.c_str())];
}
void Native_OpenInstalledApplication(std::string urlScheme){
    [getAppController() openInstalledApplication:@(urlScheme.c_str())];
}
