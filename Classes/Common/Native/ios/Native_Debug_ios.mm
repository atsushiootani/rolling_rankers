//
//  Native_Debug_ios.mm
//  
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#import <Foundation/Foundation.h>
#import "Native_Debug.h"
#import "Native_Common_ios.h"


bool Native_IsDebug(){
#if DEBUG
    return true;
#else
    return false;
#endif
}

bool Native_IsIos() {
    return true;
}

bool Native_IsAndroid() {
    return false;
}

void Native_Print(std::string text){
    if (Native_IsDebug()) {
        NSLog(@"%@", [NSString stringWithUTF8String:text.c_str()]);
    }
}
