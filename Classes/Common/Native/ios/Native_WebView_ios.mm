//
//  Native_WebView_ios.mm
//  
//
//  Created by OtaniAtsushi1 on 2015/03/03.
//
//

#import <Foundation/Foundation.h>
#import "Native_WebView.h"
#import "Native_Common_ios.h"


void Native_OpenWebPage(const char *url) {
    [getAppController() openWebPage:@(url)];
}

void Native_OpenStorePage(const char *url) {
    [getAppController() openWebPage:@(url)]; //通常のページと同じ
}

void Native_OpenWebView(const char *url){
    //NSURL* nsurl = [NSURL URLWithString: [NSString stringWithUTF8String:url]];
    //[[UIApplication sharedApplication] openURL: nsurl];
    
    NSURL* nsurl = [NSURL URLWithString:[NSString stringWithUTF8String:url]];
    [getRootViewController() openWebView:nsurl];
}

void Native_CloseWebView(){
    [getRootViewController() closeWebView];
}

void Native_PreloadWebView(const char *url){
    NSURL* nsurl = [NSURL URLWithString:[NSString stringWithUTF8String:url]];
    [getRootViewController() preloadWebView:nsurl];
}
void Native_ShowWebView(){
    [getRootViewController() showWebView];
}
void Native_HideWebView(){
    [getRootViewController() hideWebView];
}
bool Native_HasWebView(){
    return [getRootViewController() hasWebView];
}

/*
void Native_playMovie(const char *url){
    NSURL* nsurl = [NSURL URLWithString:[NSString stringWithUTF8String:url]];
    [getRootViewController() playMovie:nsurl];
}

void Native_playYouTubeMovie(const char *youtubeId){
    [getRootViewController() playYouTubeMovie:[NSString stringWithUTF8String:youtubeId]];
}*/
