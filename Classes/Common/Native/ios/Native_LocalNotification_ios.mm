//
//  Native_LocalNotification_ios.mm
//
//
//
//
//

#import "Native_LocalNotification.h"
#import "Native_Common_ios.h"


//カレンダー取得
NSCalendar* getCalendar() {
    return [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
}

//通知してよい時間帯か
BOOL isNotifiableTime(NSDate* date) {
    NSDateComponents* comp = [getCalendar() components:NSCalendarUnitHour fromDate:date];
    
    //毎日0時~1時,8時~24時が通知可能
    return comp.hour < 1 || comp.hour >= 8;
}

//ローカル通知の登録
void Native_RegisterLocalNotification(){
    UIUserNotificationType types = UIUserNotificationTypeBadge | // アイコンバッチ
    UIUserNotificationTypeSound | // サウンド
    UIUserNotificationTypeAlert;  // テキスト
    
    // UIUserNotificationSettingsの生成
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];

    // アプリケーションに登録
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
}

//指定秒後のローカル通知を作成、スケジュール登録
void Native_ScheduleLocalNotification(int secSinceNow, const char* text){

    NSDate* fireDate = [NSDate dateWithTimeIntervalSinceNow:secSinceNow];
    if (isNotifiableTime(fireDate)) {
        UILocalNotification *notification = [[UILocalNotification alloc] init];
    
        notification.fireDate = fireDate;
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.alertBody = [NSString stringWithUTF8String:text];
        notification.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

//スケジュールされたローカル通知をすべて削除
void Native_UnscheduleAllLocalNotifications(){
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
