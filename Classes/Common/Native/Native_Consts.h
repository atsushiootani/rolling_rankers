//
//  Native_Consts.h
//
//
//  Created by OtaniAtsushi1 on 2016/08/22.
//
//

#ifndef Native_Consts_h
#define Native_Consts_h


//pronounce
#define PRONOUNCE_SUBMIT              "sub"
#define PRONOUNCE_ENABLE_APPLY_LINK   "eal"
#define PRONOUNCE_APPLY_LINK_URL      "applylink"
#define PRONOUNCE_SNS_TEXT_HEAD       "head"
#define PRONOUNCE_SNS_TEXT_TAIL       "tail"
#define PRONOUNCE_SNS_TEXT_MAIN       "main"
#define PRONOUNCE_SPEED_UP_RATE       "speed_up_rate"
#define PRONOUNCE_SPEED_UP_ADD        "speed_up_add"
#define PRONOUNCE_SPEED_UP_DURATION   "speed_up_duration"
#define PRONOUNCE_REWARD_COIN         "reward_coin"
#define PRONOUNCE_CONTINUE_COIN       "continue_coin"
#define PRONOUNCE_GACHA_COIN          "gacha_coin"
#define PRONOUNCE_REWARD_PLAY_INTERVAL "reward_play_interval"
#define PRONOUNCE_RANKING_BUTTON_NAME "ranking_button_name"


#endif /* Native_Consts_h */
