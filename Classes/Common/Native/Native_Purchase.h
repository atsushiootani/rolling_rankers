//
//  Native_Purchase.h
//
//
//  Created by OtaniAtsushi1 on 2015/03/28.
//
//

#ifndef Native_Purchase_h
#define Native_Purchase_h

#include "AppInfo.h"
#include <functional>


typedef std::function<void(bool)>        LoadedCallback_t;
typedef std::function<void(std::string)> PurchaseCallback_t;
typedef std::function<void()>            PurchaseClosedCallback_t;
typedef std::function<void()>            RestoreCompletedCallback_t;

extern void Native_CheckPurchaseCallback();

extern bool Native_PurchaseUnRegister();
extern bool Native_PurchaseRegister(LoadedCallback_t loadedCallback);
extern bool Native_PurchaseRequestHasFinished();
extern bool Native_PurchaseInitHasSucceeded();

extern bool Native_Restore(PurchaseCallback_t successCallback, RestoreCompletedCallback_t failureCallback, RestoreCompletedCallback_t completedCallback, PurchaseClosedCallback_t closedCallback);

extern bool Native_PurchaseItem(std::string productId, PurchaseCallback_t successCallback, PurchaseCallback_t failureCallback, PurchaseCallback_t cancelCallback, PurchaseClosedCallback_t closedCallback);

extern std::string Native_GetProductName(std::string productId);
extern std::string Native_GetProductDescription(std::string productId);
extern std::string Native_GetProductPrice(std::string productId);


#endif
