//
//  Native.h
//
//
//  Created by OtaniAtsushi1 on 2015/11/03.
//
//

#ifndef Native_h
#define Native_h

/**
 * ゲーム側からは、Native関連ではこのファイルだけインクルードすれば良い
 */

#include "Native_Ad.h"
#include "Native_Alert.h"
#include "Native_Analytics.h"
#include "Native_Debug.h"
#include "Native_LocalNotification.h"
#include "Native_Pronounce.h"
#include "Native_Purchase.h"
#include "Native_SNS.h"
#include "Native_Store.h"
#include "Native_User.h"
#include "Native_WebView.h"

#include "Native_Consts.h"

//各種Native側へのポーリングを一手に行う
extern void Native_CheckPollingAll();


#endif
