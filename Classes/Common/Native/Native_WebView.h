//
//  Native_WebView.h
//
//
//  Created by OtaniAtsushi1 on 2015/03/03.
//
//

#ifndef Native_WebView_h
#define Native_WebView_h


extern void Native_OpenWebPage(const char *url);
extern void Native_OpenStorePage(const char *url);
extern void Native_OpenWebView(const char *url);
extern void Native_CloseWebView();
extern void Native_PreloadWebView(const char *url);
extern void Native_ShowWebView();
extern void Native_HideWebView();
extern bool Native_HasWebView();
//extern void Native_playMovie(const char *url);
//extern void Native_playYouTubeMovie(const char *youtubeId);

#endif
