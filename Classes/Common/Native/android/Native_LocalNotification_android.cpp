#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/NotificationUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_NotificationUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_NotificationUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_LocalNotification.h"

void Native_RegisterLocalNotification(){
    //do nothing in android
}

void Native_ScheduleLocalNotification(int secSinceNow, const char* text){
    JniMethodInfo methodInfo;
    const char* methodName = "Native_ScheduleLocalNotification";
    const char* paramCode = "(ILjava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
    	jstring jtext = methodInfo.env->NewStringUTF(text);
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, secSinceNow, jtext);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtext);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_UnscheduleAllLocalNotifications(){
    JniMethodInfo methodInfo;
    const char* methodName = "Native_UnscheduleAllLocalNotifications";
    const char* paramCode = "()V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}


