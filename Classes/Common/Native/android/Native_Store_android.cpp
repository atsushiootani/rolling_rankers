#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/StoreUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_StoreUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_StoreUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_Store.h"

JNI_METHOD_VOID_NO_ARG(    Native_OpenStore);
JNI_METHOD_VOID_ARG_STRING(Native_OpenStore, packageName);
JNI_METHOD_BOOL_NO_ARG(    Native_CheckStorePageExists);
JNI_METHOD_BOOL_ARG_STRING(Native_CheckStorePageExists, packageName);

//JNI_METHOD_VOID_ARG_STRING2(Native_ReviewPromptShow, title, text);
void Native_ReviewPromptShow(std::string title, std::string text, std::string now, std::string later, std::string never){
    JniMethodInfo methodInfo;
    const char* methodName = "Native_ReviewPromptShow";
    const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jtitle = methodInfo.env->NewStringUTF(title.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jtext  = methodInfo.env->NewStringUTF(text.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jnow   = methodInfo.env->NewStringUTF(now.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jlater = methodInfo.env->NewStringUTF(later.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jnever = methodInfo.env->NewStringUTF(never.c_str());
        JNI_CHECK_EXCEPTION;
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jtitle, jtext, jnow, jlater, jnever);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtitle);
        methodInfo.env->DeleteLocalRef(jtext);
        methodInfo.env->DeleteLocalRef(jnow);
        methodInfo.env->DeleteLocalRef(jlater);
        methodInfo.env->DeleteLocalRef(jnever);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

JNI_METHOD_BOOL_NO_ARG(Native_ReviewHasDone);
JNI_METHOD_BOOL_NO_ARG(Native_ReviewHasRejected);
JNI_METHOD_BOOL_NO_ARG(Native_ReviewHasNotYet);

