#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/HttpUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_HttpUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_HttpUtils_##func##Jni

#include "Native_Consts.h"
#include "Native_Common_android.h"
#include "Native_Pronounce.h"



extern "C" {
}


#pragma mark - pronounce

JNI_METHOD_BOOL_NO_ARG(Native_InitPronounce);

JNI_METHOD_BOOL_NO_ARG(Native_HasPronounceLoaded);

int         Native_GetIntPronounce(   std::string key, int defaultValue /*= 0*/){
	int ret = defaultValue;
	JniMethodInfo methodInfo;
	const char* methodName = "Native_GetIntPronounce";
	const char* paramCode = "(Ljava/lang/String;I)I";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jkey = methodInfo.env->NewStringUTF(key.c_str());
        JNI_CHECK_EXCEPTION;
	    ret = methodInfo.env->CallStaticIntMethod(methodInfo.classID , methodInfo.methodID, jkey, defaultValue);
        JNI_CHECK_EXCEPTION;
	    methodInfo.env->DeleteLocalRef(jkey);
	    methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
	return ret;
}

float       Native_GetFloatPronounce( std::string key, float defaultValue /*= 0.f*/){
	float ret = defaultValue;
	JniMethodInfo methodInfo;
	const char* methodName = "Native_GetFloatPronounce";
	const char* paramCode = "(Ljava/lang/String;F)F";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jkey = methodInfo.env->NewStringUTF(key.c_str());
        JNI_CHECK_EXCEPTION;
	    ret = methodInfo.env->CallStaticFloatMethod(methodInfo.classID , methodInfo.methodID, jkey, defaultValue);
        JNI_CHECK_EXCEPTION;
	    methodInfo.env->DeleteLocalRef(jkey);
	    methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
	return ret;
}

double      Native_GetDoublePronounce(std::string key, double defaultValue /*= 0.0*/){
	double ret = defaultValue;
	JniMethodInfo methodInfo;
	const char* methodName = "Native_GetDoublePronounce";
	const char* paramCode = "(Ljava/lang/String;D)D";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jkey = methodInfo.env->NewStringUTF(key.c_str());
        JNI_CHECK_EXCEPTION;
	    ret = methodInfo.env->CallStaticDoubleMethod(methodInfo.classID , methodInfo.methodID, jkey, defaultValue);
        JNI_CHECK_EXCEPTION;
	    methodInfo.env->DeleteLocalRef(jkey);
	    methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
	return ret;
}

bool        Native_GetBoolPronounce(  std::string key, bool defaultValue /*= false*/){
	bool ret = defaultValue;
	JniMethodInfo methodInfo;
	const char* methodName = "Native_GetBoolPronounce";
	const char* paramCode = "(Ljava/lang/String;Z)Z";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jkey = methodInfo.env->NewStringUTF(key.c_str());
        JNI_CHECK_EXCEPTION;
	    ret = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID , methodInfo.methodID, jkey, defaultValue);
        JNI_CHECK_EXCEPTION;
	    methodInfo.env->DeleteLocalRef(jkey);
	    methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
	return ret;
}

std::string Native_GetStringPronounce(std::string key, std::string defaultValue /*= ""*/){
	std::string ret = defaultValue;
	JniMethodInfo methodInfo;
	const char* methodName = "Native_GetStringPronounce";
	const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jkey = methodInfo.env->NewStringUTF(key.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jdefaultValue = methodInfo.env->NewStringUTF(defaultValue.c_str());
        JNI_CHECK_EXCEPTION;
		jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID , methodInfo.methodID, jkey, jdefaultValue);
        JNI_CHECK_EXCEPTION;
		ret = JniHelper::jstring2string((jstring)objResult);

	    methodInfo.env->DeleteLocalRef(jkey);
	    methodInfo.env->DeleteLocalRef(jdefaultValue);
	    methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
	return ret;
}

JNI_METHOD_VOID_ARG_BOOL(Native_ShowPronounceLaunchAlertView, force);

bool Native_IsInSubmit(){
    return Native_GetBoolPronounce(PRONOUNCE_SUBMIT, 1);
}

