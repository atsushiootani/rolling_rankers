#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/DatabaseUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_DatabaseUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_DatabaseUtils_##func##Jni

#include "Native_Consts.h"
#include "Native_Common_android.h"
#include "Native_Pronounce.h"



static bool                                   s_toDbCompletionCallback = false;
static std::function<void(bool, std::string)>   s_dbCompletionCallback = nullptr;
static bool                                     s_dbSucceeded = false;
static std::string                              s_dbResultString = "";

extern "C" {
    //DB系
    //TODO: 複数の呼び出しを同時にできるよう、s_XXX変数をmapで保持した方がよい
    //TODO: getStringFromNSDictionary() に相当する処理は、Android側でやっておくこと
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(selectDatabaseAtPathCallback)(JNIEnv* env, jobject self, bool succeeded, jstring record) {
        s_toDbCompletionCallback = true;
        s_dbSucceeded = succeeded;
        s_dbResultString = JNI_CSTRING_FROM_JSTRING(record);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(updateDatabaseAtPathCallback)(JNIEnv* env, jobject self, bool succeeded, jstring record) {
        s_toDbCompletionCallback = true;
        s_dbSucceeded = succeeded;
        s_dbResultString = JNI_CSTRING_FROM_JSTRING(record);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(replaceDatabaseAtPathCallback)(JNIEnv* env, jobject self, bool succeeded, jstring record) {
        s_toDbCompletionCallback = true;
        s_dbSucceeded = succeeded;
        s_dbResultString = JNI_CSTRING_FROM_JSTRING(record);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(deleteDatabaseAtPathCallback)(JNIEnv* env, jobject self, bool succeeded, jstring record) {
        s_toDbCompletionCallback = true;
        s_dbSucceeded = succeeded;
        s_dbResultString = JNI_CSTRING_FROM_JSTRING(record);
    }
}

void Native_CheckDBCallback(){
    if (s_toDbCompletionCallback) {
        if (s_dbCompletionCallback) {
            s_dbCompletionCallback(s_dbSucceeded, s_dbResultString);
            s_dbCompletionCallback = nullptr;
        }
        s_dbSucceeded = false;
        s_dbResultString = "";
        s_toDbCompletionCallback = nullptr;
    }
}


#pragma mark - database

JNI_METHOD_VOID_NO_ARG(Native_InitDatabase);

void Native_SelectDatabaseAtPath (NativeStorageType type, std::string path,                         std::function<void(bool, std::string jsonString)> completion){
    s_dbCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_SelectDatabaseAtPath";
    const char* paramCode = "(ILjava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_UpdateDatabaseAtPath (NativeStorageType type, std::string path, std::string jsonRecord, std::function<void(bool, std::string jsonString)> completion){
    s_dbCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_UpdateDatabaseAtPath";
    const char* paramCode = "(ILjava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        jstring jdata  = methodInfo.env->NewStringUTF(jsonRecord.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath, jdata);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(jdata);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_ReplaceDatabaseAtPath(NativeStorageType type, std::string path, std::string jsonRecord, std::function<void(bool, std::string jsonString)> completion){
    s_dbCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_ReplaceDatabaseAtPath";
    const char* paramCode = "(ILjava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        jstring jdata  = methodInfo.env->NewStringUTF(jsonRecord.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath, jdata);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(jdata);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_DeleteDatabaseAtPath (NativeStorageType type, std::string path,                         std::function<void(bool, std::string jsonString)> completion){
    s_dbCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_DeleteDatabaseAtPath";
    const char* paramCode = "(ILjava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

