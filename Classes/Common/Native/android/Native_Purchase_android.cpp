#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/PurchaseUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_PurchaseUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_PurchaseUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_Purchase.h"
#include "Native_Debug.h"

static LoadedCallback_t           s_purchaseLoadedCallback;
static PurchaseCallback_t         s_restoreSuccessCallback;
static RestoreCompletedCallback_t s_restoreCompletedCallback;
static RestoreCompletedCallback_t s_restoreFailureCallback;
static PurchaseClosedCallback_t   s_restoreClosedCallback;
static PurchaseCallback_t         s_purchaseSuccessCallback;
static PurchaseCallback_t         s_purchaseFailureCallback;
static PurchaseCallback_t         s_purchaseCancelCallback;
static PurchaseClosedCallback_t   s_purchaseClosedCallback;

static bool s_toPurchaseLoaded;
static bool s_toRestoreSuccess;
static bool s_toRestoreCompleted;
static bool s_toRestoreFailure;
static bool s_toRestoreClosed;
static bool s_toPurchaseSuccess;
static bool s_toPurchaseFailure;
static bool s_toPurchaseCancel;
static bool s_toPurchaseClosed;
static bool s_isPurchaseLoadedSuccess;
static std::string s_callbackProductId;

extern "C" {
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(purchaseLoadedCallback)(JNIEnv* env, jobject self, bool success) {
        s_toPurchaseLoaded = true;
        s_isPurchaseLoadedSuccess = success;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(restoreSuccessCallback)(JNIEnv* env, jobject self, jstring jproductId) {
        s_toRestoreSuccess = true;
        s_callbackProductId = JNI_CSTRING_FROM_JSTRING(jproductId);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(restoreCompletedCallback)(JNIEnv* env, jobject self) {
        s_toRestoreCompleted = true;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(restoreFailureCallback)(JNIEnv* env, jobject self) {
        s_toRestoreFailure = true;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(restoreClosedCallback)(JNIEnv* env, jobject self) {
        s_toRestoreClosed = true;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(purchaseSuccessCallback)(JNIEnv* env, jobject self, jstring jproductId) {
        s_toPurchaseSuccess = true;
        s_callbackProductId = JNI_CSTRING_FROM_JSTRING(jproductId);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(purchaseFailureCallback)(JNIEnv* env, jobject self, jstring jproductId) {
        s_toPurchaseFailure = true;
        s_callbackProductId = JNI_CSTRING_FROM_JSTRING(jproductId);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(purchaseCancelCallback)(JNIEnv* env, jobject self, jstring jproductId) {
        s_toPurchaseCancel = true;
        s_callbackProductId = JNI_CSTRING_FROM_JSTRING(jproductId);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(purchaseClosedCallback)(JNIEnv* env, jobject self) {
        s_toPurchaseClosed = true;
    }
}


//TODO: メインスレッドから呼び出すよう対応したい
void Native_CheckPurchaseCallback() {
    if (s_toPurchaseLoaded) {
        if (s_purchaseLoadedCallback) {
            s_purchaseLoadedCallback(s_isPurchaseLoadedSuccess);
            s_purchaseLoadedCallback = nullptr;
        }
        s_toPurchaseLoaded = false;
    }
    if (s_toRestoreSuccess) {
        if (s_restoreSuccessCallback){
            s_restoreSuccessCallback(s_callbackProductId);
            s_restoreSuccessCallback = nullptr;
        }
        s_toRestoreSuccess = false;
    }
    if (s_toRestoreCompleted) {
        if (s_restoreCompletedCallback){
            s_restoreCompletedCallback();
            s_restoreCompletedCallback = nullptr;
        }
        s_toRestoreCompleted = false;
    }
    if (s_toRestoreFailure) {
        if (s_restoreFailureCallback){
            s_restoreFailureCallback();
            s_restoreFailureCallback = nullptr;
        }
        s_toRestoreFailure = false;
    }
    if (s_toRestoreClosed) {
        if (s_restoreClosedCallback){
            s_restoreClosedCallback();
            s_restoreClosedCallback = nullptr;
        }
        s_toRestoreClosed = false;
    }
    if (s_toPurchaseSuccess) {
        if (s_purchaseSuccessCallback){
            s_purchaseSuccessCallback(s_callbackProductId);
            s_purchaseSuccessCallback = nullptr;
        }
        s_toPurchaseSuccess = false;
    }
    if (s_toPurchaseFailure) {
        if (s_purchaseFailureCallback){
            s_purchaseFailureCallback(s_callbackProductId);
            s_purchaseFailureCallback = nullptr;
        }
        s_toPurchaseFailure = false;
    }
    if (s_toPurchaseCancel) {
        if (s_purchaseCancelCallback){
            s_purchaseCancelCallback(s_callbackProductId);
            s_purchaseCancelCallback = nullptr;
        }
        s_toPurchaseCancel = false;
    }
    if (s_toPurchaseClosed) {
        if (s_purchaseClosedCallback){
            s_purchaseClosedCallback();
            s_purchaseClosedCallback = nullptr;
        }
        s_toPurchaseClosed = false;
    }
}

//unregister
JNI_METHOD_BOOL_NO_ARG(Native_PurchaseUnRegister);

//register
JNI_METHOD_BOOL_NO_ARG(Native_PurchaseRegister);

bool Native_PurchaseRegister(LoadedCallback_t loadedCallback){
    s_purchaseLoadedCallback = loadedCallback;
    return Native_PurchaseRegister();
}

JNI_METHOD_BOOL_NO_ARG(Native_PurchaseRequestHasFinished);

JNI_METHOD_BOOL_NO_ARG(Native_PurchaseInitHasSucceeded);


//restore
JNI_METHOD_BOOL_NO_ARG(Native_Restore);
bool Native_Restore(PurchaseCallback_t successCallback, RestoreCompletedCallback_t failureCallback, RestoreCompletedCallback_t completedCallback, PurchaseClosedCallback_t closedCallback){
    s_restoreSuccessCallback   = successCallback;
    s_restoreFailureCallback   = failureCallback;
    s_restoreCompletedCallback = completedCallback;
    s_restoreClosedCallback    = closedCallback;
    return Native_Restore();
}

//buy
bool Native_PurchaseItem(std::string productId, PurchaseCallback_t successCallback, PurchaseCallback_t failureCallback, PurchaseCallback_t cancelCallback, PurchaseClosedCallback_t closedCallback){
    s_purchaseSuccessCallback = successCallback;
    s_purchaseFailureCallback = failureCallback;
    s_purchaseCancelCallback  = cancelCallback;
    s_purchaseClosedCallback  = closedCallback;
    
    bool ret = false;
    JniMethodInfo methodInfo;
    const char* methodName = "Native_PurchaseItem";
    const char* paramCode = "(Ljava/lang/String;)Z";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jstr = methodInfo.env->NewStringUTF(productId.c_str());
        JNI_CHECK_EXCEPTION;
        ret = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID , methodInfo.methodID, jstr);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jstr);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
    return ret;
}

JNI_METHOD_STRING_ARG_STRING(Native_GetProductName, productId);
JNI_METHOD_STRING_ARG_STRING(Native_GetProductDescription, productId);
JNI_METHOD_STRING_ARG_STRING(Native_GetProductPrice, productId);

