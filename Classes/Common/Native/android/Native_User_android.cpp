#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/UserUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_UserUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_UserUtils_##func##Jni

#include <functional>
#include "Native_Common_android.h"
#include "Native_User.h"



JNI_METHOD_VOID_NO_ARG(Native_InitUserAuth);
JNI_METHOD_VOID_NO_ARG(Native_UserAuthentication);
JNI_METHOD_BOOL_NO_ARG(Native_IsUserAuthenticated);
JNI_METHOD_STRING_NO_ARG(Native_GetUserAuthId);
JNI_METHOD_STRING_NO_ARG(Native_GetUserAuthName);
JNI_METHOD_STRING_NO_ARG(Native_GetUserAuthEmail);
void Native_SetUserAuthListenerSignIn(std::function<void(std::string uid)> inCallback, std::function<void()> outCallback){
    //not used
}
