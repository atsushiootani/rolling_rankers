#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/rolling/MainActivity"
#define JNI_CLASS_PREFIX  kazuma_saitou_rolling_MainActivity
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_rolling_MainActivity_##func##Jni

#include "Native_Common_android.h"
#include "Native_Debug.h"

JNI_METHOD_BOOL_NO_ARG(Native_IsDebug);

bool Native_IsIos() {
	return false;
}

bool Native_IsAndroid(){
	return true;
}

JNI_METHOD_VOID_ARG_STRING(Native_Print, text);
