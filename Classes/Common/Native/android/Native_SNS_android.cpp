#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/TwitterUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_TwitterUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_TwitterUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_SNS.h"

static ShareCallback_t s_shareSuccessCallback = nullptr;
static ShareCallback_t s_shareFailureCallback = nullptr;
static ShareCallback_t s_shareCancelCallback = nullptr;
static ShareCallback_t s_shareAlwaysCallback = nullptr;
static bool s_toShareSuccessCallback = false;
static bool s_toShareFailureCallback = false;
static bool s_toShareCancelCallback = false;
static bool s_toShareAlwaysCallback = false;

extern "C" {
	JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(shareSuccessCallback)(JNIEnv* env, jobject self) {
		s_toShareSuccessCallback = true;
	}
	JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(shareFailureCallback)(JNIEnv* env, jobject self) {
		s_toShareFailureCallback = true;
	}
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(shareCancelCallback)(JNIEnv* env, jobject self) {
		s_toShareCancelCallback = true;
	}
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(shareAlwaysCallback)(JNIEnv* env, jobject self) {
        s_toShareAlwaysCallback = true;
    }
}

void Native_CheckSNSCallback(){
	if (s_toShareSuccessCallback) {
		if (s_shareSuccessCallback) {
			s_shareSuccessCallback();
		}
		s_shareSuccessCallback = nullptr;
		s_toShareSuccessCallback = false;
	}
	if (s_toShareFailureCallback) {
		if (s_shareFailureCallback) {
			s_shareFailureCallback();
		}
		s_shareFailureCallback = nullptr;
		s_toShareFailureCallback = false;
	}
	if (s_toShareCancelCallback) {
		if (s_shareCancelCallback) {
			s_shareCancelCallback();
		}
		s_shareCancelCallback = nullptr;
		s_toShareCancelCallback = false;
	}
    if (s_toShareAlwaysCallback) {
        if (s_shareAlwaysCallback) {
            s_shareAlwaysCallback();
        }
        s_shareAlwaysCallback = nullptr;
        s_toShareAlwaysCallback = false;
    }
}

void Native_FacebookPost(std::string text, std::string imageName /*= nullptr*/, std::string url /*= nullptr*/, ShareCallback_t successCallback /*= nullptr*/, ShareCallback_t failureCallback /*= nullptr*/, ShareCallback_t cancelCallback /*= nullptr*/, ShareCallback_t alwaysCallback /*= nullptr*/){
	//do nothing
}

void Native_TwitterPost(std::string text, std::string imageName /*= nullptr*/, std::string url /*= nullptr*/, ShareCallback_t successCallback /*= nullptr*/, ShareCallback_t failureCallback /*= nullptr*/, ShareCallback_t cancelCallback /*= nullptr*/, ShareCallback_t alwaysCallback /*= nullptr*/) {
	s_shareSuccessCallback = successCallback;
	s_shareFailureCallback = failureCallback;
	s_shareCancelCallback = cancelCallback;
    s_shareAlwaysCallback = alwaysCallback;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_TwitterPost";
    const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
    	jstring jtext  = methodInfo.env->NewStringUTF(text.c_str());
    	jstring jimage = methodInfo.env->NewStringUTF(imageName.c_str());
    	jstring jurl   = methodInfo.env->NewStringUTF(url.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jtext, jimage, jurl);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtext);
        methodInfo.env->DeleteLocalRef(jimage);
        methodInfo.env->DeleteLocalRef(jurl);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

