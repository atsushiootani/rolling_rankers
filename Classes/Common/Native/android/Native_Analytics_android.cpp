#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/AnalyticsUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_AnalyticsUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_AnalyticsUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_Analytics.h"


void Native_Analytics(std::string category, std::string action, std::string label, int value){
	JniMethodInfo methodInfo;
	const char* methodName = "Native_Analytics";
	const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jcategory = methodInfo.env->NewStringUTF(category.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jaction   = methodInfo.env->NewStringUTF(action.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jlabel    = methodInfo.env->NewStringUTF(label.c_str());
        JNI_CHECK_EXCEPTION;
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jcategory, jaction, jlabel, value);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jcategory);
        methodInfo.env->DeleteLocalRef(jaction);
        methodInfo.env->DeleteLocalRef(jlabel);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
}

JNI_METHOD_VOID_ARG_STRING2(Native_ErrorReport, error, subData);

JNI_METHOD_VOID_ARG_STRING3(Native_AnalyticsCustom, eventName, key1, value1);
void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1, std::string key2, std::string value2){} //TODO:
void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1, std::string key2, std::string value2, std::string key3, std::string value3){} //TODO:

JNI_METHOD_VOID_NO_ARG(Native_AnalyticsTutorialBegin);
JNI_METHOD_VOID_NO_ARG(Native_AnalyticsTutorialEnded);
JNI_METHOD_VOID_NO_ARG(Native_AnalyticsAppOpen);
JNI_METHOD_VOID_ARG_STRING2(Native_AnalyticsShare, contentType, itemId);
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsShareTwitter, itemId);
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsShareFacebook, itemId);
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsEcommercePurchaseBegin, itemName);
JNI_METHOD_VOID_ARG_INT_STRING(Native_AnalyticsEcommercePurchase, price, itemName);
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsEcommercePurchase, itemName);
void Native_AnalyticsSpendVirtualCurrency(std::string itemName, std::string currencyName, int value){} //TODO:
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsBuyItem, itemName);
JNI_METHOD_VOID_ARG_STRING_INT(Native_AnalyticsBuyItem, itemName, price);
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsUnlockAchievement, achievementId);
JNI_METHOD_VOID_ARG_STRING(Native_AnalyticsSelectContent, contentName);
JNI_METHOD_VOID_ARG_STRING2(Native_AnalyticsSelectContent, contentName, itemId);
JNI_METHOD_VOID_ARG_STRING_INT(Native_AnalyticsSelectContent, contentName, itemId);
JNI_METHOD_VOID_NO_ARG(Native_AnalyticsReview);
