//#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/rolling/MainActivity"
#define JNI_CLASS_PREFIX  kazuma_saitou_rolling_MainActivity
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_rolling_MainActivity_##func##Jni

#include "Native_Common_android.h"
#include "Native_Ad.h"

static std::function<void(int)>  s_insentiveCallback;
static std::function<void(bool)> s_closedCallback;
static std::function<void()>     s_failureCallback;
static bool s_toInsentive = false;
static bool s_toClosed    = false;
static bool s_toFailure   = false;
static int  s_insentiveAmount = 1;
static bool s_insentiveSuccess = false;

extern "C" {
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(incentiveCallback)(JNIEnv* env, jobject self, int amount) {
        s_toInsentive = true;
        s_insentiveAmount = amount;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(incentiveFailureCallback)(JNIEnv* env, jobject self) {
        s_toFailure = true;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(incentiveClosedCallback)(JNIEnv* env, jobject self, bool success) {
        s_toClosed = true;
        s_insentiveSuccess = success;
    }
}

void Native_CheckAdCallback(){
	if (s_toInsentive) {
		if (s_insentiveCallback) {
			s_insentiveCallback(s_insentiveAmount);
			s_insentiveCallback = nullptr;
			s_insentiveAmount = 1;
		}
		s_toInsentive = false;
	}
    if (s_toFailure) {
        if (s_failureCallback) {
            s_failureCallback();
            s_failureCallback = nullptr;
        }
    }
    if (s_toClosed) {
        if (s_closedCallback) {
            s_closedCallback(s_insentiveSuccess);
            s_closedCallback = nullptr;
            s_insentiveSuccess = false;
        }
    }
}


JNI_METHOD_VOID_NO_ARG(Native_AdRegister);

void Native_AdEnterBackground(){
    //do nothing //TODO:
}
void Native_AdEnterForeground(){
    //do nothing //TODO:
}

void Native_AdAllHide(){
    Native_AdBannerHide();
}


JNI_METHOD_VOID_NO_ARG(Native_AdBannerShow);
JNI_METHOD_VOID_NO_ARG(Native_AdBannerHide);
JNI_METHOD_VOID_NO_ARG(Native_AdBannerSetEnable);
JNI_METHOD_VOID_NO_ARG(Native_AdBannerSetDisable);

JNI_METHOD_VOID_ARG_INT(Native_AdIconShowAt, pos);
JNI_METHOD_VOID_NO_ARG(Native_AdIconShow);
JNI_METHOD_VOID_NO_ARG(Native_AdIconHide);
JNI_METHOD_VOID_ARG_INT(Native_AdIconHideAt, pos);

JNI_METHOD_VOID_NO_ARG(Native_AdInterstitialShow);
JNI_METHOD_VOID_NO_ARG(Native_AdInterstitialAlwaysShow);

JNI_METHOD_VOID_NO_ARG(Native_AdHeaderShow);
JNI_METHOD_VOID_NO_ARG(Native_AdHeaderHide);

JNI_METHOD_VOID_NO_ARG(Native_AdWallShow);

void Native_AdInsentiveShow(std::function<void(int)> insentiveCallback, std::function<void()> failureCallback, std::function<void(bool)> closedCallback) {
	s_insentiveCallback = insentiveCallback;
    s_failureCallback = failureCallback;
    s_closedCallback = closedCallback;

    JniMethodInfo methodInfo;
    const char* methodName = "Native_AdInsentiveShow";
    const char* paramCode = "()V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

JNI_METHOD_BOOL_NO_ARG(Native_AdCanGetInsentive);
JNI_METHOD_BOOL_NO_ARG(Native_AdMovieIsPlaying);

//他のアプリ
JNI_METHOD_BOOL_ARG_STRING(Native_CheckApplicationInstalled, urlScheme);
JNI_METHOD_VOID_ARG_STRING(Native_OpenInstalledApplication, urlScheme);

