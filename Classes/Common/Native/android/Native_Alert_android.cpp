//#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/AlertUtils"
#define JNI_CLASS_PREFIX kazuma_saitou_util_AlertUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_AlertUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_Alert.h"
#include "Native_Debug.h"
#include <string>

static AlertCallback_t  s_alertCallback;
static bool s_toAlertCallback;
static int s_index;

static std::function<void(const char*)> s_keyboardInputCallback;
static std::function<void()>            s_keyboardCancelCallback;
static bool        s_toKeyboardInputCallback;
static bool        s_toKeyboardCancelCallback;
static std::string s_keyboardString;


extern "C" {
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(alertCallback)(JNIEnv* env, jobject self, jint jindex) {
		s_toAlertCallback = true;
		s_index = jindex;
	}

    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(showKeyboardCallback)(JNIEnv* env, jobject self, jstring jtext) {
        s_toKeyboardInputCallback = true;
        s_keyboardString = JNI_CSTRING_FROM_JSTRING(jtext);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(showKeyboardNumPadCallback)(JNIEnv* env, jobject self, jstring jtext) {
        s_toKeyboardInputCallback = true;
        s_keyboardString = JNI_CSTRING_FROM_JSTRING(jtext);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(showKeyboardCancelCallback)(JNIEnv* env, jobject self) {
        s_toKeyboardCancelCallback = true;
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(showKeyboardNumPadCancelCallback)(JNIEnv* env, jobject self) {
        s_toKeyboardCancelCallback = true;
    }
}

//アラートビュー
bool Native_IsAlertOn(){
    return s_alertCallback != nullptr;
}

void Native_CheckAlertViewCallback() {
	//TODO: Java側からメインスレッドでコールバック呼び出せばこの関数は不要
	if (s_toAlertCallback) {
		if (s_alertCallback) {
			s_alertCallback((AlertButtonIndex)s_index);
			s_alertCallback = nullptr;
		}
		s_toAlertCallback = false;
	}
}

void Native_CheckKeyboardCallback(){
    if (s_toKeyboardInputCallback) {
        if (s_keyboardInputCallback) {
            s_keyboardInputCallback(s_keyboardString.c_str());
            s_keyboardInputCallback = nullptr;
            s_keyboardString = "";
        }
        s_toKeyboardInputCallback = false;
    }
    if (s_toKeyboardCancelCallback) {
        if (s_keyboardCancelCallback) {
            s_keyboardCancelCallback();
            s_keyboardCancelCallback = nullptr;
        }
        s_toKeyboardCancelCallback = false;
    }
}


void Native_AlertView(std::string title, std::string message, std::string okTitle, AlertCallback_t alertCallback /*= nullptr*/){
	JniMethodInfo methodInfo;
	const char* methodName = "Native_AlertView";
	const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jtitle   = methodInfo.env->NewStringUTF(title.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jmessage = methodInfo.env->NewStringUTF(message.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jokTitle = methodInfo.env->NewStringUTF(okTitle.c_str());
        JNI_CHECK_EXCEPTION;
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jtitle, jmessage, jokTitle);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtitle);
        methodInfo.env->DeleteLocalRef(jmessage);
        methodInfo.env->DeleteLocalRef(jokTitle);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);

        s_alertCallback = alertCallback;
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_AlertView(std::string title, std::string message, std::string noTitle, std::string yesTitle, AlertCallback_t alertCallback /*= nullptr*/){
	JniMethodInfo methodInfo;
	const char* methodName = "Native_AlertView";
	const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jtitle   = methodInfo.env->NewStringUTF(title.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jmessage = methodInfo.env->NewStringUTF(message.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jnoTitle = methodInfo.env->NewStringUTF(noTitle.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jyesTitle= methodInfo.env->NewStringUTF(yesTitle.c_str());
        JNI_CHECK_EXCEPTION;
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jtitle, jmessage, jnoTitle, jyesTitle);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtitle);
        methodInfo.env->DeleteLocalRef(jmessage);
        methodInfo.env->DeleteLocalRef(jnoTitle);
        methodInfo.env->DeleteLocalRef(jyesTitle);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);

        s_alertCallback = alertCallback;
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_AlertView(std::string title, std::string message, std::string cancelTitle, std::string opt1Title, std::string opt2Title, AlertCallback_t alertCallback /*= nullptr*/){
	JniMethodInfo methodInfo;
	const char* methodName = "Native_AlertView";
	const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
		jstring jtitle   = methodInfo.env->NewStringUTF(title.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jmessage = methodInfo.env->NewStringUTF(message.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jcancelTitle = methodInfo.env->NewStringUTF(cancelTitle.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jopt1Title = methodInfo.env->NewStringUTF(opt1Title.c_str());
        JNI_CHECK_EXCEPTION;
		jstring jopt2Title = methodInfo.env->NewStringUTF(opt2Title.c_str());
        JNI_CHECK_EXCEPTION;
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jtitle, jmessage, jcancelTitle, jopt1Title, jopt2Title);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtitle);
        methodInfo.env->DeleteLocalRef(jmessage);
        methodInfo.env->DeleteLocalRef(jcancelTitle);
        methodInfo.env->DeleteLocalRef(jopt1Title);
        methodInfo.env->DeleteLocalRef(jopt2Title);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);

        s_alertCallback = alertCallback;
	}
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_AlertView(std::string title, std::string message, std::string cancelTitle, std::string opt1Title, std::string opt2Title, std::string opt3Title, AlertCallback_t alertCallback /*= nullptr*/){
    JniMethodInfo methodInfo;
    const char* methodName = "Native_AlertView";
    const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jtitle   = methodInfo.env->NewStringUTF(title.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jmessage = methodInfo.env->NewStringUTF(message.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jcancelTitle = methodInfo.env->NewStringUTF(cancelTitle.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jopt1Title = methodInfo.env->NewStringUTF(opt1Title.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jopt2Title = methodInfo.env->NewStringUTF(opt2Title.c_str());
        JNI_CHECK_EXCEPTION;
        jstring jopt3Title = methodInfo.env->NewStringUTF(opt3Title.c_str());
        JNI_CHECK_EXCEPTION;
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jtitle, jmessage, jcancelTitle, jopt1Title, jopt2Title, jopt3Title);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jtitle);
        methodInfo.env->DeleteLocalRef(jmessage);
        methodInfo.env->DeleteLocalRef(jcancelTitle);
        methodInfo.env->DeleteLocalRef(jopt1Title);
        methodInfo.env->DeleteLocalRef(jopt2Title);
        methodInfo.env->DeleteLocalRef(jopt3Title);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
        
        s_alertCallback = alertCallback;
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}


#undef  JNI_CLASS_NAME
#undef  JNI_CLASS_PREFIX
#define JNI_CLASS_NAME   "kazuma/saitou/rolling/MainActivity"
#define JNI_CLASS_PREFIX  kazuma_saitou_rolling_MainActivity
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_rolling_MainActivity_##func##Jni

//インジケータ
JNI_METHOD_VOID_ARG_STRING(Native_ShowIndicator, message);
JNI_METHOD_VOID_NO_ARG(Native_HideIndicator);

//キーボード
void Native_ShowKeyboard(      std::string message, std::string text, std::function<void(const char*)> inputCallback, std::function<void()> cancelCallback /*= nullptr*/){
    s_keyboardInputCallback  = inputCallback;
    s_keyboardCancelCallback = cancelCallback;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_ShowKeyboard";
    const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jmessage = methodInfo.env->NewStringUTF(message.c_str());
        jstring jtext    = methodInfo.env->NewStringUTF(text.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jmessage, jtext);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jmessage);
        methodInfo.env->DeleteLocalRef(jtext);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_ShowKeyboardNumPad(std::string message, std::string text, std::function<void(const char*)> inputCallback, std::function<void()> cancelCallback /*= nullptr*/){
    s_keyboardInputCallback  = inputCallback;
    s_keyboardCancelCallback = cancelCallback;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_ShowKeyboardNumPad";
    const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jmessage = methodInfo.env->NewStringUTF(message.c_str());
        jstring jtext    = methodInfo.env->NewStringUTF(text.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jmessage, jtext);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jmessage);
        methodInfo.env->DeleteLocalRef(jtext);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

//スリープモード
JNI_METHOD_VOID_NO_ARG(Native_SleepableMode);
JNI_METHOD_VOID_NO_ARG(Native_KeepScreenMode);

