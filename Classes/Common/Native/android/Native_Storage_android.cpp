#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/StorageUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_StsorageUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_StorageUtils_##func##Jni

#include "Native_Consts.h"
#include "Native_Common_android.h"
#include "Native_Pronounce.h"


static bool                                   s_toStorageFetchCompletionCallback = false;
static bool                                   s_toStorageSetCompletionCallback = false;
static std::function<void(bool, std::string)> s_storageFetchCompletionCallback = nullptr;
static std::function<void(bool)>              s_storageSetCompletionCallback = nullptr;
static bool                                   s_storageSucceeded = false;
static std::string                            s_storageString = "";


extern "C" {
    //storage系
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(getStorageDataAndSaveToFileCallback)(JNIEnv* env, jobject self, bool succeeded, jstring jtext) {
        s_toStorageFetchCompletionCallback = true;
        s_storageSucceeded = succeeded;
        s_storageString = JNI_CSTRING_FROM_JSTRING(jtext);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(getStorageDataAsStringCallback)(JNIEnv* env, jobject self, bool succeeded, jstring jtext) {
        s_toStorageFetchCompletionCallback = true;
        s_storageSucceeded = succeeded;
        s_storageString = JNI_CSTRING_FROM_JSTRING(jtext);
    }
    JNIEXPORT void JNICALL JNI_CALLBACK_FUNC_NAME_MAKE(setStorageDataWithStringCallback)(JNIEnv* env, jobject self, bool succeeded) {
        s_toStorageSetCompletionCallback = true;
        s_storageSucceeded = succeeded;
    }
}

void Native_CheckStorageCallback(){
    if (s_toStorageFetchCompletionCallback) {
        if (s_storageFetchCompletionCallback) {
            s_storageFetchCompletionCallback(s_storageSucceeded, s_storageString);
            s_storageFetchCompletionCallback = nullptr;
        }
        s_storageSucceeded = false;
        s_storageString = "";
        s_toStorageFetchCompletionCallback = nullptr;
    }
    
    if (s_toStorageSetCompletionCallback) {
        if (s_storageSetCompletionCallback) {
            s_storageSetCompletionCallback(s_storageSucceeded);
            s_storageSetCompletionCallback = nullptr;
        }
        s_storageSucceeded = false;
        s_toStorageSetCompletionCallback = nullptr;
    }
}


JNI_METHOD_BOOL_NO_ARG(Native_InitStorage);

void Native_GetStorageDataAndSaveToFile(NativeStorageType type, std::string path, std::function<void(bool, std::string           )> completion){
    s_storageFetchCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_GetStorageDataAndSaveToFile";
    const char* paramCode = "(ILjava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_GetStorageDataAsString     (NativeStorageType type, std::string path, std::function<void(bool, std::string           )> completion){
    s_storageFetchCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_GetStorageDataAsString";
    const char* paramCode = "(ILjava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_GetStorageDataAndAlloc     (NativeStorageType type, std::string path, std::function<void(bool, unsigned char*, size_t)> completion){
    //not used
}

void Native_DeallocStorageData(unsigned char* data){
    //not used
}

void Native_SetStorageDataWithString(NativeStorageType type, std::string path, std::string str,                     std::function<void(bool)> completion){
    s_storageSetCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_SetStorageDataWithString";
    const char* paramCode = "(ILjava/lang/String;Ljava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        jstring jstr   = methodInfo.env->NewStringUTF(str.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath, jstr);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(jstr);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

void Native_SetStorageData          (NativeStorageType type, std::string path, unsigned char* byteData, size_t len, std::function<void(bool)> completion){
    //not used
}

void Native_DeleteStorageData       (NativeStorageType type, std::string path,                                      std::function<void(bool)> completion){
    s_storageSetCompletionCallback = completion;
    
    JniMethodInfo methodInfo;
    const char* methodName = "Native_DeleteStorageData";
    const char* paramCode = "(ILjava/lang/String;)V";
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {
        jstring jpath  = methodInfo.env->NewStringUTF(path.c_str());
        methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, type, jpath);
        JNI_CHECK_EXCEPTION;
        methodInfo.env->DeleteLocalRef(jpath);
        methodInfo.env->DeleteLocalRef(methodInfo.classID);
    }
    else{
        JNI_CHECK_EXCEPTION;
    }
}

