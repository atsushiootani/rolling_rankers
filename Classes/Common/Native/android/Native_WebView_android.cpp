#include "Native_Common_android_consts.h"
#define JNI_CLASS_NAME   "kazuma/saitou/util/WebViewUtils"
#define JNI_CLASS_PREFIX  kazuma_saitou_util_WebViewUtils
#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_kazuma_saitou_util_WebViewUtils_##func##Jni

#include "Native_Common_android.h"
#include "Native_WebView.h"


JNI_METHOD_VOID_ARG_CCSTRING(Native_OpenWebPage, url);
void Native_OpenStorePage(const char *url){
    Native_OpenWebPage(url);
}

JNI_METHOD_VOID_ARG_CCSTRING(Native_OpenWebView, url);
JNI_METHOD_VOID_NO_ARG(Native_CloseWebView);

void Native_PreloadWebView(const char *url){}
void Native_ShowWebView(){} //TODO:
void Native_HideWebView(){} //TODO:
JNI_METHOD_BOOL_NO_ARG(Native_HasWebView);
