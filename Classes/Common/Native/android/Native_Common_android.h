#ifndef Native_Common_android_h
#define Native_Common_android_h

#include <jni.h> 
#include "platform/android/jni/JniHelper.h"
#include "Native_Debug.h"

using namespace cocos2d;



#if !defined (JNI_CLASS_NAME)
#error need to define JNI_CLASS_NAME
#endif

#if !defined (JNI_CLASS_PREFIX)
#error need to define JNI_CLASS_PREFIX
#endif

//マクロの展開順序が不定のため、各ファイルにて定義のこと
//#define JNI_CALLBACK_FUNC_NAME_MAKE(func) Java_##JNI_CLASS_PREFIX##_##func##Jni

#define JNI_CSTRING_FROM_JSTRING(jstr) JniHelper::jstring2string(jstr)

#define JNI_CHECK_EXCEPTION do {                 \
		if (methodInfo.env->ExceptionCheck()) {  \
			methodInfo.env->ExceptionDescribe(); \
			methodInfo.env->ExceptionClear();    \
		}                                        \
	}while(0)


/**
 * JNIのメソッド定義用マクロ
 */

//void func();
#define JNI_METHOD_VOID_NO_ARG(func)                                                             \
	void func(){                                                                                 \
    	JniMethodInfo methodInfo;                                                                \
    	const char* methodName = #func;                                                          \
    	const char* paramCode = "()V";                                                           \
    	if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) { \
    		methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID);      \
            JNI_CHECK_EXCEPTION;                                                                 \
    		methodInfo.env->DeleteLocalRef(methodInfo.classID);                                  \
    	}                                                                                        \
        else{                                                                                    \
            JNI_CHECK_EXCEPTION;                                                                 \
        }                                                                                        \
	}

//void func(int);
#define JNI_METHOD_VOID_ARG_INT(func, arg)                                                        \
	void func(int arg){                                                                           \
		JniMethodInfo methodInfo;                                                                 \
		const char* methodName = #func;                                                           \
		const char* paramCode = "(I)V";                                                           \
		if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {  \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, arg);  \
            JNI_CHECK_EXCEPTION;                                                                  \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                   \
		}                                                                                         \
        else{                                                                                     \
            JNI_CHECK_EXCEPTION;                                                                  \
        }                                                                                         \
	}

//void func(bool);
#define JNI_METHOD_VOID_ARG_BOOL(func, arg)                                                       \
    void func(bool arg){                                                                          \
        JniMethodInfo methodInfo;                                                                 \
        const char* methodName = #func;                                                           \
        const char* paramCode = "(Z)V";                                                           \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {  \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, arg);  \
            JNI_CHECK_EXCEPTION;                                                                  \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                   \
        }                                                                                         \
        else{                                                                                     \
            JNI_CHECK_EXCEPTION;                                                                  \
        }                                                                                         \
    }

//void func(std::string);
#define JNI_METHOD_VOID_ARG_STRING(func, arg)                                                     \
    void func(std::string arg){                                                                   \
        JniMethodInfo methodInfo;                                                                 \
        const char* methodName = #func;                                                           \
        const char* paramCode = "(Ljava/lang/String;)V";                                          \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {  \
            jstring jstr = methodInfo.env->NewStringUTF(arg.c_str());                             \
            JNI_CHECK_EXCEPTION;                                                                  \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jstr); \
            JNI_CHECK_EXCEPTION;                                                                  \
            methodInfo.env->DeleteLocalRef(jstr);                                                 \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                   \
        }                                                                                         \
        else{                                                                                     \
            JNI_CHECK_EXCEPTION;                                                                  \
        }                                                                                         \
    }

//void func(const char*);
#define JNI_METHOD_VOID_ARG_CCSTRING(func, arg)                                                   \
    void func(const char* arg){                                                                   \
        JniMethodInfo methodInfo;                                                                 \
        const char* methodName = #func;                                                           \
        const char* paramCode = "(Ljava/lang/String;)V";                                          \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {  \
            jstring jstr = methodInfo.env->NewStringUTF(arg);                                     \
            JNI_CHECK_EXCEPTION;                                                                  \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jstr); \
            JNI_CHECK_EXCEPTION;                                                                  \
            methodInfo.env->DeleteLocalRef(jstr);                                                 \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                   \
        }                                                                                         \
        else{                                                                                     \
            JNI_CHECK_EXCEPTION;                                                                  \
        }                                                                                         \
    }

//void func(std::string, std::string)
#define JNI_METHOD_VOID_ARG_STRING2(func, arg, arg2)                                                     \
    void func(std::string arg, std::string arg2){                                                        \
        JniMethodInfo methodInfo;                                                                        \
        const char* methodName = #func;                                                                  \
        const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;)V";                               \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {         \
            jstring jstr  = methodInfo.env->NewStringUTF(arg.c_str());                                   \
            JNI_CHECK_EXCEPTION;                                                                         \
            jstring jstr2 = methodInfo.env->NewStringUTF(arg2.c_str());                                  \
            JNI_CHECK_EXCEPTION;                                                                         \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jstr, jstr2); \
            JNI_CHECK_EXCEPTION;                                                                         \
            methodInfo.env->DeleteLocalRef(jstr);                                                        \
            methodInfo.env->DeleteLocalRef(jstr2);                                                       \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                          \
        }                                                                                                \
        else{                                                                                            \
            JNI_CHECK_EXCEPTION;                                                                         \
        }                                                                                                \
    }

//void func(std::string, std::string, std::string)
#define JNI_METHOD_VOID_ARG_STRING3(func, arg, arg2, arg3)                                               \
    void func(std::string arg, std::string arg2, std::string arg3){                                      \
        JniMethodInfo methodInfo;                                                                        \
        const char* methodName = #func;                                                                  \
        const char* paramCode = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";             \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {         \
            jstring jstr  = methodInfo.env->NewStringUTF(arg.c_str());                                   \
            JNI_CHECK_EXCEPTION;                                                                         \
            jstring jstr2 = methodInfo.env->NewStringUTF(arg2.c_str());                                  \
            JNI_CHECK_EXCEPTION;                                                                         \
            jstring jstr3 = methodInfo.env->NewStringUTF(arg3.c_str());                                  \
            JNI_CHECK_EXCEPTION;                                                                         \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jstr, jstr2, jstr3); \
            JNI_CHECK_EXCEPTION;                                                                         \
            methodInfo.env->DeleteLocalRef(jstr);                                                        \
            methodInfo.env->DeleteLocalRef(jstr2);                                                       \
            methodInfo.env->DeleteLocalRef(jstr3);                                                       \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                          \
        }                                                                                                \
        else{                                                                                            \
            JNI_CHECK_EXCEPTION;                                                                         \
        }                                                                                                \
    }

//void func(int, std::string);
#define JNI_METHOD_VOID_ARG_INT_STRING(func, arg, arg2)                                                 \
    void func(int arg, std::string arg2){                                                               \
        JniMethodInfo methodInfo;                                                                       \
        const char* methodName = #func;                                                                 \
        const char* paramCode = "(ILjava/lang/String;)V";                                               \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {        \
            jstring jstr2 = methodInfo.env->NewStringUTF(arg2.c_str());                                 \
            JNI_CHECK_EXCEPTION;                                                                        \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, arg, jstr2); \
            JNI_CHECK_EXCEPTION;                                                                        \
            methodInfo.env->DeleteLocalRef(jstr2);                                                      \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                         \
        }                                                                                               \
        else{                                                                                           \
            JNI_CHECK_EXCEPTION;                                                                        \
        }                                                                                               \
    }

//void func(std::string, int);
#define JNI_METHOD_VOID_ARG_STRING_INT(func, arg, arg2)                                                 \
    void func(std::string arg, int arg2){                                                               \
        JniMethodInfo methodInfo;                                                                       \
        const char* methodName = #func;                                                                 \
        const char* paramCode = "(Ljava/lang/String;I)V";                                               \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {        \
            jstring jstr = methodInfo.env->NewStringUTF(arg.c_str());                                   \
            JNI_CHECK_EXCEPTION;                                                                        \
            methodInfo.env->CallStaticVoidMethod(methodInfo.classID , methodInfo.methodID, jstr, arg2); \
            JNI_CHECK_EXCEPTION;                                                                        \
            methodInfo.env->DeleteLocalRef(jstr);                                                       \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                         \
        }                                                                                               \
        else{                                                                                           \
            JNI_CHECK_EXCEPTION;                                                                        \
        }                                                                                               \
    }


//bool func();
#define JNI_METHOD_BOOL_NO_ARG(func)                                                                 \
    bool func(){                                                                                     \
        bool ret = false;                                                                            \
        JniMethodInfo methodInfo;                                                                    \
        const char* methodName = #func;                                                              \
        const char* paramCode = "()Z";                                                               \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {     \
            JNI_CHECK_EXCEPTION;                                                                     \
            ret = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID , methodInfo.methodID); \
            JNI_CHECK_EXCEPTION;                                                                     \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                      \
        }                                                                                            \
        else{                                                                                        \
            JNI_CHECK_EXCEPTION;                                                                     \
        }                                                                                            \
        return ret;                                                                                  \
	}

//bool func(std::string);
#define JNI_METHOD_BOOL_ARG_STRING(func, arg)                                                        \
    bool func(std::string arg){                                                                      \
        bool ret = false;                                                                            \
        JniMethodInfo methodInfo;                                                                    \
        const char* methodName = #func;                                                              \
        const char* paramCode = "(Ljava/lang/String;)Z";                                             \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {     \
            JNI_CHECK_EXCEPTION;                                                                     \
            jstring jarg = methodInfo.env->NewStringUTF(arg.c_str());                                \
            JNI_CHECK_EXCEPTION;                                                                     \
            ret = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID , methodInfo.methodID, jarg); \
            JNI_CHECK_EXCEPTION;                                                                     \
            methodInfo.env->DeleteLocalRef(jarg);                                                    \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                      \
        }                                                                                            \
        else{                                                                                        \
            JNI_CHECK_EXCEPTION;                                                                     \
        }                                                                                            \
        return ret;                                                                                  \
    }

//int func();
#define JNI_METHOD_INT_NO_ARG(func)                                                                  \
    int func(){                                                                                      \
        int ret = 0;                                                                                 \
        JniMethodInfo methodInfo;                                                                    \
        const char* methodName = #func;                                                              \
        const char* paramCode = "()I";                                                               \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {     \
            JNI_CHECK_EXCEPTION;                                                                     \
            ret = methodInfo.env->CallStaticIntMethod(methodInfo.classID , methodInfo.methodID);     \
            JNI_CHECK_EXCEPTION;                                                                     \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                      \
            JNI_CHECK_EXCEPTION;                                                                     \
        }                                                                                            \
        else{                                                                                        \
            JNI_CHECK_EXCEPTION;                                                                     \
        }                                                                                            \
        return ret;                                                                                  \
	}

//std::string func();
#define JNI_METHOD_STRING_NO_ARG(func)                                                                              \
    std::string func() {                                                                                            \
    std::string ret;                                                                                                \
    JniMethodInfo methodInfo;                                                                                       \
    const char* methodName = #func;                                                                                 \
    const char* paramCode = "()Ljava/lang/String;";                                                                 \
    if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {                        \
        JNI_CHECK_EXCEPTION;                                                                 						\
        jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID , methodInfo.methodID);       \
        JNI_CHECK_EXCEPTION;                                                                                        \
        ret = JniHelper::jstring2string((jstring)objResult);                                                        \
        JNI_CHECK_EXCEPTION;                                                                 						\
        methodInfo.env->DeleteLocalRef(objResult);                                                                  \
        methodInfo.env->DeleteLocalRef(methodInfo.classID);                                                         \
    }                                                                                                               \
    else{                                                                                                           \
        JNI_CHECK_EXCEPTION;                                                                                        \
    }                                                                                                               \
    return ret;                                                                                                     \
}

//std::string func(std::string);
#define JNI_METHOD_STRING_ARG_STRING(func, arg)                                                                         \
    std::string func(std::string arg) {                                                                                 \
        std::string ret;                                                                                                \
        JniMethodInfo methodInfo;                                                                                       \
        const char* methodName = #func;                                                                                 \
        const char* paramCode = "(Ljava/lang/String;)Ljava/lang/String;";                                               \
        if (JniHelper::getStaticMethodInfo(methodInfo, JNI_CLASS_NAME, methodName, paramCode)) {                        \
            jstring jarg = methodInfo.env->NewStringUTF(arg.c_str());                                                   \
            JNI_CHECK_EXCEPTION;                                                                 						\
            jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID , methodInfo.methodID, jarg); \
            JNI_CHECK_EXCEPTION;                                                                 						\
            ret = JniHelper::jstring2string((jstring)objResult);                                                        \
            JNI_CHECK_EXCEPTION;                                                                 						\
            methodInfo.env->DeleteLocalRef(objResult);                                                                  \
            methodInfo.env->DeleteLocalRef(jarg);                                                                       \
            methodInfo.env->DeleteLocalRef(methodInfo.classID);                                                         \
        }                                                                                                               \
        else{                                                                                                           \
            JNI_CHECK_EXCEPTION;                                                                                        \
        }                                                                                                               \
        return ret;                                                                                                     \
    }

#endif
