//
//  Native_Alert.h
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#ifndef Native_Alert_h
#define Native_Alert_h

#include <string>
#include <functional>


enum AlertButtonIndex{
    
    ALERT_BUTTON_UNKNOWN_INDEX = -1,
    
    //1択のアラートビュー
    ALERT_BUTTON_1_OK_INDEX = 0,
    
    //2択のアラートビュー
    ALERT_BUTTON_2_NO_INDEX = 0,
    ALERT_BUTTON_2_YES_INDEX = 1,

    //3択のアラートビュー
    ALERT_BUTTON_3_CANCEL_INDEX = 0,
    ALERT_BUTTON_3_OPTION1_INDEX = 1,
    ALERT_BUTTON_3_OPTION2_INDEX = 2,

    //4択のアラートビュー
    ALERT_BUTTON_4_CANCEL_INDEX = 0,
    ALERT_BUTTON_4_OPTION1_INDEX = 1,
    ALERT_BUTTON_4_OPTION2_INDEX = 2,
    ALERT_BUTTON_4_OPTION3_INDEX = 3,
};


typedef std::function<void(int)> AlertCallback_t;


//アラートビュー
extern bool Native_IsAlertOn();
extern void Native_CheckAlertViewCallback();
extern void Native_AlertView(std::string title, std::string message, std::string okTitle, AlertCallback_t alertCallback = nullptr);
extern void Native_AlertView(std::string title, std::string message, std::string noTitle, std::string yesTitle, AlertCallback_t alertCallback = nullptr);
extern void Native_AlertView(std::string title, std::string message, std::string cancelTitle, std::string opt1Title, std::string opt2Title, AlertCallback_t alertCallback = nullptr);
extern void Native_AlertView(std::string title, std::string message, std::string cancelTitle, std::string opt1Title, std::string opt2Title, std::string opt3Title, AlertCallback_t alertCallback = nullptr);

//インジケータ
extern void Native_ShowIndicator(std::string message);
extern void Native_HideIndicator();

//キーボード
extern void Native_CheckKeyboardCallback();
extern void Native_ShowKeyboard(std::string message, std::string text, std::function<void(const char*)> inputCallback, std::function<void()> cancelCallback = nullptr);
extern void Native_ShowKeyboardNumPad(std::string message, std::string text, std::function<void(const char*)> inputCallback, std::function<void()> cancelCallback = nullptr);

//スリープモード
extern void Native_SleepableMode();
extern void Native_KeepScreenMode();


#endif
