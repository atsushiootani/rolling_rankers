//
//  Native_User.h
//
//
//  Created by OtaniAtsushi1 on 2016/10/17.
//
//

#ifndef Native_User_h
#define Native_User_h

extern void Native_InitUserAuth();
extern void Native_UserAuthentication();
extern bool Native_IsUserAuthenticated();
extern std::string Native_GetUserAuthId();
extern std::string Native_GetUserAuthName();
extern std::string Native_GetUserAuthEmail();
extern void Native_SetUserAuthListenerSignIn(std::function<void(std::string uid)> inCallback, std::function<void()> outCallback);


#endif /* Native_User_h */
