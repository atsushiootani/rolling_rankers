//
//  Native_Ad.h
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#ifndef Native_Ad_h
#define Native_Ad_h

#include <functional>

extern void Native_CheckAdCallback();

//初期化処理
extern void Native_AdRegister();

//バックグラウンド・フォアグラウンド処理
extern void Native_AdEnterBackground();
extern void Native_AdEnterForeground();

//全部隠す
extern void Native_AdAllHide();

//バナー
extern void Native_AdBannerShow();
extern void Native_AdBannerHide();
extern void Native_AdBannerSetEnable();
extern void Native_AdBannerSetDisable();

//アイコン
extern void Native_AdIconShowAt(int pos);
extern void Native_AdIconShow();
extern void Native_AdIconHide();
extern void Native_AdIconHideAt(int pos);

//インタースティシャル
extern void Native_AdInterstitialShow();
extern void Native_AdInterstitialAlwaysShow();

//ヘッダー位置
extern void Native_AdHeaderShow();
extern void Native_AdHeaderHide();

//ウォール
extern void Native_AdWallShow();

//動画
extern void Native_AdInsentiveShow(std::function<void(int)> insentiveCallback, std::function<void()> failureCallback, std::function<void(bool)> closedCallback);
extern bool Native_AdCanGetInsentive();
extern bool Native_AdMovieIsPlaying();

//他のアプリ
extern bool Native_CheckApplicationInstalled(std::string urlScheme); //iOSの場合urlScheme, Androidの場合packageNameになる
extern void Native_OpenInstalledApplication(std::string urlScheme);

#endif
