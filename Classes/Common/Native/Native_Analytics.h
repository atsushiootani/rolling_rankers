//
//  Native_Analytics.h
//
//
//  Created by OtaniAtsushi1 on 2015/04/19.
//
//

#ifndef Native_Analytics_h
#define Native_Analytics_h

#include <string>

extern void Native_Analytics(std::string category, std::string action = "", std::string label = "", int value = 0);
extern void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1);
extern void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1, std::string key2, std::string value2);
extern void Native_AnalyticsCustom(std::string eventName, std::string key1, std::string value1, std::string key2, std::string value2, std::string key3, std::string value3);
extern void Native_ErrorReport(std::string error, std::string subData = "");

extern void Native_AnalyticsTutorialBegin();
extern void Native_AnalyticsTutorialEnded();
extern void Native_AnalyticsAppOpen();
extern void Native_AnalyticsShare(std::string contentType, std::string itemId);
extern void Native_AnalyticsShareTwitter(std::string itemId);
extern void Native_AnalyticsShareFacebook(std::string itemId);
extern void Native_AnalyticsEcommercePurchaseBegin(std::string itemName);
extern void Native_AnalyticsEcommercePurchase(int price, std::string itemName);
extern void Native_AnalyticsEcommercePurchase(std::string itemName);
extern void Native_AnalyticsSpendVirtualCurrency(std::string itemName, std::string currencyName, int value);
extern void Native_AnalyticsBuyItem(std::string itemName);
extern void Native_AnalyticsBuyItem(std::string itemName, int price);
extern void Native_AnalyticsUnlockAchievement(std::string achievementId);
extern void Native_AnalyticsSelectContent(std::string contentName);
extern void Native_AnalyticsSelectContent(std::string contentName, std::string itemId);
extern void Native_AnalyticsSelectContent(std::string contentName, int itemId);
extern void Native_AnalyticsReview();

#endif
