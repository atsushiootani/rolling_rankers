//
//  Native_Debug.h
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#ifndef Native_OS_h
#define Native_OS_h

#include <string>


extern bool Native_IsDebug();
extern bool Native_IsIos();
extern bool Native_IsAndroid();
extern void Native_Print(std::string text);

#endif
