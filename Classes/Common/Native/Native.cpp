//
//  Native.cpp
//  
//
//  Created by OtaniAtsushi1 on 2015/11/03.
//
//

#include "Native.h"

void Native_CheckPollingAll(){
    Native_CheckSNSCallback();
    Native_CheckAlertViewCallback();
    Native_CheckKeyboardCallback();
    Native_CheckAdCallback();
    Native_CheckPurchaseCallback();
    Native_CheckDBCallback();
    Native_CheckStorageCallback();
}
