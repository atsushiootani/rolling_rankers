//
//  Native_Store.h
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#ifndef Native_Store_h
#define Native_Store_h

#include "AppInfo.h"
#include <string>

//ストアを開く
extern void Native_OpenStore();
extern void Native_OpenStore(std::string packageName);
extern bool Native_CheckStorePageExists();
extern bool Native_CheckStorePageExists(std::string packageName);

//レビュー要求
extern void Native_ReviewPromptShow(std::string title, std::string text, std::string now = "レビューする", std::string later = "あとで", std::string never = "しない");
extern bool Native_ReviewHasDone();
extern bool Native_ReviewHasRejected();
extern bool Native_ReviewHasNotYet();


//バージョンチェック


#endif
