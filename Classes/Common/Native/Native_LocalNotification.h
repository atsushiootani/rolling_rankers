//
//  Native_LocalNotification.h
//
//
//
//
//

#ifndef Native_LocalNotification_h
#define Native_LocalNotification_h

extern void Native_RegisterLocalNotification();
extern void Native_ScheduleLocalNotification(int secSinceNow, const char* text);
extern void Native_UnscheduleAllLocalNotifications();


#endif
