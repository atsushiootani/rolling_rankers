//
//  Native_SNS.h
//
//
//  Created by OtaniAtsushi1 on 2015/01/10.
//
//

#ifndef Native_SNS_h
#define Native_SNS_h

#include <string>
#include <functional>

typedef std::function<void()> ShareCallback_t;

extern void Native_FacebookPost(std::string text, std::string imageName = "", std::string url = "", ShareCallback_t successCallback = nullptr, ShareCallback_t failureCallback = nullptr, ShareCallback_t cancelCallback = nullptr, ShareCallback_t alwaysCallback = nullptr);
extern void Native_TwitterPost( std::string text, std::string imageName = "", std::string url = "", ShareCallback_t successCallback = nullptr, ShareCallback_t failureCallback = nullptr, ShareCallback_t cancelCallback = nullptr, ShareCallback_t alwaysCallback = nullptr);
extern void Native_CheckSNSCallback();

#endif
