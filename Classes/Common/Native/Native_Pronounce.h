//
//  Native_Pronounce.h
//
//
//  Created by OtaniAtsushi1 on 2016/02/08.
//
//

#ifndef Native_Pronounce_h
#define Native_Pronounce_h

#include <string>
#include <functional>

#pragma mark - pronounce

extern bool        Native_InitPronounce();
extern bool        Native_HasPronounceLoaded();
extern int         Native_GetIntPronounce(   std::string key, int defaultValue);
extern float       Native_GetFloatPronounce( std::string key, float defaultValue);
extern double      Native_GetDoublePronounce(std::string key, double defaultValue);
extern bool        Native_GetBoolPronounce(  std::string key, bool defaultValue);
extern std::string Native_GetStringPronounce(std::string key, std::string defaultValue);

extern void Native_ShowPronounceLaunchAlertView(bool force = false);
extern bool Native_IsInSubmit();


//AppController+Data.h と同じ並びであること
enum NativeStorageType{
    STORAGE_TYPE_NONE = 0, //指定なし。ディレクトリ名ふくめ完全なパスを自前で指定する場合。非推奨
    STORAGE_TYPE_PUBLIC,   //公開ディレクトリ。外部からもアクセス可能。SNSシェア用データなど
    STORAGE_TYPE_COMMON,   //共通ディレクトリ。認証済みユーザなら誰でも読み書き可能。チャットなど
    STORAGE_TYPE_SETTINGS, //設定ディレクトリ。認証済みユーザなら誰でも読み込み可能。書き込みは不可。ゲームパラメータなど
    STORAGE_TYPE_READABLE, //ユーザ個別の公開ディレクトリ。ユーザ本人のみ書き込み可能。認証済みユーザなら誰でも読み込み可能。ランキングなど
    STORAGE_TYPE_USER,     //ユーザ個別ディレクトリ。ユーザ本人のみアクセス可能。セーブデータなど
    STORAGE_TYPE_SPECIAL,  //特殊ディレクトリ。特定のユーザだけアクセスできるなど
    STORAGE_TYPE_ADMIN,    //管理者用ディレクトリ。デバッグデータなど
};

#pragma mark - database

extern void Native_CheckDBCallback();

extern void Native_InitDatabase();
extern void Native_SelectDatabaseAtPath (NativeStorageType type, std::string path,                         std::function<void(bool, std::string jsonString)> completion);
extern void Native_UpdateDatabaseAtPath (NativeStorageType type, std::string path, std::string jsonRecord, std::function<void(bool, std::string jsonString)> completion);
extern void Native_ReplaceDatabaseAtPath(NativeStorageType type, std::string path, std::string jsonRecord, std::function<void(bool, std::string jsonString)> completion);
extern void Native_DeleteDatabaseAtPath (NativeStorageType type, std::string path,                         std::function<void(bool, std::string jsonString)> completion);


#pragma mark - storage

extern void Native_CheckStorageCallback();

extern bool Native_InitStorage();
extern void Native_GetStorageDataAndSaveToFile(NativeStorageType type, std::string path, std::function<void(bool, std::string           )> completion);
extern void Native_GetStorageDataAsString     (NativeStorageType type, std::string path, std::function<void(bool, std::string           )> completion);
extern void Native_GetStorageDataAndAlloc     (NativeStorageType type, std::string path, std::function<void(bool, unsigned char*, size_t)> completion);
extern void Native_DeallocStorageData(unsigned char* data);
extern void Native_SetStorageDataWithString(NativeStorageType type, std::string path, std::string str,                     std::function<void(bool)> completion);
extern void Native_SetStorageData          (NativeStorageType type, std::string path, unsigned char* byteData, size_t len, std::function<void(bool)> completion);
extern void Native_DeleteStorageData       (NativeStorageType type, std::string path,                                      std::function<void(bool)> completion);


#endif /* Native_Pronounce_h */
