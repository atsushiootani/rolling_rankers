//
//  TextData.h
//
//
//
//
//

/**
 * 固定テキスト
 */
#ifndef TextData_h
#define TextData_h

#include "Consts.h"
#include "cocos2d.h"
#include <string>

using namespace std;



#pragma mark - 単位


#pragma mark - キャラ

extern const string getTextCharaName(int index);
extern const string getTextCharaDescription(int index);
extern const string getTextCharaHiddenName(int index);
extern const string getTextCharaHiddenDescription(int index);


#pragma mark - achievement

extern const string getTextAchievementTitle(int index);
extern const string getTextAchievementDesc(int index);


#pragma mark - SNS関連

extern const string getTextSNSTail();
extern const string getTextShare();

#endif
