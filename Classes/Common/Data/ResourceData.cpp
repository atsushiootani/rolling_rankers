//
//  ResourceData.cpp
//
//
//
//
//

#include "ResourceData.h"
#include "AppInfo.h" //for shop (depend on product_id)



#pragma mark - 汎用

string getResourceNoImageName(){
    return "transparent.png";
}

string getResourceBgName() {
    return "bg.png";
}


#pragma mark - キャラ

string getResourceCharaName(int charaType){
    return string("ball_") + to_string(charaType+1) + ".png";
}

//ステージにおける、キャラごとの中心座標(画像によってずれているので)
Vec2 getResourceCharaAnchorPoint(int charaType){
    return Vec2(0.5f, 0.3f);
}
//ステージにおける、キャラごとの表示スケール
float getResourceCharaScale(int charaType){
    return 1.6f;
}

string getResourceTransCharaName(){
    return "transparent.png";
}


#pragma mark - ステージ

string getResourceStageBgName(int index){
    return string("stage_bg_") + to_string(index + 1) + ".png";
}

string getResourceStageGroundName(int index){
    return string("stage_ground_") + to_string(index + 1) + ".png";
}


#pragma mark - ガチャ

string getResourceGachaBallName(int type){
    return string("gacha_ball_") + to_string(type + 1) + ".png";
}


#pragma mark - ショップ

string getResourceShopIconName(std::string productId){
    if      (productId == PRODUCT_ID_1) { return "shop_icon_1.png"; }
    else if (productId == PRODUCT_ID_2) { return "shop_icon_2.png"; }
    else if (productId == PRODUCT_ID_3) { return "shop_icon_3.png"; }
    else if (productId == PRODUCT_ID_4) { return "shop_icon_4.png"; }
    else { return ""; }
}

#pragma mark - 実績

string getResrouceAchievementIconName(bool unlocked, int index){
    if (unlocked) {
        return string("achievement_icon_") + to_string(index + 1) + (".png");
    }
    else{
        return "achievement_icon_hidden.png";
    }
}
