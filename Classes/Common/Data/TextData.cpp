//
//  TextData.cpp
//
//
//  Created by OtaniAtsushi1 on 2016/06/20.
//
//

#include "TextData.h"
#include "Native.h" //for pronounce
#include "GameHelper.h" //for commastring
#include "GameManager.h"
#include "GameData.h" //for AchievementData

#pragma mark - データ型

struct _textData {
    const string name;
    const string description;
    const string hiddenName;
    const string hiddenDescription;
    
    _textData(const string n, const string d, const string hn = "？？？", const string hd = "？？？")
    : name(n), description(d), hiddenName(hn), hiddenDescription(hd)
    {}
};


const char* MOKOU_TEXT[CHARA_COUNT][2] = {
    { "もこう", "お馴染みの例の顔文字", },
    { "トゲトゲもこう", "トゲが沢山生えているもこう", },
    { "ファイアーもこう", "火をまとったもこう", },
    { "サンダーもこう", "電気をまとったもこう", },
    { "アイスもこう", "氷をまとったもこう", },
    { "デブもこう", "肥満体型と化したもこう", },
    { "ほっそりもこう", "やせ細ってしまったもこう", },
    { "中国産もこう", "これじゃない感があるもこう", },
    { "韓国産もこう", "これじゃない感がとてつもないもこう", },
    { "可愛いもこう", "ほっぺを赤らめているもこう", },
    { "違和感のあるもこう", "違和感のあるもこう", },
    { "天狗もこう", "天狗になってしまったもこう", },
    { "さざめくもこう", "ただ延々とさざめくもこう", },
    { "冥界王・もこう ", "冥界をつかさどるといわれている", },
    { "ゴッド・もこう", "全てを生み出した創造神？", },
    { "ポセイドン・もこう", "大海をつかさどる、海の神", },
    { "ぺったんこもこう", "何かしらにプレスされてしまっている", },
    { "鏡餅もこう", "お正月によく見かけるもこう", },
    { "ねこみみもこう", "生き物と同化したもこう", },
    { "サイボーグもこう", "改造を施されたもこう", },
    { "ふわふわもこう", "ちょっぴり軽量化されたもこう", },
    { "レアなメタルもこう", "レアリティの高いもこう", },
    { "レインボーもこう", "虹をえがくもこう", },
    { "黒いもこう", "何故か黒いもこう", },
    { "概念と化したもこう", "自分自身を概念へと昇華させたもこう", },
    { "ぐにゃっとしたもこう", "空間のはざまに取り残されたもこう", },
    { "暗黒もこう", "闇の中で輝くダークネス・サン", },
    { "光輝もこう", "表の世界で輝くもこう", },
    { "デビルもこう", "悪魔の象徴と呼ばれたもこう", },
    { "エンジェルもこう", "天使の象徴と呼ばれたもこう", },
    { "がいこつもこう", "白骨化したもこう", },
    { "ふうせんもこう", "風船のようにぷかぷか浮くもこう", },
    { "おばけもこう", "成仏できなかったもこうの姿", },
    { "ゴーストもこう", "人間にイタズラをするのが大好き", },
    { "うさみみもこう", "うさぎの耳のようなものが生えているもこう", },
    { "チンチラもこう", "小動物のようなもこう", },
    { "もこうドラゴン", "その姿はまるで龍である", },
    { "エンシェントもこう", "最古のもこうと言われている", },
    { "イカもこう", "イカと同化したもこう", },
    { "土星もこう", "土星の形をしているもこう", },
    { "スターもこう", "星の形をしているもこう", },
    { "コケもこう", "長年放置されすっかりコケだらけになったもこう", },
    { "もこうゾンビ", "ゾンビ化したもこう", },
    { "レジェンドもこう", "かつて魔王との死闘を繰り広げたもこう", },
    { "グラサンもこう", "ある意味本当の創造神かもしれない", },
    { "ボルケーノもこう", "人々が畏怖の念を抱く火山", },
    { "フォレストもこう", "森羅万象は、このフォレストもこうに回帰する", },
    { "もこうウィンド", "ある日突然、突風が街を破壊した", },
    { "キモオタもこう", "どこかで道を踏み外したもこう", },
    { "イケメンもこう", "流行りの髪型をしたもこう", },
    { "デッドリーもこう", "常に何かを狙っている、狂気に満ちたもこう", },
    { "玄武もこう", "四神・玄武とのフュージョンに成功したもこう", },
    { "白虎もこう", "四神・白虎とのフュージョンに成功したもこう", },
    { "朱雀もこう", "四神・朱雀とのフュージョンに成功したもこう", },
    { "青龍もこう", "四神・青龍とのフュージョンに成功したもこう", },
};



#pragma mark - 単位


#pragma mark - キャラ

const _textData TEXT_RARE_CHARA_DATA[1] = {
    _textData("キャラ名1", "説明1"),
};

const string getTextCharaName(int index){
    //return string("キャラ") + to_string(index + 1);
    return MOKOU_TEXT[index][0];
}
const string getTextCharaDescription(int index){
    //return string("説明文") + to_string(index + 1);
    return MOKOU_TEXT[index][1];
}
const string getTextCharaHiddenName(int index){
    return "？？？";
}
const string getTextCharaHiddenDescription(int index){
    return "？？？";
}


#pragma mark - achievement


const char* ACHIEVEMENT_TEXT[ACHIEVEMENT_COUNT][2] = {
    { "ようこそ転がる世界へ", "1回プレイする", },
    { "とりあえず転がった", "もこうの合計転がり距離が150mに達する", },
    { "まだまだ転がりたい", "もこうの合計転がり距離が1kmに達する", },
    { "転がることには慣れてきた", "もこうの合計転がり距離が10kmに達する", },
    { "山手線一周", "もこうの合計転がり距離が34.5kmに達する", },
    { "フルマラソン", "もこうの合計転がり距離が42.195kmに達する", },
    { "転がりのエキスパート", "もこうの合計転がり距離が200kmに達する", },
    { "東京-大阪", "もこうの合計転がり距離が552.6kmに達する", },
    { "転がりは生活の一部", "もこうの合計転がり距離が1,500kmに達する", },
    { "ツール・ド・フランス", "もこうの合計転がり距離が3,300kmに達する", },
    { "転がることこそが人生", "もこうの合計転がり距離が7,000kmに達する", },
    { "日本一周", "もこうの合計転がり距離が12,480kmに達する", },
    { "もこうコレクション始めました", "初めてガチャでもこうを手に入れた", },
    { "もこうが好き", "もこうのコンプ率が%s％に達する", },
    { "もこうが増えてきた", "もこうのコンプ率が%s％に達する", },
    { "もこうコレクター", "もこうのコンプ率が%s％に達する", },
    { "もこうがもこもこしてきた", "もこうのコンプ率が%s％に達する", },
    { "もこうは私のもの", "もこうのコンプ率が%s％に達する", },
    { "コイン大好き", "コインの合計取得枚数が%s枚に達する", },
    { "コイン中毒", "コインの合計取得枚数が%s枚に達する", },
    { "富豪", "コインの合計取得枚数が%s枚に達する", },
    { "大富豪", "コインの合計取得枚数が%s枚に達する", },
    { "金の亡者", "コインの合計取得枚数が%s枚に達する", },
    { "人気ゲーム実況者が転落", "グラサンもこうをゲットする", },
    { "再生数%s", "動画広告を%s回視聴する", },
    { "再生数%s", "動画広告を%s回視聴する", },
    { "再生数%s", "動画広告を%s回視聴する", },
    { "再生数%s", "動画広告を%s回視聴する", },
    { "再生数%s", "動画広告を%s回視聴する", },
    { "再生数%s", "動画広告を%s回視聴する", },
    { "1週間ありがとう！", "累計7日ログインする", },
    { "1ヶ月ありがとう！", "累計30日ログインする", },
    { "3ヶ月ありがとう！", "累計90日ログインする", },
};

const string getTextAchievementTitle(int index){
    string baseText = ACHIEVEMENT_TEXT[index][0];
    
    if (baseText.find("%s") < baseText.length()) {
        char text[200] = {};
        auto data = s_achievementData[index];
        string numberStr = getStringFromNumber(data.num, true, 0);
        sprintf(text, baseText.c_str(), numberStr.c_str());
        return string(text);
    }
    else{
        return baseText;
    }
}

const string getTextAchievementDesc(int index){
    string baseText = ACHIEVEMENT_TEXT[index][1];
    
    if (baseText.find("%s") < baseText.length()) {
        char text[200] = {};
        auto data = s_achievementData[index];
        string numberStr = getStringFromNumber(data.num, true, 0);
        sprintf(text, baseText.c_str(), numberStr.c_str());
        return string(text);
    }
    else{
        return baseText;
    }
}



#pragma mark - SNS関連

const string getTextSNSHashTag(){
    return string("#") + APP_NAME;
}

const string getTextSNSLink(){
    return string("goo.gl/xxx");
}

const string getTextSNSHead(){
    return Native_GetStringPronounce(PRONOUNCE_SNS_TEXT_HEAD, "人気ゲーム実況者「もこう」がまさかの転落！？");
}

const string getTextSNSTail(){
    return Native_GetStringPronounce(PRONOUNCE_SNS_TEXT_TAIL, getTextSNSLink() + "\n" + getTextSNSHashTag());
}

const string getTextShare() {
    return getTextSNSHead() + "\n" + getTextSNSTail();
}

