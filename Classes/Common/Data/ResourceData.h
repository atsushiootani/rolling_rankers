//
//  ResourceData.h
//
//
//
//
//

#ifndef ResourceData_h
#define ResourceData_h

#include <string>
#include "Consts.h"
#include "AndroidHelper.h"
#include "cocos2d.h"

using namespace std;
USING_NS_CC;


//汎用
extern string getResourceNoImageName();
extern string getResourceBgName();


//キャラ
extern string getResourceCharaName(int charaType);
extern Vec2 getResourceCharaAnchorPoint(int charaType);
extern float getResourceCharaScale(int charaType);

//ステージ
extern string getResourceStageBgName(int index);
extern string getResourceStageGroundName(int index);

//ガチャ
extern string getResourceGachaBallName(int type);

//ショップ
extern string getResourceShopIconName(std::string productId);

//実績
extern string getResrouceAchievementIconName(bool unlocked, int index);


#endif
