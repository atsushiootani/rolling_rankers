//
//  SoundData.h
//
//
//
//
//

#ifndef SoundData_h
#define SoundData_h

#include <string>
#include "Consts.h"

//bgm
#define BGM_MENU         "bgm_menu.mp3"
#define BGM_STAGE_1      "bgm_stage_1.mp3"
#define BGM_STAGE_2      "bgm_stage_2.mp3"
#define BGM_BOOST        "bgm_boost.mp3"

//se
#define SE_OK            "se_ok.mp3"
#define SE_CANCEL        "se_jump.mp3"
#define SE_LOCKED        "se_jump.mp3"
#define SE_UNLOCK        "se_gacha_get.mp3"
#define SE_TAP           "se_ok.mp3"

#define SE_JUMP           "se_jump.mp3"
#define SE_COIN           "se_coin.mp3"
#define SE_MISS           "se_miss.mp3"
#define SE_CLASH          "se_clash.mp3"
#define SE_HIGH_SCORE     "se_highscore.mp3"
#define SE_GACHA_ROTATE   "se_gacha_rotate.mp3"
#define SE_GACHA_ZOOM_IN  "se_gacha_capsule.mp3"
#define SE_GACHA_GET      SE_UNLOCK

#endif
