//
//  Screen.h
//  
//
//
//

//
//  画面関係の定数などを定義
//

#ifndef Screen_h
#define Screen_h

#include "AppDelegate.h"


//cocosに設定する仮想画面サイズ
#define DESIGN_RESOLUTION_WIDTH  (1136)
#define DESIGN_RESOLUTION_HEIGHT  (640)

//デバイスの画面サイズを考慮に含めた、実際にレイアウトするときの仮想画面サイズ
//#define SCREEN_WIDTH  (DESIGN_RESOLUTION_WIDTH)
//#define SCREEN_HEIGHT (DESIGN_RESOLUTION_WIDTH / AppDelegate::getDeviceScreenSize().width * AppDelegate::getDeviceScreenSize().height) //FIXED_WIDTHなので、デバイスのアスペクト比によって高さが変わる

//画面サイズ
#define VIRTUAL_SCREEN_WIDTH  (1136)
#define VIRTUAL_SCREEN_HEIGHT  (640)

#define SCREEN_WIDTH  VIRTUAL_SCREEN_WIDTH
#define SCREEN_HEIGHT VIRTUAL_SCREEN_HEIGHT


//広告サイズ
#define AD_BANNER_HEIGHT  (0) //TODO:

//ヘッダーサイズ
#define HEADER_HEIGHT (0) //TODO:

//フッターサイズ
#define FOOTER_HEIGHT (0) //TODO:

//FPS
#define FPS            (60)
#define FRAME_INTERVAL (1.0 / FPS)


#define FONT_TEXT_NAME "fonts/rounded-l-mplus-1c-medium.ttf"
#define FONT_BOLD_TEXT_NAME "fonts/rounded-l-mplus-1c-black.ttf"


#pragma mark - global z

//グローバルZ値(プラス値ほど手前に描画)
enum{
    Z_GLOBAL_SCENEGRAPH = 0,

    Z_GLOBAL_FADE,
    Z_GLOBAL_POPUP,
};


#pragma mark - priority

//イベントリスナーの固定値(マイナスほど優先)
enum {
    //シーンマネージャのチェック。すべてに優先
    kEventListenerPriority_SceneManager = -10000,
    kEventListenerPriority_Popup        = -10001,
    kEventListenerPriority_NoticeWindow = -10002,
};


#pragma mark - layer

//レイヤーの種類
enum LayerType {
    LAYER_UNKNOWN,
    LAYER_GAME,
    LAYER_RESULT,
    LAYER_GACHA,
    LAYER_SHOP,
    
    LAYER_COUNT,
};



#endif
