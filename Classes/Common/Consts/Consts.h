//
//  Consts.h
//
//
//
//
//

#ifndef Consts_h
#define Consts_h


#pragma mark - 型



#pragma mark - 定数

//デフォルトのプレイ速度
#define DEFAULT_GAME_SPEED (3.f)


//キャラ数
#define CHARA_COUNT (55)


#pragma mark - コイン

//ステージのコイン獲得によるコイン増加数
#define COIN_FOR_ONE_GET       (10)

//ブーストに必要なコイン数
#define NEED_COIN_FOR_BOOST    (20)

//ブーストでの想定獲得コイン数
#define ASSUMED_GET_COIN_FOR_ONE_BOOST (100 * COIN_FOR_ONE_GET)

//1プレイの想定獲得コイン数
#define ASSUMED_GET_COIN_FOR_ONE_PLAY  (ASSUMED_GET_COIN_FOR_ONE_BOOST * 2.5)

//動画報酬コイン
#define DEFAULT_COIN_REWARD            (3000)//(ASSUMED_GET_COIN_FOR_ONE_PLAY * 4)

//ガチャの必要コイン
#define DEFAULT_NEED_COIN_TO_GACHA     (3000)//(ASSUMED_GET_COIN_FOR_ONE_PLAY * 4 + DEFAULT_COIN_REWARD)

//コンティニュー必要コイン
#define DEFAULT_COIN_TO_CONTINUE (1500)



//表示用の距離
#define RATE_OF_DISTANCE_TO_SHOW (0.1)



#pragma mark - 実績

#define ACHIEVEMENT_COUNT (33)


#pragma mark - ログインボーナス

#define LOGIN_BONUS_COUNT (30)



#pragma mark - 課金

#define ADD_COIN_OF_PURCHASE_1  ( 6000) //120. DEFAULT_NEED_COIN_TO_GACHA *  3
#define ADD_COIN_OF_PURCHASE_2  (15000) //240. DEFAULT_NEED_COIN_TO_GACHA *  7
#define ADD_COIN_OF_PURCHASE_3  (40000) //480. DEFAULT_NEED_COIN_TO_GACHA * 15
#define ADD_COIN_OF_PURCHASE_4  (60000) //600. DEFAULT_NEED_COIN_TO_GACHA * 30



#endif
