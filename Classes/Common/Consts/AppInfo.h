//
//  AppInfo.h
//
//
//
//
//

#ifndef AppInfo_h
#define AppInfo_h


/**
 * セーブデータバージョン
 */
#define SAVE_DATA_VERSION (1)



/**
 * アプリ名
 */

//ゲーム内で表示する用
#define APP_NAME   "ころころもこう"



/**
 * app store
 */

//appID
#define APP_ID "1163090716"

//app store のURL
#define APP_STORE_URL   "https://itunes.apple.com/jp/app/id" APP_ID




/**
 * google play
 */

//id
#define GOOGLE_PLAY_ID  "kazuma.saitou.rolling"

//google play のurl
#define GOOGLE_PLAY_URL "https://play.google.com/store/apps/details?id=" GOOGLE_PLAY_ID




/**
 * 課金アイテム
 */

//app store用
#define PRODUCT_ID_1             "product_id_rolling_coin1"
#define PRODUCT_ID_2             "product_id_rolling_coin2"
#define PRODUCT_ID_3             "product_id_rolling_coin3"
#define PRODUCT_ID_4             "product_id_rolling_coin4"

#if __OBJC__
#define ALL_PRODUCT_IDS @[@(PRODUCT_ID_1), @(PRODUCT_ID_2), @(PRODUCT_ID_3), @(PRODUCT_ID_4)]
#endif

//デフォルトの商品名（localizedTitleが取得できなかった場合に使用する）
#define PRODUCT_NAME_1  "6,000コイン"
#define PRODUCT_NAME_2  "15,000コイン"
#define PRODUCT_NAME_3  "40,000コイン"
#define PRODUCT_NAME_4  "60,000コイン"
#define PRODUCT_DESCRIPTION_1  "6,000コインが追加されます。"
#define PRODUCT_DESCRIPTION_2  "15,000コインが追加されます。"
#define PRODUCT_DESCRIPTION_3  "40,000コインが追加されます。"
#define PRODUCT_DESCRIPTION_4  "60,000コインが追加されます。"


#endif
