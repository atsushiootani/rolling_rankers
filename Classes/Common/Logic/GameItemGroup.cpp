//
//  GameItemGroup.cpp
//  balls
//
//  Created by OtaniAtsushi1 on 2016/09/05.
//
//

#include "GameItemGroup.h"
#include "Debug.h"

GameItemGroup::~GameItemGroup(){
    if (saveData) {
        delete saveData;
        saveData = nullptr;
    }
}

GameItemGroup::GameItemGroup()
: delegate(nullptr)
, data(nullptr)
, dataCount(0)
, saveDataPrefix()
, unlockFunc(nullptr)
, saveData(new std::vector<int>())
, multipleBuyCount(1)
, needSequentially(false)
{
    
}


bool GameItemGroup::setupWithData(GameItemGroupDelegate* delegate, GameItemData* data, int dataCount, std::string saveDataPrefix, std::function<bool(int)> unlockFunc/* = nullptr*/){
    this->delegate = delegate;
    this->data = data;
    this->dataCount = dataCount;
    this->saveDataPrefix = saveDataPrefix;
    this->unlockFunc = unlockFunc;
    
    this->loadAll();
    
    return true;
}

GameItemGroup* GameItemGroup::setMultipleBuyCount(int times){
    multipleBuyCount = times;
    return this;
}

GameItemGroup* GameItemGroup::setNeedSequentialBuy(bool f){
    needSequentially = f;
    return this;
}

//購入数を強制セット
void GameItemGroup::forceSet(int index, int buyNum, bool needSave){
    saveData->at(index) = buyNum;
    if (needSave) {
        saveAll();
    }
}


#pragma mark - setter, getter

//アイテム総数
int GameItemGroup::getTypeCount(){
    return dataCount;
}

//アイテム価格を取得
int GameItemGroup::getPrice(int index){
    return data[index].price;
}

//アイテムを購入できる額を持っているかどうか
bool GameItemGroup::hasEnoughMoney(int index){
    if (delegate) {
        return delegate->getItemMoney(this) >= getPrice(index);
    }
    else{
        DERROR("GameItemGroup: no delegate set. \n");
        return false;
    }
}


//アイテムが購入可能になる条件
bool GameItemGroup::isUnlocked(int index){
    /*if (!unlockFunc) {
        DERROR("no unlockFunc set.\n");
        return false;
    }*/
    
    //購入数が上限到達していたら購入不可
    if (getBoughtNum(index) >= multipleBuyCount) {
        return false;
    }
    
    //順序通りの購入が必要な場合は購入不可
    if (needSequentially &&
        (index > 0 && !hasBought(index - 1))) {
        return false;
    }

    //unlockFuncの判定
    if (unlockFunc) {
        return unlockFunc(index);
    }

    return true;
}


//アイテムが購入済みか
bool GameItemGroup::hasBought(int index){
    return getBoughtNum(index) > 0;
}

//アイテムが今購入可能か
bool GameItemGroup::canBuyNow(int index){
    return isUnlocked(index) && hasEnoughMoney(index);
}

//アイテムを購入する
bool GameItemGroup::buyNow(int index){
    if (!canBuyNow(index) ||
        !delegate) {
        return false;
    }
    
    delegate->subItemMoney(this, index, getPrice(index));
    saveData->at(index)++;
    
    if (delegate->isSaveItemOk(this)) {
        saveAll();
        delegate->saveItemMoney(this);
    }
    
    return true;
}

//アイテムを購入した数を返す
int GameItemGroup::getBoughtNum(int index){
    return saveData->at(index);
}

//現在購入可能な最大のindexを返す
int GameItemGroup::getMaxUnlockedIndex(){
    for (int i = getTypeCount() - 1; i >= 0; --i) {
        if (isUnlocked(i)) {
            return true;
        }
    }
    return false;
}

//今までに購入した最大のindexを返す
int GameItemGroup::getMaxBoughtIndex(){
    for (int i = getTypeCount() - 1; i >= 0; --i) {
        if (hasBought(i)) {
            return true;
        }
    }
    return false;
}

//今までに購入した種類数
int GameItemGroup::getBoughtTypeCount(){
    int count = 0;
    for (int i = 0; i < getTypeCount(); ++i) {
        if (hasBought(i)) {
            ++count;
        }
    }
    return count;
}


#pragma mark - SaveDataProtocol

const std::string GameItemGroup::getSaveDataPrefix() {
    return std::string("GameItemGroup") + saveDataPrefix;
}

bool GameItemGroup::saveImpl(SaveManager*) {
    if (!saveData) {
        return false;
    }
    
    SAVE_BEGIN;
    
    SAVE_INT_VECTOR(saveData);
    
    SAVE_END;
    
    return true;
}

bool GameItemGroup::loadImpl(SaveManager*) {
    if (!saveData) {
        return false;
    }
    
    saveData->clear();
    
    LOAD_BEGIN;
    
    LOAD_INT_VECTOR(saveData);
    
    LOAD_END;
    
    return true;
}

bool GameItemGroup::loadAll() {
    getSaveManager()->load(this);
    
    //データが足りない分は追加
    size_t len = saveData->size();
    for (; len < dataCount; ++len){
        saveData->push_back(0);
    }
    
    return true;
}
