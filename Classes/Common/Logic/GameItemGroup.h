//
//  GameItemGroup.h
//  balls
//
//  Created by OtaniAtsushi1 on 2016/09/05.
//
//

#ifndef GameItemGroup_h
#define GameItemGroup_h


#include "SaveManager.h"



//ゲームアイテムデータ
//  必要に応じて、継承してメンバ変数を増やして良い(アイテム効果の変数など)
struct GameItemData{
public:
    int index;
    int price;
public:
    GameItemData()            : index(-1), price(0) {}
    GameItemData(int i, int p): index(i), price(p) {}
};


//前方宣言
class GameItemGroupDelegate;


//ゲーム内のアイテム群
//  購入条件(順不同か、複数回可能かなど)は、関数を引数で渡すことで設定する
//  ゲーム内通貨のみ想定
class GameItemGroup : public SaveDataProtocol{
public:
    virtual ~GameItemGroup();
    GameItemGroup();
    
    //データセット
    bool setupWithData(GameItemGroupDelegate* delegate, GameItemData* data, int dataCount, std::string saveDataPrefix, std::function<bool(int)> unlockFunc = nullptr);

    //unlockFunc のヘルパー
    //1種のアイテムが何個まで購入可能か(デフォルトは1) //種類によって異なるなど、複雑な場合はunlockFunc内で定義してね
    GameItemGroup* setMultipleBuyCount(int times);
    
    //itemIndex順序通りの購入が必要か(デフォルトはfalse)
    GameItemGroup* setNeedSequentialBuy(bool f);
    
    //購入数を強制セット
    void forceSet(int index, int buyNum, bool needSave);
    
    
    //アイテム総数
    int getTypeCount();
    
    //アイテム価格を取得
    int getPrice(int index);
    
    //アイテムを購入できる額を持っているかどうか
    bool hasEnoughMoney(int index);
    

    //アイテムが購入可能になる条件
    bool isUnlocked(int index);
    
    //アイテムが購入済みか
    bool hasBought(int index);

    //アイテムが今購入可能か
    bool canBuyNow(int index);
    
    //アイテムを購入する
    bool buyNow(int index);
    
    //アイテムを購入した数を返す
    int getBoughtNum(int index);
    
    
    //現在購入可能な最大のindexを返す
    int getMaxUnlockedIndex();
    
    //今までに購入した最大のindexを返す
    int getMaxBoughtIndex();

    //今までに購入した種類数
    int getBoughtTypeCount();
    
    
    //SaveDataProtocol
    virtual const std::string getSaveDataPrefix() override;
    virtual bool saveImpl(SaveManager*) override;
    virtual bool loadImpl(SaveManager*) override;
    virtual bool loadAll() override;
    
private:
    GameItemGroupDelegate* delegate;
    GameItemData* data;
    int dataCount;
    std::string saveDataPrefix;
    std::function<bool(int)> unlockFunc;
    int multipleBuyCount;
    bool needSequentially;
    
    std::vector<int>* saveData;
};


//GameItemGroupから、所持金操作を行う処理
class GameItemGroupDelegate{
public:
    //所持金を返す
    virtual int  getItemMoney(GameItemGroup*) = 0;
    
    //所持金を減らす
    virtual int  subItemMoney(GameItemGroup*, int itemIndex, int sub) = 0;
    
    //所持金を増やす
    virtual int  addItemMoney(GameItemGroup*, int itemIndex, int add) = 0;

    //セーブ可能か
    virtual bool isSaveItemOk(GameItemGroup*) = 0;
    
    //セーブさせる
    virtual bool saveItemMoney(GameItemGroup*) = 0;
};

#endif /* GameItemGroup_h */
