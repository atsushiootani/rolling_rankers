//
//  CocosHelper.h
//
//
//  Created by OtaniAtsushi1 on 2014/12/07.
//
//

#ifndef CocosHelper_h
#define CocosHelper_h

#include "cocos2d.h"
#include <extensions/cocos-ext.h>

USING_NS_CC;
USING_NS_CC_EXT;


//各種ノード作成ヘルパー
#pragma mark - node

extern Node* createNode(Node* parent, int zOrder, Vec2 pos = Vec2::ZERO, Size size = Size::ZERO);
extern Node* createNode(Node* parent, int zOrder, std::string name, Vec2 pos = Vec2::ZERO, Size size = Size::ZERO);

extern Sprite* createSprite(Node* parent, const std::string& filename, int zOrder, Vec2 anchor, Vec2 pos);

extern MenuItemImage* createMenuItemImage(const std::string& normalImage,                                                                     const ccMenuCallback& callback, Vec2 anchor, Vec2 pos);
extern MenuItemImage* createMenuItemImage(const std::string& normalImage, const std::string& selectedImage,                                   const ccMenuCallback& callback, Vec2 anchor, Vec2 pos);
extern MenuItemImage* createMenuItemImage(const std::string& normalImage, const std::string& selectedImage, const std::string& disabledImage, const ccMenuCallback& callback, Vec2 anchor, Vec2 pos);
extern Menu* createMenu(Node* parent, MenuItem* item1, MenuItem* item2, MenuItem* item3, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
extern Menu* createMenu(Node* parent, const Vector<MenuItem*>& items, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
extern Menu* createOneMenu(Node* parent, const std::string& normalImage,                                                                     const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
extern Menu* createOneMenu(Node* parent, const std::string& normalImage, const std::string& selectedImage,                                   const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
extern Menu* createMenuWithArrayZ(Node* parent, const Vector<MenuItem*>& items, int xCount, float width,  int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
extern Menu* createMenuWithArrayN(Node* parent, const Vector<MenuItem*>& items, int yCount, float height, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
extern Menu* createTextButton(Node* parent, const std::string& normalImage, const std::string& text, const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority = 0);
 
extern Label* createSystemLabel(Node* parent,                             const std::string& text, float fontSize, TextHAlignment hAlignment, int zOrder, Vec2 anchor, Vec2 pos);
extern Label* createSystemLabel(Node* parent, const std::string fontName, const std::string& text, float fontSize, TextHAlignment hAlignment, int zOrder, Vec2 anchor, Vec2 pos);extern Label* createTTFLabel(   Node* parent, const std::string fontName, const std::string& text, float fontSize, TextHAlignment hAlignment, int zOrder, Vec2 anchor, Vec2 pos);

extern ScrollView* createScrollView(Node* parent, Node* contentSizedNode, ScrollView::Direction dir, bool isBounceable, Size viewSize, int zOrder, Vec2 anchor, Vec2 pos);

extern LayerColor* createLayerColor(Node* parent, Color4B color, int zOrder, Size size, Vec2 anchor, Vec2 pos = Vec2::ZERO);

extern EventListenerTouchOneByOne* createSingleTouchListener(Node* node, std::function<bool(Touch*, Event*)> onTouchBegan, std::function<void(Touch*, Event*)> onTouchMoved, std::function<void(Touch*, Event*)> onTouchEnded, std::function<void(Touch*, Event*)> onTouchCancelled, bool isSwallow, int fixedPriority = 0);
extern void destroySingleTouchListener(EventListenerTouchOneByOne* listener, Node* node);



#pragma mark - Node

//サイズを指定し、ノードのscaleを自動設定する
extern Node* setNodeViewWidth( Node* node, float width);
extern Node* setNodeViewHeight(Node* node, float height);
extern Node* setNodeViewSize(  Node* node, Size size);
extern Node* setNodeViewWidthFixedAspect( Node* node, float width);
extern Node* setNodeViewHeightFixedAspect(Node* node, float height);
extern Node* setNodeViewSizeFixedAspect(  Node* node, Size size);

//ラベルを範囲内に収める
extern Node* setLabelWidth(Label* label, float width, float maxSize = 0.f);
extern Node* setLabelWidth(Label* label, std::string text, float width, float maxSize = 0.f);
extern Node* setTTFLabelWidth(Label* label, float width, float maxSize = 0.f);
extern Node* setTTFLabelWidth(Label* label, std::string text, float width, float maxSize = 0.f);

extern void changeParent(Node* child, Node* newParent, bool copyPosition = false);


//Vec2,Rect,Sizeヘルパー
#pragma mark - Vec, Rect, Size

extern Vec2 getCenterOfRect(Rect rect);

//指定ノードのローカル座標系での中心座標を返す
extern Vec2 getLocalCenterPos(Node* node);

//座標を、指定の矩形からはみ出さない座標にして返す
extern Vec2 getPosOfInnerRect(Vec2 pos, Rect rect);

//2つの矩形の重なり部分を返す(そもそも重なっていない場合の戻り値は不定)
extern Rect intersectWithRect(Rect rect1, Rect rect2);

extern Vec2 getRectMidPos(Rect rect);
extern Vec2 getNodePositionOf(Node* node, Vec2 anchor);
extern Vec2 convertNodeCenterPosToAnotherNodeSpace(Node* fromNode, Node* toNode);
extern Vec2 convertNodeAnchorPosToAnotherNodeSpace(Node* fromNode, Node* toNode);
extern Vec2 convertNodeAnchorPosToAnotherNodeSpace(Node* fromNode, Vec2 anchor, Node* toNode);


#pragma mark - touch

//タッチが指定のノード内だったかどうか
extern bool isTouchInside(Node* target, Touch* touch);

//指定のノード以下(or FixedPriority以下)にタッチイベントを伝播させない
// ただし、FixedPriorityが指定されているタッチリスナーは伝播してしまう
extern EventListenerTouchOneByOne* stopTouchEventListenerUnder(Node* node, int fixedPriority = 0);

//ふたたび伝播を許可する.済んだらゼッタイ呼ぶこと！
extern void restartTouchEventListenerUnder(Node* node, EventListenerTouchOneByOne* listener);


#pragma mark - 親子関係

//指定のノードから親を辿り、第2指定のノードの直の子に打ち当たったら、そのノードを返す
extern Node* getChildOfTargetNodeUpstreamFrom(Node* from, Node* target);

//指定のノード(スクロールビューの子孫)が、見えているかどうか(一部でも見えていればtrue)
extern bool isNodeVisibleInScrollView(ScrollView* scrollView, Node* node);

//子孫すべてに指定の処理を実行
extern void recursiveCallFunc(Node* node, const std::function<void(Node*)> &func);

//グローバルZを指定(グローバルZは子孫ノードに伝播しないので、明示的に子孫含め全ノードにグローバルZ指定をすることが必要。そのヘルパー関数)
extern void setGlobalZRecursive(Node* node, float globalZ);


#pragma mark - screen

extern RenderTexture* renderNode(Node* targetNode);
extern void renderNodeAndSaveToFileAsPng(Node* targetNode, std::string fname, std::function<void(RenderTexture* texture, const std::string& fullpath)> callback = nullptr);
extern void captureScreenWithRect(Node* targetNode, Rect rect, std::string fname, std::function<void(const std::string)> successCallback, std::function<void()> failureCallback);
extern void captureFullScreen(std::string fname, std::function<void(const std::string)> successCallback, std::function<void()> failureCallback);


#pragma mark - animation

//アニメーションを作成する
Animation* createAnimationWithFormat(float delayPerUnit, int loops, bool restoreOriginalFrame, const char* animNameFormat, int startIndex = 1, int endIndex = 999);
Animation* createAnimation(float delayPerUnit, int loops, bool restoreOriginalFrame, const char* anim1, ...) CC_REQUIRES_NULL_TERMINATION;
Animation* createAnimation(float delayPerUnit, int loops, bool restoreOriginalFrame, std::vector<std::string>& anims);


#pragma mark - delayed

//遅延実行
extern Action* delayedExecution(Node* node, float duration, const std::function<void()>& func);


#pragma mark - label

extern void labelShowSequentially2(Label* label, std::string text, float duration, std::function<void()> callback = nullptr);
extern void labelShowInstantly2(Label* label, std::string text);

extern void labelSequentialShow(Label *label, float duration, std::function<void()> callback = nullptr);
extern void labelShowInstantly(Label *label);


#pragma mark - ActionInterval

class BgmFade : public ActionInterval
{
public:
    BgmFade();
    
    static BgmFade* create(float d, float volume, bool pauseOnComplete = true );
    bool initWithDuration(float d, float volume, bool pauseOnComplete );
    
    virtual BgmFade* clone() const override; // pure virtual
    virtual BgmFade* reverse() const override; // pure virtual
    virtual void startWithTarget(cocos2d::Node *pTarget);
    virtual void update(float time);
    virtual void stop(void);
    
protected:
    float m_targetVal;
    float m_initialVal;
    bool m_bPauseOnComplete;
};


#pragma mark - macros

#define SAFE_REMOVE_FROM_PARENT(node) do { \
    if (node != nullptr) {                 \
        node->removeFromParent();          \
        node = nullptr;                    \
    }                                      \
} while(0)


#endif
