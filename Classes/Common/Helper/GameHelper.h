//
//  GameHelper.h
//
//
//  Created by OtaniAtsushi1 on 2014/12/23.
//
//

#ifndef GameHelper_h
#define GameHelper_h

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "AndroidHelper.h"


#pragma mark - 時刻

extern time_t getNowDate();
extern double getSystemTime();


#pragma mark - 乱数

//乱数取得[0.0, 1.0)の範囲
extern double getRand();

//double乱数取得[min, max)の範囲
extern double getFRand(double min, double max);

//int乱数取得[min, max]の範囲
extern int getIntRand(int min, int max);


#pragma mark - 文字列

//末尾の改行を削除
extern std::string chompString(std::string str);

//time_tを文字列にして返す。YYYY/MM/DD HH:mm:ss
extern std::string getFullDateString(time_t date);

//time_tを文字列にして返す。HH:mm:ss
extern std::string getLocalTimeString(time_t date);

//time_tを文字列にして返す。mm:ss
extern std::string getRestTimeMSString(int sec);


#pragma mark - 数値から文字列への変換

//doubleから文字列を作成。小数点以下の桁数を指定する
extern std::string getStringFromDouble(double value, int digitsUnderPeriod);


//数値を文字列にして返す
//  カンマ区切り、小数点以下の桁数指定が可能
template <class T>std::string getStringFromNumber(T num, bool camma, int digitsUnderPeriod = -1){
    
    std::string strNum = "";
    
    //小数点以下の桁数指定がある場合、指定通りに文字列へ変換
    if (digitsUnderPeriod >= 0) {
        std::ostringstream oss;
        oss << std::setprecision(digitsUnderPeriod) << std::setiosflags(std::ios::fixed) << num;
        strNum = oss.str();
    }
    else{
        //単純にto_string
        strNum = std::to_string(num);
    }
    
    //カンマ区切りする場合
    if (camma) {
        //小数点の位置を探す
        size_t posDot = strNum.find(".");
        /*if (posDot != std::string::npos) {
         if (strNum.length() > posDot + 1) {
         strNum.erase(posDot + 2);
         }
         }*/
        
        //桁数を取得
        size_t keta;
        if (posDot == std::string::npos) {
            keta = strNum.length();
        }
        else{
            keta = posDot;
        }
        
        //3桁ごとにカンマを追加
        for (int cammaPos = 3; cammaPos < keta; cammaPos += 3) {
            strNum.insert(keta - cammaPos, ",");
        }
    }
    
    return strNum;
}

//巨大な数値を「万、億、兆、京」の単位付きの文字列にして返す(最上位の単位のみ)
//  小数点以下は桁数指定可能
//  template型は無指定だと、第1引数の型と見做す。この関数はtemplate型を明示する必要は無い
template <class T>std::string getUnitedStringFromNumber(T num, bool camma, int digitsUnderPeriod = 0) {
    
    //万、億、兆、京の単位で表示
    static const long double MAN = 10000;
    static const long double OKU = MAN * MAN;
    static const long double CHO = OKU * MAN;
    static const long double KEI = CHO * MAN;
    
    std::string unit = std::string("");
    std::string nForUnit = std::string("");
    if (num >= KEI) {
        nForUnit = getStringFromNumber(num / KEI, camma, digitsUnderPeriod);
        unit = "京";
    }
    else if (num >= CHO) {
        nForUnit = getStringFromNumber(num / CHO, camma, digitsUnderPeriod);
        unit = "兆";
    }
    else if (num >= OKU) {
        nForUnit = getStringFromNumber(num / OKU, camma, digitsUnderPeriod);
        unit = "億";
    }
    else if (num >= MAN) {
        nForUnit = getStringFromNumber(num / MAN, camma, digitsUnderPeriod);
        unit = "万";
    }
    else{
        nForUnit = getStringFromNumber(num      , camma, 0); //TODO: 注意。Tが小数点型の場合はここもdigitsUnderPeriodが相応しい
    }
    
    return nForUnit + unit;
}




#pragma mark - 時刻

extern time_t getNow();


#endif
