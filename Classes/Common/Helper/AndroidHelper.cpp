//
//  AndroidHelper.cpp
//
//
//  Created by OtaniAtsushi1 on 2016/02/22.
//
//

#include <stdlib.h>
#include <string>

#if defined CC_TARGET_OS_ANDROID

namespace std{
    
int                stoi  (const std::string& __str, size_t* __idx, int __base)
{
    const char* p = __str.c_str();
    char* end;
    int x = (int)strtol(p, &end, __base); //strtoiというのはない
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

long               stol  (const std::string& __str, size_t* __idx, int __base)
{
    // http://cpprefjp.github.io/reference/string/stol.html
    
    const char* p = __str.c_str();
    char* end;
    long x = strtol(p, &end, __base);
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

unsigned long      stoul (const std::string& __str, size_t* __idx, int __base)
{
    const char* p = __str.c_str();
    char* end;
    unsigned long x = strtoul(p, &end, __base);
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

long long          stoll (const std::string& __str, size_t* __idx, int __base)
{
    const char* p = __str.c_str();
    char* end;
    long long x = strtoll(p, &end, __base);
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

unsigned long long stoull(const std::string& __str, size_t* __idx, int __base)
{
    const char* p = __str.c_str();
    char* end;
    unsigned long long x = strtoull(p, &end, __base);
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

float       stof (const std::string& __str, size_t* __idx)
{
    const char* p = __str.c_str();
    char* end;
    float x = strtof(p, &end);
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

double      stod (const std::string& __str, size_t* __idx)
{
    const char* p = __str.c_str();
    char* end;
    double x = strtod(p, &end);
    if (__idx != nullptr) {
        *__idx = static_cast<std::size_t>(end - p);
    }
    return x;
}

}

#endif
