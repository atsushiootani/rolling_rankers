//
//  CocosHelper.cpp
//
//
//  Created by OtaniAtsushi1 on 2014/12/07.
//
//

#include "CocosHelper.h"
#include "SimpleAudioEngine.h"
#include "Screen.h"

using namespace CocosDenshion;


/**
 * ノード作成ヘルパー
 */
Node* createNode(Node* parent, int zOrder, Vec2 pos/*= Vec2::ZERO*/, Size size/*= Size::ZERO*/){
    return createNode(parent, zOrder, "Node", pos, size);
}
Node* createNode(Node* parent, int zOrder, std::string name, Vec2 pos/* = Vec2::ZERO*/, Size size/* = Size::ZERO*/){
    Node* node = Node::create();
    node->setPosition(pos);
    node->setContentSize(size);
    node->setName(name);
    //アンカーポイントは左下固定
    
    if (parent) {
        parent->addChild(node, zOrder);
    }
    
    return node;
}

/**
 * スプライト作成ヘルパー
 */
Sprite* createSprite(Node* parent, const std::string& filename, int zOrder, Vec2 anchor, Vec2 pos){
    FileUtils::getInstance()->setPopupNotify(false); //ファイルが存在しなくても、デバッグ出力しないようにする

    Sprite* sprite = FileUtils::getInstance()->isFileExist(filename) ?
    Sprite::create(filename) :
    Sprite::createWithSpriteFrameName(filename);
    
    sprite->setAnchorPoint(anchor);
    sprite->setPosition(pos);
    sprite->setName(filename);
    
    if (parent) {
        parent->addChild(sprite, zOrder);
    }
    
    return sprite;
}

/**
 * MenuItemImage作成ヘルパー
 * ここで作ったものを引数にしてMenuを作る
 */
MenuItemImage* createMenuItemImage(const std::string& normalImage, const ccMenuCallback& callback, Vec2 anchor, Vec2 pos){
    auto item = createMenuItemImage(normalImage, "", callback, anchor, pos);
    item->setAutoSelected(true);
    item->setAutoDisabled(true);
    return item;
}
MenuItemImage* createMenuItemImage(const std::string& normalImage, const std::string& selectedImage, const ccMenuCallback& callback, Vec2 anchor, Vec2 pos){
    auto item = createMenuItemImage(normalImage, selectedImage, "", callback, anchor, pos);
    item->setAutoDisabled(true);
    return item;
}
MenuItemImage* createMenuItemImage(const std::string& normalImage, const std::string& selectedImage, const std::string& disabledImage, const ccMenuCallback& callback, Vec2 anchor, Vec2 pos){
    auto closeItem = MenuItemImage::create(normalImage, selectedImage, disabledImage, callback);
    closeItem->ignoreAnchorPointForPosition(false);
    closeItem->setAnchorPoint(anchor);
    closeItem->setPosition(pos);
    closeItem->setName(normalImage);
    return closeItem;
}

/**
 * メニュー作成ヘルパー
 * 最大3個までアイテムを持てる。3つ未満の場合はitem2,item3にnullptrを指定のこと
 */
Menu* createMenu(Node* parent, MenuItem* item1, MenuItem* item2, MenuItem* item3, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /*= 0*/){
    auto items = Vector<MenuItem*>();
    if (item1) { items.pushBack(item1); }
    if (item2) { items.pushBack(item2); }
    if (item3) { items.pushBack(item3); }
    return createMenu(parent, items, zOrder, anchor, pos, touchFixedPriority);
    /*auto menu = Menu::create(item1, item2, item3, nullptr);
    menu->ignoreAnchorPointForPosition(false);
    menu->setAnchorPoint(anchor);
    menu->setPosition(pos);
    menu->setName(std::string("menu") + item1->getName());
    
    if (parent) {
        parent->addChild(menu, zOrder);
    }
    
    return menu;*/
}

/**
 * メニュー作成ヘルパー
 */
Menu* createMenu(Node* parent, const Vector<MenuItem*>& items, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /*= 0*/){
    auto menu = Menu::create();
    menu->initWithArray(items, touchFixedPriority);
    menu->ignoreAnchorPointForPosition(false);
    menu->setAnchorPoint(anchor);
    menu->setPosition(pos);
    menu->setName(std::string("menu") + items.at(0)->getName());
    
    if (parent){
        parent->addChild(menu, zOrder);
    }
    
    return menu;
}


Menu *createOneMenu(Node* parent, const std::string& normalImage, const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /* = 0*/) {
    MenuItemImage* item = createMenuItemImage(normalImage, callback, anchor, pos);
    Menu* menu = createMenu(parent, item, nullptr, nullptr, zOrder, Vec2::ZERO, Vec2::ZERO, touchFixedPriority);
    return menu;
}
Menu *createOneMenu(Node* parent, const std::string& normalImage, const std::string& selectedImage, const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /* = 0*/) {
    MenuItemImage* item = createMenuItemImage(normalImage, selectedImage, callback, anchor, pos);
    Menu* menu = createMenu(parent, item, nullptr, nullptr, zOrder, Vec2::ZERO, Vec2::ZERO, touchFixedPriority);
    return menu;
}
Menu *createOneMenu(Node* parent, const std::string& normalImage, const std::string& selectedImage, const std::string& disabledImage, const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /* = 0*/) {
    MenuItemImage* item = createMenuItemImage(normalImage, selectedImage, disabledImage, callback, anchor, pos);
    Menu* menu = createMenu(parent, item, nullptr, nullptr, zOrder, Vec2::ZERO, Vec2::ZERO, touchFixedPriority);
    return menu;
}

//アイテムをZ型に並べる
Menu* createMenuWithArrayZ(Node* parent, const Vector<MenuItem*>& items, int xCount, float width, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /* = 0*/) {
    
    float itemWidth  = items.at(0)->getContentSize().width;
    float itemHeight = items.at(0)->getContentSize().height;
    int itemCount = (int)items.size();
    int sx = xCount;
    int sy = (itemCount + sx - 1) / sx;
    float diffWidth  = (width - (itemWidth * sx)) / (sx + 1);
    float diffHeight = diffWidth; //これは引数として受け取っても良い
    
    for (int i = 0; i < itemCount; ++i) {
        int ix = i % sx;
        int iy = i / sx;
        
        //座標決定(MenuItem のデフォルトの anchorPoint は Vec2::ANCHOR_BOTTOM_LEFT)
        float x = (ix) * itemWidth + (ix + 1) * diffWidth;
        float y = (sy - iy - 1) * itemHeight + (sy - iy - 1 + 1) * diffHeight;
        items.at(i)->setAnchorPoint(Vec2::ZERO);
        items.at(i)->setPosition(Vec2(x, y));
    }
    float totalHeight = (sy) * itemHeight + (sy + 1) * diffHeight;
    
    Menu* menu = createMenu(parent, items, zOrder, anchor, pos, touchFixedPriority);
    menu->setContentSize(Size(width, totalHeight));
    return menu;
}

//アイテムをN型に並べる
Menu* createMenuWithArrayN(Node* parent, const Vector<MenuItem*>& items, int yCount, float height, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /* = 0*/) {
    
    float itemWidth  = items.at(0)->getContentSize().width;
    float itemHeight = items.at(0)->getContentSize().height;
    int itemCount = (int)items.size();
    int sy = yCount;
    int sx = (itemCount + sy - 1) / sy;
    float diffHeight = (height - (itemHeight * sy)) / (sy + 1);
    float diffWidth  = diffHeight; //これは引数として受け取っても良い
    
    for (int i = 0; i < itemCount; ++i) {
        int ix = i / sy;
        int iy = i % sy;
        
        //座標決定(MenuItem のデフォルトの anchorPoint は Vec2::ANCHOR_BOTTOM_LEFT)
        float x = (ix) * itemWidth + (ix + 1) * diffWidth;
        float y = (sy - iy - 1) * itemHeight + (sy - iy - 1 + 1) * diffHeight;
        items.at(i)->setAnchorPoint(Vec2::ZERO);
        items.at(i)->setPosition(Vec2(x, y));
    }
    float totalWidth = (sx) * itemWidth + (sx + 1) * diffWidth;
    
    Menu* menu = createMenu(parent, items, zOrder, anchor, pos, touchFixedPriority);
    menu->setContentSize(Size(totalWidth, height));
    return menu;
}

Menu* createTextButton(Node* parent, const std::string& normalImage, const std::string& text, const ccMenuCallback& callback, int zOrder, Vec2 anchor, Vec2 pos, int touchFixedPriority /* = 0*/){
    Menu* menu = createOneMenu(parent, normalImage, callback, zOrder, anchor, pos, touchFixedPriority);
    MenuItemImage* item = dynamic_cast<MenuItemImage*>(menu->getChildren().at(0));
    Node* image = item->getNormalImage();
    Size imageSize = image->getContentSize();
    
    createSystemLabel(image, "", text, imageSize.height * 0.8f, TextHAlignment::CENTER, 1, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(image));
    return menu;
}


Label* createSystemLabel(Node* parent, const std::string& text, float fontSize, TextHAlignment hAlignment, int zOrder, Vec2 anchor, Vec2 pos){
    return createSystemLabel(parent, "", text, fontSize, hAlignment, zOrder, anchor, pos);
}

Label* createSystemLabel(Node* parent, const std::string fontName, const std::string& text, float fontSize, TextHAlignment hAlignment, int zOrder, Vec2 anchor, Vec2 pos){
    if (Application::getInstance()->getTargetPlatform() == Application::Platform::OS_ANDROID) {
        fontSize *= 0.95f;
    }

    Label* label = Label::createWithSystemFont(text, fontName, fontSize);
    label->setAnchorPoint(anchor);
    label->setPosition(pos);
    label->setAlignment(hAlignment);
    label->setName(text);
    
    if (parent) {
        parent->addChild(label, zOrder);
    }
    
    return label;
}

Label* createTTFLabel(Node* parent, const std::string fontName, const std::string& text, float fontSize, TextHAlignment hAlignment, int zOrder, Vec2 anchor, Vec2 pos){
    Label* label = Label::createWithTTF(text, fontName, fontSize);
    
    label->setAnchorPoint(anchor);
    label->setPosition(pos);
    label->setAlignment(hAlignment);
    label->setName(text);
    
    if (parent) {
        parent->addChild(label, zOrder);
    }
    
    return label;
}

/**
 * スクロールビュー作成ヘルパー
 *  contentSizedNodeは、scrollViewの直の子となるノード。必ずcontentSizeを設定しておくこと。スクロール画面に載せるノードは、すべてこのノードの子にしておくこと
 *  contentSizedNodeは、cocos内でsetPosition(0)されてしまう。scrollViewの中心合わせで表示するなどのことはできない。間にNodeを挟むこと(このメソッドで対応したい)
 */
ScrollView* createScrollView(Node* parent, Node* contentSizedNode, ScrollView::Direction dir, bool isBounceable, Size viewSize, int zOrder, Vec2 anchor, Vec2 pos){
    auto scrollView = ScrollView::create();
    
    scrollView->ignoreAnchorPointForPosition(false); //アンカーポイントを有効にする(ScrollViewはデフォルトでアンカー無効)
    scrollView->setDirection(dir);
    scrollView->setContainer(contentSizedNode);
    scrollView->setAnchorPoint(anchor);
    scrollView->setPosition(pos);
    scrollView->setViewSize(viewSize);
    scrollView->setContentOffset(dir == ScrollView::Direction::HORIZONTAL ? scrollView->maxContainerOffset() : scrollView->minContainerOffset());
    scrollView->setBounceable(isBounceable);
    
    if (parent) {
        parent->addChild(scrollView, zOrder);
    }
    
    return scrollView;
}

/**
 * LayerColor作成ヘルパー
 */
LayerColor* createLayerColor(Node* parent, Color4B color, int zOrder, Size size, Vec2 anchor, Vec2 pos/*=Vec2::ZERO*/){
    auto layerColor = LayerColor::create(color);
    
    layerColor->ignoreAnchorPointForPosition(false); //アンカーポイントを有効にする(LayerColorはデフォルトでアンカー無効)
    layerColor->setContentSize(size);
    layerColor->setAnchorPoint(anchor);
    layerColor->setPosition(pos);
    layerColor->setName("layerColor");
    
    if (parent) {
        parent->addChild(layerColor, zOrder);
    }
    
    return layerColor;
}

/**
 * EventTouchListener（シングルタップ）作成ヘルパー
 */
EventListenerTouchOneByOne* createSingleTouchListener(Node* node, std::function<bool(Touch*, Event*)> onTouchBegan, std::function<void(Touch*, Event*)> onTouchMoved, std::function<void(Touch*, Event*)> onTouchEnded, std::function<void(Touch*, Event*)> onTouchCancelled, bool isSwallow, int fixedPriority /*= 0*/){
    
    //シングルタップを受け取るイベントのインスタンス
    auto listener = EventListenerTouchOneByOne::create();
    
    //各種イベントメソッド。onTouchBeganだけは必須。その他は不要ならnullptrでよい
    listener->onTouchBegan = onTouchBegan;
    listener->onTouchMoved = onTouchMoved;
    listener->onTouchEnded = onTouchEnded;
    listener->onTouchCancelled = onTouchCancelled;
    
    //イベントを飲み込むか（他のイベントに伝播するかどうか）
    listener->setSwallowTouches(isSwallow);
    
    //引数で固定プライオリティを指定していれば、設定
    if (fixedPriority != 0) {
        node->getEventDispatcher()->addEventListenerWithFixedPriority(listener, fixedPriority);
    }
    //指定がなければ、通常のシーングラフ通りの優先度で設定
    else{
        node->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, node);
    }
    
    return listener;
}

void destroySingleTouchListener(EventListenerTouchOneByOne* listener, Node* node){
    node->getEventDispatcher()->removeEventListener(listener);
}


#pragma mark - Node

//横を指定のサイズにする（縦はそのまま）
Node* setNodeViewWidth( Node* node, float width){
    node->setScaleX(width / node->getContentSize().width);
    return node;
}

//縦を指定のサイズにする（横はそのまま）
Node* setNodeViewHeight(Node* node, float height){
    node->setScaleY(height / node->getContentSize().height);
    return node;
}

//縦横を指定のサイズにする
Node* setNodeViewSize(  Node* node, Size size){
    node->setScale(size.width / node->getContentSize().width, size.height / node->getContentSize().height);
    return node;
}

//縦横比を維持してサイズを合わせる（X指定、Yはそれに準ずる）
Node* setNodeViewWidthFixedAspect( Node* node, float width){
    node->setScale(width / node->getContentSize().width);
    return node;
}

//縦横比を維持してサイズを合わせる（Y指定、Xはそれに準ずる）
Node* setNodeViewHeightFixedAspect(Node* node, float height){
    node->setScale(height / node->getContentSize().height);
    return node;
}

//縦横比を維持してサイズを合わせる（小さい方に合わせる＝はみ出ないように）
Node* setNodeViewSizeFixedAspect(Node* node, Size size){
    float scaleX = size.width / node->getContentSize().width;
    float scaleY = size.height / node->getContentSize().height;
    node->setScale(std::min(scaleX, scaleY));
    return node;
}

//ラベルを範囲内に収める
Node* setLabelWidth(Label* label, float width, float maxSize /*= 0.f*/){
    float realWidth = label->getContentSize().width;
    float realFontSize = label->getSystemFontSize();
    
    float maxCoef = maxSize >= 0.1 ? maxSize / realFontSize : 1.f;
    float coef = std::min(maxCoef, width / realWidth);
    float toFontSize = realFontSize * coef;
    label->setSystemFontSize(toFontSize);
    return label;
}

Node* setLabelWidth(Label* label, std::string text, float width, float maxSize /*= 0.f*/){
    label->setString(text);
    return setLabelWidth(label, width, maxSize);
}

Node* setTTFLabelWidth(Label* label, float width, float maxSize /*= 0.f*/){
    TTFConfig conf = label->getTTFConfig();
    float realWidth = label->getContentSize().width;
    float realFontSize = conf.fontSize;
    
    float maxCoef = maxSize >= 0.1 ? maxSize / realFontSize : 1.f;
    float coef = std::min(maxCoef, width / realWidth);
    float toFontSize = realFontSize * coef;
    conf.fontSize = toFontSize;
    label->setTTFConfig(conf);
    return label;
}

Node* setTTFLabelWidth(Label* label, std::string text, float width, float maxSize /*= 0.f*/){
    label->setString(text);
    return setTTFLabelWidth(label, width, maxSize);
}

//親を変更する
void changeParent(Node* child, Node* newParent, bool copyPosition /*= false*/){
    if ((!child) || (child->getParent() == newParent)) {
        return;
    }
    
    auto newPosition = copyPosition ? child->getPosition() : convertNodeAnchorPosToAnotherNodeSpace(child, newParent);
    child->retain();
    child->removeFromParentAndCleanup(false);
    newParent->addChild(child);
    child->release();
    child->setPosition(newPosition);
}


#pragma mark - Vec, Rect, Size

Vec2 getCenterOfRect(Rect rect){
    return Vec2(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height / 2);
}

Vec2 getLocalCenterPos(Node* node){
    return Vec2(node->getContentSize().width / 2, node->getContentSize().height / 2);
}

Vec2 getPosOfInnerRect(Vec2 pos, Rect rect){
    if (pos.x < rect.getMinX()) { pos.x = rect.getMinX(); }
    if (pos.x > rect.getMaxX()) { pos.x = rect.getMaxX(); }
    if (pos.y < rect.getMinY()) { pos.y = rect.getMinY(); }
    if (pos.y > rect.getMaxY()) { pos.y = rect.getMaxY(); }
    return pos;
}

bool isRectContainsPoint(Rect rect, Vec2 pos){
    return rect.getMinX() <= pos.x && pos.x <= rect.getMaxX() &&
    rect.getMinY() <= pos.y && pos.y <= rect.getMaxY();
}

//2つのRectの交差部分を返す。交差していない場合は戻り値は不定. rect1.intersectsRect(rect2)が真であることを先に確認のこと
Rect intersectWithRect(Rect rect1, Rect rect2){
    float thisLeftX = rect1.origin.x;
    float thisRightX = rect1.origin.x + rect1.size.width;
    float thisTopY = rect1.origin.y + rect1.size.height;
    float thisBottomY = rect1.origin.y;
    
    if (thisRightX < thisLeftX)
    {
        std::swap(thisRightX, thisLeftX);   // This rect has negative width
    }
    
    if (thisTopY < thisBottomY)
    {
        std::swap(thisTopY, thisBottomY);   // This rect has negative height
    }
    
    float otherLeftX = rect2.origin.x;
    float otherRightX = rect2.origin.x + rect2.size.width;
    float otherTopY = rect2.origin.y + rect2.size.height;
    float otherBottomY = rect2.origin.y;
    
    if (otherRightX < otherLeftX)
    {
        std::swap(otherRightX, otherLeftX);   // Other rect has negative width
    }
    
    if (otherTopY < otherBottomY)
    {
        std::swap(otherTopY, otherBottomY);   // Other rect has negative height
    }
    
    float combinedLeftX = std::max(thisLeftX, otherLeftX);
    float combinedRightX = std::min(thisRightX, otherRightX);
    float combinedTopY = std::min(thisTopY, otherTopY);
    float combinedBottomY = std::max(thisBottomY, otherBottomY);
    
    return Rect(combinedLeftX, combinedBottomY, combinedRightX - combinedLeftX, combinedTopY - combinedBottomY);
}


/**
 * Rectの中心座標を返す
 */
Vec2 getRectMidPos(Rect rect){
    return Vec2(rect.origin.x + rect.size.width / 2,
                rect.origin.y + rect.size.height / 2);
}

/**
 * あるノードの指定アンカー座標を自座標系で返す
 */
Vec2 getNodePositionOf(Node* node, Vec2 anchor){
    return Vec2(node->getContentSize().width * anchor.x, node->getContentSize().height * anchor.y);
}

/**
 * あるノードの中心座標を、別のノード相対の座標に変換して返す
 */
Vec2 convertNodeCenterPosToAnotherNodeSpace(Node* fromNode, Node* toNode){
    Vec2 centerPos = Vec2(fromNode->getContentSize().width / 2, fromNode->getContentSize().height);
    Vec2 worldPos = fromNode->convertToWorldSpace(centerPos);
    return toNode->convertToNodeSpace(worldPos);
}

/**
 * あるノードのアンカー座標を、別のノード相対の座標に変換して返す
 */
Vec2 convertNodeAnchorPosToAnotherNodeSpace(Node* fromNode, Node* toNode){
    Vec2 anchorPos = fromNode->getPosition();
    Vec2 worldPos = fromNode->getParent()->convertToWorldSpace(anchorPos);
    return toNode->convertToNodeSpace(worldPos);
}

/**
 * あるノードの指定アンカー座標を、別のノード相対の座標に変換して返す
 */
Vec2 convertNodeAnchorPosToAnotherNodeSpace(Node* fromNode, Vec2 anchor, Node* toNode){
    Vec2 anchorPos = getNodePositionOf(fromNode, anchor);
    Vec2 worldPos = fromNode->getParent()->convertToWorldSpace(anchorPos);
    return toNode->convertToNodeSpace(worldPos);
}


#pragma mark - touch

//タッチが指定のノード内だったかどうか
bool isTouchInside(Node* target, Touch* touch){
    Rect targetBox = target->getBoundingBox();
    Vec2 touchPos  = target->getParent()->convertTouchToNodeSpace(touch);
    return targetBox.containsPoint(touchPos);
}

//指定のノード以下にタッチイベントを伝播させない(ただし、FixedPriorityが指定されているタッチリスナーは伝播してしまう)
EventListenerTouchOneByOne* stopTouchEventListenerUnder(Node* node, int fixedPriority/*= 0*/){
    auto listener = createSingleTouchListener(node,    //nodeに付けたタッチリスナーが、
                                              [](Touch* touch, Event* event){return true;}, //すべてのタッチを受け入れ、
                                              nullptr, nullptr, nullptr,
                                              true, //それを飲み込む(swallow)ことで、シーングラフ的に下層のノードにタッチが伝播しなくなる
                                              fixedPriority);
    return listener;
}

//ふたたび伝播を許可する
void restartTouchEventListenerUnder(Node* node, EventListenerTouchOneByOne* listener){
    destroySingleTouchListener(listener, node);
}

#pragma mark - 親子関係

//指定のノードから親を辿り、第2指定のノードの直の子に打ち当たったら、そのノードを返す
Node* getChildOfTargetNodeUpstreamFrom(Node* from, Node* target) {
    Node* current = from;
    while (1) {
        if (current->getParent() == target) {
            return current;
        }
        else{
            current = current->getParent();
        }
    }
    return nullptr;
}

//指定のノード(スクロールビューの子孫)が、見えているかどうか(一部でも見えていればtrue)
bool isNodeVisibleInScrollView(ScrollView* scrollView, Node* node) {
    return scrollView->isNodeVisible(getChildOfTargetNodeUpstreamFrom(node, scrollView->getContainer()));
}


//子孫すべてに指定の処理を実行
void recursiveCallFunc(Node* node, const std::function<void(Node*)> &func){
    func(node);
    
    for (int i = 0; i < node->getChildrenCount(); ++i) {
        Node* child = node->getChildren().at(i);
        recursiveCallFunc(child, func);
    }
}

//グローバルZを指定(グローバルZは子孫ノードに伝播しないので、明示的に子孫含め全ノードにグローバルZ指定をすることが必要。そのヘルパー関数)
void setGlobalZRecursive(Node* node, float globalZ){
    recursiveCallFunc(node, [=](Node* node){node->setGlobalZOrder(globalZ);});
}


#pragma mark - screen

RenderTexture* renderNode(Node* targetNode) {
#if DEBUG
    if (!targetNode->getPosition().isZero()){
        printf("renderNode(Node* targetNode): targetNodeにtransition成分が入っているので、レンダーテクスチャからずれる可能性があります\n");
        printf("targetNodeの上にダミーノードを作り、そちらにtransition成分をセットするのが上策\n");
    }
#endif

    Size renderSize = Size(targetNode->getContentSize().width  * targetNode->getScaleX(),
                           targetNode->getContentSize().height * targetNode->getScaleY());
    RenderTexture* texture = RenderTexture::create(renderSize.width, renderSize.height);
    
    texture->begin();
    targetNode->visit();
    texture->end();
    
    return texture;
}

void renderNodeAndSaveToFileAsPng(Node* targetNode, std::string fname, std::function<void(RenderTexture* texture, const std::string& fullpath)> callback/*= nullptr*/) {
    RenderTexture* texture = renderNode(targetNode);
    texture->saveToFile(fname, Image::Format::PNG, true, callback);
}

void captureScreenWithRect(Node* targetNode, Rect rect, std::string fname, std::function<void(const std::string)> successCallback, std::function<void()> failureCallback){
    RenderTexture* texture = RenderTexture::create(rect.size.width, rect.size.height);
    texture->setVirtualViewport(Vec2(rect.origin), rect, Rect(0,0,rect.size.width, rect.size.height));

    texture->begin();
    targetNode->visit();
    texture->end();
    
    texture->saveToFile(fname, Image::Format::PNG, true, [=](RenderTexture*, const std::string& fullpath){
        if (successCallback) {
            successCallback(fullpath);
        }
    });
    //saveToFileは失敗を返すことがない
}

void captureFullScreen(std::string fname, std::function<void(const std::string)> successCallback, std::function<void()> failureCallback){
    cocos2d::utils::captureScreen([=](bool success, const std::string fullpath){
        if (success) {
            //画像が更新されたことを反映するため,キャッシュクリアしておく
            Director::getInstance()->getTextureCache()->removeTextureForKey(fullpath);
            
            if (successCallback) {
                successCallback(fullpath);
            }
        }
        else{
            if (failureCallback) {
                failureCallback();
            }
        }
    }, fname);
}


#pragma mark - animation

//Animationインスタンスを作成する
//  戻り値をAnimate::create()に引数として渡すと、アクションとして使えるようになる
Animation* createAnimationWithFormat(float delayPerUnit, int loops, bool restoreOriginalFrame, const char* animNameFormat, int startIndex/* = 1*/, int endIndex/* = 999*/){
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    
    std::vector<std::string> anims;
    for (int i = startIndex; i <= endIndex; ++i) {
        
        //formatからファイル名を連続自動生成
        char fname[256] = {0};
        snprintf(fname, sizeof(fname)/sizeof(fname[0]) - 1, animNameFormat, i);
        
        //ファイル名が存在する限り、animsに追加し続ける
        if (FileUtils::getInstance()->isFileExist(fname) ||
            cache->getSpriteFrameByName(fname)) {
            
            std::string str = fname;
            anims.push_back(str);
        }
        else{
            break;
        }
    }
    
    return createAnimation(delayPerUnit, loops, restoreOriginalFrame, anims);
}

Animation* createAnimation(float delayPerUnit, int loops, bool restoreOriginalFrame, const char* anim1, ...) {

    std::vector<std::string> anims;

    //vector型に変換
    va_list args;
    va_start(args, anim1);
    const char* fname = anim1;
    while (fname){
        anims.push_back(fname);
        fname = va_arg(args, const char*);
    }
    va_end(args);
    
    return createAnimation(delayPerUnit, loops, restoreOriginalFrame, anims);
}

Animation* createAnimation(float delayPerUnit, int loops, bool restoreOriginalFrame, std::vector<std::string>& anims) {
    Animation* anim = Animation::create();
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    
    for (auto it = anims.begin(); it != anims.end(); ++it) {
        const char* fname = (*it).c_str();
        if (FileUtils::getInstance()->isFileExist(fname)) {
            anim->addSpriteFrameWithFile(fname);
        }
        else{
            anim->addSpriteFrame(cache->getSpriteFrameByName(fname));
        }
    }
    
    anim->setDelayPerUnit(delayPerUnit);
    anim->setLoops(loops);
    anim->setRestoreOriginalFrame(restoreOriginalFrame);
    
    return anim;
}

#pragma mark - delayed

Action* delayedExecution(Node* node, float duration, const std::function<void()>& func){
    return node->runAction(Sequence::create(DelayTime::create(duration),
                                            CallFunc::create(func),
                                            NULL));
}


#pragma mark - label

int TAG_LABEL_ACTION        = 12345;
int TAG_LABEL_FINISH_ACTION = 12346;
int LENGTH_PER_CHARACTER_UTF8 = 3;

void labelShowSequentially2_inner(Label* label, std::string text, float duration, int length, std::function<void()> callback = nullptr){
    //char* doOnce = setlocale(LC_CTYPE, "ja_JP.UTF-8");

    label->setString(text.substr(0, length));
    //printf("%s\n", label->getString().c_str());
    
    if (length < text.length()) {
        //次の文字のバイト数を取得
        //int thisCharByte = mblen(text.c_str() + length, MB_CUR_MAX);
        int thisCharByte = LENGTH_PER_CHARACTER_UTF8;
        
        unsigned char nextByte = text.at(length);
        if (nextByte >= 0x0 && nextByte <= 0x7f) {
            thisCharByte = 1; //asciiの場合は1バイトだけ表示のこと。ref. https://ja.wikipedia.org/wiki/UTF-8
        }

        delayedExecution(label, duration, [=](){
            labelShowSequentially2_inner(label, text, duration, length + thisCharByte, callback);
        })->setTag(TAG_LABEL_ACTION);
    }
    else{
        label->setString(text);
        if (callback) {
            callback();
        }
    }
}

void labelShowSequentially2(Label* label, std::string text, float duration, std::function<void()> callback/*= nullptr*/){
    label->setString("");
    
    labelShowSequentially2_inner(label, text, duration, 0, callback);
}

void labelShowInstantly2(Label* label, std::string text) {
    label->stopActionByTag(TAG_LABEL_ACTION);
    label->setString(text);
}


void labelSequentialShow(Label *label, float duration, std::function<void()> callback /*= nullptr*/){
    int letterIndex = 0;
    while (letterIndex < label->getStringLength() + label->getStringNumLines()) {
        Sprite* sp = label->getLetter(letterIndex);
        if (sp) {
            Sequence* seq = Sequence::create(CallFunc::create([=](){ sp->setVisible(false); }),
                                             DelayTime::create(duration * letterIndex),
                                             CallFunc::create([=](){ sp->setVisible(true);  }),
                                             nullptr);
            seq->setTag(TAG_LABEL_ACTION);
            sp->runAction(seq);
        }
        ++letterIndex;
    }
    
    //最後の文字にcallback呼び出しアクションを追加
    Sprite* lastSp = label->getLetter(letterIndex - 1);
    if (lastSp) {
        auto seq = Sequence::create(DelayTime::create(duration * letterIndex),
                                    CallFunc::create([=](){if (callback) callback();}),
                                    NULL);
        seq->setTag(TAG_LABEL_FINISH_ACTION);
        lastSp->runAction(seq);
    }
}

void labelShowInstantly(Label *label) {
    for (int letterIndex = 0; letterIndex < label->getStringLength() + label->getStringNumLines(); ++letterIndex) {
        Sprite *sp = label->getLetter(letterIndex);
        if (sp) {
            sp->stopActionByTag(TAG_LABEL_ACTION);
            sp->setVisible(true);
        }
    }
}


#pragma mark - ActionInterval

BgmFade::BgmFade()
{
    m_initialVal = 0;
    m_targetVal = 0;
}

BgmFade* BgmFade::create(float duration, float volume, bool pauseOnComplete)
{
    BgmFade *pAction = new BgmFade();
    pAction->initWithDuration(duration, volume, pauseOnComplete);
    pAction->autorelease();
    
    return pAction;
}

bool BgmFade::initWithDuration(float duration, float volume, bool pauseOnComplete)
{
    if (ActionInterval::initWithDuration(duration))
    {
        m_targetVal = volume;
        m_bPauseOnComplete = pauseOnComplete;
        return true;
    }
    
    return false;
}

void BgmFade::update(float time)
{
    float vol = m_initialVal + time*(m_targetVal - m_initialVal);
    SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(vol);
}

BgmFade* BgmFade::clone(void) const
{
    // no copy constructor
    auto a = new BgmFade();
    a->initWithDuration(_duration, m_targetVal, m_bPauseOnComplete);
    a->autorelease();
    return a;
}

BgmFade* BgmFade::reverse() const
{
    return BgmFade::create(_duration, m_targetVal);
}

void BgmFade::startWithTarget(Node *pTarget)
{
    ActionInterval::startWithTarget( pTarget );
    m_initialVal = SimpleAudioEngine::getInstance()->getBackgroundMusicVolume();
}

void BgmFade::stop(void)
{
    if(m_bPauseOnComplete) SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    ActionInterval::stop();
}
