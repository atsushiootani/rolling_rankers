//
//  Debug.h
//
//
//  Created by OtaniAtsushi1 on 2014/11/09.
//
//

#ifndef Debug_h
#define Debug_h

//デバッグ用出力
#ifdef DEBUG
#define DPRINTF(...) printf(__VA_ARGS__)
#else
#define DPRINTF(...)
#endif

#ifdef DEBUG
#define DERROR(...)  do { printf("=======ERROR======= %s %d\n", __FILE__, __LINE__); printf(__VA_ARGS__); } while(0)
#else
#define DERROR(...)
#endif

#endif
