//
//  GameHelper.cpp
//
//
//  Created by OtaniAtsushi1 on 2014/12/23.
//
//

#include "GameHelper.h"
#include <random>
#include <sstream>
#include <iostream>
#include <iomanip>


#pragma mark - 時刻

time_t getNowDate() {
    return time(nullptr);
}

double getSystemTime() {
    return 1.0 * clock() / CLOCKS_PER_SEC;
}


#pragma mark - 乱数

double getRand() {
    static std::random_device seed_gen;
    static std::mt19937 engine(seed_gen());
    static std::uniform_real_distribution <> dist1(0.0, 1.0);
    return dist1(engine);
}

double getFRand(double min, double max) {
    double r = getRand();
    return r * (max - min) + min;
}

int getIntRand(int min, int max) {
    double r = getRand();
    return r * (max - min + 1) + min;
}


#pragma mark - 文字列

//末尾の改行を削除
std::string chompString(std::string str){
    while (str.length() > 0) {
        size_t endPos = str.length() - 1;
        char c = str[endPos];
        if (c == '\n' || c == '\r') {
            str.erase(endPos);
        }
        else{
            break;
        }
    }
    
    return str;
}

//time_tを文字列にして返す。YYYY/MM/DD HH:mm:ss
std::string getFullDateString(time_t date) {
    char text[50]={0};
    
    struct tm *t = localtime(&date);
    sprintf(text, "%d/%02d/%02d %02d:%02d:%02d",
            1900 + t->tm_year, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
    
    return text;
}

//time_tを文字列にして返す。HH:mm:ss
std::string getLocalTimeString(time_t date) {
    char text[50]={0};
    
    struct tm *t = localtime(&date);
    sprintf(text, "%02d:%02d:%02d",
            t->tm_hour, t->tm_min, t->tm_sec);
    
    return text;
}

//time_tを文字列にして返す。mm:ss
std::string getRestTimeMSString(int sec) {
    char text[50]={0};
    
    sprintf(text, "%02d:%02d",
            sec / 60, sec % 60);
    return text;
}


#pragma mark - 数値から文字列への変換

//doubleから文字列を作成。小数点以下の桁数を指定する
std::string getStringFromDouble(double value, int digitsUnderPeriod){
    std::ostringstream oss;
    oss << std::setprecision(digitsUnderPeriod) << std::setiosflags(std::ios::fixed) << value;
    return oss.str();
}


#pragma mark - 時刻

time_t getNow(){
    return time(nullptr);
}

