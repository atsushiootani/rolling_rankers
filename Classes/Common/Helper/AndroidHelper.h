//
//  AndroidHelper.h
//
//
//  Created by OtaniAtsushi1 on 2016/02/20.
//
//

#ifndef AndroidHelper_h
#define AndroidHelper_h

#include <ostream>
#include <sstream>
#include <string>

//Android NDK では対応していない stl の関数などを自前実装する
//Android.mk で
// APP_STL := gnustl_static
//だと、std::to_string, std::stol などが使えない
// APP_STL := c++_static
//だと使えるが、今度は java.lang.UnsatisfiedLinkError: dlopen failed: unknown reloc type 160 という実行時エラーが出るので、結局不可

#if defined CC_TARGET_OS_ANDROID

namespace std{

    template <typename T>
    std::string to_string(T value)
    {
        std::ostringstream os;
        os << value;
        return os.str();
    }
    
    //__strは、変換対象の文字列
    //__idxは、変換後の最初の文字のインデックスを格納して返す
    //__baseは、何進数にするかの指定
    int                stoi  (const std::string& __str, size_t* __idx = 0, int __base = 10);
    long               stol  (const std::string& __str, size_t* __idx = 0, int __base = 10);
    unsigned long      stoul (const std::string& __str, size_t* __idx = 0, int __base = 10);
    long long          stoll (const std::string& __str, size_t* __idx = 0, int __base = 10);
    unsigned long long stoull(const std::string& __str, size_t* __idx = 0, int __base = 10);
    float              stof  (const std::string& __str, size_t* __idx = 0);
    double             stod  (const std::string& __str, size_t* __idx = 0);
    
}

#endif

#endif
