#include "AppDelegate.h"
#include "GameManager.h"
#include "SaveManager.h"
#include "Sound.h"
#include "Screen.h"
#include "Native.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

#include "MainScene.h"
#define LAUNCH_SCENE MainScene

USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

AppDelegate::AppDelegate() {
    isBgmPlaying = false;
}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("rolling", Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("rolling");
#endif
        director->setOpenGLView(glview);
    }

    // turn on display FPS
#if DEBUG
    director->setDisplayStats(true);
#else
    director->setDisplayStats(false);
#endif
    
    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // Set the design resolution
    glview->setDesignResolutionSize(SCREEN_WIDTH, SCREEN_HEIGHT, ResolutionPolicy::EXACT_FIT);

    //デバイス画面サイズと仮想スクリーンサイズの自動調整だが、却って余計なのでコメントアウト
    /*
    Size frameSize = glview->getFrameSize();
    // if the frame's height is larger than the height of medium size.
    if (frameSize.height > mediumResolutionSize.height)
    {        
        director->setContentScaleFactor(MIN(largeResolutionSize.height/designResolutionSize.height, largeResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is larger than the height of small size.
    else if (frameSize.height > smallResolutionSize.height)
    {        
        director->setContentScaleFactor(MIN(mediumResolutionSize.height/designResolutionSize.height, mediumResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is smaller than the height of medium size.
    else
    {        
        director->setContentScaleFactor(MIN(smallResolutionSize.height/designResolutionSize.height, smallResolutionSize.width/designResolutionSize.width));
    }
     */
    
    register_all_packages();

    
    /**
     * 各種ゲームの初期化
     */
    getGameManager();
    getSaveManager();
    
    //サウンドの初期化
    registerSoundWithModerateMode();
    
    Native_InitPronounce();
    Native_AdRegister();
    Native_RegisterLocalNotification();
    Native_InitUserAuth();
    Native_InitStorage();
    Native_InitDatabase();

    //ロード
    getGameManager()->loadAll();
    getGameManager()->onApplicationLaunched();
    
    
    // create a scene. it's an autorelease object
    auto scene = LAUNCH_SCENE::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->pause();
    Director::getInstance()->stopAnimation();
    isBgmPlaying = CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying();
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
    
    getGameManager()->onEnterBackground();
    getGameManager()->saveAll();
 
    Native_SleepableMode();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    if (!Native_AdMovieIsPlaying()) {
        Director::getInstance()->resume();
        Director::getInstance()->startAnimation();
        if (isBgmPlaying) {
            CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        }
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    }
    
    getGameManager()->loadAll();
    getGameManager()->onEnterForeground();
    
    Native_KeepScreenMode();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
