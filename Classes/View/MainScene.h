//
//  MainScene.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/29.
//
//

#ifndef MainScene_h
#define MainScene_h

#include "cocos2d.h"
#include "GameManager.h"
#include "RankingManager.h"
#include "Screen.h"


USING_NS_CC;

class BaseLayer;

class MainScene : public cocos2d::Layer, public GameManagerDelegate
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init() override;
    
    CREATE_FUNC(MainScene);
    
    
public:
    void finScene();
    
    //layer
    bool initCurrentLayer(LayerType layerType, Rect layerRect, bool isVisible = true);
    void finCurrentLayer();
    BaseLayer* getCurrentLayer();
    void changeLayer(LayerType newLayerType);
    
    //update
    virtual void update(float delta) override;
    
    //proc
    void scheduleLocalNotifications();
    void procTutorial();
    void setEnableHeaderFooter(bool enabled);
    bool isEnabledHeaderFooter();
    void checkAndShowNoticeWindow();
    void noticeWindowCallback(Ref* pSender);
    void closeNoticeWindowButtonCallback(Ref* pSender);
    void procGameStartable();
    
    //draw
    void updateView();
    
    //effects

    //button
    
    //touch
    virtual bool onTouchBegan(Touch* touch, Event *event) override;
    virtual void onTouchMoved(Touch* touch, Event *event) override;
    virtual void onTouchEnded(Touch* touch, Event *event) override;
    virtual void onTouchCancelled(Touch* touch, Event *event) override;
    
    //GameManager delegate
    virtual void onEnterBackground(GameManager* gameManager) override;
    virtual void onEnterForeground(GameManager* gameManager) override;
    virtual void onUnlockAchievement(GameManager* gameManager, int index) override;
    virtual void onCanUnlockAchievement(GameManager* gameManager, int index) override;

private:
    //シーンの生存時間
    float sceneTime;
    
    //レイヤーサイズ
    Rect layerRect;
    
    //各種レイヤー
    BaseLayer* currentLayer;
    
    //レイヤータイプ
    LayerType currentLayerType;
    
    //暗転用
    LayerColor* fadeLayer;
    
private:
    //ヘッダー
    Sprite* headerSprite;
    Label* headerLabel;
    
    //表示すべき実績
    //std::vector<int> needsToShowAchievement;
    
    //告知ウィンドウ
    Node* noticeBaseNode;
    Sprite* noticeWindow;
    
    //タッチ
    EventListenerTouchOneByOne *touchListener;
    EventListenerTouchOneByOne *touchListenerForStop;
    EventListenerTouchOneByOne *touchListenerForNoticeWindow;
};




#endif /* MainScene_h */
