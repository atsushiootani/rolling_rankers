//
//  MainScene.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/29.
//
//

#include "MainScene.h"
#include "BaseLayer.h"
#include "GameLayer.h"
#include "GameManager.h"
#include "SceneManager.h"
#include "GameData.h"
#include "GameHelper.h"
#include "CocosHelper.h"
#include "Sound.h"
#include "SoundData.h"
#include "ResourceData.h"
#include "TextData.h"
#include "Native.h"
#include "Screen.h"
#include "Debug.h"


USING_NS_CC;


//ノードのzOrder値
enum {
    zOrderBg,
    
    //レイヤー
    zOrderLayerBase,
    
    //自社ロゴ
    zOrderLogoBanner,
    
    //ヘッダ
    zOrderHeader,
    zOrderHeaderText,
    zOrderHeaderIcon,
    zOrderHeaderButton,
    
    //フェード
    zOrderFade,
    
    //オーバーレイヤー
    zOrderOverLayer,
    
    //ポップアップレイヤー
    zOrderPopup,
    
    //告知ウィンドウ
    zOrderNoticeWindow,
};


Scene* MainScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MainScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


// on "init" you need to initialize your instance
bool MainScene::init()
{
    if (!Layer::init()) {
        return false;
    }
    
    //画面サイズは、SCREEN_WIDTH/SCREEN_HEIGHTを使用
    
    //各画面の描画サイズ(header, footerが決まると計算できる)
    layerRect = Rect(0,
                     FOOTER_HEIGHT + AD_BANNER_HEIGHT,
                     SCREEN_WIDTH,
                     SCREEN_HEIGHT - FOOTER_HEIGHT - HEADER_HEIGHT - AD_BANNER_HEIGHT);
    
    /**
     * 変数の初期化
     */
    
    //時刻
    sceneTime = 0.f;
    
    
    /**
     * 各種マネージャの初期化
     */
    
    //フェード作成
    getSceneManager()->setContext(this);
    getSceneManager()->registerFade(        createNode(this, zOrderFade,  Vec2::ZERO, Size(SCREEN_WIDTH, SCREEN_HEIGHT)));
    getSceneManager()->registerPopupTopNode(createNode(this, zOrderPopup, Vec2::ZERO, Size(SCREEN_WIDTH, SCREEN_HEIGHT)));
    
    //GameManagerにdelegateをセット
    getGameManager()->setDelegate(this);
    
    //ランキング初期化
    getRankingManager()->registerRanking();
    getRankingManager()->fetchRankingInfo();

    
    /**
     * 画面作成
     */
    
    //背景表示
    createSprite(this, "bg.png", zOrderBg, Vec2::ANCHOR_MIDDLE, Vec2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
    
    fadeLayer = createLayerColor(this, Color4B(0,0,0,255), zOrderFade, getContentSize(), Vec2::ZERO);
    fadeLayer->setOpacity(0);
    
    noticeWindow = nullptr;
    
    //タッチリスナー
    touchListener = createSingleTouchListener(this,
                                              CC_CALLBACK_2(MainScene::onTouchBegan, this),
                                              CC_CALLBACK_2(MainScene::onTouchMoved, this),
                                              CC_CALLBACK_2(MainScene::onTouchEnded, this),
                                              CC_CALLBACK_2(MainScene::onTouchCancelled, this),
                                              true);
    touchListenerForStop = nullptr;
    touchListenerForNoticeWindow = nullptr;
    
    /**
     * 各画面生成
     */
    currentLayerType = LAYER_GAME;
    initCurrentLayer(currentLayerType, layerRect, true);
    
    /**
     * 各種起動時処理
     */
    
    //BGM再生
    //playBgm(BGM_MENU);
    
    //ロンチ時用のアラートビュー表示
    Native_ShowPronounceLaunchAlertView();
    
    //起動をアナリティクス記録
    Native_AnalyticsAppOpen();
    
    //広告
    //Native_AdBannerShow();
    
    //起動時画像
    checkAndShowNoticeWindow();
    
    //tutorial
    //procTutorial(); TODO:
    
    
    //update
    scheduleUpdate();
    
    return true;
}

void MainScene::finScene(){
    if (touchListener) {
        destroySingleTouchListener(touchListener, this);
        touchListener = nullptr;
    }
    if (!isEnabledHeaderFooter()) {
        setEnableHeaderFooter(true);
    }
}


#pragma mark - layer

//レイヤー作成
bool MainScene::initCurrentLayer(LayerType layerType, Rect layerRect, bool isVisible/*= true*/) {
    switch (layerType) {
        case LAYER_GAME: currentLayer = GameLayer::create();           break;
        default:         DERROR("not LAYER CREATE set:(%d)\n", layerType); return false;
    }
    
    currentLayer->initLayer(this, layerRect);
    this->addChild(currentLayer, zOrderLayerBase);
    currentLayer->setVisible(isVisible);
    
    return true;
}

//レイヤー破棄
void MainScene::finCurrentLayer(){
    SAFE_REMOVE_FROM_PARENT(currentLayer);
}

//現在のレイヤーインスタンスを返す
BaseLayer* MainScene::getCurrentLayer() {
    return currentLayer;
}

//レイヤー切り替え
void MainScene::changeLayer(LayerType newLayerType){
    
    if (currentLayerType != newLayerType) {
        
        //前タブの終了処理
        finCurrentLayer();
        
        //タブ変更
        currentLayerType = newLayerType;
        
        //新タブの開始処理
        if (!initCurrentLayer(newLayerType, layerRect, true)) {
            DERROR("cannot init layer: %d\n", newLayerType);
            return;
        }
        getCurrentLayer()->drawRefresh();
        
        //Native_AdBannerShow();
        Native_AdInterstitialShow();
    }
}


#pragma mark - update

void MainScene::update(float delta){
    sceneTime += delta;
    
    //ゲームマネージャ実行
    getGameManager()->update(delta);
    
    //Native周りのポーリング処理
    Native_CheckPollingAll();
    
    //描画更新
    updateView();
}


#pragma mark - proc

//ローカル通知を作成
void MainScene::scheduleLocalNotifications(){
}

void MainScene::procTutorial(){
}

//ヘッダ・フッタの操作のセット（半分暗くなる）
void MainScene::setEnableHeaderFooter(bool enabled){
    if (enabled){
        if (touchListenerForStop) {
            restartTouchEventListenerUnder(fadeLayer, touchListenerForStop);
            touchListenerForStop = nullptr;
        }
        fadeLayer->setOpacity(0);
    }
    else{
        if (!touchListenerForStop) {
            touchListenerForStop = stopTouchEventListenerUnder(fadeLayer);
        }
        fadeLayer->setOpacity(127);
    }
}

bool MainScene::isEnabledHeaderFooter(){
    return touchListenerForStop == nullptr;
}

//ロンチ時告知ウィンドウ
void MainScene::checkAndShowNoticeWindow(){
    
    std::string noticePath = Native_GetStringPronounce("launch_notice_window", "");
    
    if (noticePath.length() > 0) {
        
        //ノーティス以外のタッチを停止
        noticeBaseNode = createNode(this, zOrderNoticeWindow);
        touchListenerForNoticeWindow = stopTouchEventListenerUnder(noticeBaseNode);
        
        Native_GetStorageDataAndSaveToFile(STORAGE_TYPE_SETTINGS, noticePath, [=](bool succeeded, std::string saveFilePath){
            if (succeeded) {
                playEffect(SE_OK);

                //urlジャンプがあるか
                bool urlJump = false;
                std::string url = Native_GetStringPronounce("launch_notice_url", "");
                if (url.length() > 0) {
                    urlJump = true;
                }

                //ウィンドウ本体
                noticeWindow = createSprite(noticeBaseNode, saveFilePath, zOrderNoticeWindow, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
                auto noticeButton = createOneMenu(noticeBaseNode, saveFilePath, CC_CALLBACK_1(MainScene::noticeWindowCallback, this), zOrderNoticeWindow, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
                auto item = noticeButton->getItemImage(0);
                item->setAutoSelected(false);
                item->setAutoDisabled(false);
                item->setEnabled(urlJump);
                
                //閉じるボタン
                Vec2 closePos = Vec2(noticeWindow->getContentSize().width - 20, noticeWindow->getContentSize().height - 20);
                createOneMenu(item, "notice_close_button.png", CC_CALLBACK_1(MainScene::closeNoticeWindowButtonCallback, this), 1000, Vec2::ANCHOR_MIDDLE, closePos);
            }
            else{
                restartTouchEventListenerUnder(noticeBaseNode, touchListenerForNoticeWindow);
                SAFE_REMOVE_FROM_PARENT(noticeBaseNode);
                procGameStartable();
            }
        });
    }
    else{
        procGameStartable();
    }
}

void MainScene::noticeWindowCallback(Ref* pSender){
    std::string url = Native_GetStringPronounce("launch_notice_url", "");
    if (url.length() > 0) {
        Native_OpenWebPage(url.c_str());

        //閉じる
        closeNoticeWindowButtonCallback(pSender);
    }
}

void MainScene::closeNoticeWindowButtonCallback(Ref* pSender){
    restartTouchEventListenerUnder(noticeBaseNode, touchListenerForNoticeWindow);
    SAFE_REMOVE_FROM_PARENT(noticeBaseNode);
    procGameStartable();

    playEffect(SE_OK);
}

void MainScene::procGameStartable(){
    if (currentLayerType == LAYER_GAME) {
        ((GameLayer*)getCurrentLayer())->setGameStartable(true);
    }
}


#pragma mark - draw

//毎フレーム描画するもの
void MainScene::updateView(){
    
}


#pragma mark - effects


#pragma mark - button



#pragma mark - touch

bool MainScene::onTouchBegan(Touch* touch, Event *event){
    if (getCurrentLayer()){
        return getCurrentLayer()->onTouchBegan(touch, event);
    }
    else{
        return false;
    }
}
void MainScene::onTouchMoved(Touch* touch, Event *event){
    if (getCurrentLayer()){
        getCurrentLayer()->onTouchMoved(touch, event);
    }
}
void MainScene::onTouchEnded(Touch* touch, Event *event){
    if (getCurrentLayer()) {
        getCurrentLayer()->onTouchEnded(touch, event);
    }
}
void MainScene::onTouchCancelled(Touch* touch, Event *event){
    if (getCurrentLayer()) {
        getCurrentLayer()->onTouchCancelled(touch, event);
    }
}


#pragma mark - GameManagerDelegate

//バックグラウンドに入った時のコールバック
void MainScene::onEnterBackground(GameManager* gameManager) {
    //ローカル通知を全削除
    Native_UnscheduleAllLocalNotifications();
    
    //ローカル通知作成
    scheduleLocalNotifications();
}

//フォアグラウンドに入った時のコールバック
void MainScene::onEnterForeground(GameManager* gameManager) {
    Native_AnalyticsAppOpen();
    
    //すぐ閉じてしまうので、一瞬待つ
    /*delayedExecution(this, 0.5f, [=](){
        Native_AdInterstitialAlwaysShow();
    });*/
}

//実績解除された時
void MainScene::onUnlockAchievement(GameManager* gameManager, int index) {
    //getSceneManager()->addToNeedAchivementIndex(index);
}

//実績解除可能になった時
void MainScene::onCanUnlockAchievement(GameManager* gameManager, int index) {
    getSceneManager()->addToNeedAchivementIndex(index);
}
