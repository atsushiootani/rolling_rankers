//
//  GameLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/30.
//
//

#ifndef GameLayer_h
#define GameLayer_h

#include "BaseLayer.h"
#include "CocosHelper.h"
#include "Consts.h"


USING_NS_CC;
USING_NS_CC_EXT;

class StageLayer;

class GameLayer : public BaseLayer
{
    typedef BaseLayer SuperClass;
    
public:
    CREATE_FUNC(GameLayer);
    
public:
    bool initLayer(BaseLayer::Context* context, Rect layerRect) override;
    void finLayer();
    
    //life cycle
    virtual void setVisible(bool isVisible) override;
    virtual void pauseLayer() override;
    virtual void resumeLayer() override;
    
    //update
    virtual void update(float delta) override;
    void updateView(float dt);
    
    //proc
    void setGameStartable(bool f);
    
    //draw
    virtual void drawRefresh() override;
    
    //touch
    virtual bool onTouchBegan(Touch* touch, Event *event) override;
    virtual void onTouchMoved(Touch* touch, Event *event) override;
    virtual void onTouchEnded(Touch* touch, Event *event) override;
    virtual void onTouchCancelled(Touch* touch, Event *event) override;
    
private:
    //nodes
    StageLayer* stageLayer;
};


#endif /* GameLayer_h */
