//
//  GameLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/30.
//
//

#include "GameLayer.h"
#include "MainScene.h"
#include "StageLayer.h"
#include "GameHelper.h"
#include "CocosHelper.h"
#include "Sound.h"
#include "SceneManager.h"
#include "Native.h"
#include "TextData.h"
#include "ResourceData.h"
#include "SoundData.h"
#include "Consts.h"
#include "Screen.h"
#include "Debug.h"

USING_NS_CC;


enum {
    
    zOrderBg,
    
    zOrderStage,
    
    zOrderTitle,
    zOrderText,
    
};

// on "init" you need to initialize your instance
bool GameLayer::initLayer(BaseLayer::Context *context, Rect viewRect) {
    SuperClass::initLayer(context, viewRect);
    
    
    /**
     * 画面構成
     */
    
    //タイトル
    //createSprite(this, "title_logo.png", zOrderTitle, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
    
    //テキスト
    //createSprite(this, "tap_to_start.png", zOrderText, Vec2::ANCHOR_MIDDLE, Vec2(viewRect.size.width / 2, viewRect.size.height * 0.2));
    
    
    //ステージ
    stageLayer = StageLayer::create();
    stageLayer->setup(this);
    this->addChild(stageLayer, zOrderStage);
    
    /**
     * 変数初期化
     */
    
    
    /**
     * 初期化処理
     */
    
    //update
    scheduleUpdate();
    
    
    return true;
}

void GameLayer::finLayer(){
}


#pragma mark - layer life cycle

void GameLayer::setVisible(bool isVisible) {
    SuperClass::setVisible(isVisible);
    
    if (isVisible) {
        
        Native_AdIconShow();
        Native_AdInterstitialShow();
    }
    else {
        Native_AdIconHide();
    }
}

void GameLayer::pauseLayer() {
    SuperClass::pauseLayer();
}

void GameLayer::resumeLayer() {
    SuperClass::resumeLayer();
}


#pragma mark - update

void GameLayer::update(float delta) {
    SuperClass::update(delta);
    
    //描画更新
    updateView(delta);
    
}

void GameLayer::updateView(float dt){
}


#pragma mark - proc

void GameLayer::setGameStartable(bool f){
    stageLayer->setGameStartable(f);
}


#pragma mark - draw

void GameLayer::drawRefresh() {
    SuperClass::drawRefresh();
}


#pragma mark - touch

bool GameLayer::onTouchBegan(Touch* touch, Event *event){
    if (stageLayer){
        return stageLayer->onTouchBegan(touch, event);
    }
    return true;
}
void GameLayer::onTouchMoved(Touch* touch, Event *event){
    if (stageLayer) {
        stageLayer->onTouchMoved(touch, event);
    }
}
void GameLayer::onTouchEnded(Touch* touch, Event *event){
    if (stageLayer) {
        stageLayer->onTouchEnded(touch, event);
    }
}
void GameLayer::onTouchCancelled(Touch* touch, Event *event){
    if (stageLayer) {
        stageLayer->onTouchCancelled(touch, event);
    }
}
