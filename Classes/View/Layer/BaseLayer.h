//
//  BaseLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/29.
//
//

#ifndef BaseLayer_h
#define BaseLayer_h

#include "CocosHelper.h"

class MainScene;

class BaseLayer : public Layer
{
protected:
    typedef MainScene Context;
    
public:
    virtual bool initLayer(Context* context, Rect viewRect);
    
    //context
    virtual void setContext(Context* context);
    virtual Context* getContext() const;
    Node* getTopNode();
    
    //update
    virtual void update(float delta) override;
    float getLifeTime();
    
    //pause/resume
    virtual void pauseLayer();
    virtual void resumeLayer();
    bool getPausingLayer();
    
    //draw
    virtual void drawRefresh();
    
    //touch
    virtual bool onTouchBegan(Touch* touch, Event *event) override;
    virtual void onTouchMoved(Touch* touch, Event *event) override;
    virtual void onTouchEnded(Touch* touch, Event *event) override;
    virtual void onTouchCancelled(Touch* touch, Event *event) override;
    
protected:
    Context *context;
    
    float lifeTime;
    bool isPausingLayer;
};


#endif /* BaseLayer_h */
