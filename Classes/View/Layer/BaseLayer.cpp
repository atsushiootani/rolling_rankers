//
//  BaseLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/29.
//
//

#include "BaseLayer.h"
#include "MainScene.h"

bool BaseLayer::initLayer(Context* context, Rect viewRect){
    setContext(context);
    this->setPosition(viewRect.origin);
    this->setContentSize(viewRect.size);
    
    lifeTime = 0.f;
    isPausingLayer = false;
    
    return true;
}


#pragma mark - context

void BaseLayer::setContext(Context* context){
    this->context = context;
}

BaseLayer::Context* BaseLayer::getContext() const{
    return context;
}

Node* BaseLayer::getTopNode(){
    return this;
}


#pragma mark - update

void BaseLayer::update(float delta){
    lifeTime += delta;
}

float BaseLayer::getLifeTime(){
    return lifeTime;
}


#pragma mark - pause/resume

void BaseLayer::pauseLayer(){
    Layer::pause();
    isPausingLayer = true;
}

void BaseLayer::resumeLayer(){
    Layer::resume();
    isPausingLayer = false;
}

bool BaseLayer::getPausingLayer(){
    return isPausingLayer;
}


#pragma mark - draw

void BaseLayer::drawRefresh(){
    
}


#pragma mark - touch

bool BaseLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void BaseLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void BaseLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void BaseLayer::onTouchCancelled(Touch* touch, Event *event){
    
}
