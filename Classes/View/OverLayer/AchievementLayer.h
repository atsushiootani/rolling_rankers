//
//  AchievementLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/05.
//
//

#ifndef AchievementLayer_h
#define AchievementLayer_h

#include "CocosHelper.h"
#include "ShopLayer.h"


class ResultLayer;
class GachaLayer;
class ShopLayer;

//実績画面
class AchievementLayer : public Node{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(AchievementLayer);
    
private:
    AchievementLayer();
    virtual ~AchievementLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    bool setup();
    void setContext(ResultLayer* context);
    void hide();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);
    
private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
public:
    
    //draw
public:
    void drawRefresh();
    
private:
    //button
    void plateCallback(Ref* pSender);
    void backButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    Menu* menu;
    
    EventListenerTouchOneByOne* touchStopListener;
    ResultLayer* context;
    
private:
    bool isPausing;
};


#endif /* AchievementLayer_h */
