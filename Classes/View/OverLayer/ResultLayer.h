//
//  ResultLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/08.
//
//

#ifndef ResultLayer_h
#define ResultLayer_h

#include "CocosHelper.h"
#include "LoginBonusLayer.h"
#include "StatisticsLayer.h"

class StageLayer;
class SelectLayer;
class AchievementLayer;

class ResultLayer : public Node, public LoginBonusLayerDelegate, public StatisticsLayerDelegate{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(ResultLayer);
    
private:
    ResultLayer();
    virtual ~ResultLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    bool setupWithContext(StageLayer* stageLayer);
    void hide();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);
    
private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
public:
    void checkProcsOnOpen();
    void retryGame();
    bool canContinuePlay();
    void continueGame();
    bool canPlayAdReward();
    void procReward();
    std::string getHighScoreText(double score);
    void showSelectLayer();
    void hideSelectLayer();
    void showAchievementLayer();
    void hideAchievementLayer();
    void showStatisticsLayer();
    virtual void hideStatisticsLayer() override;
    void showLoginBonusLayer();
    virtual void hideLoginBonusLayer() override;
    
    //draw
    void drawRefresh();
    void drawRefreshCoin();
    
    //effect
    void showHighScoreEffects();
    
    //button
private:
    void retryButtonCallback(Ref* pSender);
    void continueButtonCallback(Ref* pSender);
    void rewardButtonCallback(Ref* pSender);
    void toGachaButtonCallback(Ref* pSender);
    void rankingButtonCallback(Ref* pSender);
    void twitterButtonCallback(Ref* pSender);
    void recommendButtonCallback(Ref* pSender);
    void achievementButtonCallback(Ref* pSender);
    void statisticsButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    Label* coinLabel;
    Menu* continueButton;
    Sprite* continueCoin;
    Label* continueCoinLabel;
    Label* adCoinLabel;
    Label* distLabel;
    Label* highLabel;
    
    EventListenerTouchOneByOne* touchStopListener;
    Menu* rewardButton;
    StageLayer* context;
    SelectLayer* selectLayer;
    AchievementLayer* achievementLayer;
    StatisticsLayer* statisticsLayer;
    LoginBonusLayer* loginBonusLayer;
    
private:
    bool isPausing;
    bool canContinue;
};


#endif /* ResultLayer_h */
