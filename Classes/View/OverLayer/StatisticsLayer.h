//
//  StatisticsLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/12/16.
//
//

#ifndef StatisticsLayer_h
#define StatisticsLayer_h

#include "CocosHelper.h"

class StatisticsLayerDelegate;

class StatisticsLayer : public Node{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(StatisticsLayer);
    
private:
    StatisticsLayer();
    virtual ~StatisticsLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    void setContext(StatisticsLayerDelegate* d) { context = d;}
    bool setup();
    void hide();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);
    
private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
    
    //draw
public:
    void drawRefresh();
    void drawRefreshCoin();
    
private:
    //effects
    
    //button
    void backButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    
private:
    Sprite* bgSprite;
    Menu* menu;
    
    EventListenerTouchOneByOne* touchStopListener;
    
private:
    StatisticsLayerDelegate* context;
    bool isPausing;
};

class StatisticsLayerDelegate{
public:
    virtual void hideStatisticsLayer() = 0;
};


#endif /* StatisticsLayer_h */
