//
//  SelectLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/08.
//
//

#include "SelectLayer.h"
#include "GameManager.h"
#include "ResultLayer.h"
#include "GachaLayer.h"
#include "ShopLayer.h"
#include "Stage.h"
#include "ResourceData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "CocosHelper.h"
#include "Screen.h"
#include "Debug.h"


#define CHARA_WIDTH   (240)
#define CHARA_XMARGIN (180)

//アイテムの表示サイズ
#define FOCUS_ITEM_SCALE    (1.2f)
#define NO_FOCUS_ITEM_SCALE (0.6f)


enum {
    zOrderBg,
    
    zOrderButton,
    
    zOrderChara,
    
    zOrderPlate,
    zOrderText,
    
    zOrderCoin,
    
    zOrderOverLayer,
};
enum{
    kTagActionItemScale = 100,
};

SelectLayer::SelectLayer()
: coinLabel(nullptr)
, charaScrollView(nullptr)
, charaNameLabel(nullptr)
, charaDescLabel(nullptr)
, badgeSprite(nullptr)
, menu(nullptr)
, touchStopListener(nullptr)
, context(nullptr)
, gachaLayer(nullptr)
, shopLayer(nullptr)
, isPausing(false)
, lastSeId(-1)
{
}

SelectLayer::~SelectLayer(){
    unsetup();
}

bool SelectLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void SelectLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
}

bool SelectLayer::setup(){
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //背景
    createSprite(this, "bg.png", zOrderBg, Vec2::ZERO, Vec2::ZERO);
    
    //コイン追加ボタン
    auto coinAddButton = createOneMenu(this, "shop_button.png", CC_CALLBACK_1(SelectLayer::shopButtonCallback, this), zOrderButton,
                                       Vec2::ANCHOR_TOP_RIGHT, Vec2(getContentSize().width - 20, getContentSize().height - 20));
    auto coinItem = coinAddButton->getItem(0);
    
    //コイン表示
    auto coin = createSprite(this, "coin_S.png", zOrderCoin,
                             Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(coinItem->getBoundingBox().getMinX() - 20, coinItem->getBoundingBox().getMidY()));
    coin->setScale(0.6f);
    coinLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "---", 36, TextHAlignment::RIGHT, zOrderCoin,
                               Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(coin->getBoundingBox().getMinX() - 10, coin->getBoundingBox().getMidY()));
    coinLabel->setColor(Color3B::YELLOW);
    drawRefreshCoin();
    
    //戻るボタン
    createOneMenu(this, "back_button.png", CC_CALLBACK_1(SelectLayer::backButtonCallback, this), zOrderButton,
                  Vec2::ANCHOR_TOP_LEFT, Vec2(20, getContentSize().height - 20));
    
    //ゲーム開始ボタン
    createOneMenu(this, "play_button.png",     CC_CALLBACK_1(SelectLayer::playButtonCallback,    this), zOrderButton, Vec2::ANCHOR_BOTTOM_RIGHT, Vec2(600 - 20, 30));
    
    //ガチャボタン
    createOneMenu(this, "to_gacha_button.png", CC_CALLBACK_1(SelectLayer::toGachaButtonCallback, this), zOrderButton, Vec2::ANCHOR_BOTTOM_LEFT,  Vec2(600 + 20, 30));
    
    //ツイッターボタン
    createOneMenu(this, "twitter_button.png",  CC_CALLBACK_1(SelectLayer::twitterButtonCallback, this), zOrderButton, Vec2::ANCHOR_BOTTOM_LEFT,  Vec2(30, 30));
    
    //キャラ名
    Vec2 charaNamePos = Vec2(viewRect.size.width / 2, coinItem->getBoundingBox().getMidY());
    createSprite(this, "gacha_name_plate.png", zOrderPlate, Vec2::ANCHOR_MIDDLE, charaNamePos);
    charaNameLabel = createTTFLabel(this, FONT_TEXT_NAME, "", 28, TextHAlignment::CENTER, zOrderText, Vec2::ANCHOR_MIDDLE, charaNamePos);
    
    //キャラ説明台紙
    Vec2 charaDescPos = Vec2(viewRect.size.width / 2, 210);
    createSprite(this, "gacha_text_plate.png", zOrderPlate, Vec2::ANCHOR_MIDDLE, charaDescPos);
    charaDescLabel = createTTFLabel(this, FONT_TEXT_NAME, "", 28, TextHAlignment::CENTER, zOrderText, Vec2::ANCHOR_MIDDLE, charaDescPos);
    
    //キャラ一覧
    int charaIndex = getGameManager()->getCharaIndex();
    Vector<MenuItem*> items;
    for (int i = 0; i < CHARA_COUNT; ++i) {
        std::string charaName = getResourceCharaName(i);
        int x = getScrollOffset(i);
        auto item = createMenuItemImage(charaName, CC_CALLBACK_1(SelectLayer::charaButtonCallback, this),
                                        Vec2(0.5f, 0.3f), Vec2(x + 120, 80));
                                        //Vec2::ANCHOR_BOTTOM_LEFT, Vec2(x, 0));
        item->setTag(i);
        
        //選択中ならバッジ表示
        if (i == charaIndex) {
            badgeSprite = createSprite(item->getNormalImage(), "selecting_badge.png", 1,
                                       Vec2::ANCHOR_TOP_RIGHT, Vec2(item->getContentSize().width - 10, item->getContentSize().height - 10));
            item->setScale(FOCUS_ITEM_SCALE);
        }
        else{
            item->setScale(NO_FOCUS_ITEM_SCALE);
        }
        
        //未解放なら暗く、押下不可に
        if (!getGameManager()->hasCharaUnlocked(i)) {
            item->setColor(Color3B::BLACK);
            item->setEnabled(false);
        }
        
        items.pushBack(item);
    }
    menu = createMenu(nullptr, items, zOrderChara, Vec2::ANCHOR_BOTTOM_LEFT, Vec2::ZERO);
    menu->setContentSize(Size(getScrollOffsetMax(), 240));
    charaScrollView = createScrollView(this, menu, ScrollView::Direction::HORIZONTAL, true, Size(SCREEN_WIDTH, 480),
                                       zOrderChara, Vec2::ANCHOR_BOTTOM_LEFT, Vec2(0, 240));
    charaScrollView->setContentOffset(-Vec2(getScrollOffset(charaIndex - 1), 0));
    
    
    
    //自分で更新
    scheduleUpdate();
    
    return true;
}

void SelectLayer::setContext(ResultLayer* context){
    this->context = context;
}

void SelectLayer::hide(){
    context->hideSelectLayer();
    context->drawRefresh();
    
    stopEffect(lastSeId);
}

void SelectLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;

    coinLabel->setVisible(false);

    stopEffect(lastSeId);
    procSetPauseNodes(this, isPausing);
    procSetPauseNodes(charaScrollView, isPausing);
}

void SelectLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;

    coinLabel->setVisible(true);

    procSetPauseNodes(charaScrollView, isPausing);
    procSetPauseNodes(this, isPausing);
    for (auto it = menu->getChildren().begin(); it != menu->getChildren().end(); ++it) {
        auto item = dynamic_cast<MenuItem*>(*it);
        if (item) {
            int charaIndex = item->getTag();
            item->setEnabled(getGameManager()->hasCharaUnlocked(charaIndex));
        }
    }
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void SelectLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}


#pragma mark - update

void SelectLayer::update(float dt){
    
    updateView(dt);
}

void SelectLayer::updateView(float dt) {
    static int prevFocusIndex = 0;
    
    //中心のキャラの名前を表示
    int scrollOffset = charaScrollView->getContentOffset().x;
    int focusIndex = getIndexFromScrollOffset(scrollOffset);
    
    if (getGameManager()->hasCharaUnlocked(focusIndex)) {
        charaNameLabel->setString(getTextCharaName(focusIndex));
        charaDescLabel->setString(getTextCharaDescription(focusIndex));
    }
    else{
        charaNameLabel->setString(getTextCharaHiddenName(focusIndex));
        charaDescLabel->setString(getTextCharaHiddenDescription(focusIndex));
    }
    
    //フォーカスしたアイテムは大きく見せる
    if (focusIndex != prevFocusIndex) {
        
        if (getGameManager()->hasCharaUnlocked(prevFocusIndex)) {
            menu->getItem(prevFocusIndex)->stopActionByTag(kTagActionItemScale);

            auto action = ScaleTo::create(0.1f, NO_FOCUS_ITEM_SCALE);
            action->setTag(kTagActionItemScale);
            menu->getItem(prevFocusIndex)->runAction(action);
        }
        
        if (getGameManager()->hasCharaUnlocked(focusIndex)) {
            menu->getItem(focusIndex)->stopActionByTag(kTagActionItemScale);

            auto action = ScaleTo::create(0.1f, FOCUS_ITEM_SCALE);
            action->setTag(kTagActionItemScale);
            menu->getItem(focusIndex)->runAction(action);
        }
    }
    
    prevFocusIndex = focusIndex;
}


#pragma mark - proc

int SelectLayer::getScrollOffsetMin(){
    int startX = SCREEN_WIDTH / 2 - CHARA_WIDTH / 2;
    return startX;
}

int SelectLayer::getScrollOffsetMax(){
    return getScrollOffset(CHARA_COUNT - 1) + SCREEN_WIDTH / 2 + CHARA_WIDTH / 2;
}

int SelectLayer::getScrollOffset(int index) {
    int x = getScrollOffsetMin() + (CHARA_XMARGIN + CHARA_WIDTH) * index;
    //DPRINTF("i=(%d) -> %d\n", index, x);
    return x;
}

int SelectLayer::getIndexFromScrollOffset(int offsetX){
    offsetX *= -1;
    int index = (offsetX - getScrollOffsetMin() + (CHARA_WIDTH + CHARA_XMARGIN) * 1.5) / (CHARA_XMARGIN + CHARA_WIDTH);
    //DPRINTF("%d -> i=(%d)\n", offsetX, index);
    return std::max(0, std::min(CHARA_COUNT - 1, index));
}

void SelectLayer::showGachaLayer(){
    if (gachaLayer) {
        return;
    }
    
    gachaLayer = GachaLayer::create();
    gachaLayer->setup();
    gachaLayer->setContext(this);
    this->addChild(gachaLayer, zOrderOverLayer);

    this->pauseLayer();
}
void SelectLayer::hideGachaLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(gachaLayer);
    });
    
    this->resumeLayer();
}

void SelectLayer::showShopLayer(){
    if (shopLayer) {
        return;
    }
    
    shopLayer = ShopLayer::create();
    shopLayer->setup();
    shopLayer->setContext(this);
    this->addChild(shopLayer, zOrderOverLayer);
    
    this->pauseLayer();
}
void SelectLayer::hideShopLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(shopLayer);
    });
    
    this->resumeLayer();
    this->drawRefresh();
}


#pragma mark - draw

void SelectLayer::drawRefresh(){
    drawRefreshCoin();
    drawRefreshChara();
}

void SelectLayer::drawRefreshCoin(){
    coinLabel->setString(getStringFromNumber(getGameManager()->getCoin(), true));
}

void SelectLayer::drawRefreshChara(){
    for (int i = 0; i < CHARA_COUNT; ++i) {
        auto item = menu->getChildByTag<MenuItemImage*>(i);
        
        //バッジ表示
        if (badgeSprite) {
            if (i == getGameManager()->getCharaIndex()) {
                changeParent(badgeSprite, item->getNormalImage(), true);
            }
        }
        
        //未解放なら暗く
        if (!getGameManager()->hasCharaUnlocked(i)) {
            item->setColor(Color3B::BLACK);
        }else{
            item->setColor(Color3B::WHITE);
        }
    }
}

#pragma mark - button

void SelectLayer::shopButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    showShopLayer();
}

void SelectLayer::backButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    hide();
}

void SelectLayer::playButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    hide();
    context->retryGame();
}

void SelectLayer::toGachaButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    showGachaLayer();
}

void SelectLayer::twitterButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    int charaIndex = getGameManager()->getCharaIndex();
    std::string text = string("『") + getTextCharaName(charaIndex) + "』 \n" + getTextCharaDescription(charaIndex) + "  \n" + getTextSNSTail();
    Native_TwitterPost(text, getResourceCharaName(charaIndex));
}

void SelectLayer::charaButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    auto item = dynamic_cast<MenuItemImage*>(pSender);
    int tag = item->getTag();

    if (!getGameManager()->hasCharaUnlocked(tag)) {
        return;
    }
    
    //選択中バッジを変更
    if (tag >= 0 && tag < CHARA_COUNT) {
        getGameManager()->setCharaIndex(item->getTag());
        getGameManager()->saveAll();
        
        //対応した音を鳴らす
        stopEffect(lastSeId);
        std::string seName = "se_chara_" + to_string(tag + 1) + ".mp3";
        if (!FileUtils::getInstance()->isFileExist(FileUtils::getInstance()->fullPathForFilename(seName))) {
            seName =  SE_JUMP;
        }
        lastSeId = playEffect(seName);
    }
    
    //それを中心位置にスクロール
    Vec2 itemPos = item->getPosition();
    charaScrollView->setContentOffset(Vec2(-getScrollOffset(tag-1), charaScrollView->getContentOffset().y), true);
    
    if (badgeSprite) {
        changeParent(badgeSprite, item->getNormalImage(), true);
    }
}


#pragma mark - touch

bool SelectLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void SelectLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void SelectLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void SelectLayer::onTouchCancelled(Touch* touch, Event *event){
    
}
