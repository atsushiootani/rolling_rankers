//
//  ResultLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/08.
//
//

#include "ResultLayer.h"
#include "GameManager.h"
#include "SceneManager.h"
#include "RankingManager.h"
#include "StageLayer.h"
#include "SelectLayer.h"
#include "AchievementLayer.h"
#include "StatisticsLayer.h"
#include "LoginBonusLayer.h"
#include "Stage.h"
#include "ResourceData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "Screen.h"
#include "Debug.h"

#define DEBUG_AD_MOVIE_SKIP   (0 && DEBUG)
#define DEBUG_AD_MOVIE_ALWAYS (0 && DEBUG)

#define GROUND_BASE_HEIGHT (320)
#define GROUND_VIEW_WIDTH  (488)


enum {
    zOrderBg,
    
    zOrderButton,
    
    zOrderChara,
    
    zOrderText,
    
    zOrderCoin,
    zOrderDist,
    
    zOrderEffect,
    
    zOrderOverLayer,
};

enum{
    
    kTagDebugPos = 1001,
    kTagDebugSpeed,
    kTagDebugJumping,
    kTagDebugTime,
    
    kTagTapToStartAction,
};

ResultLayer::ResultLayer()
: coinLabel(nullptr)
, touchStopListener(nullptr)
, rewardButton(nullptr)
, context(nullptr)
, selectLayer(nullptr)
, achievementLayer(nullptr)
, isPausing(false)
{
}

ResultLayer::~ResultLayer(){
    unsetup();
}

bool ResultLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void ResultLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
}

bool ResultLayer::setupWithContext(StageLayer* stageLayer){
    
    context = stageLayer;

    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //暗くする
    createLayerColor(this, Color4B(0,0,0,127), zOrderBg, getContentSize(), Vec2::ZERO);
    
    //コイン表示
    auto coin = createSprite(this, "coin_S.png", zOrderCoin,
                             Vec2::ANCHOR_TOP_RIGHT, Vec2(getContentSize().width - 10, getContentSize().height - 10));
    coin->setScale(0.6f);
    coinLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "---", 36, TextHAlignment::RIGHT, zOrderCoin,
                               Vec2::ANCHOR_BOTTOM_RIGHT, Vec2(coin->getBoundingBox().getMinX() - 10, coin->getBoundingBox().getMinY()));
    coinLabel->setColor(Color3B::YELLOW);
    drawRefreshCoin();
    
    //距離表示
    double dist = context->getDistance();
    distLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, getStringFromNumber(dist, true, 1) + "m", 48, TextHAlignment::CENTER, zOrderDist, Vec2::ANCHOR_MIDDLE, Vec2(viewRect.size.width / 2, viewRect.size.height - 40));
    
    //過去最高距離表示(直前までの記録)
    double highScore = context->getPrevRecord();//getGameManager()->getHighScore();
    highLabel = createTTFLabel(this, FONT_TEXT_NAME, getHighScoreText(highScore), 32, TextHAlignment::CENTER, zOrderDist, Vec2::ANCHOR_MIDDLE, Vec2(viewRect.size.width / 2, viewRect.size.height - 90));
    //ハイスコアが出た場合、この直後の演出で更新する
    
    int retryY = 360;
    int downY = 140;
    int lineY = 410;
    int dy = -140;
    
    //リトライボタン
    createOneMenu(this, "retry_button.png",    CC_CALLBACK_1(ResultLayer::retryButtonCallback,    this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(420, retryY));

    //コンティニューボタン
    continueCoin = nullptr;
    continueCoinLabel = nullptr;
    continueButton = nullptr;
    {
        continueButton = createOneMenu(this, "continue_button.png", CC_CALLBACK_1(ResultLayer::continueButtonCallback, this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(860, retryY - 20));
        {
            auto parent = continueButton->getItemImage(0)->getNormalImage();
            if (parent){
                continueCoin = createSprite(parent, "coin_S.png", 1, Vec2::ANCHOR_MIDDLE, Vec2(80, 65));
                setNodeViewHeightFixedAspect(continueCoin, 80);
                continueCoinLabel = createTTFLabel(parent, FONT_TEXT_NAME, to_string(getGameManager()->getContinueCoin()), 48, TextHAlignment::CENTER, 2, Vec2::ANCHOR_MIDDLE, Vec2(200, 65));
            }
        }

        //そもそもコンティニュー可能かどうか、ここで判断。これ以降の判断はしない。（ランキング登録後、コンティニュー可能になってしまわないようにするため）
        // 必要コイン額はこの判断に入れない。必要額は、後に満たせばコンティニュー可能とするため
        this->canContinue = stageLayer->canContinue();
        
        if (canContinuePlay()){
            continueButton->setEnabled(false);
            //continueButton->getItemImage(0)->getNormalImage()->setColor(Color3B::GRAY);
            continueCoin->setColor(Color3B::GRAY);
            continueCoinLabel->setColor(Color3B::GRAY);
        }
    }
    
    //動画広告ボタン
    rewardButton= nullptr;
    adCoinLabel = nullptr;
    {
        rewardButton = createOneMenu(this, "reward_button.png",   CC_CALLBACK_1(ResultLayer::rewardButtonCallback,   this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(620, downY));
        auto parent = rewardButton->getItemImage(0)->getNormalImage();
        Size itemSize = parent->getContentSize();
        adCoinLabel = createTTFLabel(parent, FONT_TEXT_NAME, to_string(getGameManager()->getRewardCoin()), 36, TextHAlignment::CENTER, 2, Vec2::ANCHOR_MIDDLE, Vec2(itemSize.width / 2, 120));
    }
    if (!canPlayAdReward()) {
        rewardButton->setEnabled(false);
        adCoinLabel->setColor(Color3B::GRAY);
    }
    
    //ランキングボタン
    std::string rankingButtonName = getRankingManager()->canRankingEntryNow() ? Native_GetStringPronounce(PRONOUNCE_RANKING_BUTTON_NAME, "ranking_with_prize_button.png") : "ranking_off_button.png";
    createOneMenu(this, rankingButtonName,  CC_CALLBACK_1(ResultLayer::rankingButtonCallback, this),  zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(910, downY));
    

    //ガチャボタン
    createOneMenu(this, "to_select_button.png", CC_CALLBACK_1(ResultLayer::toGachaButtonCallback,  this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(330, downY));

    //ツイッターボタン
    createOneMenu(this, "twitter_button.png",  CC_CALLBACK_1(ResultLayer::twitterButtonCallback, this),  zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(70, lineY));
    
    //オススメボタン
    createOneMenu(this, "recommend_button.png", CC_CALLBACK_1(ResultLayer::recommendButtonCallback, this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(70, lineY + dy));
    
    //実績ボタン
    createOneMenu(this, "acheivement_button.png", CC_CALLBACK_1(ResultLayer::achievementButtonCallback, this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(70, lineY + dy * 2));

    //統計ボタン
    //createOneMenu(this, "statistics_button.png", CC_CALLBACK_1(ResultLayer::statisticsButtonCallback, this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(70, 540));
    
    //サブレイヤー
    selectLayer = nullptr;
    achievementLayer = nullptr;
    loginBonusLayer = nullptr;
    statisticsLayer = nullptr;
    
    //自分で更新
    //scheduleUpdate();
    
    
    //初期処理
    
    //ハイスコア演出があれば最優先
    bool needNewRecordEffects = stageLayer->getIsNewRecord();
#if 0 && DEBUG
    needNewRecordEffects = true;
#endif
    if (needNewRecordEffects) {
        showHighScoreEffects();
    }
    else{
        checkProcsOnOpen();
    }
    
    
    playBgm(BGM_MENU);
    
    return true;
}

void ResultLayer::hide(){
    delayedExecution(context, 0.1f, [=](){
        this->removeFromParent();
    });
    
    stopBgm();
}

void ResultLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;

    coinLabel->setVisible(false);
    procSetPauseNodes(this, isPausing);
}

void ResultLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;

    coinLabel->setVisible(true);
    procSetPauseNodes(this, isPausing);
    
    //実績
    getSceneManager()->checkAndShowPopupAchievement([=](){drawRefreshCoin();});
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void ResultLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
    
    bool continueEnable = !isPausing && canContinuePlay();
    if (continueButton) {
        continueButton->setEnabled(continueEnable);
        continueButton->getItemImage(0)->setEnabled(continueEnable);
    }
    if (continueCoin)      { continueCoin->setColor(     continueEnable ? Color3B::WHITE : Color3B::GRAY); }
    if (continueCoinLabel) { continueCoinLabel->setColor(continueEnable ? Color3B::WHITE : Color3B::GRAY); }

    bool rewardEnable = !isPausing && canPlayAdReward();
    if (rewardButton) {
        rewardButton->setEnabled(rewardEnable);
        rewardButton->getItemImage(0)->setEnabled(rewardEnable);
    }
    if (adCoinLabel)       { adCoinLabel->setColor(rewardEnable ? Color3B::WHITE : Color3B::GRAY); }
}


#pragma mark - update

void ResultLayer::update(float dt){
    
    updateView(dt);
}

void ResultLayer::updateView(float dt) {
    
}


#pragma mark - proc


//起動時の各種処理チェック
void ResultLayer::checkProcsOnOpen(){
    
    //ランキング表示
    if (getRankingManager()->isRankingPlayingNow()) {
        getRankingManager()->recordScore(context->getDistance() * 10, getStringFromNumber(context->getDistance(), true, 1) + "m",
                                         [=](){ retryGame(); },
                                         [=](){ checkProcsOnOpen(); });
    }
    //ログインボーナス画面を表示
    else if (getGameManager()->canGetLoginBonus(getNowDate())) {
        showLoginBonusLayer();
    }
    //何もなければ、実績表示(ログインボーナスがある場合、ログインボーナスを閉じてから実績表示になる)
    else{
        resumeLayer();
    }
}

void ResultLayer::retryGame(){
    context->restart();
    hide();
}

bool ResultLayer::canContinuePlay(){
    return canContinue;
}

void ResultLayer::continueGame(){
    getGameManager()->subCoin(getGameManager()->getContinueCoin());
    getGameManager()->saveAll();

    hide();
    context->continuePlay(); //hideのあとに呼び出すこと。bgmの処理など
}

bool ResultLayer::canPlayAdReward(){
#if DEBUG_AD_MOVIE_ALWAYS
    return true;
#else
    return (Native_AdCanGetInsentive() &&
            (context->getPlayNum() % getGameManager()->getPlayIntervalCoin() == 1));
#endif
}

void ResultLayer::procReward(){
    getGameManager()->addCoin(getGameManager()->getRewardCoin());
    getGameManager()->saveAll();
    drawRefreshCoin();
}

std::string ResultLayer::getHighScoreText(double score){
    return string("最高記録: ") + getStringFromNumber(score, true, 1) + "m";
}


void ResultLayer::showSelectLayer(){
    if (selectLayer) {
        return;
    }
    
    selectLayer = SelectLayer::create();
    selectLayer->setup();
    selectLayer->setContext(this);
    this->addChild(selectLayer, zOrderOverLayer);
    
    this->pauseLayer();
}

void ResultLayer::hideSelectLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(selectLayer);
    });

    this->resumeLayer();
}

void ResultLayer::showAchievementLayer(){
    if (achievementLayer) {
        return;
    }
    
    achievementLayer = AchievementLayer::create();
    achievementLayer->setup();
    achievementLayer->setContext(this);
    this->addChild(achievementLayer, zOrderOverLayer);
    
    this->pauseLayer();
}

void ResultLayer::hideAchievementLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(achievementLayer);
    });
    
    this->resumeLayer();
}

void ResultLayer::showStatisticsLayer(){
    if (statisticsLayer) {
        return;
    }
    
    statisticsLayer = StatisticsLayer::create();
    statisticsLayer->setup();
    statisticsLayer->setContext(this);
    this->addChild(statisticsLayer, zOrderOverLayer);
    
    this->pauseLayer();
}

void ResultLayer::hideStatisticsLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(statisticsLayer);
    });
    
    this->resumeLayer();
}

void ResultLayer::showLoginBonusLayer(){
    if (loginBonusLayer) {
        return;
    }
    
    procSetPauseNodes(this, true);
    delayedExecution(this, 1.f, [=](){
        loginBonusLayer = LoginBonusLayer::create();
        loginBonusLayer->setup();
        loginBonusLayer->setContext(this);
        this->addChild(loginBonusLayer, zOrderOverLayer);
    });
}

void ResultLayer::hideLoginBonusLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(loginBonusLayer);

        drawRefreshCoin();
        
        procSetPauseNodes(this, false);
        
        //次の起動時処理
        checkProcsOnOpen();
    });
}



#pragma mark - draw

void ResultLayer::drawRefresh(){
    drawRefreshCoin();
}

void ResultLayer::drawRefreshCoin(){
    coinLabel->setString(getStringFromNumber(getGameManager()->getCoin(), true));
}


#pragma mark - effects

void ResultLayer::showHighScoreEffects(){
    
    //操作不能に
    procSetPauseNodes(this, true);
    
    delayedExecution(this, 0.75f, [=](){
        
        //テキスト色変
        Color3B newRecordColor = Color3B::YELLOW;
        distLabel->setColor(newRecordColor);
        highLabel->setColor(newRecordColor);
        highLabel->setString(getHighScoreText(getGameManager()->getHighScore()));
        
        //SE鳴らす
        playEffect(SE_HIGH_SCORE);
        
        //アニメーション再生(ブーストエフェクトと同じ)
        Vec2 pos = Vec2(highLabel->getBoundingBox().getMidX(), highLabel->getBoundingBox().getMidY());
        float delayPerUnit = 0.05f;
        auto effect = createSprite(this, getResourceNoImageName(), zOrderEffect, Vec2::ANCHOR_MIDDLE, pos);
        auto anim = createAnimationWithFormat(delayPerUnit, 1, true, "boost_effect_%d.png");
        effect->runAction(Sequence::create(Animate::create(anim),
                                           CallFunc::create([=](){
            
            procSetPauseNodes(this, false);
            
            //次の処理へ
            checkProcsOnOpen();
        }),
                                           nullptr));
    });
}


#pragma mark - button

void ResultLayer::retryButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    retryGame();
}

void ResultLayer::continueButtonCallback(Ref* pSender){
    if (isPausing || !canContinuePlay()) {
        return;
    }
    
    playEffect(SE_TAP);
    if (getGameManager()->getCoin() >= getGameManager()->getContinueCoin()) {
        continueGame();
    }
    else{
        std::string text = "コインが足りません";
        Native_AlertView("", text, "OK", [=](int){playEffect(SE_OK);});
    }
}

void ResultLayer::rewardButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    if (canPlayAdReward()) {
        std::string text = string("短い動画を見て、コインを") + std::to_string(getGameManager()->getRewardCoin()) + "枚ゲットしますか？";
        Native_AlertView("", text, "いいえ", "はい", [=](int index) {
#if DEBUG_AD_MOVIE_SKIP
            procReward();
#else
            if (index == ALERT_BUTTON_2_YES_INDEX) {
                playEffect(SE_OK);
                
                delayedExecution(this, 0.5f, [=](){
                    Native_AdInsentiveShow([=](int amount){
                        procReward();
                    }, [](){
                        //失敗時
                    }, [=](bool succeed){
                        //閉じた時
                        if (succeed) {
                            //コイン獲得演出
                            rewardButton->setVisible(false);
                            
                            //動画視聴回数カウント
                            getGameManager()->addAdMovieWatchedCount();
                            
                            //通知
                            delayedExecution(this, 0.1f, [=](){
                                playEffect(SE_COIN);
                                std::string oktext = std::to_string(getGameManager()->getRewardCoin()) + "コインをゲットしました！";
                                Native_AlertView("", oktext, "OK", [=](int){
                                    playEffect(SE_OK);
                                    
                                    //新たな実績をアンロックした場合、ここで表示
                                    getSceneManager()->checkAndShowPopupAchievement([=](){drawRefreshCoin();});
                                });
                            });
                        }
                        else{
                            delayedExecution(this, 0.1f, [=](){
                                Native_AlertView("", "エラーが発生しました", "OK", [=](int){playEffect(SE_OK);});
                            });
                        }
                    });
                });
            }
            else{
                playEffect(SE_CANCEL);
            }
#endif
        });
    }
}
void ResultLayer::toGachaButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    showSelectLayer();
}

void ResultLayer::rankingButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    getRankingManager()->showRankingView([=](){
        //開始
        retryGame();
    });
}

void ResultLayer::twitterButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    Native_TwitterPost(getTextShare(), context->getLastScreenShotName());
}
void ResultLayer::recommendButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    const char* url = "http://pittite.sakura.ne.jp";
    Native_OpenWebPage(url);
}

void ResultLayer::achievementButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    showAchievementLayer();
}

void ResultLayer::statisticsButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    showStatisticsLayer();
}


#pragma mark - touch

bool ResultLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void ResultLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void ResultLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void ResultLayer::onTouchCancelled(Touch* touch, Event *event){
    
}
