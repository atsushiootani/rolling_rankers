//
//  SelectLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/08.
//
//

#ifndef SelectLayer_h
#define SelectLayer_h

#include "CocosHelper.h"
#include "ShopLayer.h"


class ResultLayer;
class GachaLayer;
class ShopLayer;

//キャラ選択
class SelectLayer : public Node, public ShopLayerDelegate{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(SelectLayer);
    
private:
    SelectLayer();
    virtual ~SelectLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    bool setup();
    void setContext(ResultLayer* context);
    void hide();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);

private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
    int getScrollOffsetMin();
    int getScrollOffsetMax();
    int getScrollOffset(int index);
    int getIndexFromScrollOffset(int offsetX);
public:
    void showGachaLayer();
    void hideGachaLayer();
    void showShopLayer();
    virtual void hideShopLayer() override;

    //draw
public:
    void drawRefresh();
    void drawRefreshCoin();
    void drawRefreshChara();

private:
    //button
    void shopButtonCallback(Ref* pSender);
    void backButtonCallback(Ref* pSender);
    void playButtonCallback(Ref* pSender);
    void toGachaButtonCallback(Ref* pSender);
    void twitterButtonCallback(Ref* pSender);
    void charaButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    Label* coinLabel;
    ScrollView* charaScrollView;
    Label* charaNameLabel;
    Label* charaDescLabel;
    Sprite* badgeSprite;
    Menu* menu;
    
    EventListenerTouchOneByOne* touchStopListener;
    ResultLayer* context;
    GachaLayer* gachaLayer;
    ShopLayer* shopLayer;
    
private:
    bool isPausing;
    unsigned int lastSeId;
};


#endif /* SelectLayer_h */
