//
//  LoginBonusLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/24.
//
//

#ifndef LoginBonusLayer_h
#define LoginBonusLayer_h

#include "CocosHelper.h"
#include "PurchaseManager.h"

class LoginBonusLayerDelegate;

class LoginBonusLayer : public Node{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(LoginBonusLayer);
    
private:
    LoginBonusLayer();
    virtual ~LoginBonusLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    void setContext(LoginBonusLayerDelegate* d) { context = d;}
    bool setup();
    void hide();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);
    
private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
    bool shouldShowBonusAsGot(int day);

    //draw
public:
    void drawRefresh();
    void drawRefreshCoin();
    
private:
    //effects
    void setCheckEffects(int day);
    
    //button
    void itemCallback(Ref* pSender);
    void backButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    
private:
    Sprite* bgSprite;
    Menu* menu;
    
    EventListenerTouchOneByOne* touchStopListener;
    
private:
    LoginBonusLayerDelegate* context;
    bool isPausing;
};

class LoginBonusLayerDelegate{
public:
    virtual void hideLoginBonusLayer() = 0;
};


#endif /* LoginBonusLayer_h */
