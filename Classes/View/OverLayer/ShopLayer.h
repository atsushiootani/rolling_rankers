//
//  ShopLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/09.
//
//

#ifndef ShopLayer_h
#define ShopLayer_h

#include "CocosHelper.h"
#include "PurchaseManager.h"

class SelectLayer;
class ShopLayerDelegate;

class ShopLayer : public Node, public PurchaseManagerDelegate{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(ShopLayer);
    
private:
    ShopLayer();
    virtual ~ShopLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    bool setup();
    void setContext(ShopLayerDelegate* context);
    void hide();
    Node* createPricePlate(std::string productId, Node* parent, Vec2 pos);
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);
    
private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
    int getPurchaseIndexFromProductId(std::string productId);
    int getPurchaseCoinFromIndex(int index);

    //draw
public:
    void drawRefresh();
    void drawRefreshCoin();
    
private:
    //effects
    
    //button
    void itemCallback(Ref* pSender);
    void backButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    virtual void purchaseHasInitializedCallback(PurchaseManager* purchaseManager, bool success) override;
    virtual void purchaseSuccessCallback(       PurchaseManager* purchaseManager, std::string productId) override;
    virtual void purchaseFailureCallback(       PurchaseManager* purchaseManager, std::string productId) override;
    virtual void purchaseCancelCallback(        PurchaseManager* purchaseManager, std::string productId) override;
    virtual void purchaseClosedCallback(        PurchaseManager* purchaseManager) override;
    virtual void restoreSuccessCallback(        PurchaseManager* purchaseManager, std::string productId) override;
    virtual void restoreFailureCallback(        PurchaseManager* purchaseManager) override;
    virtual void restoreCompletedCallback(      PurchaseManager* purchaseManager) override;
    virtual void restoreClosedCallback(         PurchaseManager* purchaseManager) override;
    

private:
    Label* coinLabel;
    
    EventListenerTouchOneByOne* touchStopListener;
    ShopLayerDelegate* context;
    
private:
    bool isPausing;
};

class ShopLayerDelegate{
public:
    virtual void hideShopLayer() = 0;
};


#endif /* ShopLayer_h */
