//
//  GachaLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/08.
//
//

#include "GachaLayer.h"
#include "GameManager.h"
#include "SceneManager.h"
#include "SelectLayer.h"
#include "ResourceData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "Screen.h"
#include "Debug.h"



enum {
    zOrderBg,
    
    zOrderBallBackOfGacha,
    
    zOrderGachaEjecter,
    zOrderGacha,
    zOrderGachaLever,
    
    zOrderBallFrontOfGacha,
    
    zOrderEffect,
    
    zOrderPlate,
    zOrderText,
    
    zOrderButton,
    
    zOrderChara,
    
    zOrderCoin,
    
    zOrderFadeLayer,
    zOrderOverLayer,
};


GachaLayer::GachaLayer()
: isPausing(false)
, isGiftViewing(false)
, shopLayer(nullptr)
{
}

GachaLayer::~GachaLayer(){
    unsetup();
}

bool GachaLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void GachaLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
}

bool GachaLayer::setup(){
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //背景
    createSprite(this, "gacha_bg_sub.png", zOrderBg, Vec2::ZERO, Vec2::ZERO);

    //白と黒のフェード
    whiteFadeLayer = createLayerColor(this, Color4B::WHITE, zOrderFadeLayer, getContentSize(), Vec2::ANCHOR_BOTTOM_LEFT);
    whiteFadeLayer->setOpacity(0);
    blackFadeLayer = createLayerColor(this, Color4B::BLACK, zOrderFadeLayer, getContentSize(), Vec2::ANCHOR_BOTTOM_LEFT);
    blackFadeLayer->setOpacity(0);
    
    //ガチャ
    gachaSprite = createSprite(this, "gacha_back.png", zOrderGacha, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
    gachaMachineSprite = createSprite(gachaSprite, "gacha.png", zOrderGachaEjecter, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(gachaSprite));
    
    //つまみ
    Vec2 gachaEjecterPos = Vec2(167, 170);
    gachaLeverSprite = createSprite(gachaSprite, "gacha_lever.png", zOrderGachaLever, Vec2::ANCHOR_MIDDLE, gachaEjecterPos);
    
    //獲得演出(放射線)
    gotEffectSprite = createSprite(gachaSprite, getResourceNoImageName(), zOrderEffect, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(gachaSprite));
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("gacha_effects.plist");
    
    //コイン追加ボタン
    coinAddButton = createOneMenu(this, "shop_button.png", CC_CALLBACK_1(GachaLayer::shopButtonCallback, this), zOrderButton,
                                       Vec2::ANCHOR_TOP_RIGHT, Vec2(getContentSize().width - 20, getContentSize().height - 20));
    auto coinItem = coinAddButton->getItem(0);
    
    //コイン表示
    auto coin = createSprite(this, "coin_S.png", zOrderCoin,
                             Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(coinItem->getBoundingBox().getMinX() - 20, coinItem->getBoundingBox().getMidY()));
    coin->setScale(0.6f);
    coinLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "---", 36, TextHAlignment::RIGHT, zOrderCoin,
                               Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(coin->getBoundingBox().getMinX() - 10, coin->getBoundingBox().getMidY()));
    coinLabel->setColor(Color3B::YELLOW);
    drawRefreshCoin();
    
    //キャラ名
    Vec2 charaNamePos = Vec2(getContentSize().width / 2, coinLabel->getBoundingBox().getMidY());
    charaNamePlate = createSprite(this, "gacha_name_plate.png", zOrderPlate, Vec2::ANCHOR_MIDDLE, charaNamePos);
    charaNamePlate->setVisible(false);
    charaNameLabel = createTTFLabel(this, FONT_TEXT_NAME, "", 28, TextHAlignment::CENTER, zOrderText, Vec2::ANCHOR_MIDDLE, charaNamePos);
    charaNameLabel->setVisible(false);

    //キャラ説明
    Vec2 charaDescPos = Vec2(getContentSize().width / 2, 80);
    charaDescPlate = createSprite(this, "gacha_text_plate.png", zOrderPlate, Vec2::ANCHOR_MIDDLE, charaDescPos);
    charaDescPlate->setVisible(false);
    charaDescLabel = createTTFLabel(this, FONT_TEXT_NAME, "", 28, TextHAlignment::CENTER, zOrderText, Vec2::ANCHOR_MIDDLE, charaDescPos);
    charaDescLabel->setVisible(false);

    //戻るボタン
    createOneMenu(this, "back_button.png", CC_CALLBACK_1(GachaLayer::backButtonCallback, this), zOrderButton,
                  Vec2::ANCHOR_TOP_LEFT, Vec2(20, getContentSize().height - 20));
    
    //ガチャボタン
    gachaButton = createOneMenu(this, "gacha_button.png", CC_CALLBACK_1(GachaLayer::gachaButtonCallback,  this), zOrderButton, Vec2::ANCHOR_MIDDLE, Vec2(568, 100));
    auto item = gachaButton->getItemImage(0)->getNormalImage();
    auto priceSprite = createSprite(item, "coin_S.png", 1, Vec2::ANCHOR_MIDDLE, Vec2(140, 40));
    setNodeViewHeightFixedAspect(priceSprite, 40);
    auto priceLabel = createTTFLabel(item, FONT_TEXT_NAME, to_string(getGameManager()->getGachaCoin()), 36, TextHAlignment::RIGHT, 1, Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(260, 40));
    priceLabel->setColor(Color3B::WHITE);

    //自分で更新
    //scheduleUpdate();
    
    return true;
}

void GachaLayer::setContext(SelectLayer* context){
    this->context = context;
}

void GachaLayer::hide(){
    context->hideGachaLayer();
    context->drawRefresh();
}

void GachaLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;
    
    coinLabel->setVisible(false);

    procSetPauseNodes(this, isPausing);
}

void GachaLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;

    coinLabel->setVisible(true);

    procSetPauseNodes(this, isPausing);
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void GachaLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}


#pragma mark - update

void GachaLayer::update(float dt){
    
    updateView(dt);
}

void GachaLayer::updateView(float dt) {
    
}


#pragma mark - proc

void GachaLayer::showShopLayer(){
    if (shopLayer) {
        return;
    }
    
    shopLayer = ShopLayer::create();
    shopLayer->setup();
    shopLayer->setContext(this);
    this->addChild(shopLayer, zOrderOverLayer);
    
    this->pauseLayer();
}
void GachaLayer::hideShopLayer(){
    delayedExecution(this, 0.01f, [=](){
        SAFE_REMOVE_FROM_PARENT(shopLayer);
    });
    
    this->resumeLayer();
    this->drawRefresh();
}


#pragma mark - draw

void GachaLayer::drawRefresh(){
    drawRefreshCoin();
}

void GachaLayer::drawRefreshCoin(){
    coinLabel->setString(getStringFromNumber(getGameManager()->getCoin(), true));
}


#pragma mark - effects

void GachaLayer::playGachaEffects(int index){
    
    this->pauseLayer();
    
    //ボタンを消す
    gachaButton->setVisible(false);
    
    
    //ガチャ本体がガタガタ揺れる
    float duration = 0.07f;
    float angle = 5.f;
    gachaSprite->runAction(Sequence::create(Spawn::create(//ガチャ本体が揺れる
                                                          Sequence::create(Repeat::create(Sequence::create(RotateTo::create(duration, angle),
                                                                                                           RotateTo::create(duration, -angle),
                                                                                                           nullptr), 4),
                                                                           RotateTo::create(duration, 0),
                                                                           DelayTime::create(0.5f),
                                                                           nullptr),
                                                          //つまみが回転
                                                          CallFunc::create([=](){
        gachaLeverSprite->runAction(RotateTo::create(1.f, 180));
        playEffect(SE_GACHA_ROTATE);
    }),
                                                          nullptr),
                                            CallFunc::create([=](){
        
            //ガチャの裏からボールが出てくる
            Vec2 gachaAnchorPos = Vec2::ZERO;//Vec2(gachaSprite->getBoundingBox().getMinX(), gachaSprite->getBoundingBox().getMinY());
            Vec2 ballCreatePos   = Vec2(165, 300) + gachaAnchorPos;
            Vec2 ballEjectPos    = Vec2(165, 100) + gachaAnchorPos;
            Vec2 ballJumpPos1     = Vec2(300, 100) + gachaAnchorPos;
            float ballJumpHeight1 = 150;
            float jumpDuration1   = 0.3f;
            Vec2 ballJumpPos2     = Vec2(450, 100) + gachaAnchorPos;
            float ballJumpHeight2 = 200;
            float jumpDuration2   = 0.4f;
            ccBezierConfig bezier;
            bezier.controlPoint_1 = Vec2(450, 250) + gachaAnchorPos;
            bezier.controlPoint_2 = Vec2(250, 400) + gachaAnchorPos;
            bezier.endPosition    = Vec2(165, 300) + gachaAnchorPos;
            float bezierDuration  = 0.5f;
            float scaleMax = 3.f;
            float openScale = 2.f;
            
            int ballType = getIntRand(0, 4);
            std::string name = getResourceGachaBallName(ballType);
            gachaBallSprite = createSprite(gachaSprite, name, zOrderBallBackOfGacha, Vec2::ANCHOR_MIDDLE, ballCreatePos);
            gachaBallSprite->runAction(Sequence::create(MoveTo::create(0.4f, ballEjectPos),
                                                        JumpTo::create(0.3f, ballEjectPos, 10, 2),
                                                        DelayTime::create(0.2f),
                                                        CallFunc::create([=](){
                //描画が手前に移動
                gachaBallSprite->setLocalZOrder(zOrderBallFrontOfGacha);
            }),
                                                        //跳ね、拡大する
                                                        Spawn::create(ScaleTo::create(jumpDuration1 + jumpDuration2 + bezierDuration, scaleMax),
                                                                      Sequence::create(JumpTo::create(jumpDuration1, ballJumpPos1, ballJumpHeight1, 1),
                                                                                       JumpTo::create(jumpDuration2, ballJumpPos2, ballJumpHeight2, 1),
                                                                                       BezierTo::create(bezierDuration, bezier),
                                                                                       nullptr),
                                                                      nullptr),
                                                        
                                                        //ボールがズバッと大きくなり、白フェード
                                                        DelayTime::create(0.1f),
                                                        CallFunc::create([=](){
                playEffect(SE_GACHA_ZOOM_IN);
            }),
                                                        ScaleBy::create(0.1f, openScale),
                                                        CallFunc::create([=](){
                whiteFadeLayer->runAction(FadeIn::create(0.15f));
            }),
                                                        DelayTime::create(0.7f),
                                                        
                                                        //フェードの裏で、表示物を用意
                                                        CallFunc::create([=](){

                //ボール非表示
                gachaBallSprite->setVisible(false);
                
                //ガチャを半分暗く（キャラを手前に表示するため）
                gachaMachineSprite->setColor(Color3B::GRAY);
                
                //キャラ名
                charaNamePlate->setVisible(true);
                charaNameLabel->setVisible(true);
                charaNameLabel->setString(getTextCharaName(index));

                //キャラ説明
                charaDescPlate->setVisible(true);
                charaDescLabel->setVisible(true);
                charaDescLabel->setString(getTextCharaDescription(index));

                //キャラ表示
                std::string charaName = getResourceCharaName(index);
                charaSprite = createSprite(gachaSprite, charaName, zOrderChara,
                                           Vec2::ANCHOR_MIDDLE, Vec2(gachaBallSprite->getPosition()));
                
                //フェード消す
                whiteFadeLayer->setOpacity(0);
                
                //背景に演出
                auto anim = createAnimationWithFormat(0.05, -1, true, "gacha_effects_%d.png");
                gotEffectSprite->runAction(Animate::create(anim));
                gotEffectSprite->setVisible(true);
                
                //SE
                playEffect(SE_GACHA_GET);
                
                //キャラスケーリング
                charaSprite->setScale(3.f);
                charaSprite->runAction(Sequence::create(ScaleTo::create(0.2f, 2.f),
                                                        DelayTime::create(1.f),
                                                        CallFunc::create([=](){
                    isGiftViewing = true;
                    
                    this->resumeLayer();
                    gachaButton->getItem(0)->setEnabled(false);
                    coinAddButton->getItem(0)->setEnabled(false);

                }), nullptr));
            }), nullptr));
    }), nullptr));
}

//ガチャ演出を終了する
void GachaLayer::offGachaEffects() {

    float actionTime = 0.f;
    
    //フェード
    actionTime += 0.2f;
    blackFadeLayer->runAction(FadeIn::create(0.2f));

    //背後を出す
    delayedExecution(this, actionTime, [=](){
        //元に戻す
        gachaButton->setVisible(true);
        gachaMachineSprite->setColor(Color3B::WHITE);
        gachaLeverSprite->setRotation(0);
        SAFE_REMOVE_FROM_PARENT(gachaBallSprite);
        SAFE_REMOVE_FROM_PARENT(charaSprite);
        charaNamePlate->setVisible(false);
        charaNameLabel->setVisible(false);
        charaDescPlate->setVisible(false);
        charaDescLabel->setVisible(false);
        gotEffectSprite->setVisible(false);
        gotEffectSprite->stopAllActions();
        drawRefresh();
        
    });
    
    //明るく戻す
    actionTime += 0.1f;
    delayedExecution(this, actionTime, [=](){
        blackFadeLayer->runAction(FadeOut::create(0.2f));
    });
    
    //操作系も戻す
    actionTime += 0.2f;
    delayedExecution(this, actionTime, [=](){
        this->resumeLayer();
        
        gachaButton->getItem(0)->setEnabled(true);
        coinAddButton->getItem(0)->setEnabled(true);
        
        //実績のチェック
        getSceneManager()->checkAndShowPopupAchievement([=](){drawRefreshCoin();});
    });
}


#pragma mark - button

void GachaLayer::shopButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    showShopLayer();
}

void GachaLayer::gachaButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);

    //アニメーション
    if (getGameManager()->getLockedCharaCount() == 0) {
        Native_AlertView("", "全ての「もこう」をコンプ済みです！\n (⌒,_ゝ⌒)", "OK", [=](int){playEffect(SE_OK);});
    }
    else if (!getGameManager()->canPlayGacha()) {
        std::string text = string("コインが足りません！\nステージで集めるか、購入することができます");
        Native_AlertView("", text, "OK", [=](int){playEffect(SE_OK);});
    }
    else{
        int newIndex = getGameManager()->playGacha();
        getGameManager()->saveAll();
        
        playGachaEffects(newIndex);
        
        drawRefresh();
    }
}

void GachaLayer::backButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    if (isGiftViewing) {
        offGachaEffects();
        isGiftViewing = false;
    }
    else{
        hide();
    }
}


#pragma mark - touch

bool GachaLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void GachaLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void GachaLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void GachaLayer::onTouchCancelled(Touch* touch, Event *event){
    
}
