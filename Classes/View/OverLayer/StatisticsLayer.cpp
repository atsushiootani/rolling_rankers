//
//  StatisticsLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/12/16.
//
//

#include "StatisticsLayer.h"
#include "GameManager.h"
#include "SceneManager.h"
#include "SelectLayer.h"
#include "ResourceData.h"
#include "GameData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "Screen.h"
#include "Debug.h"



enum {
    zOrderFade,
    
    zOrderBg,
    zOrderText,
    
    zOrderButton,
};


StatisticsLayer::StatisticsLayer()
: isPausing(false)
{
}

StatisticsLayer::~StatisticsLayer(){
    unsetup();
}

bool StatisticsLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void StatisticsLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
    
    getPurchaseManager()->setDelegate(nullptr);
}

bool StatisticsLayer::setup(){
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //背景
    createLayerColor(this, Color4B(0,0,0,127), zOrderFade, getContentSize(), Vec2::ANCHOR_BOTTOM_LEFT);
    
    //背景
    bgSprite = createSprite(this, "statistics_window.png", zOrderBg, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
    Size bgSize = bgSprite->getContentSize();
    
    //本文
    int lx = 60;
    int rx = bgSize.width - lx;
    int y = 400;
    int my = -90;
    int fontSize = 48;

    std::string title = string("プレイ回数：");
    std::string value = to_string(getGameManager()->getPlayNum()) + "回";
    createTTFLabel(bgSprite, FONT_TEXT_NAME, title, fontSize, TextHAlignment::LEFT,  zOrderText, Vec2::ANCHOR_MIDDLE_LEFT,  Vec2(lx, y));
    createTTFLabel(bgSprite, FONT_TEXT_NAME, value, fontSize, TextHAlignment::RIGHT, zOrderText, Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(rx, y));
    y += my;
    
    title = string("ハイスコア：");
    value = getStringFromDouble(getGameManager()->getHighScore(), 1) + "m";
    createTTFLabel(bgSprite, FONT_TEXT_NAME, title, fontSize, TextHAlignment::LEFT,  zOrderText, Vec2::ANCHOR_MIDDLE_LEFT,  Vec2(lx, y));
    createTTFLabel(bgSprite, FONT_TEXT_NAME, value, fontSize, TextHAlignment::RIGHT, zOrderText, Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(rx, y));
    y += my;

    title = string("コンプ率：");
    value = getStringFromDouble((100.0 * getGameManager()->getUnlockedCharaCount() / CHARA_COUNT), 1) + "％";
    createTTFLabel(bgSprite, FONT_TEXT_NAME, title, fontSize, TextHAlignment::LEFT,  zOrderText, Vec2::ANCHOR_MIDDLE_LEFT,  Vec2(lx, y));
    createTTFLabel(bgSprite, FONT_TEXT_NAME, value, fontSize, TextHAlignment::RIGHT, zOrderText, Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(rx, y));
    y += my;

    title = string("総コイン取得数：");
    value = getStringFromNumber(getGameManager()->getTotalCoin(), true) + "枚";
    createTTFLabel(bgSprite, FONT_TEXT_NAME, title, fontSize, TextHAlignment::LEFT,  zOrderText, Vec2::ANCHOR_MIDDLE_LEFT,  Vec2(lx, y));
    createTTFLabel(bgSprite, FONT_TEXT_NAME, value, fontSize, TextHAlignment::RIGHT, zOrderText, Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(rx, y));
    y += my;

    //戻るボタン
    createOneMenu(this, "back_button.png", CC_CALLBACK_1(StatisticsLayer::backButtonCallback, this), zOrderButton,
                  Vec2::ANCHOR_TOP_LEFT, Vec2(20, getContentSize().height - 20));
    
    
    //自分で更新
    //scheduleUpdate();
    
    return true;
}

void StatisticsLayer::hide(){
    context->hideStatisticsLayer();
}

void StatisticsLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;
    
    procSetPauseNodes(this, isPausing);
}

void StatisticsLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;
    
    procSetPauseNodes(this, isPausing);
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void StatisticsLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}


#pragma mark - update

void StatisticsLayer::update(float dt){
    
    updateView(dt);
}

void StatisticsLayer::updateView(float dt) {
    
}


#pragma mark - proc


#pragma mark - draw

void StatisticsLayer::drawRefresh(){
}


#pragma mark - effects



#pragma mark - button

void StatisticsLayer::backButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_OK);
    hide();
}


#pragma mark - touch

bool StatisticsLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void StatisticsLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void StatisticsLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void StatisticsLayer::onTouchCancelled(Touch* touch, Event *event){
    
}

