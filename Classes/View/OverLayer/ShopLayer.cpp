//
//  ShopLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/09.
//
//

#include "ShopLayer.h"
#include "GameManager.h"
#include "PurchaseManager.h"
#include "SelectLayer.h"
#include "ResourceData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "Screen.h"
#include "Debug.h"



enum {
    zOrderFade,
    
    zOrderBg,
    
    zOrderBallBackOfGacha,
    
    zOrderGachaEjecter,
    zOrderGacha,
    zOrderGachaLever,
    
    zOrderBallFrontOfGacha,
    
    zOrderButton,
    
    zOrderChara,
    
    zOrderCoin,
};


ShopLayer::ShopLayer()
: isPausing(false)
{
}

ShopLayer::~ShopLayer(){
    unsetup();
}

bool ShopLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void ShopLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
    
    getPurchaseManager()->setDelegate(nullptr);
}

bool ShopLayer::setup(){
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //背景
    createLayerColor(this, Color4B(0,0,0,127), zOrderFade, getContentSize(), Vec2::ANCHOR_BOTTOM_LEFT);
    
    //コイン追加の絵
    auto coinAddButton = createSprite(this, "shop_button.png", zOrderButton,
                                      Vec2::ANCHOR_TOP_RIGHT, Vec2(getContentSize().width - 20, getContentSize().height - 20));
    coinAddButton->setColor(Color3B::GRAY);
    auto coinItem = coinAddButton;
    
    //コイン表示
    auto coin = createSprite(this, "coin_S.png", zOrderCoin,
                             Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(coinItem->getBoundingBox().getMinX() - 20, coinItem->getBoundingBox().getMidY()));
    coin->setScale(0.6f);
    coinLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "---", 36, TextHAlignment::RIGHT, zOrderCoin,
                               Vec2::ANCHOR_MIDDLE_RIGHT, Vec2(coin->getBoundingBox().getMinX() - 10, coin->getBoundingBox().getMidY()));
    coinLabel->setColor(Color3B::YELLOW);
    drawRefreshCoin();
    
    //戻るボタン
    createOneMenu(this, "back_button.png", CC_CALLBACK_1(ShopLayer::backButtonCallback, this), zOrderButton,
                  Vec2::ANCHOR_TOP_LEFT, Vec2(20, getContentSize().height - 20));
    
    //プレート
    createPricePlate(PRODUCT_ID_1, this, Vec2(viewRect.size.width * 0.3, viewRect.size.height * 0.66));
    createPricePlate(PRODUCT_ID_2, this, Vec2(viewRect.size.width * 0.7, viewRect.size.height * 0.66));
    createPricePlate(PRODUCT_ID_3, this, Vec2(viewRect.size.width * 0.3, viewRect.size.height * 0.34));
    createPricePlate(PRODUCT_ID_4, this, Vec2(viewRect.size.width * 0.7, viewRect.size.height * 0.34));
    
    //自分で更新
    //scheduleUpdate();
    
    //ロジック系
    getPurchaseManager()->setDelegate(this);
    
    return true;
}

Node* ShopLayer::createPricePlate(std::string productId, Node* parent, Vec2 pos){
    //台紙
    auto plateButton = createOneMenu(parent, "shop_plate.png", CC_CALLBACK_1(ShopLayer::itemCallback, this), 1, Vec2::ANCHOR_MIDDLE, pos);
    plateButton->getItem(0)->setName(productId);
    
    auto baseNode = plateButton->getItemImage(0)->getNormalImage();
    
    //コイン
    auto coinSprite = createSprite(baseNode, getResourceShopIconName(productId), 1, Vec2::ANCHOR_MIDDLE, Vec2(baseNode->getContentSize().height / 2, baseNode->getContentSize().height / 2));
    
    //獲得コイン
    int coin = getPurchaseCoinFromIndex(getPurchaseIndexFromProductId(productId));
    std::string text = string("x") + getStringFromNumber(coin, true);
    auto coinLabel = createTTFLabel(baseNode, FONT_TEXT_NAME, text, 40, TextHAlignment::LEFT, 1, Vec2::ANCHOR_MIDDLE_LEFT, Vec2(coinSprite->getContentSize().width + 30, 120));
    coinLabel->setColor(Color3B::YELLOW);
    
    //値段
    std::string priceString = Native_GetProductPrice(productId);
    createTTFLabel(baseNode, FONT_TEXT_NAME, priceString, 32, TextHAlignment::RIGHT, 2, Vec2::ANCHOR_BOTTOM_RIGHT, Vec2(baseNode->getContentSize().width - 20, 20));
    
    return plateButton;
}

void ShopLayer::setContext(ShopLayerDelegate* context){
    this->context = context;
}

void ShopLayer::hide(){
    context->hideShopLayer();
}

void ShopLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;
    
    procSetPauseNodes(this, isPausing);
}

void ShopLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;
    
    procSetPauseNodes(this, isPausing);
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void ShopLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}


#pragma mark - update

void ShopLayer::update(float dt){
    
    updateView(dt);
}

void ShopLayer::updateView(float dt) {
    
}


#pragma mark - proc

int ShopLayer::getPurchaseIndexFromProductId(std::string productId) {
    if      (productId == PRODUCT_ID_1) { return 0; }
    else if (productId == PRODUCT_ID_2) { return 1; }
    else if (productId == PRODUCT_ID_3) { return 2; }
    else if (productId == PRODUCT_ID_4) { return 3; }
    else { return -1; }
}

int ShopLayer::getPurchaseCoinFromIndex(int index) {
    if      (index == 0) { return ADD_COIN_OF_PURCHASE_1; }
    else if (index == 1) { return ADD_COIN_OF_PURCHASE_2; }
    else if (index == 2) { return ADD_COIN_OF_PURCHASE_3; }
    else if (index == 3) { return ADD_COIN_OF_PURCHASE_4; }
    else return 0;
}

#pragma mark - draw

void ShopLayer::drawRefresh(){
    drawRefreshCoin();
}

void ShopLayer::drawRefreshCoin(){
    coinLabel->setString(getStringFromNumber(getGameManager()->getCoin(), true));
}


#pragma mark - effects


#pragma mark - button

void ShopLayer::itemCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_OK);
    
    std::string productId = dynamic_cast<Node*>(pSender)->getName();
    this->pauseLayer();
    
    //購入処理
    getPurchaseManager()->purchaseItem(productId);
}

void ShopLayer::backButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_OK);
    hide();
}


#pragma mark - touch

bool ShopLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void ShopLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void ShopLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void ShopLayer::onTouchCancelled(Touch* touch, Event *event){
    
}


#pragma mark - PurchaseManager

void ShopLayer::purchaseHasInitializedCallback(PurchaseManager* purchaseManager, bool success) {
    //不要。起動直後にこの画面にくることはない
}
void ShopLayer::purchaseSuccessCallback(       PurchaseManager* purchaseManager, std::string productId) {
    int coin = getPurchaseCoinFromIndex(getPurchaseIndexFromProductId(productId));
    int after = getGameManager()->getCoin();
    std::string text = getStringFromNumber(coin, true) + "コインが追加されました！\n所持コイン: " + getStringFromNumber(after, true) + "コイン";
    Native_AlertView("", text, "OK", [=](int){playEffect(SE_OK);});
    playEffect(SE_COIN);
}
void ShopLayer::purchaseFailureCallback(       PurchaseManager* purchaseManager, std::string productId) {
    Native_AlertView("", "購入にエラーが発生しました。\n通信環境の良いところでやり直してください", "OK", [=](int){playEffect(SE_OK);});
    drawRefresh();
    this->resumeLayer();
}
void ShopLayer::purchaseCancelCallback(        PurchaseManager* purchaseManager, std::string productId) {
    this->resumeLayer();
}
void ShopLayer::purchaseClosedCallback(        PurchaseManager* purchaseManager) {
    drawRefresh();
    this->resumeLayer();
}
void ShopLayer::restoreSuccessCallback(        PurchaseManager* purchaseManager, std::string productId) {
}
void ShopLayer::restoreFailureCallback(        PurchaseManager* purchaseManager) {
}
void ShopLayer::restoreCompletedCallback(      PurchaseManager* purchaseManager) {
}
void ShopLayer::restoreClosedCallback(         PurchaseManager* purchaseManager) {
    this->resumeLayer();
}
