//
//  AchievementLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/05.
//
//

#include "AchievementLayer.h"
#include "GameManager.h"
#include "ResultLayer.h"
#include "GachaLayer.h"
#include "ShopLayer.h"
#include "Stage.h"
#include "GameData.h"
#include "ResourceData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "CocosHelper.h"
#include "Screen.h"
#include "Debug.h"


#define CHARA_WIDTH   (240)
#define CHARA_XMARGIN (180)

//アイテムの表示サイズ
#define FOCUS_ITEM_SCALE    (1.2f)
#define NO_FOCUS_ITEM_SCALE (0.6f)


enum {
    zOrderBg,
    zOrderFade,
    
    zOrderButton,
    
    zOrderScrollView,
    
    
    zOrderOverLayer,
};

AchievementLayer::AchievementLayer()
: menu(nullptr)
, touchStopListener(nullptr)
, context(nullptr)
, isPausing(false)
{
}

AchievementLayer::~AchievementLayer(){
    unsetup();
}

bool AchievementLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void AchievementLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
}

bool AchievementLayer::setup(){
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //背景
    //createLayerColor(this, Color4B(0,0,0,127), zOrderFade, getContentSize(), Vec2::ANCHOR_BOTTOM_LEFT);
    createSprite(this, "bg.png", zOrderBg, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
    

    //プレートの整列
    Node* scrollContentNode = Node::create();
    Vector<MenuItem*> items;
    for (int i = 0; i < ACHIEVEMENT_COUNT; ++i) {
        auto item = createMenuItemImage("achievement_plate.png", "achievement_plate.png", "achievement_plate.png", CC_CALLBACK_1(AchievementLayer::plateCallback, this), Vec2::ZERO, Vec2::ZERO);
        item->setEnabled(false); //押すことはできない
        
        auto parent = item->getDisabledImage(); //押せない扱いにしているため
        Size itemSize = parent->getContentSize();
        
        std::string iconName = getResrouceAchievementIconName(getGameManager()->isAchievementUnlocked(i), s_achievementData[i].type);
        auto icon = createSprite(parent, iconName, 1, Vec2::ANCHOR_MIDDLE, Vec2(itemSize.width - itemSize.height / 2, itemSize.height / 2));
        
        float width = itemSize.width - icon->getContentSize().width - 20;
        auto title = createTTFLabel(parent, FONT_TEXT_NAME, "", 48, TextHAlignment::CENTER, 2, Vec2::ANCHOR_MIDDLE, Vec2(365, 120));
        setLabelWidth(title, getTextAchievementTitle(i), width);
        auto desc = createTTFLabel(parent, FONT_TEXT_NAME, "", 32, TextHAlignment::CENTER, 2, Vec2::ANCHOR_MIDDLE, Vec2(365,60));
        setLabelWidth(desc, getTextAchievementDesc(i), width);
        
        item->setTag(i);
        items.pushBack(item);
    }
    
    menu = createMenuWithArrayZ(scrollContentNode, items, 1, viewRect.size.width * 0.9, 1, Vec2::ZERO, Vec2::ZERO);
    scrollContentNode->setContentSize(menu->getContentSize());
    
    createScrollView(this, scrollContentNode, ScrollView::Direction::VERTICAL, true, getContentSize(), zOrderScrollView, Vec2::ZERO, Vec2::ZERO);

    //戻るボタン
    createOneMenu(this, "back_button.png", CC_CALLBACK_1(AchievementLayer::backButtonCallback, this), zOrderButton,
                  Vec2::ANCHOR_TOP_LEFT, Vec2(20, getContentSize().height - 20));
    
    
    //自分で更新
    scheduleUpdate();
    
    return true;
}

void AchievementLayer::setContext(ResultLayer* context){
    this->context = context;
}

void AchievementLayer::hide(){
    context->hideAchievementLayer();
    context->drawRefresh();
    //delayedExecution(context, 0.1f, [=](){
    //    removeFromParent();
    //});
}

void AchievementLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;
    
    procSetPauseNodes(this, isPausing);
}

void AchievementLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;
    
    procSetPauseNodes(this, isPausing);
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void AchievementLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}


#pragma mark - update

void AchievementLayer::update(float dt){
    
    updateView(dt);
}

void AchievementLayer::updateView(float dt) {
}


#pragma mark - proc


#pragma mark - draw

void AchievementLayer::drawRefresh(){
}


#pragma mark - button

void AchievementLayer::plateCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
}

void AchievementLayer::backButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_TAP);
    hide();
}


#pragma mark - touch

bool AchievementLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void AchievementLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void AchievementLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void AchievementLayer::onTouchCancelled(Touch* touch, Event *event){
    
}
