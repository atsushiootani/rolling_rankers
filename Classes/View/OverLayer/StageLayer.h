//
//  StageLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/30.
//
//

#ifndef StageLayer_h
#define StageLayer_h

#include "CocosHelper.h"
#include "Stage.h"

class PuzzleLogic;
class Stage;
class StageDelegate;
class GameLayer;
class ResultLayer;

class StageLayer : public Node, public StageDelegate{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(StageLayer);
    
private:
    StageLayer();
    virtual ~StageLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    bool setup(GameLayer* context);
    void restart();
    void newPlay();
    void continuePlay();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);
    Stage* getStage()    { return stage; }
    int getPlayNum()     { return playNum; }
    int getContinueNum() { return continueNum; }
    bool canContinue();
    int getGotCoin()     { return gotCoin; }
    std::string getTopUserId() { return topUserId; }
    double getDistance()  { return distance; }
    bool getIsNewRecord() { return isNewRecord; }
    double getPrevRecord(){ return prevRecord; }
    
private:
    void update(float dt) override;
    void updateSpeed(float dt);
    void updateStage(float dt);
    void updateView(float dt);
    void updateViewCoin();
    void updateViewBoost();
    void updateViewDebug(float dt);

    //proc
    float getViewScale();
    Vec2  getViewOffset();
    Vec2  getViewPosFromLogicPos(Vec2 logicPos);
public:
    void setGameStartable(bool f);
    std::string getLastScreenShotName() { return lastScreenShotName; }
private:

    //draw
    void showTitleViews();
    void hideTitleViews();
    void showTapToStartView();
    void hideTapToStartView();
    void showInGameViews();
    void hideInGameViews();
    void showResult();
    void hideResult();
    
    //effect
    void showRockCrashEffect(Vec2 pos);
    void showJumpEffect(Vec2 pos);
    void showDropEffect(Vec2 pos);
    void showCoinEffect(Vec2 pos);
    void showCoinGetEffect(Vec2 pos);
    void showDieEffect();
    void showSpeedUpEffect();
    void showShakeEffect();
    void showBoostEffect();
    void hideBoostEffect();

    //button
    void restartButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
    //StageDelegate
    virtual void onJump(Stage*, Vec2 pos) override;
    virtual void onDrop(Stage*, Vec2 pos) override;
    virtual void onGotCoin(Stage*, Vec2 pos) override;
    virtual void onBoost(Stage*) override;
    virtual void onBoostEnded(Stage*) override;
    virtual void onRockCrashed(Stage*, Vec2 pos) override;
    virtual void onGameOver(Stage*) override;


private:
    Sprite* titleSprite;
    Sprite* tapToStartSprite;
    
    Sprite* bgSprite;
    Sprite* bgSprite2;
    Node* rotateNode;
    Node* scrollerNode;
    Node* groundTopNode;
    Node* coinTopNode;
    Node* rockTopNode;
    Node* debugTopNode;
    Sprite* charaSprite;
    Label* distLabel;
    Label* speedUpLabel;
    Sprite* gotCoinSprite;
    Label* gotCoinLabel;
    Sprite* boostBarSprite;
    Label* rankingPlayLabel;
    
    ResultLayer* resultLayer;
    
    EventListenerTouchOneByOne* touchStopListener;
    
private:
    GameLayer* context;
    Stage *stage;
    bool isGameStartable;
    bool isPausing;
    int playNum;
    int continueNum;
    int gotCoin;
    int gotCoinForBoost;
    float playTime;
    float playSpeed;
    double distance;
    bool isNewRecord;
    double prevRecord;
    std::string topUserId;
    std::string lastScreenShotName;
    
    std::string bgmName;
    std::string seJumpName;
    unsigned int lastJumpSeId;
    
    Vec2 touchBeganLocation;
};


#endif /* StageLayer_h */
