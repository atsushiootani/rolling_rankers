//
//  StageLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/09/30.
//
//

#include "StageLayer.h"
#include "GameManager.h"
#include "SceneManager.h"
#include "RankingManager.h"
#include "MainScene.h"
#include "Stage.h"
#include "GameLayer.h"
#include "ResultLayer.h"
#include "ResourceData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "SaveJsonUtils.h"
#include "GameHelper.h"
#include "Screen.h"
#include "Debug.h"


#define ENABLE_DEBUG_VIEW (1 && DEBUG)

#define GROUND_BASE_HEIGHT (320)
#define GROUND_VIEW_WIDTH  (488)

#define GROUND_SPRITE_NUM (20)
#define COIN_SPRITE_NUM (30)
#define ROCK_SPRITE_NUM (20)

#define DEFAULT_SPEED_UP_DURATION (3.f)
#define DEFAULT_SPEED_UP_ADD      (0.f)
#define DEFAULT_SPEED_UP_RATE     (1.1f)

enum {

    zOrderBg,
    
    zOrderBoostEffect,
    
    zOrderRotate,
    zOrderField,
    
    zOrderCoin,
    zOrderRock,
    zOrderGround,
    zOrderChara,
    zOrderEffect,
    
    zOrderDist,
    zOrderSpeedUp,
    zOrderGotCoin,
    zOrderBoost,
    zOrderText,
    
    zOrderTitle,
    
    zOrderButton,
    
    zOrderDebug,
    
    zOrderResult,
};

enum{
    
    kTagDebugPos = 1001,
    kTagDebugParam,
    kTagDebugSpeed,
    kTagDebugGrav,
    kTagDebugTime,
    kTagDebugJumping,
    kTagDebugStage,
    
    kTagTapToStartAction,
    kTagCharaBoostEffect,
};

StageLayer::StageLayer()
: bgSprite(nullptr)
, bgSprite2(nullptr)
, rotateNode(nullptr)
, scrollerNode(nullptr)
, groundTopNode(nullptr)
, coinTopNode(nullptr)
, rockTopNode(nullptr)
, debugTopNode(nullptr)
, charaSprite(nullptr)
, distLabel(nullptr)
, speedUpLabel(nullptr)
, gotCoinSprite(nullptr)
, gotCoinLabel(nullptr)
, boostBarSprite(nullptr)
, rankingPlayLabel(nullptr)
, resultLayer(nullptr)
, touchStopListener(nullptr)
, stage(nullptr)
, isPausing(false)
, playNum(0)
, continueNum(0)
, gotCoin(0)
, gotCoinForBoost(0)
, playTime(0)
, playSpeed(0)
, isNewRecord(false)
, topUserId()
, touchBeganLocation()
{
}

StageLayer::~StageLayer(){
    unsetup();
}

bool StageLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void StageLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
}

bool StageLayer::setup(GameLayer* context){
    
    this->context = context;
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    //ロジック
    stage = new Stage();
    stage->setup();
    stage->setDelegate(this);

    //
    isGameStartable = false;
    
    
    //各種エフェクト
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("fall_effects.plist");
    cache->addSpriteFramesWithFile("jump_effects.plist");
    cache->addSpriteFramesWithFile("coin_effects.plist");
    cache->addSpriteFramesWithFile("get_effects.plist");
    cache->addSpriteFramesWithFile("boost_effects.plist");

    //スタート前画面
    titleSprite = nullptr;
    tapToStartSprite = nullptr;
    showTitleViews();
    
    //背景
    bgSprite  = createSprite(this, getResourceStageBgName(0), zOrderBg, Vec2::ANCHOR_BOTTOM_LEFT, Vec2::ZERO);
    bgSprite2 = createSprite(this, getResourceStageBgName(0), zOrderBg, Vec2::ANCHOR_BOTTOM_LEFT, Vec2::ZERO);
    
    //回転を持たせるノード
    rotateNode = createNode(this, zOrderRotate);
    rotateNode->setRotation(15);
    
    //フィールドのトップ
    scrollerNode = createNode(rotateNode, zOrderField);
    
    //キャラ
    int charaIndex = getGameManager()->getCharaIndex();
    std::string charaName = getResourceCharaName(charaIndex);
    Vec2 charaAnchorPoint = getResourceCharaAnchorPoint(charaIndex);
    float charaScale      = getResourceCharaScale(charaIndex);
    charaSprite = createSprite(scrollerNode, charaName, zOrderChara, charaAnchorPoint, Vec2(200, GROUND_BASE_HEIGHT));
    setNodeViewWidthFixedAspect(charaSprite, stage->getBallRadius() * 2 * getViewScale() * charaScale);

    //地形
    groundTopNode = createNode(scrollerNode, zOrderGround, "groundTopNode");
    for (int i = 0; i < GROUND_SPRITE_NUM; ++i) { //何個先まで地形を作っておくか
        auto spr = createSprite(groundTopNode, getResourceStageGroundName(0), zOrderGround, Vec2::ANCHOR_TOP_LEFT, Vec2::ZERO);
        spr->setTag(i);
        setNodeViewWidth(spr, stage->getGroundBaseWidth() * getViewScale());
    }
    
    //コイン
    coinTopNode = createNode(scrollerNode, zOrderCoin, "coinTopNode");
    for (int i = 0; i < COIN_SPRITE_NUM; ++i) {
        auto spr = createSprite(coinTopNode, "coin_S.png", zOrderCoin, Vec2::ANCHOR_MIDDLE, Vec2::ZERO);
        spr->setTag(i);
        spr->setVisible(false);
        setNodeViewWidthFixedAspect(spr, stage->getCoinRadius() * 2 * getViewScale());
        
        //キラキラエフェクトを常時つける
        auto effect = createSprite(spr, getResourceNoImageName(), zOrderEffect, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(spr));
        effect->setScale(3.f);
        auto anim   = createAnimationWithFormat(0.1f, 1, true, "coin_effects_%02d.png");
        effect->runAction(RepeatForever::create(Animate::create(anim)));
    }
    
    //落下物
    rockTopNode = createNode(scrollerNode, zOrderRock, "rockTopNode");
    for (int i = 0; i < ROCK_SPRITE_NUM; ++i) {
        auto spr = createSprite(rockTopNode, "stage_fall_1.png", zOrderRock, Vec2::ANCHOR_MIDDLE, Vec2::ZERO);
        spr->setTag(i);
        spr->setVisible(false);
        setNodeViewWidthFixedAspect(spr, stage->getRockRadius() * 2 * getViewScale() * 1.5); //画像に隙間が多い為大きめにしておく
    }
    
    
    /**
     * 以下、HUD
     */
    
    //走行距離表示(ResultLayerも同じことを表示)
    distLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "0m", 48, TextHAlignment::CENTER, zOrderDist, Vec2::ANCHOR_MIDDLE, Vec2(viewRect.size.width / 2, viewRect.size.height - 40));
    
    //スピードアップ表示
    speedUpLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "SPEED UP !!", 36, TextHAlignment::CENTER, zOrderSpeedUp, Vec2::ANCHOR_MIDDLE, Vec2(viewRect.size.width / 2, viewRect.size.height - 80));
    speedUpLabel->setColor(Color3B::YELLOW);
    speedUpLabel->setOpacity(0);
    
    //獲得コイン表示
    gotCoinSprite = createSprite(this, "coin_S.png", zOrderCoin,
                             Vec2::ANCHOR_TOP_RIGHT, Vec2(getContentSize().width - 10, getContentSize().height - 10));
    gotCoinSprite->setScale(0.6f);
    gotCoinLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "0", 36, TextHAlignment::RIGHT, zOrderGotCoin,
                                  Vec2::ANCHOR_BOTTOM_RIGHT, Vec2(gotCoinSprite->getBoundingBox().getMinX() - 10, gotCoinSprite->getBoundingBox().getMinY()));
    gotCoinLabel->setColor(Color3B::YELLOW);
    updateViewCoin();
    
    //ブーストゲージ
    Vec2 boostPos = Vec2(viewRect.size.width - 10, gotCoinLabel->getBoundingBox().getMinY() - 50);
    boostBarSprite = createSprite(this, "boost_bar.png", zOrderBoost, Vec2::ANCHOR_BOTTOM_RIGHT, boostPos);
    Size barSize = boostBarSprite->getContentSize();
    for (int i = 0; i < NEED_COIN_FOR_BOOST; ++i) {
        float margin = 10;
        float gaugeWidth = barSize.width - margin*2;
        Vec2 pos = Vec2(10 + (240 / NEED_COIN_FOR_BOOST) * (i + 0.5), barSize.height / 2);
        auto boostGaugeSprite = createSprite(boostBarSprite, "boost_gauge.png", zOrderBoost, Vec2::ANCHOR_MIDDLE, pos);
        setNodeViewWidth(boostGaugeSprite, gaugeWidth / NEED_COIN_FOR_BOOST * 0.85);
        boostGaugeSprite->setTag(i);
        boostGaugeSprite->setVisible(false);
    }
    updateViewBoost();
    
    //ランキングプレイ表示
    rankingPlayLabel = createTTFLabel(this, FONT_BOLD_TEXT_NAME, "ランキングプレイ中！", 24, TextHAlignment::LEFT, zOrderText, Vec2::ANCHOR_TOP_LEFT, Vec2(20, viewRect.size.height - 20));
    rankingPlayLabel->setVisible(false);
    
    //起動時はインゲームビューはなし
    hideInGameViews();
    
    //デバッグ表示
#if ENABLE_DEBUG_VIEW
    debugTopNode = createNode(this, zOrderDebug, "debugTopNode");
    int y = viewRect.size.height;
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "param",20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugParam);
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "speed",20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugSpeed);
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "time", 20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugTime);
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "pos" , 20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugPos);
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "grav", 20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugGrav);
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "jump", 20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugJumping);
    createTTFLabel(debugTopNode, FONT_TEXT_NAME, "stage",20, TextHAlignment::LEFT, zOrderDebug, Vec2::ANCHOR_TOP_LEFT, Vec2(20, y -= 30))->setTag(kTagDebugStage);
    
    //createTextButton(this, "button_small_blank.png", "restart", CC_CALLBACK_1(StageLayer::restartButtonCallback, this), zOrderDebug, Vec2::ANCHOR_TOP_RIGHT, Vec2(viewRect.size.width - 20, viewRect.size.height - 20));
#endif
    
    //音
    bgmName = BGM_STAGE_1;
    seJumpName =  SE_JUMP;
    lastJumpSeId = -1;

    
    isPausing = false;
    playNum = 0;
    continueNum = 0;
    gotCoin = 0;
    gotCoinForBoost = 0;
    playTime = 0;
    playSpeed = 1.f;
    distance = 0.0;
    isNewRecord = false;
    prevRecord = getGameManager()->getHighScore();
    topUserId = "";

    //常にリザルトに表示する特定ユーザのユーザIDを取得しておく
    Native_GetStorageDataAsString(STORAGE_TYPE_SETTINGS, "top_user_id", [=](bool succeeded, std::string str){
        if (succeeded) {
            topUserId = chompString(str);
        }
    });
    
    //ゲーム設定
    restart();
    
    //自分で更新
    scheduleUpdate();
    
    return true;
}

void StageLayer::restart(){
    
    ++playNum;
    continueNum = 0;
    distance = 0.0;
    
    stage->setup();
    
    gotCoin = 0;
    gotCoinForBoost = 0;
    playTime = 0.f;
    playSpeed = 1.f;
    touchBeganLocation = Vec2::ZERO;
    
    
    //背景を差し替え
    bgSprite->setTexture(getResourceStageBgName(playNum % 11));
    bgSprite2->setTexture(getResourceStageBgName(playNum % 11));
    for (auto it = groundTopNode->getChildren().begin(); it != groundTopNode->getChildren().end(); ++it) {
        auto spr = dynamic_cast<Sprite*>(*it);
        if (spr){
            spr->setTexture(getResourceStageGroundName(playNum % 11));
        }
    }
    
    
    //キャラ絵を差し替え
    int charaIndex = getGameManager()->getCharaIndex();
    std::string charaName = getResourceCharaName(charaIndex);
    charaSprite->setTexture(charaName);

    
    //タイトル表示
    showTitleViews();
    
    //ゲームビューは消す
    hideInGameViews();
    
    //ゲーム表示を先頭に戻す
    scrollerNode->setPosition(Vec2::ZERO);
    float charaScale      = getResourceCharaScale(charaIndex);
    setNodeViewWidthFixedAspect(charaSprite, stage->getBallRadius() * 2 * getViewScale() * charaScale);
    for (auto it = groundTopNode->getChildren().begin(); it != groundTopNode->getChildren().end(); ++it) {
        auto spr = dynamic_cast<Sprite*>(*it);
        setNodeViewWidth(spr, stage->getGroundBaseWidth() * getViewScale());
    }
    distLabel->setVisible(true);


    //サウンド
    seJumpName = "se_chara_" + to_string(getGameManager()->getCharaIndex() + 1) + ".mp3";
    if (!FileUtils::getInstance()->isFileExist(FileUtils::getInstance()->fullPathForFilename(seJumpName))) {
        seJumpName =  SE_JUMP;
    }

    bgmName = playNum % 2 == 1 ? BGM_STAGE_1 : BGM_STAGE_2;
    
    preloadBgm(bgmName);
    preloadBgm(BGM_BOOST);
    preloadEffect(seJumpName);
}

//タイトルで「Tap to Start」が出ている状態で、タップした時の処理
void StageLayer::newPlay(){
    stage->start();
    hideTitleViews();
    showInGameViews();
    playBgm(bgmName);
    playEffect(SE_OK);
    getGameManager()->addPlayNum();
}

//リザルト画面で「コンティニュー」をタップした時の処理(newPlay()とは呼び出されるタイミングが微妙に違うので注意)
void StageLayer::continuePlay(){
    ++continueNum;
    stage->procRespawn();

    showInGameViews();
    playBgm(bgmName);
}

void StageLayer::pauseLayer(){
    //SuperClass::pause(); //update()が呼ばれなくなると困るため
    
    isPausing = true;
    
    procSetPauseNodes(this, isPausing);
}

void StageLayer::resumeLayer(){
    //SuperClass::resume(); //update()が呼ばれなくなると困るため
    
    isPausing = false;
    
    procSetPauseNodes(this, isPausing);
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void StageLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}

bool StageLayer::canContinue()   {
    return (getContinueNum() <= 0) && !getRankingManager()->isRankingPlayingNow();
}

#pragma mark - update

void StageLayer::update(float dt){
    
    updateStage(dt);
    
    updateView(dt);
    
    updateViewDebug(dt);
}

void StageLayer::updateStage(float dt){
    if (!stage) {
        return;
    }
    
    //プレイ速度
    updateSpeed(dt);
    
    stage->setPlaySpeed(playSpeed);
    stage->update(dt * DEFAULT_GAME_SPEED);
    
    //走行距離加算
    if (stage->getState() == Stage::STATE_IN_GAME ||
        stage->getState() == Stage::STATE_BOOST ||
        stage->getState() == Stage::STATE_BOOST_END) {
        
        double rate = playSpeed; //早くなるほど、距離の増え方も上がる
        double rateOfBoost = (stage->getState() == Stage::STATE_BOOST) ? 2.0 : 1.0;
        distance += stage->getBallCurrentRunSpeed() * playSpeed * rate * rateOfBoost * dt;
    }
    
    //float len = stage->getBallPos().x;
}

void StageLayer::updateSpeed(float dt) {
    
    if (stage->getState() == Stage::STATE_IN_GAME) {
//#define DEFAULT_SPEED_UP_DURATION (3.f)
//#define DEFAULT_SPEED_UP_ADD      (0.f)
//#define DEFAULT_SPEED_UP_RATE     (1.1f)
        static double SPEED_TABLE[][2] = {
            {  0.0, 1.00},
            {  3.0, 1.15},
            {  6.0, 1.30},
            {  9.0, 1.45},
            { 12.0, 1.60},
            { 15.0, 1.75},
            { 18.0, 1.90},
            { 21.0, 2.05},
            { 24.0, 2.20},
            { 27.0, 2.35},
            { 30.0, 2.50},
            { 35.0, 2.65},
            { 40.0, 2.80},
            { 45.0, 2.95},
            { 50.0, 3.10},
            { 55.0, 3.25},
            { 60.0, 3.40},
            { 70.0, 3.55},
            { 80.0, 3.70},
            { 90.0, 3.85},
            {100.0, 4.00},
            {120.0, 4.10},
            {140.0, 4.20},
            {160.0, 4.30},
            {180.0, 4.40},
            {200.0, 4.50},
            {9999.9,4.50},
        };
        
        float prevPlaySpeed = playSpeed;
        playTime += dt;
        
        for (int i = 1; i < sizeof(SPEED_TABLE)/sizeof(SPEED_TABLE[0]); ++i) {
            if (SPEED_TABLE[i][0] >= playTime) {
                playSpeed = SPEED_TABLE[i-1][1];
                break;
            }
        }
        
        if (playSpeed > prevPlaySpeed) {
            showSpeedUpEffect();
        }
    }
}

void StageLayer::updateView(float dt) {
    
    if (!stage) {
        return;
    }
    
    Vec2 ballPos = stage->getBallPos();

    //スクロール
    static Vec2 prevScrollPos = Vec2::ZERO;
    static Vec2 prevScrollSpeed = Vec2::ZERO;
    Vec2 scrollPos = Vec2(-ballPos.x * getViewScale(), 0);
    if (stage->hasGameOver()) {
        scrollPos = prevScrollPos + prevScrollSpeed * dt;
    }
    scrollerNode->setPosition(scrollPos);
    prevScrollSpeed = (scrollPos - prevScrollPos) / dt;
    prevScrollPos = scrollPos;
    
    //ボール
    charaSprite->setPosition(getViewPosFromLogicPos(ballPos));
    charaSprite->setRotation(MATH_RAD_TO_DEG(ballPos.x / stage->getBallRadius())); //半径/円周=2PI
    int opacity = 255;
    if (stage->getState() == Stage::STATE_RESPAWN) {
        float time = stage->getTime();
        float cycle = 0.8f;
        float on    = 0.7f;
        int scale = 10;
        if ((int)(time * scale) % (int)(cycle * scale) < (int)(on * scale)) {
            opacity = 192;
        } else{
            opacity = 0;
        }
    }
    else if (stage->getState() == Stage::STATE_GAMEOVER) {
        opacity = charaSprite->getOpacity(); //そのまま。破壊演出で消えてる場合があるので
    }
    charaSprite->setOpacity(opacity);
    
    //背景
    Stage::State state = stage->getState();
    static int startX = 0;
    int curX = startX;
    int maxX = bgSprite->getContentSize().width;
    if (state == Stage::STATE_PREPARE) {
        static float BG_SCROLL_SPEED = -100.f;
        int bgMoveX = dt * BG_SCROLL_SPEED;
        curX = (int)(bgSprite->getPosition().x + bgMoveX + maxX) % maxX;
        startX = curX;
    }
    else{
        static float BG_SCROLL_RATE = -5.f;
        curX = (((int)(scrollPos.x / -getViewScale() * BG_SCROLL_RATE + startX) % maxX) + maxX) % maxX;
    }
    bgSprite->setPosition( Vec2(curX, 0));
    bgSprite2->setPosition(Vec2(curX - maxX, 0));

    //地形
    float x = (scrollPos.x / -getViewScale()) - stage->getGroundBaseWidth() * 2;
    for (auto it = groundTopNode->getChildren().begin(); it != groundTopNode->getChildren().end(); ++it) {
        auto height = stage->getGroundHeightAt(x);
        auto spr = dynamic_cast<Sprite*>(*it);
        float viewX = stage->getGroundBaseX(x) * stage->getGroundBaseWidth();
        spr->setPosition(getViewPosFromLogicPos(Vec2(viewX, height)));
        x += stage->getGroundBaseWidth();
    }
    
    //コイン
    auto coinData = stage->getCoinData();
    int coinIndex = 0;
    for (auto it = coinData.begin(); it != coinData.end(); ++it) {
        auto coinSprite = coinTopNode->getChildByTag(coinIndex);
        if (coinSprite) {
            Vec2 pos = *it;
            coinSprite->setPosition(getViewPosFromLogicPos(pos));
            coinSprite->setVisible(true);
            ++coinIndex;
        }
    }
    while (coinIndex < coinTopNode->getChildrenCount()) {
        auto coinSprite = coinTopNode->getChildByTag(coinIndex);
        if (coinSprite) {
            coinSprite->setVisible(false);
            ++coinIndex;
        }
    }
    
    //落下物
    auto rockData = stage->getRockData();
    int rockIndex = 0;
    for (auto it = rockData.begin(); it != rockData.end(); ++it) {
        auto rockSprite = rockTopNode->getChildByTag(rockIndex);
        if (rockSprite) {
            Vec2 pos = *it;
            rockSprite->setPosition(getViewPosFromLogicPos(pos));
            rockSprite->setVisible(true);
            ++rockIndex;
        }
    }
    while (rockIndex < rockTopNode->getChildrenCount()) {
        auto rockSprite = rockTopNode->getChildByTag(rockIndex);
        if (rockSprite) {
            rockSprite->setVisible(false);
            ++rockIndex;
        }
    }

    //距離表示
    std::string text = "";
    if (stage->getState() != Stage::STATE_PREPARE) {
        text = getStringFromNumber(distance, true, 1) + "m";
    }
    distLabel->setString(text);
}

void StageLayer::updateViewCoin(){
    std::string text = to_string(gotCoin);
    gotCoinLabel->setString(text);
}

void StageLayer::updateViewBoost(){
    for (auto it = boostBarSprite->getChildren().begin(); it != boostBarSprite->getChildren().end(); ++it) {
        auto gauge = dynamic_cast<Sprite*>(*it);
        if (gauge) {
            bool isVisible = (stage->isBoosting() ||
                              (gauge->getTag() < gotCoinForBoost));
            gauge->setVisible(isVisible);
        }
    }
}

void StageLayer::updateViewDebug(float dt){
#if ENABLE_DEBUG_VIEW
    if (stage && debugTopNode) {
        //パラメータ
        auto l = debugTopNode->getChildByTag<Label*>(kTagDebugParam);
        if (l) {
            l->setString(string("param: rate = ") + getStringFromNumber(Native_GetFloatPronounce(PRONOUNCE_SPEED_UP_RATE, DEFAULT_SPEED_UP_RATE), false, 2) +
                         string(", add = ")       + getStringFromNumber(Native_GetFloatPronounce(PRONOUNCE_SPEED_UP_ADD, DEFAULT_SPEED_UP_ADD),   false, 2) +
                         string(", duration= ")   + getStringFromNumber(Native_GetFloatPronounce(PRONOUNCE_SPEED_UP_DURATION, DEFAULT_SPEED_UP_DURATION), false, 2));
        }
        
        //プレイ速度
        l = debugTopNode->getChildByTag<Label*>(kTagDebugSpeed);
        if (l) { l->setString(string("speed = ") + to_string(playSpeed)); }

        //時間
        l = debugTopNode->getChildByTag<Label*>(kTagDebugTime);
        if (l) { l->setString(string("time = ") + to_string(stage->getTime()) + "sec"); }

        //座標
        l = debugTopNode->getChildByTag<Label*>(kTagDebugPos);
        if (l) { l->setString(string("pos = (") + to_string(stage->getBallPos().x) + ", " + to_string(stage->getBallPos().y) + ")"); }

        //速度
        l = debugTopNode->getChildByTag<Label*>(kTagDebugGrav);
        if (l) { l->setString(string("grav = (") + to_string(stage->getCurrentVelocity().x) + ", " + to_string(stage->getCurrentVelocity().y) + ")"); }
        
        //ジャンプ中かどうか
        l = debugTopNode->getChildByTag<Label*>(kTagDebugJumping);
        if (l) { l->setString(string("jump = ") + to_string(stage->getBallJumpingSteps())); }

        //プレイ速度
        static int lastGroundIndex = 0;
        if (lastGroundIndex != stage->getLastGroundDataIndex()) {
            lastGroundIndex  = stage->getLastGroundDataIndex();
            l = debugTopNode->getChildByTag<Label*>(kTagDebugStage);
            if (l) { l->setString((to_string(stage->getLastGroundDataIndex()) + "," + l->getString()).substr(0, 50)); }
        }
    }
#endif
}


#pragma mark - proc

float StageLayer::getViewScale(){
    static float VIEW_SCALE = 45.f;
    return VIEW_SCALE;
}

Vec2 StageLayer::getViewOffset() {
    static Vec2  VIEW_OFFSET  = Vec2(0, 320);
    return VIEW_OFFSET;
}

Vec2 StageLayer::getViewPosFromLogicPos(Vec2 logicPos) {
    return logicPos * getViewScale() + getViewOffset();
}

void StageLayer::setGameStartable(bool f){
    isGameStartable = f;
    
    
    if (isGameStartable && titleSprite) {
        showTapToStartView();
    }
}


#pragma mark - draw

void StageLayer::showTitleViews(){

    //タイトル
    if (!titleSprite) {
        titleSprite      = createSprite(this, "title_logo.png",   zOrderTitle, Vec2::ANCHOR_MIDDLE, Vec2(getContentSize().width / 2, getContentSize().height * 0.55));
    }
    titleSprite->setScale(0.82f);
    
    //tap to start
    if (isGameStartable) {
        showTapToStartView();
    }
}

void StageLayer::hideTitleViews(){
    tapToStartSprite->stopActionByTag(kTagTapToStartAction);
    SAFE_REMOVE_FROM_PARENT(titleSprite);

    hideTapToStartView();
}

void StageLayer::showTapToStartView(){
    if (!tapToStartSprite) {
        tapToStartSprite = createSprite(this, "tap_to_start.png", zOrderTitle, Vec2::ANCHOR_MIDDLE, Vec2(getContentSize().width / 2, getContentSize().height * 0.2));
    }
    if (!tapToStartSprite->getActionByTag(kTagTapToStartAction)) {
        auto action = RepeatForever::create(Sequence::create(DelayTime::create(1.2f),
                                                             FadeOut::create  (0.1f),
                                                             DelayTime::create(0.15f),
                                                             FadeIn::create   (0.1f),
                                                             nullptr));
        action->setTag(kTagTapToStartAction);
        tapToStartSprite->runAction(action);
    }
}

void StageLayer::hideTapToStartView(){
    SAFE_REMOVE_FROM_PARENT(tapToStartSprite);
}

void StageLayer::showInGameViews(){
    gotCoinSprite->setVisible(true);
    gotCoinLabel->setVisible(true);
    boostBarSprite->setVisible(true);
    distLabel->setVisible(true);
    
    if (getRankingManager()->isRankingPlayingNow()) {
        rankingPlayLabel->setVisible(true);
    }

    updateViewCoin();
    updateViewBoost();
}

void StageLayer::hideInGameViews(){
    gotCoinSprite->setVisible(false);
    gotCoinLabel->setVisible(false);
    boostBarSprite->setVisible(false);
    distLabel->setVisible(false);
    rankingPlayLabel->setVisible(false);
}

void StageLayer::showResult(){
    resultLayer = ResultLayer::create();
    resultLayer->setupWithContext(this);
    this->addChild(resultLayer, zOrderResult);
}
void StageLayer::hideResult(){
    SAFE_REMOVE_FROM_PARENT(resultLayer);
}


#pragma mark - effects

//岩の落下エフェクト
void StageLayer::showRockCrashEffect(Vec2 pos){
    auto anim = createAnimationWithFormat(0.02f, 1, true, "stage_fall_effect_%d.png");
    
    auto spr = createSprite(scrollerNode, "stage_fall_effect_1.png", zOrderEffect, Vec2(0.5f, 0.14f), getViewPosFromLogicPos(pos));
    static float scale = 6.f;
    setNodeViewWidthFixedAspect(spr, stage->getRockRadius() * 2 * getViewScale() * scale);
    spr->runAction(Sequence::create(Animate::create(anim),
                                    CallFunc::create([=](){ spr->removeFromParent(); }),
                                    nullptr));
}

//ジャンプ時エフェクト
void StageLayer::showJumpEffect(Vec2 pos){
    auto anim = createAnimationWithFormat(0.02f, 1, true, "jump_effects_%02d.png");
    
    auto spr = createSprite(scrollerNode, getResourceNoImageName(), zOrderEffect, Vec2(0.5f, 0.2f), getViewPosFromLogicPos(pos));
    spr->setScale(1.2f);
    spr->runAction(Sequence::create(Animate::create(anim),
                                    CallFunc::create([=](){ spr->removeFromParent(); }),
                                    nullptr));
}

//垂直落下時エフェクト
void StageLayer::showDropEffect(Vec2 pos){
    auto anim = createAnimationWithFormat(0.02f, 1, true, "jump_effects_%02d.png");

    auto spr = createSprite(scrollerNode, getResourceNoImageName(), zOrderEffect, Vec2(0.5f, 0.2f), getViewPosFromLogicPos(pos));
    spr->setScale(1.2f);
    spr->runAction(Sequence::create(Animate::create(anim),
                                    CallFunc::create([=](){ spr->removeFromParent(); }),
                                    nullptr));
}

//コインのキラキラエフェクト
void StageLayer::showCoinEffect(Vec2 pos){
    auto anim = createAnimationWithFormat(0.1f, 1, true, "coin_effects_%02d.png");
    
    auto spr = createSprite(scrollerNode, getResourceNoImageName(), zOrderEffect, Vec2(0.5f, 0.14f), getViewPosFromLogicPos(pos));
    spr->setScale(3.6f);
    spr->runAction(RepeatForever::create(Animate::create(anim)));
    //永久に続く
}

//コインの獲得時エフェクト
void StageLayer::showCoinGetEffect(Vec2 pos){
    auto anim = createAnimationWithFormat(0.05f, 1, true, "get_effects_%02d.png");
    
    auto spr = createSprite(scrollerNode, getResourceNoImageName(), zOrderEffect, Vec2::ANCHOR_MIDDLE, getViewPosFromLogicPos(pos));
    spr->setScale(2.4f);
    spr->runAction(Sequence::create(Animate::create(anim),
                                    CallFunc::create([=](){ spr->removeFromParent(); }),
                                    nullptr));
}

void StageLayer::showDieEffect() {
    charaSprite->runAction(FadeOut::create(0.3f));
    showRockCrashEffect(stage->getBallGroundPos());
}

void StageLayer::showSpeedUpEffect(){
    speedUpLabel->setVisible(true);
    speedUpLabel->runAction(Sequence::create(Repeat::create(Sequence::create(DelayTime::create(0.3f),
                                                                             FadeOut::create(  0.05f),
                                                                             DelayTime::create(0.05f),
                                                                             FadeIn::create(   0.05f),
                                                                             nullptr),
                                                            2),
                                             CallFunc::create([=](){ speedUpLabel->setVisible(false); }),
                                             nullptr));
}

void StageLayer::showShakeEffect(){
    float duration = 0.05f;

    Vec2 startPos = rotateNode->getPosition();
    rotateNode->runAction(Sequence::create(MoveTo::create(duration, startPos + Vec2( 8,  8)),
                                           MoveTo::create(duration, startPos + Vec2( 4, -4)),
                                           MoveTo::create(duration, startPos + Vec2(-3,  7)),
                                           MoveTo::create(duration, startPos + Vec2( 4,  6)),
                                           MoveTo::create(duration, startPos + Vec2(-2, -7)),
                                           MoveTo::create(duration, startPos),
                                           nullptr));
}

void StageLayer::showBoostEffect(){
    Vec2 pos = Vec2(568, 440);
    float delayPerUnit = 0.13f; //アニメのコマ数とブースト時間から、1ループで終わるくらいに調整
    
    auto effect = createSprite(this, getResourceNoImageName(), zOrderBoostEffect, Vec2::ANCHOR_MIDDLE, pos);
    effect->setScale(2.f);
    effect->setTag(kTagCharaBoostEffect);
    auto anim = createAnimationWithFormat(delayPerUnit, 1, true, "boost_effect_%d.png");
    effect->runAction(Animate::create(anim));
}

void StageLayer::hideBoostEffect(){
    this->removeChildByTag(kTagCharaBoostEffect);
    updateViewBoost();
}


#pragma mark - button

void StageLayer::restartButtonCallback(Ref* pSender){
    restart();
}


#pragma mark - touch

bool StageLayer::onTouchBegan(Touch* touch, Event *event){
    if (!stage) {
        return false;
    }

    if (!stage->hasStarted()) {
        newPlay();
    }
    
    //タップの瞬間、ジャンプ
    /*bool jumped =*/ stage->procBallJump();
    
    touchBeganLocation = touch->getLocation();

    return true;
}
void StageLayer::onTouchMoved(Touch* touch, Event *event){

    //下にドラッグしていれば、垂直落下を行う
    static float DRAG_Y_TO_BALL_DROP = -50;
    Vec2 curTouchingLocation = touch->getLocation();
    if ((curTouchingLocation - touchBeganLocation).y < DRAG_Y_TO_BALL_DROP) {
        if (stage) {
            stage->procDrop();
        }
    }
}

void StageLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void StageLayer::onTouchCancelled(Touch* touch, Event *event){
    
}


#pragma mark - StageDelegate

void StageLayer::onJump(Stage*, Vec2 pos) {
    showJumpEffect(pos);
    
    stopEffect(lastJumpSeId);
    lastJumpSeId = playEffect(seJumpName);
}

void StageLayer::onDrop(Stage*, Vec2 pos) {
    showDropEffect(pos);
    
    playEffect(seJumpName);
}

void StageLayer::onGotCoin(Stage*, Vec2 pos){
    gotCoin += COIN_FOR_ONE_GET;
    
    showCoinGetEffect(pos);
    
    if (stage->canCountCoinForBoost()) {
        gotCoinForBoost++;
        if (gotCoinForBoost >= NEED_COIN_FOR_BOOST) {
            stage->procBoost();
            gotCoinForBoost = 0;
        }
    }
    
    updateViewCoin();
    updateViewBoost();
    playEffect(SE_COIN);
}

void StageLayer::onBoost(Stage*) {
    showBoostEffect();
    playBgm(BGM_BOOST);
}

void StageLayer::onBoostEnded(Stage*){
    hideBoostEffect();
    playBgm(bgmName);
}

void StageLayer::onRockCrashed(Stage*, Vec2 pos){
    showRockCrashEffect(pos);
    showShakeEffect();
    
    playEffect(SE_CLASH);
}

void StageLayer::onGameOver(Stage*) {
    prevRecord = getGameManager()->getHighScore();
    
    double score = distance;
    isNewRecord = getGameManager()->recordScore(score);
    getGameManager()->addCoin(gotCoin);
    getGameManager()->saveAll();
    
    //ハイスコアなら、ランキング登録
    if (isNewRecord) {
        if (Native_IsUserAuthenticated()) {
            
            JSON_t json;
            BEGIN_ADD_TO_JSON_WITH(json);
            
            ADD_NUMBER_VALUE_TO_JSON(score);
            ADD_VARIABLE_TO_JSON(user_id, Native_GetUserAuthId());
            ADD_VARIABLE_TO_JSON(record_date, getFullDateString(getNowDate()));
           
            END_ADD_TO_JSON;
            std::string jsonString = getStringFromJson(json);
            DPRINTF("%s\n", jsonString.c_str());
            
            Native_ReplaceDatabaseAtPath(STORAGE_TYPE_READABLE, "score", jsonString, nullptr);
        }
    }

    //操作不能に
    this->pauseLayer();
    
    //死亡演出（ホワイトフラッシュ）
    getSceneManager()->fadeOut(0.05f, Color3B::WHITE);
    delayedExecution(context, 0.1f, [=](){
        getSceneManager()->fadeIn(0.05f, Color3B::WHITE);
    });

    //次の画面へ行く処理
    static float dur = 1.f;
    runAction(Sequence::create(DelayTime::create(dur),
                               CallFunc::create([=](){
        
        showResult();
        hideInGameViews();
        this->resumeLayer();
        
        //ランキング記録は、ResultLayerの初期化時に行う
        //  シーケンスを同じにするため（非ランキングプレイ時、ランキングプレイからのリトライ時、ランキングプレイからの終了時）
    }),
                               nullptr));
    
    //スクショ
    captureFullScreen("screenshot.png", [=](const std::string fname){
        lastScreenShotName = fname;
    }, [=](){ // on failed
        //do nothing
    });
    
    //サウンド
    stopBgm();
    stopEffect(lastJumpSeId);
    playEffect(stage->isGroundNarakuAtBall() ? SE_MISS : SE_CLASH); //激突か、奈落かで変わる
    
    //analytics
    Native_AnalyticsCustom("score", "len", std::to_string(score));
}

