//
//  GachaLayer.h
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/10/08.
//
//

#ifndef GachaLayer_h
#define GachaLayer_h

#include "CocosHelper.h"
#include "ShopLayer.h"

class SelectLayer;


class GachaLayer : public Node, public ShopLayerDelegate{
    typedef Node SuperClass;
    
public:
    CREATE_FUNC(GachaLayer);
    
private:
    GachaLayer();
    virtual ~GachaLayer();
    
    virtual bool init() override;
    void unsetup();
    
public:
    bool setup();
    void setContext(SelectLayer* context);
    void hide();
    void pauseLayer();
    void resumeLayer();
    void procSetPauseNodes(Node* parent, bool isPausing);

private:
    void update(float dt) override;
    void updateView(float dt);
    
    //proc
    void showShopLayer();
    virtual void hideShopLayer() override;

    //draw
public:
    void drawRefresh();
    void drawRefreshCoin();

private:
    //effects
    void playGachaEffects(int index);
    void offGachaEffects();

    //button
    void shopButtonCallback(Ref* pSender);
    void gachaButtonCallback(Ref* pSender);
    void backButtonCallback(Ref* pSender);
    
public:
    //touch
    bool onTouchBegan(Touch* touch, Event *event);
    void onTouchMoved(Touch* touch, Event *event);
    void onTouchEnded(Touch* touch, Event *event);
    void onTouchCancelled(Touch* touch, Event *event);
    
private:
    Label* coinLabel;
    Menu* gachaButton;
    Menu* coinAddButton;
    Sprite* gachaSprite;
    Sprite* gachaMachineSprite;
    Sprite* gachaLeverSprite;
    Sprite* gachaBallSprite;
    Sprite* charaSprite;
    Sprite* charaNamePlate;
    Label* charaNameLabel;
    Sprite* charaDescPlate;
    Label* charaDescLabel;
    LayerColor* whiteFadeLayer;
    LayerColor* blackFadeLayer;
    Sprite* gotEffectSprite;
    
    EventListenerTouchOneByOne* touchStopListener;
    SelectLayer* context;
    ShopLayer* shopLayer;
    
private:
    bool isPausing;
    bool isGiftViewing;
};


#endif /* GachaLayer_h */
