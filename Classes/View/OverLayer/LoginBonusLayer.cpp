//
//  LoginBonusLayer.cpp
//  rolling
//
//  Created by OtaniAtsushi1 on 2016/11/24.
//
//

#include "LoginBonusLayer.h"
#include "GameManager.h"
#include "SceneManager.h"
#include "SelectLayer.h"
#include "ResourceData.h"
#include "GameData.h"
#include "TextData.h"
#include "SoundData.h"
#include "Sound.h"
#include "Native.h"
#include "GameHelper.h"
#include "Screen.h"
#include "Debug.h"



enum {
    zOrderFade,
    
    zOrderBg,
    zOrderIcon,
    zOrderIconCover,
    
    zOrderButton,
};


LoginBonusLayer::LoginBonusLayer()
: isPausing(false)
{
}

LoginBonusLayer::~LoginBonusLayer(){
    unsetup();
}

bool LoginBonusLayer::init() {
    if (!SuperClass::init()){
        return false;
    }
    
    return true;
}

void LoginBonusLayer::unsetup(){
    if (touchStopListener) {
        restartTouchEventListenerUnder(this, touchStopListener);
        touchStopListener = nullptr;
    }
    
    getPurchaseManager()->setDelegate(nullptr);
}

bool LoginBonusLayer::setup(){
    
    //シーングラフ的に自分より下のタッチイベントを止める //TODO: ただしスクロールビューは止められない(内部でfixedPriorityでタッチイベントを受け取っているため)
    touchStopListener = nullptr;//stopTouchEventListenerUnder(this);
    
    //サイズ
    Rect viewRect = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    setContentSize(viewRect.size);
    
    
    //背景
    createLayerColor(this, Color4B(0,0,0,127), zOrderFade, getContentSize(), Vec2::ANCHOR_BOTTOM_LEFT);
    
    //背景
    bgSprite = createSprite(this, "login_bg.png", zOrderBg, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(this));
    
    //アイコン
    Vector<MenuItem*> items;
    for (int i = 0; i < LOGIN_BONUS_COUNT; ++i) {
        int day = i + 1;
        auto data = s_loginBonusData[i];
        
        std::string name = string("login_icon_") + to_string(data.type) + ".png";
        auto item = createMenuItemImage(name, CC_CALLBACK_1(LoginBonusLayer::itemCallback, this), Vec2::ANCHOR_BOTTOM_LEFT, Vec2::ZERO);
        item->setAutoDisabled(false);
        item->setEnabled(false);
        
        auto parent = item->getNormalImage();
        Size itemSize = item->getContentSize();
        auto t  = createTTFLabel(parent, FONT_BOLD_TEXT_NAME, to_string(i+1),               24, TextHAlignment::LEFT,  zOrderIcon, Vec2::ANCHOR_TOP_LEFT,     Vec2(4, itemSize.height));
        t->enableShadow();
        auto t2 = createTTFLabel(parent, FONT_BOLD_TEXT_NAME, to_string(data.getCoinNum()), 24, TextHAlignment::RIGHT, zOrderIcon, Vec2::ANCHOR_BOTTOM_RIGHT, Vec2(itemSize.width - 4, 0));
        t2->enableShadow();
        
        //獲得済みなら、暗くする
        if (shouldShowBonusAsGot(day)) {
            parent->setColor(Color3B::GRAY);
            createSprite(parent, "login_check.png", zOrderIconCover, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(parent));
        }
        
        item->setTag(day);
        items.pushBack(item);
    }
    menu = createMenuWithArrayZ(bgSprite, items, 7, 610, zOrderIcon, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(bgSprite) + Vec2(0, -30));
    
    //戻るボタン
    createOneMenu(this, "back_button.png", CC_CALLBACK_1(LoginBonusLayer::backButtonCallback, this), zOrderButton,
                  Vec2::ANCHOR_TOP_LEFT, Vec2(20, getContentSize().height - 20));
    
    
    pauseLayer();

    int today = getGameManager()->setLoginBonusGotNow();
    if (today < 0) {
        return false;
    }
    
    getGameManager()->saveAll();

    //今日の分を手前に表示
    menu->getChildByTag(today)->setLocalZOrder(100);
    
    setCheckEffects(today);
    
    
    //自分で更新
    //scheduleUpdate();
    
    return true;
}

void LoginBonusLayer::hide(){
    context->hideLoginBonusLayer();
}

void LoginBonusLayer::pauseLayer(){
    SuperClass::pause();
    
    isPausing = true;
    
    procSetPauseNodes(this, isPausing);
}

void LoginBonusLayer::resumeLayer(){
    SuperClass::resume();
    
    isPausing = false;
    
    procSetPauseNodes(this, isPausing);
}

//指定のノードの直接の子について、汎用的なポーズ処理を行う
void LoginBonusLayer::procSetPauseNodes(Node* parent, bool isPausing){
    
    for (auto it = parent->getChildren().begin(); it != parent->getChildren().end(); ++it){
        Menu* menu = dynamic_cast<Menu*>(*it);
        if (menu){
            for (auto itemit = menu->getChildren().begin(); itemit != menu->getChildren().end(); ++itemit) {
                MenuItem* item = dynamic_cast<MenuItem*>(*itemit);
                item->setEnabled(!isPausing);
            }
        }
        
        ScrollView* scrollView = dynamic_cast<ScrollView*>(*it);
        if (scrollView) {
            scrollView->setTouchEnabled(!isPausing);
        }
    }
}


#pragma mark - update

void LoginBonusLayer::update(float dt){
    
    updateView(dt);
}

void LoginBonusLayer::updateView(float dt) {
    
}


#pragma mark - proc

bool LoginBonusLayer::shouldShowBonusAsGot(int day) {
    //day : 1 ~ LOGIN_BONUS_COUNT
    if (getGameManager()->getLastGotLoginBonusIndex() == LOGIN_BONUS_COUNT) { //ループして初日の場合、全日未獲得表示となる
        return false;
    }
    //前日までの分が獲得済表示となる
    return day <= getGameManager()->getLastGotLoginBonusIndex();
}

#pragma mark - draw

void LoginBonusLayer::drawRefresh(){
}


#pragma mark - effects

void LoginBonusLayer::setCheckEffects(int day) {
    auto item = dynamic_cast<MenuItemImage*>(menu->getChildByTag(day));
    if (!item){
        return;
    }
    
    auto parent = item->getNormalImage();
    
    auto data = s_loginBonusData[day - 1];
    auto tomorrow = s_loginBonusData[day];
    
    //チェックアイコンを大きく
    Vec2 pos = convertNodeAnchorPosToAnotherNodeSpace(item, Vec2::ANCHOR_MIDDLE, bgSprite);
    auto check = createSprite(parent, "login_check.png", zOrderIconCover, Vec2::ANCHOR_MIDDLE, getLocalCenterPos(parent));
    check->setScale(5.f);
    check->setOpacity(0);

    float duration = 0.5f;
    check->runAction(Sequence::create(DelayTime::create(1.f),
                                      //チェックが付く
                                      Spawn::create(FadeIn::create(duration),
                                                    ScaleTo::create(duration, 1.f),
                                                    nullptr),
                                      
                                      //シェイク
                                      CallFunc::create([=](){
        playEffect(SE_COIN); //TODO:
        item->runAction(getSceneManager()->getShakeAction(item));
    }),
                                      
                                      DelayTime::create(1.f),
                                      CallFunc::create([=](){
        std::string title = string() + "ログインボーナス" + to_string(day) + "日目";
        std::string text = string() + "コイン" + to_string(data.getCoinNum()) + "枚をゲット！\n\n" + "明日はコイン" + to_string(tomorrow.getCoinNum()) + "枚がもらえます";
        Native_AlertView(title, text, "OK", [=](int index){
            resumeLayer();
            playEffect(SE_OK);
        });
    }),
                                      nullptr));;
}


#pragma mark - button

void LoginBonusLayer::itemCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
}

void LoginBonusLayer::backButtonCallback(Ref* pSender){
    if (isPausing) {
        return;
    }
    
    playEffect(SE_OK);
    hide();
}


#pragma mark - touch

bool LoginBonusLayer::onTouchBegan(Touch* touch, Event *event){
    return true;
}
void LoginBonusLayer::onTouchMoved(Touch* touch, Event *event){
    
}
void LoginBonusLayer::onTouchEnded(Touch* touch, Event *event){
    
}
void LoginBonusLayer::onTouchCancelled(Touch* touch, Event *event){
    
}

