//  RankersBridge.h
//
//  Copyright © 2016年 All rights reserved.

#ifndef __RANKERS_BRIDGE_H__
#define __RANKERS_BRIDGE_H__

#include <stdint.h>

#include "cocos2d.h"

namespace RankersBridge {

typedef enum {
    ClientError = 0,        // ネットワークに繋がっていない場合やサーバからのレスポンスがタイムアウトした場合
                            // error_message にエラーの情報が格納されている
                            // 環境を変更して、再度実行すると解決する場合がある
    
    InternalServerError,    // RANKERS のサーバで何か障害が起きている場合
                            // 再度実行すると解決する場合がある
    
    Maintenance,            // RANKERS がメンテナンス中の場合
                            // json にエラー情報 json フォーマットで格納されている
                            // { "error" : { "code"         : ...,     CCInteger : エラーコード
                            //               "message"      : ...,     CCString  : ユーザ向けメッセージ
                            //               "debug_message : ...}}    CCString  : 開発者向けメッセージ。含まれないこともある
    
    ErrorResponse,          // パラメータ誤り等でエラーが発生した場合
                            // 同じパラメータで再度実行した場合は失敗する
                            // json にエラー情報 json フォーマットで格納されている
                            // { "error" : { "code"         : ...,     CCInteger : エラーコード
                            //               "message"      : ...,     CCString  : ユーザ向けメッセージ
                            //               "debug_message : ...}}    CCString  : 開発者向けメッセージ。含まれないこともある
    
    SucessResponse          // API 呼び出しに成功した場合
} APIResponseType;
    
class Delegate {
public:
    virtual ~Delegate() {}
    virtual void startGame(const char *json) = 0;
    virtual void didCompetitionAppear() = 0;
    virtual void didCompetitionDisappear() = 0;
    virtual void didStartMovie() = 0;
    virtual void didStopMovie() = 0;
};

class APIHandler {
public:
    virtual ~APIHandler(){}
    virtual void handler(RankersBridge::APIResponseType responseType, const char* json, const char* error_message) = 0;
};

void setup(const char* sdkid, const char* sdkurl);
void setup(const char* sdkid, const char* sdkurl, uint32_t appVersion);
void set_delegate(Delegate *delegate);
void update_competition_with_handler(APIHandler *handler);
void set_player_name(const char* player_name);
void show_competition();
void show_competition_result();
void post_score(int score, const char* score_src, APIHandler *handler);
void post_score(int score, const char* score_src, const char* payload, APIHandler *handler);
void dispose_score();
bool is_capturing();
bool is_rec_supported();
}

#endif // __RANKERS_BRIDGE_H__
