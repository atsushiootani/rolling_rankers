#include "RankersBridge.h"

#include "platform/android/jni/JniHelper.h"

USING_NS_CC;

#define CLASS_NAME "co/rankers/sdk/cocos2dx/RankersBridge"

namespace RankersBridge {
class InternalAPIHandler {
public:
  APIHandler *m_apiHadlerPointer;

  ~InternalAPIHandler(){}
  void handler(RankersBridge::APIResponseType responseType, const char* json, const char* error_message){
    if(m_apiHadlerPointer != 0){
      m_apiHadlerPointer->handler(responseType, json, error_message);
      delete this;
    }
  }
};
}

void RankersBridge::setup(const char* sdkid, const char* sdkurl)
{
  JniMethodInfo t;
  if (JniHelper::getStaticMethodInfo(t,
                                     CLASS_NAME,
                                     "setup",
                                     "("
                                     "Ljava/lang/String;"
                                     "Ljava/lang/String;"
                                     ")V"
                                     )) {
    jstring jsdkid  = t.env->NewStringUTF(sdkid);
    jstring jsdkurl = t.env->NewStringUTF(sdkurl);
    t.env->CallStaticVoidMethod(t.classID,
                                t.methodID,
                                jsdkid,
                                jsdkurl
                                );
    t.env->DeleteLocalRef(jsdkid);
    t.env->DeleteLocalRef(jsdkurl);
    t.env->DeleteLocalRef(t.classID);
  } else {
    CCLOG("failed to find method named setup");
    CCLOG("signature (Ljava/lang/String;Ljava/lang/String;)V");
  }
}

void RankersBridge::setup(const char* sdkid, const char* sdkurl, uint32_t appVersion)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "setup",
                                       "("
                                       "Ljava/lang/String;"
                                       "Ljava/lang/String;"
                                       "I"
                                       ")V"
                                       )) {
        jstring jsdkid  = t.env->NewStringUTF(sdkid);
        jstring jsdkurl = t.env->NewStringUTF(sdkurl);
        t.env->CallStaticVoidMethod(t.classID,
                                    t.methodID,
                                    jsdkid,
                                    jsdkurl,
                                    appVersion
                                    );
        t.env->DeleteLocalRef(jsdkid);
        t.env->DeleteLocalRef(jsdkurl);
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named setup");
        CCLOG("signature (Ljava/lang/String;Ljava/lang/String;I)V");
    }
}

void RankersBridge::set_delegate(Delegate *delegate)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "setDelegate",
                                       "([B)V"
                                       )) {
        jbyteArray pointer = t.env->NewByteArray(sizeof(void*));
        t.env->SetByteArrayRegion(pointer, 0, sizeof(void*), (jbyte*)&delegate);
        t.env->CallStaticVoidMethod(t.classID,
                                    t.methodID,
                                    pointer
                                    );
        t.env->DeleteLocalRef(pointer);
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named setDelegate");
        CCLOG("signature ([B)V");
    }
}

void RankersBridge::update_competition_with_handler(APIHandler *handler)
{
  RankersBridge::InternalAPIHandler *h = new RankersBridge::InternalAPIHandler();
  h->m_apiHadlerPointer = handler;
  JniMethodInfo t;
  if (JniHelper::getStaticMethodInfo(t,
                                     CLASS_NAME,
                                     "updateCompetition",
                                     "([B)V"
                                     )) {
    jbyteArray pointer = t.env->NewByteArray(sizeof(void*));
    t.env->SetByteArrayRegion(pointer, 0, sizeof(void*), (jbyte*)&h);
    t.env->CallStaticVoidMethod(t.classID,
                                t.methodID,
                                pointer
                                );
    t.env->DeleteLocalRef(pointer);
    t.env->DeleteLocalRef(t.classID);
  } else {
    CCLOG("failed to find method named updateCompetition");
    CCLOG("signature ([B)V");
  }
}

void RankersBridge::set_player_name(const char* player_name)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "setPlayerName",
                                       "("
                                       "Ljava/lang/String;"
                                       ")V"
                                       )) {
        jstring jplayerName = t.env->NewStringUTF(player_name);
        t.env->CallStaticVoidMethod(t.classID,
                                    t.methodID,
                                    jplayerName
                                    );
        t.env->DeleteLocalRef(jplayerName);
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named setPlayerName");
        CCLOG("signature (Ljava/lang/String;)V");
    }
    
}

void RankersBridge::show_competition()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "showCompetition",
                                       "()V"
                                       )) {
        t.env->CallStaticVoidMethod(t.classID,
                                    t.methodID
                                    );
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named showCompetition");
        CCLOG("signature ()V");
    }
}

void RankersBridge::show_competition_result()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "showCompetitionResult",
                                       "()V"
                                       )) {
        t.env->CallStaticVoidMethod(t.classID,
                                    t.methodID
                                    );
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named showCompetitionResult");
        CCLOG("signature ()V");
    }
}

void RankersBridge::post_score(int score, const char* score_src, APIHandler *handler)
{
  RankersBridge::InternalAPIHandler *h = new RankersBridge::InternalAPIHandler();
  h->m_apiHadlerPointer = handler;
  JniMethodInfo t;
  if (JniHelper::getStaticMethodInfo(t,
                                     CLASS_NAME,
                                     "postScore",
                                     "([BILjava/lang/String;)V"
                                     )) {
    jbyteArray pointer = t.env->NewByteArray(sizeof(void*));
    t.env->SetByteArrayRegion(pointer, 0, sizeof(void*), (jbyte*)&h);
    jstring jscore_src  = t.env->NewStringUTF(score_src);
    t.env->CallStaticVoidMethod(t.classID,
                                t.methodID,
                                pointer,
                                score,
                                jscore_src
                                );
    t.env->DeleteLocalRef(pointer);
    t.env->DeleteLocalRef(jscore_src);
    t.env->DeleteLocalRef(t.classID);
  } else {
    CCLOG("failed to find method named postScore");
    CCLOG("signature ([BILjava/lang/String;)V");
  }
}

void RankersBridge::post_score(int score, const char* score_src, const char* payload, APIHandler *handler)
{
    RankersBridge::InternalAPIHandler *h = new RankersBridge::InternalAPIHandler();
    h->m_apiHadlerPointer = handler;
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "postScore",
                                       "([BILjava/lang/String;Ljava/lang/String;)V"
                                       )) {
        jbyteArray pointer = t.env->NewByteArray(sizeof(void*));
        t.env->SetByteArrayRegion(pointer, 0, sizeof(void*), (jbyte*)&h);
        jstring jscore_src  = t.env->NewStringUTF(score_src);
        jstring jpayload    = t.env->NewStringUTF(payload);
        t.env->CallStaticVoidMethod(t.classID,
                                    t.methodID,
                                    pointer,
                                    score,
                                    jscore_src,
                                    jpayload
                                    );
        t.env->DeleteLocalRef(pointer);
        t.env->DeleteLocalRef(jscore_src);
        t.env->DeleteLocalRef(jpayload);
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named postScore");
        CCLOG("signature ([BILjava/lang/String;Ljava/lang/String;)V");
    }
}

void RankersBridge::dispose_score()
{
  JniMethodInfo t;
  if (JniHelper::getStaticMethodInfo(t,
                                     CLASS_NAME,
                                     "disposeScore",
                                     "()V"
                                     )) {
    t.env->CallStaticVoidMethod(t.classID,
                                t.methodID
                                );
    t.env->DeleteLocalRef(t.classID);
  } else {
    CCLOG("failed to find method named disposeScore");
    CCLOG("signature ()V");
  }
}

bool RankersBridge::is_capturing()
{
    jboolean is_capturing = false;
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "isCapturing",
                                       "()Z"
                                       )) {
        is_capturing = t.env->CallStaticBooleanMethod(t.classID,
                                                      t.methodID
                                                      );
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named isCapturing");
        CCLOG("signature ()Z");
    }
    return is_capturing;
}

bool RankersBridge::is_rec_supported()
{
    jboolean is_rec_supported = false;
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t,
                                       CLASS_NAME,
                                       "isRecSupported",
                                       "()Z"
                                       )) {
        is_rec_supported = t.env->CallStaticBooleanMethod(t.classID,
                                                          t.methodID
                                                          );
        t.env->DeleteLocalRef(t.classID);
    } else {
        CCLOG("failed to find method named isRecSupported");
        CCLOG("signature ()Z");
    }
    return is_rec_supported;
}

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    apiHandler
 * Signature: ([BILjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_apiHandler
  (JNIEnv *, jclass, jbyteArray, jint, jstring, jstring);

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    startGame
 * Signature: ([BLjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_startGame
  (JNIEnv *, jclass, jbyteArray, jstring);

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onCompetitionAppear
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onCompetitionAppear
  (JNIEnv *, jclass, jbyteArray);

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onCompetitionDisappear
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onCompetitionDisappear
  (JNIEnv *, jclass, jbyteArray);
    
/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onStartMovie
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onStartMovie
  (JNIEnv *, jclass, jbyteArray);

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onStopMovie
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onStopMovie
  (JNIEnv *, jclass, jbyteArray);
    
#ifdef __cplusplus
}
#endif

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    apiHandler
 * Signature: (JILjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_apiHandler
  (JNIEnv *env, jclass thiz, jbyteArray pointer, jint responseType, jstring json, jstring error_message)
{
  RankersBridge::InternalAPIHandler *h;
  env->GetByteArrayRegion(pointer, 0, sizeof(h), (jbyte*)&h);

  const char* c_json = NULL;
  const char* c_error_message = NULL;

  if(json != NULL){
    c_json = env->GetStringUTFChars(json, NULL);
  }

  if(error_message != NULL){
    c_error_message = env->GetStringUTFChars(error_message, NULL);
  }

  h->handler((RankersBridge::APIResponseType)responseType, c_json, c_error_message);

  if(json != NULL && c_json != NULL){
    env->ReleaseStringUTFChars(json, c_json);
  }

  if(error_message != NULL && c_error_message != NULL){
    env->ReleaseStringUTFChars(error_message, c_error_message);
  }
}

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    startGame
 * Signature: ([BLjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_startGame
(JNIEnv *env, jclass thiz, jbyteArray pointer, jstring json)
{
  RankersBridge::Delegate *delegate;
  env->GetByteArrayRegion(pointer, 0, sizeof(delegate), (jbyte*)&delegate);

  const char* c_json = NULL;
  if(json != NULL){
    c_json = env->GetStringUTFChars(json, NULL);
  }

  delegate->startGame(c_json);

  if(json != NULL && c_json != NULL){
    env->ReleaseStringUTFChars(json, c_json);
  }
}

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onCompetitionAppear
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onCompetitionAppear
(JNIEnv *env, jclass thiz, jbyteArray pointer)
{
  RankersBridge::Delegate *delegate;
  env->GetByteArrayRegion(pointer, 0, sizeof(delegate), (jbyte*)&delegate);
  delegate->didCompetitionAppear();
}

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onCompetitionDisappear
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onCompetitionDisappear
(JNIEnv *env, jclass thiz, jbyteArray pointer)
{
  RankersBridge::Delegate *delegate;
  env->GetByteArrayRegion(pointer, 0, sizeof(delegate), (jbyte*)&delegate);
  delegate->didCompetitionDisappear();
}

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onStartMovie
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onStartMovie
(JNIEnv *env, jclass thiz, jbyteArray pointer)
{
    RankersBridge::Delegate *delegate;
    env->GetByteArrayRegion(pointer, 0, sizeof(delegate), (jbyte*)&delegate);
    delegate->didStartMovie();
}

/*
 * Class:     co_rankers_sdk_cocos2dx_RankersBridge
 * Method:    onStopMovie
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_co_rankers_sdk_cocos2dx_RankersBridge_onStopMovie
(JNIEnv *env, jclass thiz, jbyteArray pointer)
{
    RankersBridge::Delegate *delegate;
    env->GetByteArrayRegion(pointer, 0, sizeof(delegate), (jbyte*)&delegate);
    delegate->didStopMovie();
}




