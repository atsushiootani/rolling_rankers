package kazuma.saitou.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by otaniatsushi1 on 16/02/21.
 */
public class WebViewUtils {

    private static WebViewUtils instance;
    private static Activity mContext;

    public WebViewUtils(Activity context) {
        mContext = context;
        instance = this;
    }


    public void openWebPage(String url) {
        if (!url.startsWith("http")) {
            url = "http://" + url;
        }

        Uri uri = Uri.parse(url);
        Intent i = new Intent(Intent.ACTION_VIEW, uri);
        System.out.println(mContext);
        mContext.startActivity(i);
    }

    public void openWebView(String url) {
        //TODO:
        //FIXME: 単にブラウザに飛ばしちゃう
        openWebPage(url);
    }

    public void closeWebView(){
        //TODO:
        //OpenWebPageにしている場合、特にすることなし
    }

    public boolean hasWebView() {
        //TODO:
        //OpenWebPageにしている場合、常にfalseということになる
        return false;
    }

    /********************************/
    // Native
    /********************************/

    public static void Native_OpenWebPage(String url) {
        ActivityUtils.DebugPrint("open web page");
        instance.openWebPage(url);
    }

    public static void Native_OpenWebView(String url) {
        ActivityUtils.DebugPrint("open web view");
        instance.openWebView(url);
    }

    public static void Native_CloseWebView() {
        ActivityUtils.DebugPrint("close web view");
        instance.closeWebView();
    }

    public static boolean Native_HasWebView() {
        ActivityUtils.DebugPrint("has web view");
        return instance.hasWebView();
    }
}
