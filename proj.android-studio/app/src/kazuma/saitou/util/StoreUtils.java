package kazuma.saitou.util;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

import kazuma.saitou.rolling.MainActivity;

/**
 * Created by otaniatsushi1 on 16/02/21.
 */
public class StoreUtils {
    static final String REVIEW_STATE_PREFERENCE_KEY           = "review_state";
    static final String REVIEW_LAST_PROMPT_DATE_REFERENCE_KEY = "review_last_prompt_date";
    static final int REVIEW_STATE_NOT_YET = 0; // まだレビューしていない
    static final int REVIEW_STATE_DONE    = 1; // レビュー済み
    static final int REVIEW_STATE_NEVER   = 2; // レビュー拒否

    private static StoreUtils instance;
    private static MainActivity mContext;

    public StoreUtils(MainActivity context) {
        mContext = context;
        instance = this;
    }

    public void openStore(String packageName) {
        Uri uri = Uri.parse("market://details?id=" + packageName);
        mContext.startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    public void openStore(){
        openStore(mContext.getPackageName());
    }

    public boolean checkStorePageExists(String packageName){
        return true; //TODO:
    }

    public boolean checkStorePageExists(){
        return checkStorePageExists(mContext.getPackageName());
    }

    public void reviewPromptShow(String title, String text, String nowText, String laterText, String neverText) {

        AlertUtils.ShowAlertView(title, text, nowText, neverText, laterText, new AlertUtils.AlertCallbacks() {
            @Override
            public void alertPositiveCallback() {
                // レビュー済みを記録
                SaveUtils.SaveInt(REVIEW_STATE_PREFERENCE_KEY, REVIEW_STATE_DONE);
                // ストアに飛ばす
                openPlayStore();
            }

            @Override
            public void alertNegativeCallback() {
                // レビュー拒否を記録
                SaveUtils.SaveInt(REVIEW_STATE_PREFERENCE_KEY, REVIEW_STATE_NEVER);
            }

            @Override
            public void alertNeutralCallback() {
                // 「あとで」を記録
                SaveUtils.SaveLong(REVIEW_LAST_PROMPT_DATE_REFERENCE_KEY, ActivityUtils.GetNowMsec());
            }
        });
    }

    public boolean reviewHasDone() {
        return SaveUtils.LoadInt(REVIEW_STATE_PREFERENCE_KEY, REVIEW_STATE_NOT_YET) == REVIEW_STATE_DONE;
    }

    public boolean reviewHasRejected() {
        return SaveUtils.LoadInt(REVIEW_STATE_PREFERENCE_KEY, REVIEW_STATE_NOT_YET) == REVIEW_STATE_NEVER;
    }

    public boolean reviewHasNotYet() {
        return SaveUtils.LoadInt(REVIEW_STATE_PREFERENCE_KEY, REVIEW_STATE_NOT_YET) == REVIEW_STATE_NOT_YET;
    }

    public void openPlayStore() {
        String packageName = mContext.getPackageName();
        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException e) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id=" + packageName)));
        }
    }


    /*****************************************************/
    /*  static methods  */
    /*****************************************************/

    public static void Native_OpenStore(String packageName) {
        instance.openStore(packageName);
    }

    public static void Native_OpenStore(){
        instance.openStore();
    }

    public static boolean Native_CheckStorePageExists(String packageName){
        return instance.checkStorePageExists(packageName);
    }

    public static boolean Native_CheckStorePageExists(){
        return instance.checkStorePageExists();
    }

    public static void Native_ReviewPromptShow(String title, String text, String nowText, String laterText, String neverText) {
        instance.reviewPromptShow(title, text, nowText, laterText, neverText);
    }

    public static boolean Native_ReviewHasDone() {
        return instance.reviewHasDone();
    }

    public static boolean Native_ReviewHasRejected() {
        return instance.reviewHasRejected();
    }

    public static boolean Native_ReviewHasNotYet() {
        return instance.reviewHasNotYet();
    }
}
