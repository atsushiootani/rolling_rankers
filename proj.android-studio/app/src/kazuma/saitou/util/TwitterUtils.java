package kazuma.saitou.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Debug;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;

import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.R;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterUtils {

    //OAuth認証周り
    private Class mAuthActivity;
    private boolean mIsAuthorizing = false;

    //セーブデータ周り
    private SaveUtils mSaveUtils = null;
    private static final String PREFERENCE_NAME = "TwitterOAuthActivity";
	private static final String PREFERENCE_TOKEN_KEY        = "token";
	private static final String PREFERENCE_TOKEN_SECRET_KEY = "token_secret";

    //投稿周り
	private boolean mIsTweeting = false;
	private String mTweetText = null;
    private File mImageFile = null;
	private String mEditedTweetText = null; //投稿画面で編集した結果のテキスト
	private EditText mEditText = null;
    private LinearLayout mTweetView = null;
	private boolean mIsTwitterAlertShowing = false; //アラート表示中かどうか
    private String mText = null;
    private String mImageName = null;

    //インスタンス
	private static MainActivity mContext = null;
	private static TwitterUtils instance = null;

	/**********************************************/
	// instance methods
	/**********************************************/

	public TwitterUtils(MainActivity context, Class authActivity) {
		mSaveUtils = new SaveUtils(context, PREFERENCE_NAME);

        mAuthActivity = authActivity;
		mContext = context;
		instance = this;
	}

	// Twitterインスタンスを作成
	private Twitter getTwitterInstance() {
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(   mContext.getString(R.string.twitter_consumer_key));
		builder.setOAuthConsumerSecret(mContext.getString(R.string.twitter_consumer_secret));
		Configuration configuration = builder.build();

		TwitterFactory factory = new TwitterFactory(configuration);
		Twitter twitter = factory.getInstance();
		if (hasAccessToken()) {
			twitter.setOAuthAccessToken(loadAccessToken());
		}

		return twitter;
	}

	/**********************************************/
	// 認証周り
	/**********************************************/

	// 認証済みかどうかを返す
	private boolean isAuthorized() {
        return hasAccessToken();
	}

	// twitterの認証を行うメソッド（未認証に投稿するときだけ使用）
	private void authorize() {
        //多重実行の防止
        if (mIsAuthorizing) {
            return;
        }

		Intent intent = new Intent(mContext, mAuthActivity);
		mContext.startActivity(intent);

        mIsAuthorizing = true;
	}

    // twitterの認証開始して最初に行うメソッド(authorize() -> TwitterOAuthActivity.onCreate -> このメソッド の順に呼び出される)
    private Twitter onAuthorizeStarted() {
        return getTwitterInstance();
    }

	// 認証成功時の処理
	private void onAuthorizeSucceeded(AccessToken accessToken) {
        ActivityUtils.ShowToast(mContext, "twitter認証しました");
        storeAccessToken(accessToken);

        if (mIsTweeting) {
            showConfirmAlertView();
        }
        mIsAuthorizing = false;
	}

	// 認証失敗時の処理
	private void onAuthorizeFailed() {
        ActivityUtils.ShowToast(mContext, "twitter認証に失敗しました");

        if (mIsTweeting) {
            onTweetFailed();
        }
        mIsAuthorizing = false;
	}

    // 認証キャンセル時の処理
    private void onAuthorizeCancelled() {
        ActivityUtils.ShowToast(mContext, "twitter認証がキャンセルされました");
        mIsAuthorizing = false;
    }

	// 認証情報をクリア
	private void clearAuthorization() {
		// アクセストークンを削除
		clearAccessToken();
	}

	/**********************************************/
	// アクセストークン周り
	/**********************************************/

	// アクセストークンを保存する
	private void storeAccessToken(AccessToken accessToken) {
		mSaveUtils.saveString(PREFERENCE_TOKEN_KEY,        accessToken.getToken());
		mSaveUtils.saveString(PREFERENCE_TOKEN_SECRET_KEY, accessToken.getTokenSecret());
	}

	// アクセストークンを削除（ユーザがtwitterアカウントを変えたくて明示的に再認証を要求したとき等）
    private void clearAccessToken() {
		mSaveUtils.delete(PREFERENCE_TOKEN_KEY);
		mSaveUtils.delete(PREFERENCE_TOKEN_SECRET_KEY);
	}

	// アクセストークンをプリファレンスから読み込む
    private AccessToken loadAccessToken() {
		String token       = mSaveUtils.loadString(PREFERENCE_TOKEN_KEY,        null);
		String tokenSecret = mSaveUtils.loadString(PREFERENCE_TOKEN_SECRET_KEY, null);

		if (token != null && tokenSecret != null) {
			return new AccessToken(token, tokenSecret);
		} else {
			return null;
		}
	}

	// アクセストークンが存在するかどうか
    private boolean hasAccessToken() {
		return loadAccessToken() != null;
	}


	/**********************************************/
	// 投稿
	/**********************************************/

	//投稿する
	public void tweetPost(String text, String imageName, String url) {
        onTweetStart(text, imageName);

		mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isAuthorized()) {
                    showConfirmAlertView();
                } else {
                    authorize();
                }
            }
        });
	}

	// 投稿内容を確認するアラートを表示する
	private void showConfirmAlertView() {
        // 多重表示を防止
		if (mIsTwitterAlertShowing) {
			return;
		}

        mIsTwitterAlertShowing = true;

		AlertUtils.ShowAlertView("twitterへ投稿する", mTweetView, "投稿する", "キャンセル", "再認証する", new AlertUtils.AlertCallbacks() {
            @Override
            public void alertPositiveCallback() {
                // 投稿
                contribute();

                mIsTwitterAlertShowing = false;
            }

            @Override
            public void alertNegativeCallback() {
                // キャンセル
                onTweetCancelled();

                mIsTwitterAlertShowing = false;
            }

            @Override
            public void alertNeutralCallback() {
                // 再認証
                clearAuthorization();
                authorize();

                mIsTwitterAlertShowing = false;
            }
        });
	}

	// 実際に投稿する処理
	private void contribute() {
        mContext.Native_ShowIndicator("投稿中...");

        //投稿するテキストを取得
		mEditedTweetText = mEditText.getText().toString(); //ユーザが書き換えているかもしれないので

		AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Void... params) {

				// 呟くためのステータスを生成する
				twitter4j.StatusUpdate status = new twitter4j.StatusUpdate(mEditedTweetText);

                // 画像を追加
                if (mImageFile != null){
                    status.setMedia(mImageFile);
                }

                // ツイートする
				try {
					twitter4j.Twitter twitter = getTwitterInstance();
					twitter.updateStatus(status);
					return true;
				} catch (Exception e) {
                    ActivityUtils.DebugErrorPrint(e.toString());
                    ActivityUtils.DebugErrorPrint(e.getStackTrace().toString());

					ActivityUtils.ShowToast(mContext, "投稿に失敗しました");
                    //onTweetFailed(); //フローはまだ終わりでない。再投稿もありうるので
					return false;
				}
			}

			@Override
			protected void onPostExecute(Boolean result) {
                mContext.Native_HideIndicator();

				if (result) {
                    onTweetSucceeded();

                    ActivityUtils.ShowToast(mContext, "twitterへ投稿しました");
				} else {
					showPostFailedAlertView();
				}
			}
		};

		task.execute();
	}

	// 投稿失敗時にアラートを表示する
	private void showPostFailedAlertView() {
		AlertUtils.ShowAlertView("", "投稿に失敗しました。通信環境を確認してください", "リトライ", "投稿をやめる", "認証を再度行う",
                new AlertUtils.AlertCallbacks() {
                    @Override
                    public void alertPositiveCallback() {
                        //再投稿
                        contribute();
                    }

                    @Override
                    public void alertNegativeCallback() {
                        //終了
                        onTweetFailed();
                    }

                    @Override
                    public void alertNeutralCallback() {
                        // 再認証
                        clearAuthorization();
                        authorize();
                    }
                });
	}

    // ツイート開始処理
    private void onTweetStart(String text, String imageName) {
        mText = text;
        mImageName = imageName;

        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mIsTweeting = true;
                mTweetText = mText;

                //ビューを作成
                mTweetView = new LinearLayout(mContext);
                mTweetView.setOrientation(LinearLayout.VERTICAL);

                //画像
                if (mImageName != null && mImageName.length() > 0) {
                    ImageView imageView = new ImageView(mContext);
                    mImageFile = ActivityUtils.GetFileFromBitmapFileName(mContext, mImageName);
                    imageView.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
                    mTweetView.addView(imageView);
                }

                //テキスト
                mEditText = new EditText(mContext);
                mEditText.setText(mTweetText);
                mTweetView.addView(mEditText);

                mEditedTweetText = mText;
            }
        });
    }

    // ツイート成功時処理
    private void onTweetSucceeded() {
        shareSuccessCallbackJni();
        onTweetEnded();
    }

    // ツイート失敗時処理
    private void onTweetFailed() {
        shareFailureCallbackJni();
        onTweetEnded();
    }

    // ツイートキャセル時処理
    private void onTweetCancelled() {
        shareCancelCallbackJni();
        onTweetEnded();
    }

    // ツイート終了時共通
    private void onTweetEnded() {
        shareAlwaysCallbackJni();

        mTweetText = null;
        mImageFile = null;
        mEditText = null;
        mEditedTweetText = null;
        mTweetView = null;
        mIsTweeting = false;
    }

    /**********************************************/
	// static methods
	/**********************************************/

	// twitter投稿用のnativeメソッド
	public static void Native_TwitterPost(String text, String imageName, String url) {
		instance.tweetPost(text, imageName, url);
	}

	//認証済みかどうか
	public static boolean IsAuthorized() {
		return instance.isAuthorized();
	}

    //認証を開始する
    public static Twitter OnAuthorizeStarted() {
        return instance.onAuthorizeStarted();
    }

    //認証成功した時の処理
    public static void OnAuthorizeSucceeded(AccessToken accessToken){
        instance.onAuthorizeSucceeded(accessToken);
    }

    //認証失敗した時の処理
    public static void OnAuthorizeFailed() {
        instance.onAuthorizeFailed();
    }

    //認証をキャンセルした時の処理
    public static void OnAuthorizeCancelled() {
        instance.onAuthorizeCancelled();
    }


    public static native void shareSuccessCallbackJni();
	public static native void shareFailureCallbackJni();
	public static native void shareCancelCallbackJni();
	public static native void shareAlwaysCallbackJni();
}
