package kazuma.saitou.util;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import kazuma.saitou.rolling.MainActivity;

/**
 * Created by otaniatsushi1 on 16/12/22.
 */
public class UserUtils {

    FirebaseUser user;


    //インスタンス
    private static MainActivity mContext = null;
    private static UserUtils instance = null;

    /**********************************************/
    // instance methods
    /**********************************************/

    public UserUtils(MainActivity context) {

        mContext = context;
        instance = this;
    }

    public void initUserAuth(){
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            userAuthentication();
        }

        FirebaseAuth.AuthStateListener mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    ActivityUtils.DebugPrint("onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    ActivityUtils.DebugPrint("onAuthStateChanged:signed_out");
                }
            }
        };

        FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);
    }

    public void userAuthentication(){
        FirebaseAuth.getInstance().signInAnonymously()
                .addOnCompleteListener(mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        ActivityUtils.DebugPrint("signInAnonymously:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            ActivityUtils.DebugPrint("signInAnonymously" + task.getException().toString());
                        }
                    }
                });
    }

    public boolean isUserAuthenticated(){
        return user != null;
    }

    public String getUserAuthId() {
        return (user != null) ? user.getUid() : "";
    }

    public String getUserAuthName(){
        return (user != null) ? user.getDisplayName() : "";
    }

    public String getUserAuthEmail(){
        return (user != null) ? user.getEmail() : "";
    }

    /*****************************************************/
    /*  Native  */
    /*****************************************************/

    public static void Native_InitUserAuth(){
        instance.initUserAuth();
    }
    public static void Native_UserAuthentication(){
        instance.userAuthentication();
    }
    public static boolean Native_IsUserAuthenticated(){
        return instance.isUserAuthenticated();
    }
    public static String Native_GetUserAuthId(){
        return instance.getUserAuthId();
    }
    public static String Native_GetUserAuthName(){
        return instance.getUserAuthName();
    }
    public static String Native_GetUserAuthEmail(){
        return instance.getUserAuthEmail();
    }
}
