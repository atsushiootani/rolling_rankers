package kazuma.saitou.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.R;

/**
 * Created by otaniatsushi1 on 16/12/22.
 */
public class StorageUtils {

    /**********************************************/
    // public members
    /**********************************************/

    //定数
    public enum StorageType{
        None,     //指定なし。ディレクトリ名ふくめ完全なパスを自前で指定する場合。非推奨
        Public,   //公開ディレクトリ。外部からもアクセス可能。SNSシェア用データなど
        Common,   //共通ディレクトリ。認証済みユーザなら誰でもアクセス可能。ゲームパラメータなど
        Settings, //設定ディレクトリ。認証済みユーザなら誰でも読み込み可能。書き込みは不可。ゲームパラメータなど
        Readable, //ユーザ個別の公開ディレクトリ。ユーザ本人のみ書き込み可能。認証済みユーザなら誰でも読み込み可能。ランキングなど
        User,     //ユーザ個別ディレクトリ。ユーザ本人のみアクセス可能。セーブデータなど
        Special,  //特殊ディレクトリ。特定のユーザだけアクセスできるなど
        Admin,    //管理者用ディレクトリ。デバッグデータなど
    }

    //内部クラス
    public static abstract class OnFetchDataListener{
        abstract public void onCompletion(boolean success, String str);
    }
    public static abstract class OnFetchAndSavedDataListener{
        abstract public void onCompletion(boolean success, String fullPath);
    }
    public static abstract class OnUploadDataListener{
        abstract public void onCompletion(boolean success);
    }


    /**********************************************/
    // private members
    /**********************************************/

    //Firebase
    FirebaseStorage storage;
    StorageReference storageRootRef;

    //インスタンス
    private static MainActivity mContext = null;
    private static StorageUtils instance = null;

    /**********************************************/
    // instance methods
    /**********************************************/

    public StorageUtils(MainActivity context) {

        mContext = context;
        instance = this;
    }

    public void initStorage(){
        storage = FirebaseStorage.getInstance();
        storageRootRef = storage.getReferenceFromUrl(mContext.getString(R.string.FIREBASE_STORAGE_BUCKET_NAME));
    }

    //FirebaseStorageからpng画像をダウンロードし、ローカルにセーブ
    public void downloadStoragePNGDataAndSaveToFile(StorageType type, String filePath, final OnFetchAndSavedDataListener listener) {
        final String fullPath = GetStorageFullPath(type, filePath);
        StorageReference fileRef = storageRootRef.child(fullPath);

        long maxSize = 1 * 1024 * 1024;
        fileRef.getBytes(maxSize).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] data) {
                String saveFilePath = ActivityUtils.GetDefaultFullPathFromFileName(mContext, fullPath);
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                File f = ActivityUtils.SavePngImageInAssetsToFileExternal(mContext, bitmap, fullPath);
                if (f != null) {
                    listener.onCompletion(true, saveFilePath);
                } else {
                    listener.onCompletion(false, "");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                ActivityUtils.DebugErrorPrint("Error! storage filename = " + fullPath + exception);
                listener.onCompletion(false, "");
            }
        });
    }

    //FirebaseStorageからダウンロード
    public void downloadStorageDataAsString(StorageType type, String filePath, final OnFetchDataListener listener) {
        final String fullPath = GetStorageFullPath(type, filePath);
        StorageReference fileRef = storageRootRef.child(fullPath);

        long maxSize = 1 * 1024 * 1024;
        fileRef.getBytes(maxSize).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] data) {
                try {
                    String str = new String(data, "UTF-8");
                    listener.onCompletion(true, str);
                } catch (IOException e) {
                    listener.onCompletion(false, "");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                ActivityUtils.DebugErrorPrint("Error! storage filename = " + fullPath + exception);
                listener.onCompletion(false, "");
            }
        });
    }

    //Storageに文字列をアップロード
    public void uploadStorageDataWithString(StorageType type, String filePath, String str, final OnUploadDataListener listener) {
        try {
            byte[] data = str.getBytes("UTF-8");

            String fullPath = GetStorageFullPath(type, filePath);
            StorageReference fileRef = storageRootRef.child(fullPath);

            StorageMetadata metadata = new StorageMetadata.Builder().setContentType("application/json").build();

            UploadTask uploadTask = fileRef.putBytes(data, metadata);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    listener.onCompletion(true);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    listener.onCompletion(false);
                }
            });

        } catch(IOException e){
            listener.onCompletion(false);
        }
    }

    public void deleteStorageData(StorageType type, String filePath, final OnUploadDataListener listener) {
        String fullPath = GetStorageFullPath(type, filePath);
        StorageReference fileRef = storageRootRef.child(fullPath);

        fileRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void a) {
                listener.onCompletion(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                listener.onCompletion(false);
            }
        });
    }


    /*****************************************************/
    /*  static  */
    /*****************************************************/

    //ストレージタイプからフルパスを取得
    public static String GetStorageFullPath(StorageType type, String filePath) {
        String fullPath = "";
        if (type == StorageType.Public) {
            fullPath = fullPath + "public/";
        }
        else if (type == StorageType.Common) {
            fullPath = fullPath + "common/";
        }
        else if (type == StorageType.Settings) {
            fullPath = fullPath + "settings/";
        }
        else if (type == StorageType.Readable) {
            fullPath = fullPath + "readable/";
            fullPath = fullPath + UserUtils.Native_GetUserAuthId();
        }
        else if (type == StorageType.User) {
            fullPath = fullPath + "user/";
            fullPath = fullPath + UserUtils.Native_GetUserAuthId();
        }
        else if (type == StorageType.Special) {
            fullPath = fullPath + "special/";
        }
        else if (type == StorageType.Admin) {
            fullPath = fullPath + "admin/";
        }
        return fullPath + filePath;
    }


    /*****************************************************/
    /*  Native  */
    /*****************************************************/

    public static void Native_InitStorage() {
        instance.initStorage();
    }

    public static void Native_GetStorageDataAndSaveToFile(int type, String path) {
        instance.downloadStoragePNGDataAndSaveToFile(StorageType.values()[type], path, new OnFetchAndSavedDataListener() {
            @Override
            public void onCompletion(boolean success, String filePath) {
                getStorageDataAndSaveToFileCallbackJni(success, filePath);
            }
        });
    }

    public static void Native_GetStorageDataAsString(int type, String path) {
        instance.downloadStorageDataAsString(StorageType.values()[type], path, new OnFetchDataListener(){
            @Override
            public void onCompletion(boolean success, String str){
                getStorageDataAsStringCallbackJni(success, str);
            }
        });
    }

    public static void Native_SetStorageDataWithString(int type, String path, String str) {
        instance.uploadStorageDataWithString(StorageType.values()[type], path, str, new OnUploadDataListener() {
            @Override
            public void onCompletion(boolean success) {
                setStorageDataWithStringCallbackJni(success);
            }
        });
    }

    public static void Native_DeleteStorageData(int type, String path) {
        instance.deleteStorageData(StorageType.values()[type], path, new OnUploadDataListener() {
            @Override
            public void onCompletion(boolean success) {
                setStorageDataWithStringCallbackJni(success);
            }
        });
    }

    public static native void getStorageDataAndSaveToFileCallbackJni(boolean succeeded, String path);
    public static native void getStorageDataAsStringCallbackJni(     boolean succeeded, String text);
    public static native void setStorageDataWithStringCallbackJni(   boolean succeeded);

}
