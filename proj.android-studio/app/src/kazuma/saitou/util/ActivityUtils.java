package kazuma.saitou.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.BuildConfig;

public class ActivityUtils {

    private static MainActivity mContext = null;
    private static ActivityUtils instance = null;

    private String mMessage = null;
    private int mResourceId = 0;

    //インスタンシエート
    public ActivityUtils(MainActivity context) {
        mContext = context;
        instance = this;
    }


    /**********************************************/
    // Toast
    /**********************************************/

    //トーストを表示
    public static void ShowToast(String message) {

        instance.mMessage = message;

        instance.mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (instance.mContext != null) {
                    Toast.makeText(instance.mContext, instance.mMessage, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    //トーストを表示
    public static void ShowToast(Context context, String message) {
        instance.mMessage = message;

        instance.mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(instance.mContext, instance.mMessage, Toast.LENGTH_LONG).show();
            }
        });
    }
    
    //トーストを表示(リソースID指定式)
    public static void ShowToast(Context context, int resourceId) {
        instance.mResourceId = resourceId;

        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(instance.mContext, instance.mResourceId, Toast.LENGTH_LONG).show();
            }
        });
    }


    /**********************************************/
    // 他のアプリ
    /**********************************************/

    //指定のアプリケーションがインストールされているかを返す
    public static boolean IsApplicationInstalled(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
        	//showToast(context, "インストールされていません");
            return false;
        }
        return true;
    }


    /**********************************************/
    // バージョン情報
    /**********************************************/

    //アプリケーションのバージョンコードを返す
    public static int GetVersionCode(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("パッケージ名が見つかりません : " + e);
        }
    }

    //バージョン名を返す
    public static String GetVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (NameNotFoundException e) {
            return "";
        }
    }

    /**********************************************/
    // 日時
    /**********************************************/

    // 現在時刻を1970/1/1からのミリ秒で返す
    public static long GetNowMsec() {
        return System.currentTimeMillis();
    }


    /**********************************************/
    // math
    /**********************************************/

    // 0.0~1.0
    public static double GetRand() {
        return Math.random();
    }

    // [min, max) //minを含みmaxを含まない
    public static float GetFRand(float min, float max) {
        return min + (max - min) * (float) Math.random();
    }

    // [min, max] //minもmaxも含む
    public static int GetIntRand(int min, int max) {
        return min + (int) (Math.random() * (max - min + 1));
    }

    /**********************************************/
    // ファイル、パス
    /**********************************************/

    //外部保存パスを返す
    public static String GetDefaultDirectory(Context context) {
        return Environment.getExternalStorageDirectory().toString() + "/" + context.getPackageName();
    }

    //外部保存パスにして返す
    public static String GetDefaultFullPathFromFileName(Context context, String fileName){
        return GetDefaultDirectory(context) + "/" + fileName;
    }

    //ランダムに一時ファイル名を返す
    public static String GetTemporaryFileName(){
        return "__temporary_" + GetNowMsec() + ".tmp";
    }

    //ファイルを保存
    public static boolean SaveStringToFilePath(Context context, String filePath, String data){
        try {
            FileOutputStream outStream = context.openFileOutput(filePath, Context.MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(outStream);
            writer.write(data);
            writer.flush();
            writer.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**********************************************/
    // 画像
    /**********************************************/

    //ファイル名から(完全なパスでなくて良い)Fileインスタンスを返す。ファイルはassets内も外も両対応
    public static File GetFileFromBitmapFileName(Context context, String fname) {
        Bitmap bitmap = GetBitmapFromFileInAssets(context, fname);

        //assets内のファイルは、一度外部に保存してから File インスタンスを取得
        if (bitmap != null) {
            return SavePngImageInAssetsToFileExternal(context, bitmap, GetTemporaryFileName());
        }
        else{
            return new File(fname);
        }
    }

    //Bitmapを保存(PNG限定)
    public static File SavePngImageInAssetsToFileExternal(Context context, Bitmap bitmap, String newFilename){
        return SaveImageInAssetsToFileExternal(context, bitmap, newFilename, Bitmap.CompressFormat.PNG, 80);
    }

    //Bitmapを保存(画像フォーマット指定)
    public static File SaveImageInAssetsToFileExternal(Context context, Bitmap bitmap, String newFilename, Bitmap.CompressFormat format, int quality) {
        File file = new File(GetDefaultDirectory(context), newFilename);

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs(); // 注意：マニフェストに android.permission.WRITE_EXTERNAL_STORAGE が必要
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(format, quality, fileOutputStream);
        } catch (FileNotFoundException e){
            return null;
        }

        return file;
    }

    //画像ファイルが assets 内に存在するかどうか
    public static boolean IsBitmapFileExistsInAssets(Context context, String fname) {
        return GetBitmapFromFileInAssets(context, fname) != null;
    }

    //assets 内の画像ファイルをBitmapで取得
    public static Bitmap GetBitmapFromFileInAssets(Context context, String fname) {
        try {
            return BitmapFactory.decodeStream(context.getResources().getAssets().open(fname));
        } catch(IOException e) {
            return null;
        }
    }

    //外部のファイルをBitmapで取得
    public static Bitmap GetBitmapFromFileName(Context context, String fname) {
        return BitmapFactory.decodeFile(GetDefaultFullPathFromFileName(context, fname));
    }

    //ファイル名をBitmapで取得。assets内外どちらも対応
    public static Bitmap GetBitmapFromFile(Context context, String fname) {
        Bitmap bitmap = GetBitmapFromFileInAssets(context, fname);
        if (bitmap == null) {
            bitmap = GetBitmapFromFileName(context, fname);
        }
        return bitmap;
    }

    /**********************************************/
    // debug
    /**********************************************/
    public static boolean IsDebug() {
        return BuildConfig.DEBUG;
    }

    public static void DebugPrint(String str){
        if (BuildConfig.DEBUG){
            System.out.println(str);
        }
    }

    public static void DebugErrorPrint(String str){
        if (BuildConfig.DEBUG) {
            System.err.println(str);
        }
    }
}
