package kazuma.saitou.util;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import kazuma.saitou.rolling.MainActivity;

/**
 * Created by otaniatsushi1 on 16/07/30.
 */
public class AnalyticsUtils {
    /***********************************/
    /*  variables  */
    /***********************************/
    private FirebaseAnalytics mFirebaseAnalytics;

    private static AnalyticsUtils instance;
    private static Activity mContext;

    /***********************************/
    /* instance methods  */
    /***********************************/

    public AnalyticsUtils(MainActivity context) {
        mContext = context;
        instance = this;

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        String userId = String.valueOf(context.GetUserId());
        mFirebaseAnalytics.setUserId(userId);
    }

    public void analyticsCustom(String eventName, Bundle params) {
        mFirebaseAnalytics.logEvent(eventName, params);
    }

    /***********************************/
    /* static methods  */
    /***********************************/

    public static void Native_Analytics(String category, String action, String label, int value) {
        Native_Analytics(category, action, label, (long)value);
    }

    public static void Native_Analytics(String category, String action, String label, long value) {
        Bundle params = new Bundle();
        params.putString("category", category);
        params.putString("action", action);
        params.putString("label", label);
        params.putString("value", String.valueOf(value));
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
    }

    public static void Native_AnalyticsCustom(String eventName, Bundle params) {
        instance.analyticsCustom(eventName, params);
    }

    public static void Native_AnalyticsCustom(String eventName, String key1, String value1){
        Bundle params = new Bundle();
        params.putString(key1, value1);
        instance.analyticsCustom(eventName, params);
    }

    public static void Native_ErrorReport(String message){
        Bundle params = new Bundle();
        params.putString("message", message);

        instance.mFirebaseAnalytics.logEvent("error_report", params); //イベント名やパラメータ名はiOSと揃えること
    }

    public static void Native_AnalyticsTutorialBegin(){
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_BEGIN, null);
    }

    public static void Native_AnalyticsTutorialEnded(){
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_COMPLETE, null);
    }

    public static void Native_AnalyticsAppOpen(){
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, null);
    }

    public static void Native_AnalyticsShare(String contentType, String itemId){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        params.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, params);
    }

    public static void Native_AnalyticsShareTwitter(String itemId){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Twitter");
        params.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, params);
    }

    public static void Native_AnalyticsShareFacebook(String itemId){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, itemId);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, params);
    }

    public static void Native_AnalyticsEcommercePurchaseBegin(String itemName){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.COUPON, itemName);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, params);
    }

    public static void Native_AnalyticsEcommercePurchase(int price, String itemName){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CURRENCY, "JPY");
        params.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(price));
        params.putString(FirebaseAnalytics.Param.COUPON, itemName);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, params);
    }

    public static void Native_AnalyticsEcommercePurchase(String itemName){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.COUPON, itemName);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, params);
    }

    public static void Native_AnalyticsSpendVirtualCurrency(String itemName, String currencyName, int value){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        params.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, currencyName);
        params.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(value));
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, params);
    }

    public static void Native_AnalyticsBuyItem(String itemName){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, params);
    }

    public static void Native_AnalyticsBuyItem(String itemName, int price){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        params.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(price));
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, params);
    }

    public static void Native_AnalyticsUnlockAchievement(String achievementId){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, achievementId);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT, params);
    }

    public static void Native_AnalyticsSelectContent(String contentName){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentName);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
    }

    public static void Native_AnalyticsSelectContent(String contentName, String itemId){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentName);
        params.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
    }

    public static void Native_AnalyticsSelectContent(String contentName, int itemId){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentName);
        params.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(itemId));
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
    }

    public static void Native_AnalyticsReview(){
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "review");
        instance.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
    }

}
