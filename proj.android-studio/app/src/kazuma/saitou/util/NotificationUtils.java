package kazuma.saitou.util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.util.Calendar;
import java.util.Map;

import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.R;

/**
 * Created by otaniatsushi1 on 16/02/21.
 */
public class NotificationUtils {
    /**********************************************/
    // Native_Notification
    /**********************************************/
    private static NotificationUtils instance;
    private static MainActivity mContext;

	private int localNotificationTag          = 0;
	private int cancelledLocalNotificationTag = 0;
	private final String LOCAL_NOTIFICATION_TAG_PREFERENCE_KEY           = "local_notification_tag";
	private final String CANCELLED_LOCAL_NOTIFICATION_TAG_PREFERENCE_KEY = "cancelled_local_notification_tag";



    /**********************************************/
    // instance methods
    /**********************************************/

    public NotificationUtils(MainActivity context) {
        mContext = context;
        instance = this;

		localNotificationTag = SaveUtils.LoadInt(LOCAL_NOTIFICATION_TAG_PREFERENCE_KEY, 0);
		cancelledLocalNotificationTag = SaveUtils.LoadInt(CANCELLED_LOCAL_NOTIFICATION_TAG_PREFERENCE_KEY, 0);

        //アラーム通知が受信できるよう登録
        IntentFilter filter = new IntentFilter();
        filter.addAction(getActionName());
        context.registerReceiver(new AlarmReceiver(), filter);
    }

    private String getActionName(){
        return mContext.getPackageName() + ".alarmforlocalnotification";
    }


    // ローカル通知用のPendingIntentを作成する
	private PendingIntent makePendingIntentForAlarm(int tag, String message) {
		Intent intent = new Intent(mContext.getApplicationContext(), AlarmReceiver.class);
        intent.setAction(getActionName());
		intent.putExtra("notification_id", tag);
		intent.putExtra("message", message);
		PendingIntent sender = PendingIntent.getBroadcast(mContext, tag, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		return sender;
	}

	// ローカル通知用のPendingIntentを返す（データは入れない）
	private PendingIntent getPendingIntentForAlarm(int tag) {
		Intent intent = new Intent(mContext.getApplicationContext(), AlarmReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(mContext, tag, intent, PendingIntent.FLAG_NO_CREATE);
		return sender;
	}

	// 指定秒後の時刻をミリ秒で返す
	private long getTimeInMillisSinceNow(int secSinceNow) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, secSinceNow);
		return calendar.getTimeInMillis();
	}

	// 指定秒後に指定の文字列でローカル通知を行う
    //   正確には、指定秒後にアラーム通知を行う。アラーム通知の付随情報の中に、ローカル通知に表示するテキストなどの情報を含んでいる
	public void scheduleAlarmForLocalNotification(int secSinceNow, String text) {

		PendingIntent intentOnAlarm = makePendingIntentForAlarm(++localNotificationTag, text);

		long time = getTimeInMillisSinceNow(secSinceNow);
		AlarmManager am = (AlarmManager) mContext.getSystemService(mContext.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, time, intentOnAlarm);

		SaveUtils.SaveInt(LOCAL_NOTIFICATION_TAG_PREFERENCE_KEY, localNotificationTag);
	}

	// 登録されているローカル通知をすべて削除する
	public void unscheduleAllAlarmsForLocalNotification() {
		AlarmManager am = (AlarmManager) mContext.getSystemService(mContext.ALARM_SERVICE);

		for (int tag = cancelledLocalNotificationTag + 1; tag <= localNotificationTag; ++tag) {
			PendingIntent intentOnAlarm = getPendingIntentForAlarm(tag);
            if (intentOnAlarm != null) {
                am.cancel(intentOnAlarm);
            }
		}
		cancelledLocalNotificationTag = localNotificationTag;
		SaveUtils.SaveInt(CANCELLED_LOCAL_NOTIFICATION_TAG_PREFERENCE_KEY, cancelledLocalNotificationTag);
	}


	/**********************************************/
    // static classes
    /**********************************************/

	// AlarmManagerレシーブ用のクラス
    // AlarmManagerで指定した時刻になると、Intentを持って呼び出されるので
        // Intentの付随情報を元に、ローカル通知を発生させる
	// マニフェストにも記述のこと。また、必ずstaticをつけて宣言すること
	public static class AlarmReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
            if (ActivityUtils.IsDebug()) {
                //Toast.makeText(context, "onReceive", Toast.LENGTH_SHORT).show();
            }

			int notificationId = intent.getIntExtra("notification_id", 0);
			String message = intent.getStringExtra("message");

            //通知のタップ時に行う処理(Intent)
            Intent intentOnNotifyTapped = new Intent(context, MainActivity.class); // つまりこのアプリの起動
            intentOnNotifyTapped.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            PendingIntent pendingIntentOnNotifyTapped = PendingIntent.getActivity(context, 0, intentOnNotifyTapped, PendingIntent.FLAG_UPDATE_CURRENT);

            //ローカル通知内容
            Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setTicker(message)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntentOnNotifyTapped)
                .build();

            //通知を作成・発信
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(notificationId, notification);
		}
	}

    //起動時にプッシュ通知が飛んできた場合の処理の起点
    public static class PushNotificationReceiver extends FirebaseMessagingService {
        @Override
        public void onMessageReceived(RemoteMessage remoteMessage) {
            if (ActivityUtils.IsDebug()) {
                //Toast.makeText(context, remoteMessage.toString(), Toast.LENGTH_SHORT).show();
            }

            // TODO(developer): Handle FCM messages here.
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            ActivityUtils.DebugPrint("From: " + remoteMessage.getFrom());

            //通知テキスト本文を取得
            if (remoteMessage.getNotification() != null) {

                //本文があれば、トーストで表示しておく
                String message = remoteMessage.getNotification().getBody();
                if ((message != null) && (message.length() > 0)) {
                    ActivityUtils.ShowToast(message);
                }
                //ActivityUtils.DebugPrint("Message Notification Body: " + remoteMessage.getNotification().getBody());
            }

            //付随データ取得
            if ((remoteMessage.getData() != null) && (remoteMessage.getData().size() > 0)) {
                ActivityUtils.DebugPrint("Message data payload: " + remoteMessage.getData());

                Map<String, String> data = remoteMessage.getData();

                if (data.get("_NOTICE_TITLE").length() > 0){
                    HttpUtils.ShowAlertViewWithUrlJump(
                            data.get("_NOTICE_TITLE"),
                            data.get("_NOTICE_MESSAGE"),
                            data.get("_NOTICE_YES"),
                            data.get("_NOTICE_NO"),
                            data.get("_NOTICE_URL"));
                }
            }

            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
        }
    }

    /**********************************************/
    // static methods
    /**********************************************/

    // 指定秒後に指定の文字列でローカル通知を行う
	public static void Native_ScheduleLocalNotification(int secSinceNow, String text) {
		instance.scheduleAlarmForLocalNotification(secSinceNow, text);
	}

	// 登録されているローカル通知をすべて削除する
	public static void Native_UnscheduleAllLocalNotifications() {
		instance.unscheduleAllAlarmsForLocalNotification();
	}
}
