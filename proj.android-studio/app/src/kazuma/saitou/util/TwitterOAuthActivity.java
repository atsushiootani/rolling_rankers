package kazuma.saitou.util;

import kazuma.saitou.rolling.R;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

//Twitter認証用画面アクティビティ。layout.xmlも忘れずに作成のこと
public class TwitterOAuthActivity extends Activity {

	public static final String INTENT_VERIFIER_QUERY_KEY = "oauth_verifier";

	private String mCallbackURL;
	private Twitter mTwitter;
	private RequestToken mRequestToken;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_oauth);

		// 起動元から情報を取得する
        mCallbackURL = getString(R.string.twitter_callback_url);

		if (TwitterUtils.IsAuthorized()) {
			finish();
		}
		else{
			startAuthorize();
		}
	}

	//OAuth認証（厳密には認可）を開始
	private void startAuthorize() {
        // 認証開始。Twitterインスタンスを取得
        mTwitter = TwitterUtils.OnAuthorizeStarted();

		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					ActivityUtils.DebugPrint(mCallbackURL);

					//認証用ページのurlを取得
					mRequestToken = mTwitter.getOAuthRequestToken(mCallbackURL);
					return mRequestToken.getAuthorizationURL();
				} catch (TwitterException e) {
                    e.printStackTrace();

                    TwitterUtils.OnAuthorizeFailed();
                    finish();
				}
				return null;
			}

			@Override
			protected void onPostExecute(String url) {
				if (url != null) {
					//認証用ページへ飛ぶ
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(intent);
				} else {
					// 失敗
                    TwitterUtils.OnAuthorizeFailed();
                    finish();
				}
			}
		};
		task.execute();
		
		// OAuth認証実行状態を保存する
		//mSaveUtils.saveInt(PREFERENCE_STATUS_KEY, STATE_OAUTH_PROCESSING);
	}

	//Twitterの認証ページからアプリに戻ってきた時の処理
	@Override
	public void onNewIntent(Intent intent) {
		if (intent == null || intent.getData() == null || !intent.getData().toString().startsWith(mCallbackURL)) {
			return;
		}

		String verifier = intent.getData().getQueryParameter(INTENT_VERIFIER_QUERY_KEY);

		// 認証をキャンセルしていた場合は、何もせずゲーム画面に戻る
		if (verifier == null) {
            TwitterUtils.OnAuthorizeCancelled();
            finish();
			return;
		}

		// アクセストークンを取得する
		AsyncTask<String, Void, AccessToken> task = new AsyncTask<String, Void, AccessToken>() {

            // アクセストークンを取得する処理
			@Override
			protected AccessToken doInBackground(String... params) {
				try {
					return mTwitter.getOAuthAccessToken(mRequestToken, params[0]);
				} catch (TwitterException e) {
					e.printStackTrace();
				}
				return null;
			}

            // アクセストークンを取得した結果
			@Override
			protected void onPostExecute(AccessToken accessToken) {
				// アクセストークンが取得できていれば、認証成功
                if (accessToken != null) {
                    TwitterUtils.OnAuthorizeSucceeded(accessToken);
                    finish();
				}
                // アクセストークンが取得できなければ、認証失敗
                else {
                    TwitterUtils.OnAuthorizeFailed();
                    finish();
				}
			}
		};
		task.execute(verifier);
	}
}
