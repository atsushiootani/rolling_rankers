package kazuma.saitou.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

import kazuma.saitou.rolling.MainActivity;

/**
 * Created by otaniatsushi1 on 16/02/21.
 */
public class SaveUtils {
    private static Context defaultContext;

    // preferenceはいくつでも作成できるので、インスタンスごとに保持する
    private SharedPreferences mPreference;

    // 初期化。defaultContextの設定だけ
    public SaveUtils(Context context) {
        defaultContext = context;
    }

    // preference名を指定してのインスタンスを作成
    public SaveUtils(Context context, String pref) {
        mPreference = context.getSharedPreferences(pref, context.MODE_PRIVATE);
    }

    private SharedPreferences getPreference(){
        return mPreference;
    }

    public void saveInt(String key, int val){
        SharedPreferences.Editor e = getPreference().edit();
        e.putInt(key, val);
        e.commit();
    }

    public int loadInt(String key, int defaultValue) {
        return getPreference().getInt(key, defaultValue);
    }

    public void saveLong(String key, long val) {
        SharedPreferences.Editor e = getPreference().edit();
        e.putLong(key, val);
        e.commit();
    }

    public long loadLong(String key, long defaultValue) {
        return getPreference().getLong(key, defaultValue);
    }

    public void saveFloat(String key, float val) {
        SharedPreferences.Editor e = getPreference().edit();
        e.putFloat(key, val);
        e.commit();
    }

    public float loadFloat(String key, float defaultValue) {
        return getPreference().getFloat(key, defaultValue);
    }

    public void saveBoolean(String key, boolean val) {
        SharedPreferences.Editor e = getPreference().edit();
        e.putBoolean(key, val);
        e.commit();
    }

    public boolean loadBoolean(String key, boolean defaultValue) {
        return getPreference().getBoolean(key, defaultValue);
    }

    public void saveString(String key, String val) {
        SharedPreferences.Editor e = getPreference().edit();
        e.putString(key, val);
        e.commit();
    }

    public String loadString(String key, String defaultValue) {
        return getPreference().getString(key, defaultValue);
    }

    public void saveStringSet(String key, Set<String> vals) {
        SharedPreferences.Editor e = getPreference().edit();
        e.putStringSet(key, vals);
        e.commit();
    }

    public Set<String> loadStringSet(String key, Set<String> defaultValues) {
        return getPreference().getStringSet(key, defaultValues);
    }

    public void delete(String key) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.remove(key);
        editor.commit();
    }


    /**********************************************/
    // static methods
    /**********************************************/

    private static SharedPreferences getDefaultPreference() {
        return PreferenceManager.getDefaultSharedPreferences(defaultContext);
    }

    public static void SaveInt(String key, int val) {
        SharedPreferences.Editor e = getDefaultPreference().edit();
        e.putInt(key, val);
        e.commit();
    }

    public static int LoadInt(String key, int defaultValue) {
        return getDefaultPreference().getInt(key, defaultValue);
    }

    public static void SaveLong(String key, long val) {
        SharedPreferences.Editor e = getDefaultPreference().edit();
        e.putLong(key, val);
        e.commit();
    }

    public static long LoadLong(String key, long defaultValue) {
        return getDefaultPreference().getLong(key, defaultValue);
    }

    public static void SaveFloat(String key, float val) {
        SharedPreferences.Editor e = getDefaultPreference().edit();
        e.putFloat(key, val);
        e.commit();
    }

    public static float LoadFloat(String key, float defaultValue) {
        return getDefaultPreference().getFloat(key, defaultValue);
    }


    public static void SaveBoolean(String key, boolean val) {
        SharedPreferences.Editor e = getDefaultPreference().edit();
        e.putBoolean(key, val);
        e.commit();
    }

    public static boolean LoadBoolean(String key, boolean defaultValue) {
        return getDefaultPreference().getBoolean(key, defaultValue);
    }

    public static void SaveString(String key, String val) {
        SharedPreferences.Editor e = getDefaultPreference().edit();
        e.putString(key, val);
        e.commit();
    }

    public static String LoadString(String key, String defaultValue) {
        return getDefaultPreference().getString(key, defaultValue);
    }

    public static void SaveStringSet(String key, Set<String> vals) {
        SharedPreferences.Editor e = getDefaultPreference().edit();
        e.putStringSet(key, vals);
        e.commit();
    }

    public static Set<String> LoadStringSet(String key, Set<String> defaultValues) {
        return getDefaultPreference().getStringSet(key, defaultValues);
    }


    public static void Delete(String key) {
        SharedPreferences.Editor editor = getDefaultPreference().edit();
        editor.remove(key);
        editor.commit();
    }
}
