package kazuma.saitou.util;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONObject;

import java.util.Map;

import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.R;

/**
 * Created by otaniatsushi1 on 16/12/22.
 */
public class DatabaseUtils {

    /**********************************************/
    // public members
    /**********************************************/

    //内部クラス
    public static abstract class OnDatabaseSqlListener{
        abstract public void onCompletion(boolean success, String record);
    }


    /**********************************************/
    // private members
    /**********************************************/

    //Firebase
    FirebaseDatabase database;
    StorageReference storageRootRef;

    //インスタンス
    private static MainActivity mContext = null;
    private static DatabaseUtils instance = null;

    /**********************************************/
    // instance methods
    /**********************************************/

    public DatabaseUtils(MainActivity context) {

        mContext = context;
        instance = this;
    }

    public void initDatabase(){
        database = FirebaseDatabase.getInstance();
    }

    //パスで指定したレコード取得
    public void selectRecordAtPath(StorageUtils.StorageType type, String path, final OnDatabaseSqlListener listener){

        String fullPath = GetDatabaseFullPath(type, path);
        DatabaseReference query = database.getReference(fullPath);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                ActivityUtils.DebugPrint(value);
                listener.onCompletion(true, value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                ActivityUtils.DebugErrorPrint(error.toString());
                listener.onCompletion(false, "");
            }
        });
    }

    //パスで指定したレコードを更新or追加
    public void updateRecordAtPath(StorageUtils.StorageType type, String path, final JSONObject/* Map<String, Object>*/ updateData, final OnDatabaseSqlListener listener){

        String fullPath = GetDatabaseFullPath(type, path);
        ActivityUtils.DebugPrint(updateData.toString());
        DatabaseReference query = database.getReference(fullPath);

        query.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                //Map<String, Object> post = mutableData.getValue(Map.class);
                JSONObject post = mutableData.getValue(JSONObject.class);
                if (post == null) {
                    //return Transaction.success(mutableData);
                    post = updateData;
                }
                else{
                    //post.putAll(updateData);
                }

                // Set value and report transaction success
                mutableData.setValue(post);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                if (databaseError != null) {
                    ActivityUtils.DebugErrorPrint(databaseError.toString());
                }

                listener.onCompletion(databaseError == null && committed, dataSnapshot.getValue().toString());
            }
        });
    }

    //パスで指定したレコードを丸々入れ替える
    public void replaceRecordAtPath(StorageUtils.StorageType type, String path, final Map<String, Object> replaceData, final OnDatabaseSqlListener listener) {
        /*
        String fullPath = GetDatabaseFullPath(type, path);
        ActivityUtils.DebugPrint(replaceData.toString());
        DatabaseReference query = database.getReference(fullPath);

        query.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                mutableData.setValue(replaceData);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                if (databaseError != null) {
                    ActivityUtils.DebugErrorPrint(databaseError.toString());
                }

                listener.onCompletion(databaseError == null && committed, dataSnapshot.getValue().toString());
            }
        });
        */
    }

    //パスで指定したレコードを削除
    public void deleteRecordAtPath(StorageUtils.StorageType type, String path, final OnDatabaseSqlListener listener) {
        String fullPath = GetDatabaseFullPath(type, path);
        DatabaseReference query = database.getReference(fullPath);

        if (query != null) {
            query.setValue(null);
            listener.onCompletion(true, "");
        }
        else{
            listener.onCompletion(false, "");
        }
    }


    /*****************************************************/
    /*  static  */
    /*****************************************************/

    public String GetDatabaseFullPath(StorageUtils.StorageType type, String filePath){
        //Storageと同じルール
        return StorageUtils.GetStorageFullPath(type, filePath);
    }


    /*****************************************************/
    /*  Native  */
    /*****************************************************/

    //Database
    public static void Native_InitDatabase() {
        instance.initDatabase();
    }

    public static void Native_SelectDatabaseAtPath(int type, String path) {
        instance.selectRecordAtPath(StorageUtils.StorageType.values()[type], path, new OnDatabaseSqlListener() {
            @Override
            public void onCompletion(boolean success, String record) {
                ActivityUtils.DebugPrint(record);
                selectDatabaseAtPathCallbackJni(success, record);
            }
        });
    }

    public static void Native_UpdateDatabaseAtPath(int type, String path, String jsonRecord) {
        //TODO:jsonRecordをJSONObjectに
        instance.updateRecordAtPath(StorageUtils.StorageType.values()[type], path, null, new OnDatabaseSqlListener() {
            @Override
            public void onCompletion(boolean success, String record) {
                ActivityUtils.DebugPrint(record);
                updateDatabaseAtPathCallbackJni(success, record);
            }
        });
    }

    public static void Native_ReplaceDatabaseAtPath(int type, String path, String jsonRecord) {
        //TODO:jsonRecordをJSONObjectに
        instance.replaceRecordAtPath(StorageUtils.StorageType.values()[type], path, null, new OnDatabaseSqlListener() {
            @Override
            public void onCompletion(boolean success, String record) {
                ActivityUtils.DebugPrint(record);
                replaceDatabaseAtPathCallbackJni(success, record);
            }
        });
    }

    public static void Native_DeleteDatabaseAtPath(int type, String path) {
        instance.deleteRecordAtPath(StorageUtils.StorageType.values()[type], path, new OnDatabaseSqlListener() {
            @Override
            public void onCompletion(boolean success, String record) {
                ActivityUtils.DebugPrint(record);
                deleteDatabaseAtPathCallbackJni(success, record);
            }
        });
    }

    public static native void selectDatabaseAtPathCallbackJni(boolean succeeded, String record);
    public static native void updateDatabaseAtPathCallbackJni(boolean succeeded, String record);
    public static native void replaceDatabaseAtPathCallbackJni(boolean succeeded, String record);
    public static native void deleteDatabaseAtPathCallbackJni(boolean succeeded, String record);


}
