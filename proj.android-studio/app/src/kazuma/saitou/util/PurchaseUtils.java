package kazuma.saitou.util;

import android.content.Context;
import android.content.Intent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.BuildConfig;

/**
 * Created by otaniatsushi1 on 16/02/25.
 */
public class PurchaseUtils {

    /**********************************************/
    // 定数
    /**********************************************/

    // アプリのライセンスキー
    private final static String LISENCE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkBmXctcKNDveEQIUR8N/oj8tsvDmqP1VBLNgV9etb3OGiF2Tz1lkbh39EaEfiiIg4fDV1tlZE871Eo3foz4GiTcQ+sdJDg0WudtjFFF2QO6kLDlCUl312tomnubIgxry1Y+rCpIad/IR+p63Z891FOH4feMLL+fi5lM7x5WX/MF8RWpltasooV8eQnAVf6j53Ui7xiouY8rMHZYoT9n/LdZHtyz/EEeXln+tbu4qYZio7RdS4lGqcTsr9i6j7qyTyWYQ1l1I/bipGLT1SvsnMPwObbkXIHgUwyatVgMYaiHzwshzgNEtdpVmPbDfe+mLQY/lhwmRboDNj2Ivzx/X0QIDAQAB";

    // 商品の種別
    public final static int PRODUCT_TYPE_CONSUMABLE   = 0; //消費型
    public final static int PRODUCT_TYPE_UNCONSUMABLE = 1; //非消費型
    public final static int PRODUCT_TYPE_SUBSCRIPTION = 2; //定期購読型

    static class PurchaseItemData{
        public int productType;
        public String name;
        public String description;
        public String price;

        public PurchaseItemData(int type, String n, String d, String p){
            productType = type;
            name = n;
            description = d;
            price = p;
        }
    }
    private final static HashMap<String, PurchaseItemData> PRODUCT_DATA_MAP = new HashMap<String, PurchaseItemData>(){
        {
            put("product_id_rolling_coin1",       new PurchaseItemData(PRODUCT_TYPE_CONSUMABLE,  "4,000コイン",  "4,000コインが追加されます。", "￥120"));
            put("product_id_rolling_coin2",       new PurchaseItemData(PRODUCT_TYPE_CONSUMABLE, "10,000コイン", "10,000コインが追加されます。", "￥240"));
            put("product_id_rolling_coin3",       new PurchaseItemData(PRODUCT_TYPE_CONSUMABLE, "25,000コイン", "25,000コインが追加されます。", "￥480"));
            put("product_id_rolling_coin4",       new PurchaseItemData(PRODUCT_TYPE_CONSUMABLE, "50,000コイン", "50,000コインが追加されます。", "￥600"));
        }
    };

    private final static List<String> PRODUCT_ID_LIST = new ArrayList<String>(PRODUCT_DATA_MAP.keySet());

    // 購入済みの非消費アイテムを保存するときのpreferenceキー
    private final static String PREFERENCE_PURCHASED_UNCONSUMABLE_PRODUCTS_KEY = "PURCHASED_UNCONSUMABLE_PRODUCTS";

    // 購入処理用のActivityの識別番号
    private final static int REQUEST_CODE_FOR_PURCHASE_INTENT = 10001;

    // google analyticsに記録するときのカテゴリ名
    private final static String ANALYTICS_PURCHASE_LOG_CATEGORY_ID = "PURCHASE_LOG_TRACKER";

    // セットアップステータス
    private final static int SETUP_STATE_NONE       = 0; //最初
    private final static int SETUP_STATE_PROCESSING = 1; //処理中
    private final static int SETUP_STATE_FAILED     = 2; //失敗後
    private final static int SETUP_STATE_SUCCEEDED  = 3; //成功後

    // リストアステータス
    private final static int RESTORE_STATE_NONE       = 0; //最初
    private final static int RESTORE_STATE_PROCESSING = 1; //処理中
    private final static int RESTORE_STATE_FAILED     = 2; //失敗後
    private final static int RESTORE_STATE_CANCELLED  = 3; //キャンセル後
    private final static int RESTORE_STATE_COMPLETED  = 4; //すべてのリストア完了後

    // 購入ステータス
    private final static int PURCHASE_STATE_NONE       = 0; //最初
    private final static int PURCHASE_STATE_PROCESSING = 1; //処理中
    private final static int PURCHASE_STATE_FAILED     = 2; //失敗後
    private final static int PURCHASE_STATE_CANCELLED  = 3; //キャンセル後
    private final static int PURCHASE_STATE_PURCHASED  = 4; //購入後
    private final static int PURCHASE_STATE_CONSUMED   = 5; //消費後


    /**********************************************/
    // 変数
    /**********************************************/

    // 課金処理ヘルパークラス
    private IabHelper mIabHelper = null;

    // 課金アイテム情報
    private Inventory mInventory = null;

    // セットアップが成功したかどうか
    private int mSetupState = SETUP_STATE_NONE;

    // リストア処理中かどうか
    private int mRestoreState = RESTORE_STATE_NONE;
    private int mRestoreCount = 0; //リストアする商品数

    // 購入処理中かどうか
    private int mPurchaseState = PURCHASE_STATE_NONE;

    //インスタンス
    private static MainActivity mContext;
    private static PurchaseUtils instance;


    /**********************************************/
    // コンストラクタ
    /**********************************************/

    public PurchaseUtils(MainActivity context) {
        mContext = context;
        instance = this;
    }


    /**********************************************/
    // セットアップ系メソッド
    /**********************************************/

    // 課金処理のセットアップ（初期化）
    public boolean setup(){
        ActivityUtils.DebugPrint("PURCHASE================setup start");
        if (mSetupState == SETUP_STATE_PROCESSING){
            return false;
        }
        mSetupState = SETUP_STATE_PROCESSING;

        ActivityUtils.DebugPrint("セットアップ開始");

        // インスタンスの生成
        mIabHelper = new IabHelper(mContext, LISENCE_KEY);

        // デバッグ出力
        if (BuildConfig.DEBUG) {
            mIabHelper.enableDebugLogging(true);
        }

        // 課金処理のセットアップ
        mIabHelper.startSetup(mSetupFinishedListener);

        return true;
    }

    // 課金処理のセットアップ完了時コールバック
    private IabHelper.OnIabSetupFinishedListener mSetupFinishedListener = new IabHelper.OnIabSetupFinishedListener() {
        @Override
        public void onIabSetupFinished(IabResult result) {

            // 失敗時は処理終了
            if (!result.isSuccess() || mIabHelper == null) {
                ActivityUtils.DebugPrint("セットアップ失敗: " + result.toString());

                // セットアップ失敗
                onSetupFailed();
                return;
            }

            ActivityUtils.DebugPrint("セットアップ完了");

            try {
                //商品情報を取得（同期）
                mInventory = new Inventory();
                int ret = mIabHelper.querySkuDetails(IabHelper.ITEM_TYPE_INAPP, mInventory, getInAppProductIdList()); //注意: Subscriptionのアイテムもある場合は、別に呼び出すこと

                //成功時
                if (ret == IabHelper.BILLING_RESPONSE_RESULT_OK) {

                    // セットアップ成功
                    onSetupSucceeded();

                    // リストア処理を続けて行うことはしない(完全に別の処理として実行できるため)
                }
                //失敗時
                else {
                    ActivityUtils.DebugPrint("課金商品情報取得に失敗");

                    // セットアップ失敗
                    onSetupFailed();
                }
            } catch (Exception e) {
                e.printStackTrace();
                ActivityUtils.DebugPrint("課金商品情報取得に失敗");

                // セットアップ失敗
                onSetupFailed();
            }
        }
    };

    // セットアップ成功処理
    private void onSetupSucceeded() {
        mSetupState = SETUP_STATE_SUCCEEDED;
        purchaseLoadedCallbackJni(true);
    }

    // セットアップ失敗処理
    private void onSetupFailed() {
        mSetupState = SETUP_STATE_FAILED;
        purchaseLoadedCallbackJni(false);
    }

    // 課金の終了処理
    public void unsetup(){
        ActivityUtils.DebugPrint("PURCHASE================unsetup start");
        if (mIabHelper != null) {
            mIabHelper.disposeWhenFinished();
            mIabHelper = null;
        }
        mSetupState = SETUP_STATE_NONE;
    }

    // 初期化処理が終わったかどうか（成功・失敗を問わず）
    public boolean hasSetupFinished() {
        ActivityUtils.DebugPrint("PURCHASE================setup state=" + mSetupState);
        return mSetupState > SETUP_STATE_PROCESSING;
    }

    // 初期化が成功したかどうか
    public boolean hasSetupSucceeded(){
        return mSetupState == SETUP_STATE_SUCCEEDED;
    }


    /**********************************************/
    // リストア系メソッド
    /**********************************************/

    // リストア処理
    public boolean restore() {
        if (mRestoreState == RESTORE_STATE_PROCESSING || mSetupState != SETUP_STATE_SUCCEEDED) {
            return false;
        }
        mRestoreState = RESTORE_STATE_PROCESSING;
        mRestoreCount = 0;

        ActivityUtils.DebugPrint("リストア開始");

        // リストア処理はメインスレッドでないと実行できない(queryInventoryAsync()内で new Handler() をしているため)
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ActivityUtils.DebugPrint(Thread.currentThread().getName());
                try{
                    mIabHelper.queryInventoryAsync(true, getInAppProductIdList(), getSubscriptionProductIdList(), mGotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    onRestoreFailed();
                    return;
                }
            }
        });

        return true;
    }

    // リストア完了時のコールバック
    private IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            // 失敗時は、コールバックを呼び出し
            if (result.isFailure() || mIabHelper == null) {
                ActivityUtils.DebugErrorPrint("リストア失敗: " + result.toString());

                // リストア失敗
                onRestoreFailed();
                return;
            }

            // リストアの個数を取得
            List<String> allOwnedSkus = inventory.getAllOwnedSkus();
            for (String productId : allOwnedSkus) {
                if (isProductTypeConsumable(productId)) {
                    onRestoreStarted(productId);
                }
            }

            // 商品1つずつリストアコールバック呼び出す
            for (String productId : allOwnedSkus) {
                Purchase purchase = inventory.getPurchase(productId);

                // 消費アイテムの場合、購入したが消費する前に何らかのエラーが発生したということなので、すぐさま消費してゲームに反映する
                if (isProductTypeConsumable(productId)) {

                    // 消費処理を実行(mConsumeFinishedListenerの中で onRestoreSucceeded() が呼び出される)
                    try{
                        mIabHelper.consumeAsync(inventory.getPurchase(productId), mConsumeFinishedListener);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        onRestoreFailed();
                        return;
                    }

                    // 記録
                    ActivityUtils.DebugPrint("リストア処理: " + productId + ", orderId=" + purchase.getOrderId());
                    AnalyticsUtils.Native_Analytics(ANALYTICS_PURCHASE_LOG_CATEGORY_ID, "restoring", purchase.getOrderId(),
                            mContext.GetUserId());
                }
                // 非消費アイテムの場合、記録しておく
                else{
                    addUnconsumableProduct(productId);
                }
            }

            ActivityUtils.DebugPrint("リストア処理終了");
        }
    };

    // リストア開始時の処理
    private void onRestoreStarted(String productId) {
        ++mRestoreCount; // リストア必要数を記録
        ActivityUtils.DebugPrint("onRestoreStarted: " + mRestoreCount);
    }

    // リストア成功時の処理
    private void onRestoreSucceeded(String productId) {
        restoreSuccessCallbackJni(productId);
        --mRestoreCount;
        ActivityUtils.DebugPrint("onRestoreSucceeded: " + mRestoreCount);

        // リストアが全て終わったら、完了コールバックを呼び出す
        if (mRestoreCount == 0) {
            onRestoreCompleted();
        }
    }

    // リストア失敗時の処理
    private void onRestoreFailed() {
        mRestoreState = RESTORE_STATE_FAILED;
        restoreFailureCallbackJni();
        restoreClosedCallbackJni();
    }

    // リストア完了時の処理
    private void onRestoreCompleted() {
        mRestoreState = RESTORE_STATE_COMPLETED;
        restoreCompletedCallbackJni();
        restoreClosedCallbackJni();
    }


    /**********************************************/
    // 購入系メソッド
    /**********************************************/

    // 購入処理の開始
    public boolean purchase(String productId) {
        if (mPurchaseState == PURCHASE_STATE_PROCESSING || mPurchaseState == PURCHASE_STATE_PURCHASED || mSetupState != SETUP_STATE_SUCCEEDED) {
            return false;
        }
        mPurchaseState = PURCHASE_STATE_PROCESSING;

        ActivityUtils.DebugPrint("購入処理を開始");

        // 購入処理を実行(内部でstartActivityForResult()を呼んでいる)
        try{
            mIabHelper.launchPurchaseFlow(mContext, productId,
                isProductTypeInApp(productId) ? IabHelper.ITEM_TYPE_INAPP : IabHelper.ITEM_TYPE_SUBS, null,
                REQUEST_CODE_FOR_PURCHASE_INTENT, mPurchaseFinishedListener, null);
        } catch (IabHelper.IabAsyncInProgressException e) {
            onPurchaseFailed();
            return false;
        }

        return true;
    }

    // 購入処理が結果を返したときの処理(MainActivityから呼び出される)
    public boolean onPurchaseActivityResult(int requestCode, int resultCode, Intent data) {
        if (mIabHelper != null) {
            boolean processed = mIabHelper.handleActivityResult(requestCode, resultCode, data); //この内部で mPurchaseFinishedListener を呼び出している(processed==trueの場合)
            return processed;
        }
        return false;
    }

    // 購入処理完了時のコールバック
    private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            ActivityUtils.DebugPrint("onIabPurchaseFinished result:" + result + ", purchase: " + purchase);

            // キャンセル
            if (result.getResponse() == IabHelper.IABHELPER_USER_CANCELLED){
                ActivityUtils.DebugErrorPrint("購入キャンセル: " + result.toString());

                onPurchaseCancelled();
                return;
            }

            // 失敗
            if (result.isFailure() || mIabHelper == null){
                ActivityUtils.DebugErrorPrint("購入処理失敗: " + result.toString());

                onPurchaseFailed();
                return;
            }

            // 記録
            String productId = purchase.getSku();
            ActivityUtils.DebugPrint("購入処理成功: " + productId + ", orderId=" + purchase.getOrderId());
            AnalyticsUtils.Native_Analytics(ANALYTICS_PURCHASE_LOG_CATEGORY_ID, "purchase_finished", purchase.getOrderId(),
                    mContext.GetUserId());

            // 消費型アイテムの場合
            if (isProductTypeConsumable(productId)) {

                //購入成功
                onPurchasePurchased(productId);

                //続けて商品の消費処理を行う
                try{
                    mIabHelper.consumeAsync(purchase, mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    onPurchaseFailed();
                    return;
                }
            }
            // 非消費型アイテムの場合、完了
            else{
                onPurchaseConsumed(productId);
            }

            // この時点ではまだアイテム付与などゲーム内に反映は行わない
            ActivityUtils.DebugPrint("あなたの商品：" + productId + "を購入しました。");
            ActivityUtils.DebugPrint("INAPP_PURCHASE_DATAのJSONは：" + purchase.getOriginalJson());
        }
    };

    // 購入時の処理
    private void onPurchasePurchased(String productId) {
        mPurchaseState = PURCHASE_STATE_PURCHASED;
        // CallbackJni は、CONSUMED のタイミングで呼ぶ
    }

    // 購入失敗時の処理
    private void onPurchaseFailed(){
        mPurchaseState = PURCHASE_STATE_FAILED;
        purchaseFailureCallbackJni("");
        purchaseClosedCallbackJni();
    }

    // 購入キャンセル時の処理
    private void onPurchaseCancelled(){
        mPurchaseState = PURCHASE_STATE_CANCELLED;
        purchaseCancelCallbackJni("");
        purchaseClosedCallbackJni();
    }


    /**********************************************/
    // 消費系メソッド
    /**********************************************/

    // 消費処理が完了したときのコールバック. PurchaseからもRestoreからも呼ばれることに注意
    private IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            ActivityUtils.DebugPrint("Consumption finished. Purchase: " + purchase + ", result: " + result);

            // 失敗時
            if (result.isFailure() || mIabHelper == null) {
                ActivityUtils.DebugErrorPrint("消費処理失敗:" + result.toString());

                // 購入時の失敗
                if (mPurchaseState == PURCHASE_STATE_PURCHASED) {
                    onPurchaseFailed();
                }
                // リストア時の失敗
                else if (mRestoreState == RESTORE_STATE_PROCESSING){
                    onRestoreFailed();
                }
                else{
                    ActivityUtils.DebugErrorPrint("OnConsumeFinishedListener failure error: 購入処理中状態でもリストア処理中状態でもない, purchase=" + mPurchaseState + ", restore=" + mRestoreState);
                }
                return;
            }

            String productId = purchase.getSku();

            // 購入処理だった場合
            if (mPurchaseState == PURCHASE_STATE_PURCHASED) {
                // 購入成功により、ゲームに反映する
                onPurchaseConsumed(productId);
            }
            // リストア処理だった場合
            else if (mRestoreState == RESTORE_STATE_PROCESSING){
                // リストア成功により、ゲームに反映する
                onRestoreSucceeded(productId);
            }
            else{
                ActivityUtils.DebugErrorPrint("OnConsumeFinishedListener success error: 購入処理中状態でもリストア処理中状態でもない, purchase=" + mPurchaseState + ", restore=" + mRestoreState);
            }

            // 記録
            AnalyticsUtils.Native_Analytics(ANALYTICS_PURCHASE_LOG_CATEGORY_ID, "consume_finished", purchase.getOrderId(),
                    mContext.GetUserId());
        }
    };

    // 消費時の処理
    private void onPurchaseConsumed(String productId) {
        mPurchaseState = PURCHASE_STATE_CONSUMED;
        purchaseSuccessCallbackJni(productId);
        purchaseClosedCallbackJni();
    }


    /**********************************************/
    // 非消費アイテム系メソッド
    /**********************************************/

    // 非消費アイテムをファイルに書き込み
    private void saveUnconsumableProducts(Set<String> products) {
        try {
            File file = new File(mContext.getDir("data", Context.MODE_PRIVATE), PREFERENCE_PURCHASED_UNCONSUMABLE_PRODUCTS_KEY);
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(products);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    // 非消費アイテムをファイルから読み込み
    private Set<String> loadUnconsumableProducts() {
        try {
            File file = new File(mContext.getDir("data", Context.MODE_PRIVATE), PREFERENCE_PURCHASED_UNCONSUMABLE_PRODUCTS_KEY);
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            Object obj = ois.readObject();
            ois.close();
            return (Set<String>)obj;
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
    }

    // 非消費アイテムを1つ追加
    private boolean addUnconsumableProduct(String productId) {
        Set<String> products = (Set<String>) loadUnconsumableProducts();
        if (products == null) {
            products = new HashSet<String>();
        }
        products.add(productId);
        saveUnconsumableProducts(products);
        return true;
    }

    // 非消費アイテムを購入済みかどうか返す
    public boolean checkAlreadyPurchasedItem(String productId) {
        if (mRestoreState == RESTORE_STATE_COMPLETED) {
            return loadUnconsumableProducts().contains(productId);
        }
        return false;
    }


    /**********************************************/
    // 情報取得系メソッド
    /**********************************************/

    // 商品名を返す
    public String getProductName(String productId) {
        if (mInventory != null) {
            SkuDetails detail = mInventory.getSkuDetails(productId);
            if (detail != null) {
                String title = detail.getTitle();

                // 末尾に「（アプリ名）」があれば削除
                if (title.lastIndexOf(")") > 0) {
                    int index = title.lastIndexOf(" (");
                    if (index > 0) {
                        return title.substring(0, index);
                    }
                }
                if (title.lastIndexOf("）") > 0) {
                    int index = title.lastIndexOf(" （");
                    if (index > 0) {
                        return title.substring(0, index);
                    }
                }
                return title;
            }
        }
        return PRODUCT_DATA_MAP.get(productId).name;
        //return "";
    }

    // 商品の説明文を返す
    public String getProductDescription(String productId) {
        if (mInventory != null) {
            SkuDetails detail = mInventory.getSkuDetails(productId);
            if (detail != null) {
                return detail.getDescription();
            }
        }
        return PRODUCT_DATA_MAP.get(productId).description;
        //return "";
    }

    // 商品の価格を文字列で取得(通貨単位付き)
    public String getProductPrice(String productId) {
        if (mInventory != null) {
            SkuDetails detail = mInventory.getSkuDetails(productId);
            if (detail != null) {
                return detail.getPrice();
            }
        }
        return PRODUCT_DATA_MAP.get(productId).price;
        //return "";
    }


    // 商品の消費タイプを取得
    public int getProductType(String productId) {
        return PRODUCT_DATA_MAP.get(productId).productType;
    }

    // 商品が消費型かどうか
    public boolean isProductTypeConsumable(String productId) {
        return getProductType(productId) == PRODUCT_TYPE_CONSUMABLE;
    }

    // 商品が非消費型かどうか
    public boolean isProductTypeUnconsumable(String productId) {
        return getProductType(productId) == PRODUCT_TYPE_UNCONSUMABLE;
    }

    // 商品が定期購読型かどうか
    public boolean isProductTypeSubscription(String productId) {
        return getProductType(productId) == PRODUCT_TYPE_SUBSCRIPTION;
    }

    // 商品がInApp型かどうか
    public boolean isProductTypeInApp(String productId) {
        return isProductTypeConsumable(productId) || isProductTypeUnconsumable(productId);
    }

    // 商品IDリストを返す
    public List<String> getProductIdList(){
        return PRODUCT_ID_LIST;
    }

    // InApp型の商品IDリストを返す
    public List<String> getInAppProductIdList() {
        ArrayList<String> lists = new ArrayList<String>();
        for (String productId : PRODUCT_DATA_MAP.keySet()) {
            if (getProductType(productId) != PRODUCT_TYPE_SUBSCRIPTION) {
                lists.add(productId);
            }
        }
        return lists;
    }

    // 定期購読型の商品IDリストを返す
    public List<String> getSubscriptionProductIdList() {
        ArrayList<String> lists = new ArrayList<String>();
        for (String productId : PRODUCT_DATA_MAP.keySet()) {
            if (getProductType(productId) == PRODUCT_TYPE_SUBSCRIPTION) {
                lists.add(productId);
            }
        }
        return lists;
    }

    /**********************************************/
    // ライフサイクル系メソッド
    /**********************************************/

    public void onResume(){
        // 走っている購入処理を止める
        //  MainActivityから呼び出される
        //  コード側で感知できないタイミングで、システムが課金処理を終了するとき(「アカウント認証が必要です」アラートが閉じるなど)のための対応
        //  購入処理中にバックグラウンドへ行き、復帰したときは、呼び出されないので問題ない(システムのアラートが出ているので、MainActivity.onResume()がそもそも呼び出されない)
        if (mPurchaseState == PURCHASE_STATE_PROCESSING) {
            purchaseClosedCallbackJni();
            mPurchaseState = PURCHASE_STATE_CANCELLED;
        }

        // リストア処理も止める(対称性からしただけ。実際の起こりうる状況に対して妥当な処理かは未確認)
        if (mRestoreState == RESTORE_STATE_PROCESSING){
            restoreClosedCallbackJni();
            mRestoreState = RESTORE_STATE_CANCELLED;
        }

        // セットアップ処理を止める必要はないだろう(というか止めてはダメだろう)
    }

    public void onPause(){
        //do nothing
    }

    // 破棄時
    public void onDestroy() {
        //unsetup();
    }



    /**********************************************/
    // static methods
    /**********************************************/

    // 破棄
    public static void Native_PurchaseUnRegister() {
        instance.unsetup();
    }

    // 初期化処理
    public static boolean Native_PurchaseRegister() {
        //return instance.setup();
        return instance.hasSetupSucceeded(); //初期化処理は MainActivity.onCreate()で行うため
    }

    // 初期化が終わったかどうか（成功・失敗を問わない）
    public static boolean Native_PurchaseRequestHasFinished() {
        return instance.hasSetupFinished();
    }

    // 初期化が成功したかどうか
    public static boolean Native_PurchaseInitHasSucceeded(){
        return instance.hasSetupSucceeded();
    }

    // リストア実行
    public static boolean Native_Restore() {
        return instance.restore();
    }

    // 購入する
    public static boolean Native_PurchaseItem(String productId) {
        return instance.purchase(productId);
    }

    // アイテム名取得
    public static String Native_GetProductName(String productId) {
        return instance.getProductName(productId);
    }

    // アイテムの説明文取得
    public static String Native_GetProductDescription(String productId) {
        return instance.getProductDescription(productId);
    }

    // アイテムの価格取得（通貨単位名も含む文字列を返す）
    public static String Native_GetProductPrice(String productId) {
        return instance.getProductPrice(productId);
    }

    // 非消費アイテムを購入済みかどうか
    public static boolean /*Native_*/CheckAlreadyPurchasedItem(String productId) {
        return instance.checkAlreadyPurchasedItem(productId);
    }

    /**********************************************/
    // JNI callback methods
    /**********************************************/

    public static native void purchaseLoadedCallbackJni(boolean success);
    public static native void restoreSuccessCallbackJni(String productId);
    public static native void restoreFailureCallbackJni();
    public static native void restoreCompletedCallbackJni();
    public static native void restoreClosedCallbackJni();
    public static native void purchaseSuccessCallbackJni(String productId);
    public static native void purchaseFailureCallbackJni(String productId);
    public static native void purchaseCancelCallbackJni(String productId);
    public static native void purchaseClosedCallbackJni();
}
