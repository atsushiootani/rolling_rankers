package kazuma.saitou.util;

import android.support.annotation.NonNull;


import kazuma.saitou.rolling.MainActivity;
import kazuma.saitou.rolling.BuildConfig;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;

import java.util.HashMap;

/**
 * Created by otaniatsushi1 on 16/02/21.
 */
public class HttpUtils {
    private static HttpUtils instance;
    private static MainActivity mContext;

    FirebaseRemoteConfig mRemoteConfig;

    /***********************************/
    /* instance methods  */
    /***********************************/

    public HttpUtils(MainActivity context) {
        mContext = context;
        instance = this;

        mRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mRemoteConfig.setConfigSettings(configSettings);

    }

    public boolean initPronounces() {

        loadPronounces();

        return true;
    }

    private void loadPronounces() {
        int cacheTime = 30;
        mRemoteConfig
                .fetch(cacheTime)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //値を反映
                            mRemoteConfig.activateFetched();
                        } else {
                            //fetch失敗
                        }
                    }
                });
    }

    public boolean hasPronounceLoaded() {
        return true;
    }

    private void pronounceCompletedCallback() {
    }

    public int getIntPronounce(String key, int defaultValue) {
        FirebaseRemoteConfigValue value = mRemoteConfig.getValue(key);
        if (value.getSource() == FirebaseRemoteConfig.VALUE_SOURCE_STATIC) {
            return defaultValue;
        }
        return (int) value.asLong();
    }

    public float getFloatPronounce(String key, float defaultValue) {
        FirebaseRemoteConfigValue value = mRemoteConfig.getValue(key);
        if (value.getSource() == FirebaseRemoteConfig.VALUE_SOURCE_STATIC) {
            return defaultValue;
        }
        return (float) value.asDouble();
    }

    public double getDoublePronounce(String key, double defaultValue) {
        FirebaseRemoteConfigValue value = mRemoteConfig.getValue(key);
        if (value.getSource() == FirebaseRemoteConfig.VALUE_SOURCE_STATIC) {
            return defaultValue;
        }
        return value.asDouble();
    }

    public boolean getBooleanPronounce(String key, boolean defaultValue) {
        FirebaseRemoteConfigValue value = mRemoteConfig.getValue(key);
        if (value.getSource() == FirebaseRemoteConfig.VALUE_SOURCE_STATIC) {
            return defaultValue;
        }
        return value.asBoolean();
    }

    public String getStringPronounce(String key, String defaultValue) {
        FirebaseRemoteConfigValue value = mRemoteConfig.getValue(key);
        if (value.getSource() == FirebaseRemoteConfig.VALUE_SOURCE_STATIC) {
            return defaultValue;
        }
        return value.asString();
    }

    /*************************************/
    // lot
    /*************************************/

    public int lotPronounce(HashMap<String, Integer> lotInfo, int defaultValue) {
        int ret = defaultValue;

        Object[] allKeys = lotInfo.keySet().toArray();
        int[] weights = new int[allKeys.length];

        int totalWeights = 0;
        for (int i = 0; i < allKeys.length; ++i) {
            String key = (String) allKeys[i];

            int thisWeight = Native_GetIntPronounce(key, 0);
            totalWeights += thisWeight;
            weights[i] = thisWeight;
        }

        if (totalWeights > 0) {
            int r = ActivityUtils.GetIntRand(0, totalWeights - 1);

            int hitWeight = 0;
            for (int i = 0; i < allKeys.length; ++i) {
                String key = (String) allKeys[i];

                hitWeight += weights[i];
                if (r < hitWeight) {
                    ret = lotInfo.get(key).intValue();
                    break;
                }
            }
        }
        return ret;
    }


    /*************************************/
    //launch alert view
    /*************************************/

    private void launchAlertView(boolean force) {
        String noticeId = getStringPronounce("_NOTICE_ID", "");

        if (noticeId != null && noticeId.length() > 0 &&
                !SaveUtils.LoadBoolean(noticeId, false) || force) {
            String title = getStringPronounce("_NOTICE_TITLE", "");
            String message = getStringPronounce("_NOTICE_MESSAGE", "");
            String yes = getStringPronounce("_NOTICE_YES", "");
            String no = getStringPronounce("_NOTICE_NO", "");
            String url = getStringPronounce("_NOTICE_URL", "");

            showAlertViewWithUrlJump(title, message, yes, no, url);

            SaveUtils.SaveBoolean(noticeId, true);
        }
    }

    String urlOnLaunch; //ロンチ時のアラートビュー用

    public void showAlertViewWithUrlJump(String title, String message, String positiveText, String negativeText, String url) {
        if (positiveText == null || positiveText.length() <= 0) {
            positiveText = null;
        }

        if (url != null && url.length() > 0) {
            urlOnLaunch = url;
        } else {
            urlOnLaunch = null;
        }

        AlertUtils.ShowAlertView(title, message, positiveText, negativeText, null, new AlertUtils.AlertCallbacks() {
            @Override
            public void alertPositiveCallback() {
                //jump
                if (urlOnLaunch != null) {
                    WebViewUtils.Native_OpenWebPage(urlOnLaunch);
                }
            }

            @Override
            public void alertNegativeCallback() {
                //do nothing
            }

            @Override
            public void alertNeutralCallback() {
                //do nothing
            }
        });
    }

    /*****************************************************/
    /*  Native  */
    /*****************************************************/

    public static boolean Native_InitPronounce() {
        //return instance.initPronounces();
        return instance.hasPronounceLoaded(); //初期化処理は MainActivity.onCreate()で行うため
    }

    public static boolean Native_HasPronounceLoaded() {
        return instance.hasPronounceLoaded();
    }

    public static int Native_GetIntPronounce(String key, int defaultValue) {
        return instance.getIntPronounce(key, defaultValue);
    }

    public static float Native_GetFloatPronounce(String key, float defaultValue) {
        return instance.getFloatPronounce(key, defaultValue);
    }

    public static double Native_GetDoublePronounce(String key, double defaultValue) {
        return instance.getDoublePronounce(key, defaultValue);
    }

    public static boolean Native_GetBoolPronounce(String key, boolean defaultValue) {
        return instance.getBooleanPronounce(key, defaultValue);
    }

    public static String Native_GetStringPronounce(String key, String defaultValue) {
        return instance.getStringPronounce(key, defaultValue);
    }

    public static void Native_ShowPronounceLaunchAlertView(boolean force) {
        instance.launchAlertView(force);
    }

    public static void ShowAlertViewWithUrlJump(String title, String message, String positiveText, String negativeText, String url) {
        instance.showAlertViewWithUrlJump(title, message, positiveText, negativeText, url);
    }

}
