package kazuma.saitou.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import kazuma.saitou.rolling.MainActivity;

/**
 * Created by otaniatsushi1 on 16/02/21.
 */
public class AlertUtils {

    /***********************************/
    /* types  */
    /***********************************/

    //1択のアラートビュー
    static final int ALERT_BUTTON_1_OK_INDEX = 0;

    //2択のアラートビュー
    static final int ALERT_BUTTON_2_NO_INDEX = 0;
    static final int ALERT_BUTTON_2_YES_INDEX = 1;

    //3択のアラートビュー
    static final int ALERT_BUTTON_3_CANCEL_INDEX = 0;
    static final int ALERT_BUTTON_3_OPTION1_INDEX = 1;
    static final int ALERT_BUTTON_3_OPTION2_INDEX = 2;

    //コールバック用クラス
    abstract public static class AlertCallbacks {
        abstract public void alertPositiveCallback();
        abstract public void alertNegativeCallback();
        abstract public void alertNeutralCallback();
    };


    /***********************************/
    /*  variables  */
    /***********************************/

    private static AlertUtils instance;
    private static Activity mContext;

    Handler mHandler = new Handler();
    private AlertCallbacks mCallbacks;
    private String mTitleText;
    private String mMessageText;
    private View   mMainView;
    private String mPositiveText;
    private String mNegativeText;
    private String mNeutralText;

    /***********************************/
    /* instance methods  */
    /***********************************/

    public AlertUtils(MainActivity context) {
        mContext = context;
        instance = this;
    }

    public void showAlertView(String title, String message, String positiveText, String negativeText, String neutralText, AlertCallbacks callbacks) {
        showAlertView(title, message, null, positiveText, negativeText, neutralText, callbacks);
    }

    public void showAlertView(String title, View mainView, String positiveText, String negativeText, String neutralText, AlertCallbacks callbacks) {
        showAlertView(title, null, mainView, positiveText, negativeText, neutralText, callbacks);
    }

    public void showAlertView(String title, String message, View mainView, String positiveText, String negativeText, String neutralText, AlertCallbacks callbacks) {

        mTitleText = title;
        mMessageText = message;
        mMainView    = mainView; //NOTE: mainViewはAlertDialogの子ビューとなる。ダイアログを再生成するたび、毎回新しいダイアログの子ビューとなる。そのため、呼び出し元で何らかのビューグループの子ビューになっていてはいけない
        mPositiveText = positiveText;
        mNegativeText = negativeText;
        mNeutralText  = neutralText;
        mCallbacks = callbacks;

        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setTitle(mTitleText);
                if (mMessageText != null) {
                    alertDialogBuilder.setMessage(mMessageText);
                }
                if (mMainView != null) {
                    if (mMainView.getParent() != null) {
                        ((ViewGroup) (mMainView.getParent())).removeView(mMainView);
                    }
                    alertDialogBuilder.setView(mMainView);
                }

                if (mPositiveText != null) {
                    alertDialogBuilder.setPositiveButton(mPositiveText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            instance.mHandler.post(new Runnable() {
                                public void run() {
                                    if (mCallbacks != null) {
                                        mCallbacks.alertPositiveCallback();
                                    }
                                }
                            });
                        }
                    });
                }

                if (mNegativeText != null) {
                    alertDialogBuilder.setNegativeButton(mNegativeText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            instance.mHandler.post(new Runnable() {
                                public void run() {
                                    if (mCallbacks != null) {
                                        mCallbacks.alertNegativeCallback();
                                    }
                                }
                            });
                        }
                    });
                }

                if (mNeutralText != null) {
                    alertDialogBuilder.setNeutralButton(mNeutralText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            instance.mHandler.post(new Runnable() {
                                public void run() {
                                    if (mCallbacks != null) {
                                        mCallbacks.alertNeutralCallback();
                                    }
                                }
                            });
                        }
                    });
                }

                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.create().show();
            }
        });
    }

    public void showKeyboard(String message, String text) {
        mContext.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void showKeyboardNumPad(String message, String text) {
        mContext.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }


    /***********************************/
    /* static methods  */
    /***********************************/

    public static void ShowAlertView(String title, String message, String positiveText, String negativeText, String neutralText, AlertCallbacks callbacks) {
        instance.showAlertView(title, message, positiveText, negativeText, neutralText, callbacks);
    }

    public static void ShowAlertView(String title, View mainView, String positiveText, String negativeText, String neutralText, AlertCallbacks callbacks) {
        instance.showAlertView(title, mainView, positiveText, negativeText, neutralText, callbacks);
    }

    public static void Native_AlertView(String title, String message, String okText) {

        instance.showAlertView(title, message, okText, null, null,
                new AlertCallbacks() {
                    @Override
                    public void alertPositiveCallback() {
                        alertCallbackJni(ALERT_BUTTON_1_OK_INDEX);
                    }

                    @Override
                    public void alertNegativeCallback() {
                    }

                    @Override
                    public void alertNeutralCallback() {
                    }
                });
    }

    public static void Native_AlertView(String title, String message, String noText, String yesText) {

        instance.showAlertView(title, message, yesText, noText, null,
                new AlertCallbacks() {
                    @Override
                    public void alertPositiveCallback() {
                        alertCallbackJni(ALERT_BUTTON_2_YES_INDEX);
                    }

                    @Override
                    public void alertNegativeCallback() {
                        alertCallbackJni(ALERT_BUTTON_2_NO_INDEX);
                    }

                    @Override
                    public void alertNeutralCallback() {
                    }
                });
    }

    public static void Native_AlertView(String title, String message, String cancelText, String opt1Text, String opt2Text) {

        instance.showAlertView(title, message, opt1Text, cancelText, opt2Text,
                new AlertCallbacks(){
                    @Override
                    public void alertPositiveCallback() {
                        alertCallbackJni(ALERT_BUTTON_3_OPTION1_INDEX);
                    }

                    @Override
                    public void alertNegativeCallback() {
                        alertCallbackJni(ALERT_BUTTON_3_CANCEL_INDEX);
                    }

                    @Override
                    public void alertNeutralCallback() {
                        alertCallbackJni(ALERT_BUTTON_3_OPTION2_INDEX);
                    }
                });
    }

    //TODO: C++側からもこのメソッドを無くす
    public static void Native_AlertView(String title, String message, String cancelText, String opt1Text, String opt2Text, String opt3Text) {
        Native_AlertView(title, message, cancelText, opt1Text, opt2Text);
    }

    //キーボードを表示
    public static void Native_ShowKeyboard(String message, String text) {
        showKeyboardCallbackJni(text);
    }
    public static void Native_ShowKeyboardNumPad(String message, String text) {
        showKeyboardNumPadCallbackJni(text);
    }


    /***********************************/
    /* JNI methods  */
    /***********************************/

    public static native void alertCallbackJni(int buttonIndex);
    public static native void showKeyboardCallbackJni(String text);
    public static native void showKeyboardNumPadCallbackJni(String text);
    public static native void showKeyboardCancelCallbackJni();
    public static native void showKeyboardNumPadCancelCallbackJni();
}
