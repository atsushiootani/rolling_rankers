package kazuma.saitou.rolling;

import jp.co.imobile.sdkads.android.ImobileSdkAd;
import jp.co.imobile.sdkads.android.ImobileSdkAdListener;
import kazuma.saitou.util.ActivityUtils;
import kazuma.saitou.util.AlertUtils;
import kazuma.saitou.util.AnalyticsUtils;
import kazuma.saitou.util.DatabaseUtils;
import kazuma.saitou.util.HttpUtils;
import kazuma.saitou.util.NotificationUtils;
import kazuma.saitou.util.PurchaseUtils;
import kazuma.saitou.util.SaveUtils;
import kazuma.saitou.util.StorageUtils;
import kazuma.saitou.util.StoreUtils;
import kazuma.saitou.util.TwitterOAuthActivity;
import kazuma.saitou.util.TwitterUtils;
import kazuma.saitou.util.UserUtils;
import kazuma.saitou.util.WebViewUtils;
import kazuma.saitou.rolling.BuildConfig;
import kazuma.saitou.rolling.R;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.adcolony.sdk.*;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.HashMap;
import jp.co.imobile.sdkads.android.ImobileSdkAd;
import jp.tjkapp.adfurikunsdk.moviereward.*;


public class MainActivity extends Cocos2dxActivity {

	// shared instance
	private static MainActivity instance = null;

	// 各種Util
	private ActivityUtils mActivityUtils = null;
	private AlertUtils mAlertUtils = null;
	private AnalyticsUtils mAnalyticsUtils = null;
	private DatabaseUtils mDatabaseUtils = null;
    private HttpUtils mHttpUtils = null;
    private NotificationUtils mNotificationUtils = null;
	private PurchaseUtils mPurchaseUtils = null;
	private SaveUtils mSaveUtils = null;
	private StorageUtils mStorageUtils = null;
	private StoreUtils mStoreUtils = null;
	private TwitterUtils mTwitterUtils = null;
	private UserUtils mUserUtils = null;
	private WebViewUtils mWebViewUtils = null;

	/********************************************/
	// life cycle
	/********************************************/
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityUtils.DebugPrint("MAIN_______ON_CREATE");

		// 最初に共有インスタンスをセット。以降の初期化処理で使用するので
		instance = this;

		// 各種utils
		mActivityUtils = new ActivityUtils(this);
		mSaveUtils = new SaveUtils(this); //他からも使われるので先に

		mAlertUtils = new AlertUtils(this);
		mAnalyticsUtils = new AnalyticsUtils(this);

		mUserUtils = new UserUtils(this);
		mDatabaseUtils = new DatabaseUtils(this);
		mStorageUtils = new StorageUtils(this);

        mHttpUtils = new HttpUtils(this);
		mHttpUtils.initPronounces();
        mNotificationUtils = new NotificationUtils(this);
		mStoreUtils = new StoreUtils(this);
		mTwitterUtils = new TwitterUtils(this, TwitterOAuthActivity.class);
		mWebViewUtils = new WebViewUtils(this);
		mPurchaseUtils = new PurchaseUtils(this);
		//mPurchaseUtils.setup();
		//初期化系の処理もここで行う
		//  アプリがバックグラウンドに行って戻ってきたとき、MainActivityだけが再生成されてcocos2d-x側がそのまま生きている場合がある
		//  その場合、PurchaseUtilsなど内部ステータスを持っているUtil系は、初期化命令がcocos2d-x側からやってこないため、課金が2度とできなくなったりする

		// 広告
		onCreateAd();

		// ユーザID作っておく
		GetUserId();
		AnalyticsUtils.Native_Analytics("BOOT", "boot", "user_id", GetUserId());
	}

	@Override
	public void onStart() {
		super.onStart();

		//push通知から起動した場合
		// Firebaseから送信したpush通知のデータは、intent.getExtras()の中にある
		Intent intent = getIntent();
		Bundle dataPayload = intent.getExtras();
		if (dataPayload != null) {
			String title = dataPayload.getString("_NOTICE_TITLE");
			if (title != null && title.length() > 0) {
				ActivityUtils.DebugPrint(title);
				HttpUtils.ShowAlertViewWithUrlJump(
						title,
						dataPayload.getString("_NOTICE_MESSAGE"),
						dataPayload.getString("_NOTICE_YES"),
						dataPayload.getString("_NOTICE_NO"),
						dataPayload.getString("_NOTICE_URL")
				);
			}
		}

		onStartAd();
	}

	@Override
	public void onStop() {
		super.onStop();
		onStopAd();
	}

	@Override
	protected void onResume() {
		super.onResume();

		onResumeAd();
		mPurchaseUtils.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();

		onPauseAd();
		mPurchaseUtils.onPause();
	}

	@Override
	public void onDestroy() {
		mPurchaseUtils.onDestroy();
		onDestroyAd();

		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//dispatchKeyEventに記述
		return false;
	}

	@Override
	public void onBackPressed() {
		//dispatchKeyEventに記述
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		//キー情報
		int keyCode = event.getKeyCode();
		int action  = event.getAction();

		//イベントを処理したかどうか
		boolean consumed = false;

		//バックキーを押した時
		if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) && action == KeyEvent.ACTION_UP) {
			//全画面広告を表示
			AlertUtils.ShowAlertView("", "ゲームを終了しますか？", "OK", "キャンセル", null, new AlertUtils.AlertCallbacks() {
				@Override
				public void alertPositiveCallback(){
					adOnTerminateShow();
				}
				@Override
				public void alertNegativeCallback(){
					//do nothing
				}
				@Override
				public void alertNeutralCallback(){
					//do nothing
				}
			});

			consumed = true;
		}

		if (consumed) {
			return true;
		}
		else {
			return super.dispatchKeyEvent(event);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// まず PurchaseUtil の onActivityResult に投げる
		boolean handled = mPurchaseUtils.onPurchaseActivityResult(requestCode, resultCode, data); //必須。ここがないと課金処理ができない

		// 処理されなかったら、通常の onActivityResult を実行する
		if (!handled) {
			onActivityResult(requestCode, resultCode, data);
		}
	}

    /**********************************************/
    // UserId
    /**********************************************/
    private final static String USER_ID_PREFERENCE_KEY = "USER_ID_KEY";

    // ユーザIDを返す。なければ生成。1970/1/1からのミリ秒。かぶることはほぼないだろう（かぶっても問題無し。Analyticsの課金周りログに使うだけ）
    public static long GetUserId() {
        long userId = instance.mSaveUtils.LoadLong(USER_ID_PREFERENCE_KEY, System.currentTimeMillis());
        instance.mSaveUtils.SaveLong(USER_ID_PREFERENCE_KEY, userId);
        return userId;
    }


    /**********************************************/
	// util - screen
	/**********************************************/

	private final static int GAME_SCREEN_WIDTH = 640;
	private final static int GAME_SCREEN_HEIGHT = 1136;
	private final static int FOOTER_HEIGHT = 110;
	private final static int AD_BANNER_HEIGHT = 100;

	public static int getDeviceScreenWidth() {
		WindowManager wm = instance.getWindowManager();
		Display disp = ((WindowManager) instance.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		disp.getSize(size);
		return size.x;
	}

	public static int getDeviceScreenHeight() {
		WindowManager wm = instance.getWindowManager();
		Display disp = ((WindowManager) instance.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		disp.getSize(size);
		return size.y;
	}

	public static int getGameScreenWidth() {
		return 640;
	}

	public static int getGameScreenHeight() {
		return 1136;
	}

	public static float getGameToDeviceCoefX() {
		return 1.f * getDeviceScreenWidth() / getGameScreenWidth();
	}

	public static float getGameToDeviceCoefY() {
		return 1.f * getDeviceScreenHeight() / getGameScreenHeight();
	}

	public static float getDeviceToGameCoefX() {
		return 1.f * getGameScreenWidth() / getDeviceScreenWidth();
	}

	public static float getDeviceToGameCoefY() {
		return 1.f * getGameScreenHeight() / getDeviceScreenHeight();
	}

	public static Point getDevicePosFromGamePos(Point gamePos) {
		float coefX = getGameToDeviceCoefX();
		float coefY = getGameToDeviceCoefY();
		return new Point((int) (gamePos.x * coefX), (int) (gamePos.y * coefY));
	}

	public static Point getGamePosFromDevicePos(Point gamePos) {
		float coefX = getDeviceToGameCoefX();
		float coefY = getDeviceToGameCoefY();
		return new Point((int) (gamePos.x * coefX), (int) (gamePos.y * coefY));
	}

	public static Rect getDeviceRectFromGameRect(Rect gameRect) {
		float coefX = getGameToDeviceCoefX();
		float coefY = getGameToDeviceCoefY();
		return new Rect((int) (gameRect.left * coefX), (int) (gameRect.top * coefY), (int) (gameRect.right * coefX),
				(int) (gameRect.bottom * coefY));
	}

	public static Rect getGameRectFromDeviceRect(Rect gameRect) {
		float coefX = getDeviceToGameCoefX();
		float coefY = getDeviceToGameCoefY();
		return new Rect((int) (gameRect.left * coefX), (int) (gameRect.top * coefY), (int) (gameRect.right * coefX),
				(int) (gameRect.bottom * coefY));
	}

	// 指定のRectを覆うRelativeLayoutを作成し、parentにaddViewする
	private RelativeLayout makePositionerView(RelativeLayout parent, Rect rect) {
		int x = rect.left;
		int y = rect.top;
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;

		RelativeLayout positioner = new RelativeLayout(getApplicationContext());
		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width, height); // MATCH_PARENTではなく、バシッとpx値で指定する
		lp1.leftMargin = x;
		lp1.topMargin = y;

		parent.addView(positioner, lp1);

		return positioner;
	}

	private void addChildViewToRoot(View view, Rect rect) {
		// rectのサイズだけある中間viewを作成。その中心に本来のviewを作る
		int x = rect.left;
		int y = rect.top;
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;

		// System.out.println("rect left=" + rect.left + ", top=" + rect.top +
		// ", right=" + rect.right + ", bottom=" + rect.bottom);

		// 親ビュー（ルート）
		RelativeLayout parent = getAdRootView();

		// 中間ビューを作成。正確にrectの範囲を覆うRelativeLayout。
		RelativeLayout positioner = new RelativeLayout(getApplicationContext());

		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width, height); // MATCH_PARENTではなく、バシッとpx値で指定する
		lp1.leftMargin = x;
		lp1.topMargin = y;

		parent.addView(positioner, lp1);

		// 目的のビューをセット。今作った中間ビューの中心に配置
		RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		lp2.addRule(RelativeLayout.CENTER_VERTICAL);
		lp2.addRule(RelativeLayout.CENTER_HORIZONTAL);

		positioner.addView(view, lp2);
	}


	/**********************************************/
	// Ad - instance methods
	/**********************************************/

	//タイプ
	//必ずiOSと揃えること
	final static int AD_UNKNOWN = -1;
	final static int AD_NONE = 0;
	final static int AD_ADMOB = 1;
	final static int AD_NEND = 2;
	final static int AD_IMOBILE = 3;
	final static int AD_ADFURIKUN = 4;
	final static int AD_ADCOLONY = 5;
	final static int AD_ADSTIR = 6;
	final static int AD_APPLOVIN = 7;

	//必ずiOSと揃えること
	final static String PRONOUNCE_AD_MOVIE_ADMOB     = "_adm_aam";
	final static String PRONOUNCE_AD_MOVIE_ADFURIKUN = "_adm_aaf";
	final static String PRONOUNCE_AD_MOVIE_ADCOLONY  = "_adm_aac";
	final static String PRONOUNCE_AD_MOVIE_ADSTIR    = "_adm_aas";
	final static String PRONOUNCE_AD_MOVIE_APPLOVIN  = "_adm_aal";

	//バナー広告を無効にする
	private boolean adBannerDisabled = false;

	//インタースティシャル表示回数
	private int adInterstitialShowNum = 0;

	//動画広告タイプ
	private int adMovieType = AD_ADFURIKUN; //デフォルト設定

	private void onCreateAd() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.ad, null);
		addContentView(v, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		// setContentView(R.layout.ad);
		initImobile();
		initAdmob();

		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put(PRONOUNCE_AD_MOVIE_ADFURIKUN, AD_ADFURIKUN);
		map.put(PRONOUNCE_AD_MOVIE_ADCOLONY, AD_ADCOLONY);
		adMovieType = mHttpUtils.lotPronounce(map, AD_ADFURIKUN);
		if (adMovieType == AD_ADCOLONY) {
			initAdColony();
		}else{
			initAdfurikun();
		}
	}

	private void onStartAd() {
		onStartAdmob();
		onStartAdfurikun();
	}

	private void onStopAd() {
		onStopAdfurikun();
	}

	private void onPauseAd() {
		onPauseAdfurikun();
	}

	private void onResumeAd() {
		onResumeAdfurikun();
	}

	private void onDestroyAd() {
		onDestroyImobile();
		onDestroyAdfurikun();
	}

	private RelativeLayout getAdRootView() {
		return (RelativeLayout) findViewById(R.id.ad_root);
	}

	private Rect getBannerRect() {
		Rect bannerRect = new Rect(0, GAME_SCREEN_HEIGHT - FOOTER_HEIGHT - AD_BANNER_HEIGHT, GAME_SCREEN_WIDTH,
				GAME_SCREEN_HEIGHT - FOOTER_HEIGHT);
		return getDeviceRectFromGameRect(bannerRect);
	}

	private Rect getIconRect(int pos) {
		int maxCount = 5;
		Rect iconRect = new Rect(
				(GAME_SCREEN_WIDTH / maxCount) * pos,
				GAME_SCREEN_HEIGHT - FOOTER_HEIGHT,
				(GAME_SCREEN_WIDTH / maxCount) * (pos + 1),
				GAME_SCREEN_HEIGHT);
		return getDeviceRectFromGameRect(iconRect);
	}

	public void adRegister(){

	}

	public void adEnterBackground() {
		//do nothing
	}

	public void adEnterForeground() {
		//do nothing
	}

	public void adBannerShow(){
		if (!adBannerDisabled) {
			admobBannerShow();
		}
	}
	public void adBannerHide(){
		admobBannerHide();
	}

	public void adBannerSetEnable(){
		adBannerDisabled = false;
		adBannerShow();
	}
	public void adBannerSetDisable(){
		adBannerDisabled = true;
		adBannerHide();
	}

	public void adIconShowAt(int pos) {
	}

	public void adIconShow() {

	}

	public void adIconHide() {

	}

	public void adIconHideAt(int pos) {

	}

	public void adInterstitialShow(){

		int interval = mHttpUtils.getIntPronounce("_adi_int", 10);
		if (++adInterstitialShowNum >= interval) {
			adInterstitialShowNum -= interval;

			int imobileRate = mHttpUtils.getIntPronounce("_adi_i", 50);
			int r = mActivityUtils.GetIntRand(0, 99);
			if (r < imobileRate) {
				imobileInterstitialShow();
			}
			else {
				admobInterstitialTransitionShow();
			}
		}
	}
	public void adInterstitialAlwaysShow(){
		admobInterstitialAlwaysShow();
	}
	public void adWallShow(){
		admobInterstitialAlwaysShow();
	}
	public void adOnTerminateShow(){
		admobInterstitialTerminateShow();
	}
	public void adInsentiveShow(){
		if (adMovieType == AD_ADCOLONY) {
			adColonyMoviePlay();
		}else {
			adfurikunMoviePlay();
		}
	}
	public boolean adCanGetInsentive(){
		if (adMovieType == AD_ADCOLONY) {
			return adColonyCanMoviePlay();
		}else {
			return adfurikunCanMoviePlay();
		}
	}
	public boolean adMovieIsPlaying(){
		if (adMovieType == AD_ADCOLONY) {
			return adColonyMovieIsPlaying();
		}else {
			return adfurikunMovieIsPlaying();
		}
	}

	//指定のpackageName(xxx.xxx.xxx)のアプリがインストールされているかどうかを返す
	public boolean checkApplicationInstalled(String packageName){
		try {
			PackageManager pm = getPackageManager();
			pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
			return true;
		} catch(PackageManager.NameNotFoundException e) {
			// インストールされていない場合は例外が発生
			return false;
		}
	}

	//指定のpackageName(xxx.xxx.xxx)のアプリを起動する. 存在チェックはしない
	public void openInstalledApplication(String packageName){
		try {
			//Intent intent = new Intent();
			//intent.setClassName(packageName, packageName + ".MainActivity");

			Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK); //これを指定することで、別タスクとしてIntentが起動する。（指定がないと、ブスライブのアプリ内でオタサーのMainActivityが起動してしまう）
			startActivity(intent);
		} catch (Exception e){
			//do nothing
			ActivityUtils.DebugErrorPrint(e.toString());
		}
	}

	/**********************************************/
	// Ad - static methods
	/**********************************************/

	public static void Native_AdRegister(){
		instance.adRegister();
	}
	public static void Native_AdEnterBackground() { instance.adEnterBackground(); }
	public static void Native_AdEnterForeground() { instance.adEnterForeground(); }
	public static void Native_AdBannerShow(){
		instance.adBannerShow();
	}
	public static void Native_AdBannerHide(){
		instance.adBannerHide();
	}
    public static void Native_AdBannerSetEnable(){
		instance.adBannerSetEnable();
	}
    public static void Native_AdBannerSetDisable(){
		instance.adBannerSetDisable();
	}
	public static void Native_AdIconShowAt(int pos) { instance.adIconShowAt(pos); }
	public static void Native_AdIconShow() { instance.adIconShow(); }
	public static void Native_AdIconHide() { instance.adIconHide(); }
	public static void Native_AdIconHideAt(int pos) { instance.adIconHideAt(pos); }
	public static void Native_AdInterstitialShow(){
		instance.adInterstitialShow();
	}
	public static void Native_AdInterstitialAlwaysShow(){
		instance.adInterstitialAlwaysShow();
	}
	public static void Native_AdWallShow(){
		instance.adWallShow();
	}
	public static void Native_AdInsentiveShow(){
		instance.adInsentiveShow();
	}
	public static void Native_AdHeaderShow(){
		//TODO:
	}
	public static void Native_AdHeaderHide(){
		//TODO:
	}
	public static boolean Native_AdCanGetInsentive(){
		return instance.adCanGetInsentive();
	}
	public static boolean Native_AdMovieIsPlaying(){
		return instance.adMovieIsPlaying();
	}
    public static boolean Native_CheckApplicationInstalled(String packageName){
		return instance.checkApplicationInstalled(packageName);
	}
    public static void Native_OpenInstalledApplication(String packageName){
		instance.openInstalledApplication(packageName);
	}

	public static native void incentiveCallbackJni(int amount);
	public static native void incentiveFailureCallbackJni();
	public static native void incentiveClosedCallbackJni(boolean succeed);


	/**********************************************/
	// Ad imobile
	/**********************************************/


	private static final String IMOBILE_FULLSCREENAD_PID = "38933";
	private static final String IMOBILE_FULLSCREENAD_MID = "312774";
	private static final String IMOBILE_FULLSCREENAD_SID = "1042155";

	private void initImobile(){
		// スポット情報を設定します
		ImobileSdkAd.registerSpotFullScreen(this, IMOBILE_FULLSCREENAD_PID, IMOBILE_FULLSCREENAD_MID, IMOBILE_FULLSCREENAD_SID);
		// 広告の取得を開始します
		ImobileSdkAd.start(IMOBILE_FULLSCREENAD_SID);
	}

	private void imobileInterstitialShow(){
		// 広告を表示します
		ImobileSdkAd.showAd(this, IMOBILE_FULLSCREENAD_SID);

	}

	private void onDestroyImobile(){
		ImobileSdkAd.activityDestroy();
	}


	/**********************************************/
	// Ad admob
	/**********************************************/

	//広告ID
	private final static String ADMOB_BANNER_ID                  = "ca-app-pub-7661660979526123/3420347491";
	private final static String ADMOB_INTERSTITIAL_TRANSITION_ID = "ca-app-pub-7661660979526123/4897080698";
	private final static String ADMOB_INTERSTITIAL_ALWAYS_ID     = "ca-app-pub-7661660979526123/6373813897";
	private final static String ADMOB_INTERSTITIAL_TERMINATE_ID  = "ca-app-pub-7661660979526123/6373813897";

	//広告ビュー
	private AdView admobBannerView = null;
	private InterstitialAd admobInterstitialTransition = null;
	private InterstitialAd admobInterstitialAlways = null;
	private InterstitialAd admobInterstitialTerminate = null;

	//admob初期化
	private void initAdmob() {

		// バナー
		admobBannerView = new AdView(MainActivity.this);
		admobBannerView.setAdSize(AdSize.SMART_BANNER);
		admobBannerView.setAdUnitId(ADMOB_BANNER_ID);
		addChildViewToRoot(admobBannerView, getBannerRect());
		AdRequest adRequest;
		if (ActivityUtils.IsDebug()) {
			adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
		} else {
			adRequest = new AdRequest.Builder().build();
		}
		admobBannerView.loadAd(adRequest);
		admobBannerView.setVisibility(View.INVISIBLE);

		// インタースティシャル
		initAdmobTransitionInterstitial(ADMOB_INTERSTITIAL_TRANSITION_ID);
		initAdmobAlwaysInterstitial(ADMOB_INTERSTITIAL_ALWAYS_ID);
		initAdmobTerminateInterstitial(ADMOB_INTERSTITIAL_TERMINATE_ID);
	}

	//admobの開始時
	private void onStartAdmob(){
		if (admobBannerView == null) {
			admobBannerView = new AdView(MainActivity.this);
			admobBannerView.setAdSize(AdSize.SMART_BANNER);
			admobBannerView.setAdUnitId(ADMOB_BANNER_ID);
			addChildViewToRoot(admobBannerView, getBannerRect());
			ActivityUtils.DebugPrint("on start admob instantiate");
		}
		else{
			ActivityUtils.DebugPrint("on start admob stay");
		}
		admobBannerView.loadAd(new AdRequest.Builder().build());
	}

	//admobのインタースティシャル初期化
	private void initAdmobTransitionInterstitial(final String admobId) {
		ActivityUtils.DebugPrint("admob init " + admobId);
		InterstitialAd interstitial = new InterstitialAd(MainActivity.this);
		interstitial.setAdUnitId(admobId);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				ActivityUtils.DebugPrint("admob ad loaded: " + admobId);
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				ActivityUtils.DebugPrint("admob ad failed to load: " + admobId + ", error=" +
						(errorCode == AdRequest.ERROR_CODE_NO_FILL ? "NO_FILL"
								: errorCode == AdRequest.ERROR_CODE_INVALID_REQUEST ? "INVALID_REQUEST"
								: errorCode == AdRequest.ERROR_CODE_NETWORK_ERROR ? "NETWORK_ERROR"
								: errorCode == AdRequest.ERROR_CODE_INTERNAL_ERROR ? "INTERNAL_ERROR"
								: errorCode));
				initAdmobTransitionInterstitial(admobId);
			}

			@Override
			public void onAdClosed() {
				ActivityUtils.DebugPrint("admob ad closed");
				initAdmobTransitionInterstitial(admobId);
			}

			@Override
			public void onAdLeftApplication() {
				ActivityUtils.DebugPrint("admob ad left apply");
			}

			@Override
			public void onAdOpened() {
				ActivityUtils.DebugPrint("admob ad opened");
			}
		});
		interstitial.loadAd(new AdRequest.Builder().build());

		admobInterstitialTransition = interstitial;
	}
	private void initAdmobAlwaysInterstitial(final String admobId) {
		ActivityUtils.DebugPrint("admob init " + admobId);
		InterstitialAd interstitial = new InterstitialAd(MainActivity.this);
		interstitial.setAdUnitId(admobId);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				ActivityUtils.DebugPrint("admob ad loaded: " + admobId);
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				ActivityUtils.DebugPrint("admob ad failed to load: " + admobId + ", error=" +
						(errorCode == AdRequest.ERROR_CODE_NO_FILL ? "NO_FILL"
								: errorCode == AdRequest.ERROR_CODE_INVALID_REQUEST ? "INVALID_REQUEST"
								: errorCode == AdRequest.ERROR_CODE_NETWORK_ERROR ? "NETWORK_ERROR"
								: errorCode == AdRequest.ERROR_CODE_INTERNAL_ERROR ? "INTERNAL_ERROR"
								: errorCode));
				initAdmobAlwaysInterstitial(admobId);
			}

			@Override
			public void onAdClosed() {
				ActivityUtils.DebugPrint("admob ad closed");
				initAdmobAlwaysInterstitial(admobId);
			}

			@Override
			public void onAdLeftApplication() {
				ActivityUtils.DebugPrint("admob ad left apply");
			}

			@Override
			public void onAdOpened() {
				ActivityUtils.DebugPrint("admob ad opened");
			}
		});
		interstitial.loadAd(new AdRequest.Builder().build());

		admobInterstitialAlways = interstitial;
	}
	private void initAdmobTerminateInterstitial(final String admobId) {
		InterstitialAd interstitial = new InterstitialAd(MainActivity.this);
		interstitial.setAdUnitId(admobId);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				ActivityUtils.DebugPrint("admob ad loaded: " + admobId);
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				ActivityUtils.DebugPrint("admob ad failed to load: " + admobId + ", error=" +
						(errorCode == AdRequest.ERROR_CODE_NO_FILL ? "NO_FILL"
						: errorCode == AdRequest.ERROR_CODE_INVALID_REQUEST ? "INVALID_REQUEST"
						: errorCode == AdRequest.ERROR_CODE_NETWORK_ERROR ? "NETWORK_ERROR"
						: errorCode == AdRequest.ERROR_CODE_INTERNAL_ERROR ? "INTERNAL_ERROR"
						: errorCode));
				initAdmobTerminateInterstitial(admobId);
			}

			@Override
			public void onAdClosed() {
				ActivityUtils.DebugPrint("admob ad closed");
				initAdmobTerminateInterstitial(admobId); //結局バックグラウンドに移動するだけなので、生成はしておく。でないと2度目に起動した時、終了時広告が表示されなくなる

				//アプリ終了
				finishApplicationAfterAd();
			}

			@Override
			public void onAdLeftApplication() {
				ActivityUtils.DebugPrint("admob ad left apply");
			}

			@Override
			public void onAdOpened() {
				ActivityUtils.DebugPrint("admob ad opened");
			}
		});
		interstitial.loadAd(new AdRequest.Builder().build());

		admobInterstitialTerminate = interstitial;
	}

	//バナー表示
	private void admobBannerShow() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				admobBannerView.setVisibility(View.VISIBLE);
			}
		});
	}

	//バナー非表示
	private void admobBannerHide() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				admobBannerView.setVisibility(View.INVISIBLE);
			}
		});
	}

	//画面遷移時用の全画面広告を表示
	private void admobInterstitialTransitionShow() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (admobInterstitialAlways != null && admobInterstitialAlways.isLoaded()) {
					ActivityUtils.DebugPrint("admob show");
					admobInterstitialTransition.show();
				}
				else if (!admobInterstitialTransition.isLoading()){
					initAdmobTransitionInterstitial(ADMOB_INTERSTITIAL_TRANSITION_ID);
				}
			}
		});
	}

	//常時表示する用の全画面広告を表示
	private void admobInterstitialAlwaysShow() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (admobInterstitialAlways != null && admobInterstitialAlways.isLoaded()) {
					admobInterstitialAlways.show();
				} else if (!admobInterstitialAlways.isLoading()) {
					initAdmobAlwaysInterstitial(ADMOB_INTERSTITIAL_ALWAYS_ID);
				}
			}
		});
	}

	//終了じ表示する用の全画面広告を表示
	private void admobInterstitialTerminateShow() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (admobInterstitialTerminate != null && admobInterstitialTerminate.isLoaded()) {
					admobInterstitialTerminate.show();
				}
				else{
					//インスタンスができていなければ、アプリを終了する
					finishApplicationAfterAd();
				}
			}
		});
	}

	private void finishApplicationAfterAd(){
		//ホームボタンを押したときと同じ処理を走らせる
		// 何も起きず。KeyEventの形が違うのか、そもそもdispatchKeyEventで処理しているわけではないのか
		//KeyEvent event = new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HOME);
		//super.dispatchKeyEvent(event);

		//タスクを終了
		instance.finish();

		//一つ前に実行していたアプリが起動しちゃう。不自然
		//instance.moveTaskToBack(true); //アクティビティ(instance)がタスクルートでなくても、moveTaskToBackが処理される
	}

	/**********************************************/
	// Ad adfurikun
	/**********************************************/

	//広告ID
	private final static String ADFURIKUN_MOVIE_ID = "577b6a410d34957030000c0e"; //注: 「アプリ/サイトID」ではなく「広告枠ID」を記入すること

	//広告ビュー
	private AdfurikunMovieReward adfurikun = null;

	//変数
	private boolean isAdfurikunMoviePlaying = false;
	private boolean hasAdfurikunSucceeded = false;

	//初期化
	private void initAdfurikun(){
		adfurikun = new AdfurikunMovieReward(ADFURIKUN_MOVIE_ID, this);

		adfurikun.setAdfurikunMovieRewardListener(new AdfurikunMovieRewardListener() {
			@Override
			public void onPrepareSuccess() {
				ActivityUtils.DebugPrint("adfurikun prepare succeeded");
			}

			@Override
			public void onStartPlaying(MovieRewardData movieRewardData) {
			}

			@Override
			public void onFinishedPlaying(MovieRewardData movieRewardData) {
				ActivityUtils.DebugPrint("adfurikun incentiveCallback");
				hasAdfurikunSucceeded = true;
				incentiveCallbackJni(1);
			}

			@Override
			public void onFailedPlaying(MovieRewardData movieRewardData) {
				hasAdfurikunSucceeded = false;
				incentiveFailureCallbackJni();
				ActivityUtils.DebugErrorPrint("adfurikun failedPlaying: key=" + movieRewardData.adnetworkKey + ", name=" + movieRewardData.adnetworkName);
			}

			@Override
			public void onAdClose(MovieRewardData movieRewardData) {
				incentiveClosedCallbackJni(hasAdfurikunSucceeded);
				hasAdfurikunSucceeded = false;
				isAdfurikunMoviePlaying = false;
				ActivityUtils.DebugPrint("adfurikun incentiveClosedCallback");

				onResume(); //これがないと、画面が明転しない。cocos2d-x側の処理が走らない
			}
		});
	}

	//再生
	private void adfurikunMoviePlay(){
		if (adfurikunCanMoviePlay()) {
			adfurikun.play();
			isAdfurikunMoviePlaying = true;
		}
	}

	//再生可能か
	private boolean adfurikunCanMoviePlay(){
		return adfurikun != null && adfurikun.isPrepared();
	}

	//再生中か
	private boolean adfurikunMovieIsPlaying(){
		return isAdfurikunMoviePlaying;
	}

	//ライフサイクル
	private void onStartAdfurikun(){
		if (adfurikun != null) {
			adfurikun.onStart();
		}
	}
	private void onStopAdfurikun(){
		if (adfurikun != null) {
			adfurikun.onStop();
		}
	}
	private void onPauseAdfurikun(){
		if (adfurikun != null){
			adfurikun.onPause();
		}
	}
	private void onResumeAdfurikun(){
		if (adfurikun != null) {
			adfurikun.onResume();
		}
	}
	private void onDestroyAdfurikun(){
		if (adfurikun != null) {
			adfurikun.onDestroy();
		}
	}


	/**********************************************/
	// Ad AdColony
	/**********************************************/

	private final String ADCOLONY_APP_ID  = "app8f8191940ed84a9589";
	private final String ADCOLONY_ZONE_ID = "vzde088343535b4679ae";

	private AdColonyInterstitial adColonyAd;
	private AdColonyInterstitialListener adColonyInterstitialListener;
	private boolean isAdColonyPlaying;

	private void initAdColony(){
		AdColony.configure( this, ADCOLONY_APP_ID, ADCOLONY_ZONE_ID );

		//リワードの汎用リスナー登録
		AdColonyRewardListener rewardListener = new AdColonyRewardListener() {
			@Override
			public void onReward( AdColonyReward reward ) {
				/** Query the reward object for information here */
				isAdColonyPlaying = false;
				if (reward.success()) {
					incentiveCallbackJni(reward.getRewardAmount());
					incentiveClosedCallbackJni(true);
					ActivityUtils.DebugPrint("AdColony::onReward succeeded");
				}
				else{
					incentiveFailureCallbackJni();
					incentiveClosedCallbackJni(false);
					ActivityUtils.DebugPrint("AdColony::onReward failed");
				}

				//再度リクエスト
				AdColony.requestInterstitial(ADCOLONY_ZONE_ID, adColonyInterstitialListener);
			}
		};
		AdColony.setRewardListener(rewardListener);

		//広告リスナー登録(reward動画もinterstitialのうちという扱いになっている)
		adColonyInterstitialListener = new AdColonyInterstitialListener() {
			@Override
			public void onRequestFilled(AdColonyInterstitial ad) {
				//広告が取得できた。インスタンスを保持
				ActivityUtils.DebugPrint("AdColony:filledAd = " + ad.isExpired());
				adColonyAd = ad;
				ActivityUtils.DebugPrint("AdColony::onRequestFilled");
			}

			@Override
			public void onRequestNotFilled(AdColonyZone zone){
				//error
				ActivityUtils.DebugPrint("AdColony::onRequestNotFilled");
			}

			@Override
			public void onOpened(AdColonyInterstitial ad) {
				//広告の再生が始まった時
				ActivityUtils.DebugPrint("AdColony::onOpened");
			}

			@Override
			public void onExpiring(AdColonyInterstitial ad) {
				//広告が切れた（見た後など）。再リクエスト
				AdColony.requestInterstitial(ADCOLONY_ZONE_ID, adColonyInterstitialListener);
				ActivityUtils.DebugPrint("AdColony::onExpiring");
			}
		};

		//広告リクエスト
		AdColony.requestInterstitial(ADCOLONY_ZONE_ID, adColonyInterstitialListener);
	}

	private void adColonyMoviePlay() {
		if (adColonyCanMoviePlay()) {
			isAdColonyPlaying = true;
			adColonyAd.show();
		}
	}

	public boolean adColonyCanMoviePlay() {
		ActivityUtils.DebugPrint("AdColony::canMoviePlay " + adColonyAd + ", " + (adColonyAd != null ? adColonyAd.isExpired() : ""));
		return (adColonyAd != null && !adColonyAd.isExpired());

	}

	public boolean adColonyMovieIsPlaying(){
		return isAdColonyPlaying;
	}



	/**********************************************/
	// indicator
	/**********************************************/
	private ProgressDialog pd;
	private String m_pdText;

	public static void Native_ShowIndicator(String message) {
		instance.m_pdText = message;
		instance.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instance.pd = new ProgressDialog(instance);
				instance.pd.setMessage(instance.m_pdText);
				instance.pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				instance.pd.setCancelable(false);
				instance.pd.show();
			}
		});

	}

	public static void Native_HideIndicator() {
		instance.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (instance.pd != null) {
					instance.pd.dismiss();
				}
			}
		});
	}

	/**********************************************/
	// sleep mode
	/**********************************************/

	//スリープ可能に
	public void setSleepableMode(){
		ActivityUtils.DebugPrint("set sleepable mode");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			}
		});
	}

	//スリープ不可に
	public void setKeepScreenMode(){
		ActivityUtils.DebugPrint("set keep screen mode");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			}
		});
	}

	//スリープ可能に
	public static void Native_SleepableMode(){
		instance.setSleepableMode();
	}

	//スリープ不可に
	public static void Native_KeepScreenMode(){
		instance.setKeepScreenMode();
	}


	/**********************************************/
	// analytics
	/**********************************************/
	/*
	public static void Native_Analytics(String category, String action, String label, int value) {
		Native_Analytics(category, action, label, (long)value);
	}

	public static void Native_Analytics(String category, String action, String label, long value) {
	}*/

	/**********************************************/
	// Native_Debug
	/**********************************************/
	public static boolean Native_IsDebug() {
        return BuildConfig.DEBUG;
	}

	public static void Native_Print(String text) {
		if (Native_IsDebug()) {
			System.out.println(text);
		}
	}

}
